all_types_of_payments = (
		("cuota-mensual", "Cuota Mensual"),
		("ahorro-programado", "Ahorro Programado"),
		("cesantias", "Cesantías"),
		("subsidio-vivienda", "Subsidio de vivienda"),
		("separacion", "Separacion"),
		("otro-pago-acordado", "Otro Pago Acordado"),
		("gastos-notariales", "Gastos Notariales"),
		("otros-conceptos", "Otros Conceptos"),
		("credito-hipotecario", "Crédito Hipotecario"),
		("kit-acabados", "Kit de Acabados"),
		("cambios-modificaciones", "Cambios/Modificaciones"),
		("descuento-financiero", "Descuento Financiero"),
	)

medium_choices = (
		("bancolombia", "Bancolombia"),
		("bbva", "BBVA"),
		("banco-bogota", "Banco de Bogotá"),
		("davivienda", "Davivienda"),
		("cheque-gerencia", "Cheque de gerencia"),
		("efectivo", "Efectivo"),
		("fiduciaria-bogota", "Fiduciaria Bogotá"),
		("caja_social", "Banco Caja Social"),
		("otro", "Otro"),
	)

surplus_to_choices = (
	("cuota-mensual", "Cuota Mensual"),
	("surplus-model", "Superavit"),
)
