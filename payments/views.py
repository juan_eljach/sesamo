from rest_framework import generics
from django.shortcuts import render
from .serializers import PaymentSerializer
from .models import Payment

class PaymentCreateAPIView(generics.CreateAPIView):
	serializer_class = PaymentSerializer
	queryset = Payment.objects.all()
