from rest_framework import serializers
from .models import Payment, Surplus, SurplusPayment

class PaymentSerializer(serializers.ModelSerializer):
	class Meta:
		model = Payment
		fields = (
			'payment_agreement',
			'amount',
			'date',
			'medium',
			'type_of_payment',
			'comment'
		)


class SurplusPaymentSerializer(serializers.ModelSerializer):
	class Meta:
		model = SurplusPayment
		fields = '__all__'

class SurplusSerializer(serializers.ModelSerializer):
	surplus_payments = SurplusPaymentSerializer(many=True, read_only=True)
	class Meta:
		model = Surplus
		fields = '__all__'
