from django.contrib import admin
from .models import Payment, Surplus, SurplusPayment

class PaymentAdmin(admin.ModelAdmin):
	list_display = ['__str__', 'amount', 'payment_agreement', 'date', 'medium']

class SurplusPaymentAdmin(admin.ModelAdmin):
	list_display = ['__str__', 'surplus_model', 'amount', 'origin']

admin.site.register(Payment, PaymentAdmin)
admin.site.register(Surplus)
admin.site.register(SurplusPayment, SurplusPaymentAdmin)
