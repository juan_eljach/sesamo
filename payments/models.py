from django.db import models
from django.core.exceptions import MultipleObjectsReturned
from sesamo.utils import custom_capture_exception
from formalizations.models import PaymentAgreement, PaymentFee
from formalizations.choices import types_of_agreed_payments
from .choices import all_types_of_payments, medium_choices, surplus_to_choices


class Surplus(models.Model):
	payment_agreement = models.OneToOneField(PaymentAgreement)

	def __str__(self):
		return "Surplus Log: {}".format(self.payment_agreement.unity)

class SurplusPayment(models.Model):
	surplus_model = models.ForeignKey(Surplus, related_name="surplus_payments")
	origin = models.CharField(max_length=50, choices=all_types_of_payments)
	amount = models.IntegerField()
	timestamp = models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return "Surplus Payment"

class Payment(models.Model):
	payment_agreement = models.ForeignKey(PaymentAgreement, related_name="payments")
	amount = models.IntegerField()
	date = models.DateField()
	medium = models.CharField(max_length=20, choices=medium_choices)
	type_of_payment = models.CharField(max_length=20, choices=all_types_of_payments)
	apply_surplus_to = models.CharField(max_length=20, choices=surplus_to_choices, blank=True, null=True)
	comment = models.TextField(max_length=1000, blank=True, null=True)

	def update_next_fees(self, surplus, actual_fee, *args, **kwargs):
		surplus = surplus
		actual_fee = actual_fee
		payment_agreement = actual_fee.payment_agreement
		unity = payment_agreement.unity
		qs = PaymentFee.objects.filter(payment_agreement=payment_agreement, date_to_pay__gt=actual_fee.date_to_pay, paid=False).order_by("date_to_pay")
		for payment_fee in qs:
			if payment_fee.partially_paid == True:
				debt = payment_fee.debt
			elif payment_fee.partially_paid == False:
				debt = payment_fee.fee + payment_fee.extraordinary_fee

			debt = debt - surplus

			if debt > 0:
				payment_fee.partially_paid = True
				payment_fee.debt = debt
				payment_fee.save()
				return True
			elif debt < 0:
				surplus = abs(debt)
				payment_fee.paid = True
				payment_fee.debt = 0
				payment_fee.partially_paid = False
				payment_fee.save()
			else:
				payment_fee.paid = True
				payment_fee.save()
				return True

			if surplus == 0:
				return True
		if surplus > 0:
			surplus_model, created = Surplus.objects.get_or_create(payment_agreement=payment_agreement)
			SurplusPayment.objects.create(surplus_model=surplus_model, origin='cuota-mensual', amount=surplus)
			return True

	def update_monthly_fees(self, amount, *args, **kwargs):
		qs = PaymentFee.objects.filter(paid=False, payment_agreement=self.payment_agreement).order_by("date_to_pay")
		for payment_fee in qs:
			if payment_fee.partially_paid == True:
				debt = payment_fee.debt
			elif payment_fee.partially_paid == False:
				debt = payment_fee.fee + payment_fee.extraordinary_fee

			total_amount = amount
			debt -= total_amount

			if debt > 0:
				payment_fee.partially_paid = True
				payment_fee.debt = debt
				payment_fee.save()
				return True
			elif debt < 0:
				surplus = abs(debt)
				payment_fee.paid = True
				payment_fee.debt = 0
				payment_fee.partially_paid = False
				payment_fee.save()
				updated = self.update_next_fees(surplus, payment_fee)
				return True
			return True
		if not qs:
			surplus_model, created = Surplus.objects.get_or_create(payment_agreement=self.payment_agreement)
			SurplusPayment.objects.create(surplus_model=surplus_model, origin='cuota-mensual', amount=amount)
			return True


	def save(self, *args, **kwargs):
		if self.type_of_payment == 'cuota-mensual':
			payment_amount = self.amount
			fees_update = self.update_monthly_fees(payment_amount, *args, **kwargs)
			if fees_update:
				super(Payment, self).save(*args, **kwargs)
			else:
				print ("Error")
		elif self.type_of_payment in [x[0] for x in types_of_agreed_payments]:
			type_of_payment = self.type_of_payment
			try:
				agreed_payment = self.payment_agreement.agreed_payments.get(
					type_of_payment=type_of_payment
				)
			except MultipleObjectsReturned:
				agreed_payment = self.payment_agreement.agreed_payments.filter(type_of_payment=type_of_payment).last()
			except Exception as e:
				custom_msg = "Could not create payment of type: {}".format(self.type_of_payment)
				custom_capture_exception(e, custom_msg)
				raise (e)
				#Agregar validacion por si el error es que hay mas de un agrredpayment del mismo tipo o si no hay ningun agreedpayment de ese tipo
			if not agreed_payment.paid:
				amount = self.amount
				if agreed_payment.partially_paid == True:
					debt = agreed_payment.debt
				elif agreed_payment.partially_paid == False:
					debt = agreed_payment.fee

				debt -= amount

				if debt > 0:
					agreed_payment.partially_paid = True
					agreed_payment.debt = debt
					agreed_payment.save()
					super(Payment, self).save(*args, **kwargs)
				elif debt < 0:
					surplus = abs(debt)
					agreed_payment.paid = True
					agreed_payment.debt = 0
					agreed_payment.partially_paid = False
					agreed_payment.save()
					if agreed_payment.applied_to_initialfee == True or self.apply_surplus_to == 'cuota-mensual':
						updated = self.update_monthly_fees(surplus, *args, **kwargs)
						if updated:
							super(Payment, self).save(*args, **kwargs)
						else:
							pass
							#raise error
					elif self.apply_surplus_to == 'surplus-model':
						surplus_model, created = Surplus.objects.get_or_create(payment_agreement=self.payment_agreement)
						surplus_payment = SurplusPayment.objects.create(surplus_model=surplus_model, origin=agreed_payment.type_of_payment, amount=surplus)
						super(Payment, self).save(*args, **kwargs)

					#si hay surplus en agreedpayment y esta aplicado a CI, enviar el surplus a las cuotas iniciales.
					#si hay surplus en agreedpayment y NO esta aplicado a CI, preguntar si quiere enviarlo a CI o si no quiere, al modelo Surplus

					#En la tabla de Pagos poner pestañas para mostras las CIs canceladas, los pagos realizados a agreedpayments y los pagos a surplus
					#Al final mostrar la sumatoria de todos estos pagos.

					#Del resto de clases de pagos, solo tener el log.
				else:
					agreed_payment.paid = True
					agreed_payment.debt = 0
					agreed_payment.partially_paid = False
					agreed_payment.save()
					super(Payment, self).save(*args, **kwargs)
			else:
				return "The agreed payment is already fully paid"
	def __str__(self):
		return self.get_type_of_payment_display()
