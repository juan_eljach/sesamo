#!/usr/bin/env python
import os
import sys

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sesamo.settings.base")
import django
django.setup()


from units.models import Unity
from crm.models import UnityProfile

for unity in Unity.objects.all():
	try:
		UnityProfile.objects.create(unity=unity)
	except:
		print ("There was an error creating UnityProfile for Unity: {}".format(unity))
		pass
