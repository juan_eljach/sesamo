# System 💽

NodeJS v8.11.2

# Workflow 💻

## Install dependencies

```npm install```

## Run app dev mode

```npm run dev:presale```

## Build for production

```npm run build:presale```

```npm run build:office```

Note: The files will be generated in */assets/app*
