const gulpBase = require("./gulpfile.base");
const env = process.env.NODE_ENV;

gulpBase({
  server: {
    proxy: {
      target: "http://127.0.0.1:8000"
    }
  },
  scripts: {
    files: ["../static/app/js/*.js"]
  },
  sass: {
    files: ["./core/**/*.scss", "./presale/**/*.scss"],
    includePaths: ["./"],
    output: env === "production" ? "../assets/app/css" : "../static/app/css"
  }
});
