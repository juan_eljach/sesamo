import React, { Component } from "react";
import PropTypes from "prop-types";
import moment from "moment";

import Initials from "core/components/Initials";
import Tooltip from "core/components/Tooltip";

class CustomerCard extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {
      customer,
      cardRef,
      onOpenEditCustomer,
      onOpenCustomerInfo,
      ...rest
    } = this.props;

    const lastUpdate = customer.last_update
      ? moment(customer.last_update)
      : moment(customer.timestamp);

    return (
      <div
        className="customer-card v-align"
        ref={cardRef}
        onClick={() => onOpenCustomerInfo(customer)}
        {...rest}
      >
        <Initials
          initials={`${customer.first_name[0]}${customer.last_name[0]}`}
        />

        <div className="customer-card__info">
          <div className="customer-card__info-name">{`${customer.first_name} ${
            customer.last_name
          }`}</div>
          <div className="customer-card__info-phone">{customer.cellphone}</div>
          <div className="customer-card__info-last">
            Ult. Interacción {lastUpdate.locale("es").fromNow()}
          </div>
        </div>

        <div className="customer-card__actions">
          <Tooltip title="Editar">
            <i
              className="mdi mdi-pencil action-hover"
              onClick={event => onOpenEditCustomer(event, customer)}
            />
          </Tooltip>
        </div>
      </div>
    );
  }
}

CustomerCard.propTypes = {
  customer: PropTypes.object.isRequired
};

export default CustomerCard;
