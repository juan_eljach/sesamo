import React, { Component } from "react";
import { Field, reduxForm } from "redux-form";
import PropTypes from "prop-types";

import InputField from "core/components/InputField";
import Button from "core/components/Button";
import { adquisitionChannelList, segmentList } from "core/data/customers";

class CustomerForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isFetching: false
    };
  }

  static validate(fields) {
    const errors = {};
    const errorName = "Campo requerido";

    if (!fields.first_name) errors.first_name = errorName;
    if (!fields.last_name) errors.last_name = errorName;
    if (!fields.cellphone) errors.cellphone = errorName;
    if (!fields.email) errors.email = errorName;
    else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(fields.email)) {
      errors.email = "Email inválido";
    }
    if (!fields.adquisition_channel) errors.adquisition_channel = errorName;
    if (!fields.segment) errors.segment = errorName;

    return errors;
  }

  onSubmit = async values => {
    try {
      const {
        project,
        initialValues,
        onSaveCustomer,
        onEditCustomer,
        toggleModal,
        showNotificationPopup
      } = this.props;

      this.setState({ isFetching: true });

      const payload = {
        ...values,
        segment: values.segment.value,
        adquisition_channel: values.adquisition_channel.value,
        project: project.id,
        assigned_to: ""
      };

      if (initialValues.id) {
        await onEditCustomer(payload);
      } else {
        await onSaveCustomer(payload);
      }

      showNotificationPopup({
        message: "El cliente se ha guardado correctamente!"
      });

      this.setState({ isFetching: false });

      toggleModal("modalOpen");
    } catch (error) {
      // error
    }
  };

  render() {
    const { isFetching } = this.state;
    const { availableUnities, handleSubmit } = this.props;

    return (
      <form
        className="add-customer__form"
        onSubmit={handleSubmit(this.onSubmit)}
      >
        <Field
          name="first_name"
          type="text"
          label="Nombre"
          placeholder="Escribir nombre"
          component={InputField}
        />

        <Field
          name="last_name"
          type="text"
          label="Apellido"
          placeholder="Escribir apellido"
          component={InputField}
        />

        <Field
          name="cellphone"
          type="number"
          label="Celular"
          placeholder="Escribir celular"
          component={InputField}
        />

        <Field
          name="phone"
          type="number"
          label="Teléfono Fijo"
          placeholder="Escribir teléfono fijo"
          component={InputField}
        />

        <Field
          name="email"
          type="text"
          label="Correo Electrónico"
          placeholder="Escribir correo electrónico"
          component={InputField}
        />

        <Field
          name="adquisition_channel"
          type="select"
          label="Origen"
          placeholder="Seleccionar origen"
          component={InputField}
          isSearchable={false}
          options={adquisitionChannelList}
        />

        <Field
          name="segment"
          type="select"
          label="Segmento"
          placeholder="Seleccionar segmento"
          component={InputField}
          options={segmentList}
          isSearchable={false}
        />

        <Field
          name="unity_of_interest"
          type="select"
          label="Unidad de interés"
          placeholder="Seleccionar unidad de interés"
          component={InputField}
          options={availableUnities}
        />

        <div className="add-customer__form-button">
          <Button isFetching={isFetching}>GUARDAR</Button>
        </div>
      </form>
    );
  }
}

CustomerForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  availableUnities: PropTypes.arrayOf(
    PropTypes.shape({ value: PropTypes.number, label: PropTypes.text })
  )
};

export default reduxForm({
  form: "CustomerForm",
  validate: CustomerForm.validate,
  keepDirtyOnReinitialize: true,
  enableReinitialize: true
})(CustomerForm);
