import React, { Component } from "react";
import classnames from "classnames";
import Checkbox from "@material-ui/core/Checkbox";
import moment from "moment";
import PropTypes from "prop-types";

import Initials from "core/components/Initials";
import InputField from "core/components/InputField";
import DatePicker from "core/components/DatePicker";
import TimePicker from "core/components/TimePicker";
import Button from "core/components/Button";
import { adquisitionChannelList } from "core/data/customers";
import { segmentList } from "core/data/customers";

class CustomerInfo extends Component {
  constructor(props) {
    super(props);

    this.state = {
      editorActive: "notes",
      textAreaValue: "",
      activityTypeValue: { value: "", label: "Seleccionar" },
      activityDateValue: null,
      activityHourValue: moment("14:00:00", "HH:mm"),
      validationError: null,
      isPoolSegment: null,
      isSaving: false
    };

    this.activitiesList = [
      { value: "llamada", label: "Hacer llamada" },
      { value: "email", label: "Enviar correo" },
      { value: "reunion", label: "Reunión" }
    ];
  }

  componentDidMount() {
    if (this.props.customer.assigned_to === null) {
      this.setState({ isPoolSegment: true });
    }
  }

  onManagementChange = item => {
    this.setState({ editorActive: item });
  };

  onTextAreaChange = event => {
    this.setState({ textAreaValue: event.target.value });
  };

  renderCheckIcon = activity => {
    if (activity.done) {
      return <i className="mdi mdi-check-circle" />;
    }

    return <i className="mdi mdi-checkbox-blank-circle-outline" />;
  };

  verifyExpiredDate = activity => {
    const test = /hace/.test(moment(activity.scheduled_date).fromNow());
    return test;
  };

  renderActivityScheduling = activity => {
    const activityScheduledClassName = classnames(
      "customer-info__information-activity-scheduled",
      { "customer-info__information-activity-scheduled--done": activity.done },
      {
        "customer-info__information-activity-scheduled--expired":
          !activity.done && this.verifyExpiredDate(activity)
      }
    );

    if (activity.done) {
      return (
        <span className={activityScheduledClassName}>
          realizado {moment(activity.checked_as_done_timestamp).fromNow()}
        </span>
      );
    }

    if (!activity.done && !this.verifyExpiredDate(activity)) {
      return (
        <span className={activityScheduledClassName}>
          programado para {moment(activity.scheduled_date).fromNow()}
        </span>
      );
    }

    return (
      <span className={activityScheduledClassName}>
        venció {moment(activity.timestamp).fromNow()}
      </span>
    );
  };

  onSelectChange = (type, value) => {
    if (!value) return;

    this.setState({ [type]: value });
  };

  onSave = async () => {
    const {
      editorActive,
      textAreaValue,
      activityTypeValue,
      activityDateValue,
      activityHourValue
    } = this.state;
    const {
      user,
      customer,
      project,
      saveNote,
      saveActivity,
      addNoteToTheList,
      addActivityToTheList
    } = this.props;

    if (!textAreaValue) {
      this.setState({ validationError: "Ingresar descripción" });

      return;
    }

    if (editorActive === "notes") {
      const note = {
        note: textAreaValue,
        person: customer.id,
        assigned_to: user.id
      };

      this.setState({ isSaving: true });

      const response = await saveNote(note);

      addNoteToTheList(response);
    } else if (editorActive === "activities") {
      if (!activityTypeValue.value || !activityDateValue) {
        this.setState({ validationError: "Campos sin seleccionar." });

        return;
      }

      const activity = {
        description: textAreaValue,
        activity_type: activityTypeValue.value,
        scheduled_date: `${activityDateValue.format(
          "YYYY-MM-DD"
        )} ${activityHourValue.format("HH:mm")}`,
        person: customer.id,
        project: project.id
      };

      this.setState({ isSaving: true });

      const response = await saveActivity(activity);

      addActivityToTheList(response);
    }

    this.setState({
      textAreaValue: "",
      activityTypeValue: { value: "", label: "Seleccionar" },
      activityDateValue: null,
      activityHourValue: moment("14:00:00", "HH:mm"),
      isSaving: false
    });
  };

  render() {
    const {
      editorActive,
      textAreaValue,
      activityTypeValue,
      activityDateValue,
      activityHourValue,
      isSaving
    } = this.state;
    const {
      customer,
      onClose,
      completeActivity,
      changeSegment,
      onCustomerEditFromInfo
    } = this.props;
    const segment = segmentList.find(s => s.value === customer.segment) || {};
    const adquisitionChannel = adquisitionChannelList.find(
      a => a.value === customer.adquisition_channel
    );

    const headerNotesClassName = classnames(
      "customer-info__editor-header-item v-align",
      {
        "customer-info__editor-header-item--active": editorActive === "notes"
      }
    );

    const headerActivitiesClassName = classnames(
      "customer-info__editor-header-item v-align",
      {
        "customer-info__editor-header-item--active":
          editorActive === "activities"
      }
    );

    return (
      <div className="customer-info">
        <i className="action-hover mdi mdi-close" onClick={onClose} />

        <div className="customer-info__header">
          <div className="customer-info__name v-align">
            <Initials
              initials={`${customer.first_name &&
                customer.first_name[0]}${customer.last_name &&
                customer.last_name[0]}`}
            />
            <span>{`${customer.first_name} ${customer.last_name}`}</span>
          </div>

          <div className="customer-info__data">
            <div className="customer-info__data-item">
              <label>Celular</label>
              <span>{customer.cellphone}</span>
            </div>

            <div className="customer-info__data-item">
              <label>Teléfono</label>
              <span>{customer.phone}</span>
            </div>

            <div className="customer-info__data-item">
              <label>Correo</label>
              <span>{customer.email}</span>
            </div>

            <div className="customer-info__data-item">
              <label>Origen</label>
              <span>
                {customer.adquisition_channel &&
                  adquisitionChannel &&
                  adquisitionChannel.label}
              </span>
            </div>

            <div className="customer-info__data-item">
              <label>Segmento</label>
              <InputField
                type="select"
                value={{ value: segment.value, label: segment.label }}
                placeholder="Seleccionar segmento"
                isSearchable={false}
                options={segmentList}
                onChange={seg =>
                  changeSegment(customer, seg.value, customer.segment)
                }
                rounded
              />
            </div>

            <div className="customer-info__data-item-edit">
              <a onClick={event => onCustomerEditFromInfo(event, customer)}>
                <i className="mdi mdi-pencil" />
                <span>Editar</span>
              </a>
            </div>
          </div>
        </div>

        <div className="customer-info__body">
          <div className="customer-info__management">
            {!this.state.isPoolSegment ? (
              <div className="customer-info__editor">
                <div className="customer-info__editor-header">
                  <div
                    className={headerNotesClassName}
                    onClick={() => this.onManagementChange("notes")}
                  >
                    <i className="mdi mdi-pencil" />
                    <span>Agregar nota</span>
                  </div>
                  <div
                    className={headerActivitiesClassName}
                    onClick={() => this.onManagementChange("activities")}
                  >
                    <i className="mdi mdi-calendar-check" />
                    <span>Agregar actividad</span>
                  </div>
                </div>

                <div className="customer-info__editor-body">
                  <InputField
                    value={textAreaValue}
                    type="textarea"
                    placeholder={
                      editorActive === "notes"
                        ? "Escribe la nota"
                        : "Describe la actividad"
                    }
                    icon="feather"
                    onChange={this.onTextAreaChange}
                  />
                </div>

                {editorActive === "activities" && (
                  <div className="customer-info__editor-activity v-align">
                    <div className="customer-info__editor-activity-type">
                      <InputField
                        value={activityTypeValue}
                        type="select"
                        label="Tipo de actividad"
                        placeholder="Seleccionar"
                        isSearchable={false}
                        options={this.activitiesList}
                        onChange={value =>
                          this.onSelectChange("activityTypeValue", value)
                        }
                        rounded
                      />
                    </div>

                    <div className="customer-info__editor-activity-date">
                      <label>Fecha de actividad</label>
                      <DatePicker
                        defaultValue={activityDateValue}
                        onChange={value =>
                          this.onSelectChange("activityDateValue", value)
                        }
                      />
                    </div>

                    <div className="customer-info__editor-activity-hour">
                      <label>Hora</label>
                      <TimePicker
                        value={activityHourValue}
                        onChange={value =>
                          this.onSelectChange("activityHourValue", value)
                        }
                      />
                    </div>
                  </div>
                )}

                <div className="customer-info__editor-footer">
                  <div className="validation-error">
                    {this.state.validationError}
                  </div>
                  <Button isFetching={isSaving} onClick={this.onSave}>
                    Guardar
                  </Button>
                </div>
              </div>
            ) : (
              <div className="pool-message">
                No es posible añadir notas y actividades a un cliente en
                segmento Pool. Por favor asígnelo a otro segmento.
              </div>
            )}

            <div className="customer-info__notes">
              <h4 className="v-align">
                <i className="mdi mdi-file-outline" />
                <span>NOTAS</span>
              </h4>

              {customer.notes &&
                customer.notes.reverse().map((note, index) => {
                  const assignedTo = `${note.assigned_to.user.first_name} ${note.assigned_to.user.last_name}`;

                  return (
                    <div className="customer-info__note v-align" key={index}>
                      <div className="customer-info__note-icon">
                        <i className="mdi mdi-file-outline" />
                      </div>

                      <div className="customer-info__note-content">
                        <p className="customer-info__note-description">
                          {note.note}
                        </p>
                        <div className="customer-info__note-seller">
                          Vendedor: {assignedTo}
                        </div>
                        <div className="customer-info__note-time v-align">
                          <i className="mdi mdi-timer" />
                          <span>{moment(note.timestamp).fromNow()}</span>
                        </div>
                      </div>
                    </div>
                  );
                })}
            </div>
          </div>

          <div className="customer-info__information">
            <div className="customer-info__information-activities">
              <h4 className="v-align">
                <i className="mdi mdi-calendar" />
                <span>ACTIVIDADES PLANEADAS</span>
              </h4>

              <div className="customer-info__information-activities">
                {customer.activities &&
                  customer.activities.map(activity => (
                    <div
                      className="customer-info__information-activity v-align"
                      key={activity.id}
                    >
                      <Checkbox
                        icon={this.renderCheckIcon(activity)}
                        checkedIcon={this.renderCheckIcon(activity)}
                        style={{
                          color: activity.done ? "#81C784" : "#7e7e7e",
                          width: 20,
                          height: 20
                        }}
                        onChange={() => completeActivity(activity)}
                      />
                      <p className="customer-info__information-activity-description">
                        {activity.description}
                      </p>

                      {this.renderActivityScheduling(activity)}
                    </div>
                  ))}
              </div>
            </div>

            <div className="customer-info__information-apartments">
              <h4 className="v-align">
                <i className="mdi mdi-star-outline" />
                <span>APARTAMENTOS DE INTERÉS</span>
              </h4>

              <div className="customer-info__information-apartments-list">
                {customer.quotations &&
                  customer.quotations.map((quotation, index) => (
                    <div
                      className="customer-info__information-apartments-apartment v-align"
                      key={quotation.id}
                    >
                      <div className="customer-info__information-apartments-unity">
                        {quotation.name}
                      </div>
                      <a
                        className="customer-info__information-apartments-document v-align"
                        href={`/download/quotation/${quotation.id}`}
                        target="_blank"
                      >
                        <i className="mdi mdi-download" />
                        <span>{`Cotización ${index + 1}`}</span>
                      </a>
                    </div>
                  ))}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

CustomerInfo.propTypes = {
  customer: PropTypes.object
};

export default CustomerInfo;
