import React, { Component } from "react";
import PropTypes from "prop-types";
import { Droppable, Draggable } from "react-beautiful-dnd";

import CustomerCard from "../CustomerCard";
import colors from "core/data/colors";
const grid = 8;

class Board extends Component {
  constructor(props) {
    super(props);
  }

  getItemStyle = (isDragging, draggableStyle) => ({
    // some basic styles to make the items look a bit nicer
    userSelect: "none",
    padding: grid * 2,
    margin: `0 0 ${grid}px 0`,

    // change background colour if dragging
    border: isDragging ? `1px solid ${colors.blue}` : "",

    // styles we need to apply on draggables
    ...draggableStyle
  });

  getListStyle = isDraggingOver => ({
    background: isDraggingOver ? colors.grey : ""
  });

  render() {
    const { title, items, droppableId, ...rest } = this.props;

    return (
      <div className="board">
        <div className="board__header v-align space-between">
          <div className="board__header-title">{title}</div>
          <div className="board__header-indicator v-align">
            <i className="mdi mdi-account" />
            <span>{items.length}</span>
          </div>
        </div>

        <Droppable droppableId={droppableId}>
          {(provided, snapshot) => (
            <div
              className="board__body"
              ref={provided.innerRef}
              style={this.getListStyle(snapshot.isDraggingOver)}
            >
              {items.map((item, index) => {
                return (
                  <Draggable key={item.id} draggableId={item.id} index={index}>
                    {(prov, snap) => (
                      <CustomerCard
                        customer={item}
                        cardRef={prov.innerRef}
                        {...prov.draggableProps}
                        {...prov.dragHandleProps}
                        style={this.getItemStyle(
                          snap.isDragging,
                          prov.draggableProps.style
                        )}
                        {...rest}
                      />
                    )}
                  </Draggable>
                );
              })}
            </div>
          )}
        </Droppable>
      </div>
    );
  }
}

Board.propTypes = {
  title: PropTypes.string.isRequired,
  customers: PropTypes.array
};

export default Board;
