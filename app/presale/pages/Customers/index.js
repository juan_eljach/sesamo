import React, { Component } from "react";
import { connect } from "react-redux";
import Select from "react-select";
import CustomerForm from "./components/CustomerForm";
import { debounce } from "throttle-debounce";
import { DragDropContext, Droppable } from "react-beautiful-dnd";
import moment from "moment";

import Board from "./components/Board";
import CustomerInfo from "./components/CustomerInfo";
import Modal from "core/components/Modal";
import WithLoading from "core/components/WithLoading";
import * as customersData from "core/data/customers";
import * as customersOperations from "core/redux/customers/operations";
import * as notificationPopupActions from "core/redux/notificationPopup/actions";
import * as unitiesOperations from "core/redux/unities/operations";

class Customers extends Component {
  constructor(props) {
    super(props);
    this.state = {
      customers: [],
      segmentation: {
        pool: [],
        high: [],
        low: [],
        medium: [],
        sold: [],
        sectionLoaded: false
      },
      personsSearch: [],
      availableUnities: [],
      customerSelected: {},
      modalOpen: false,
      customerInfoOpen: false,
      isSearching: false
    };

    this.styles = {
      control: () => ({
        backgroundColor: "transparent",
        height: 25,
        border: 0
      })
    };

    this.onSearchPerson = debounce(500, this.onSearchPerson.bind(this));
  }

  async componentDidMount() {
    const segmentation = await this.props.getSegmentation();
    const availableUnities = await this.props.getAvailableUnities();
    const orderLogResponse = await this.props.getOrderLog();

    let currentOrderLog = orderLogResponse.data.find(
      l => l.salesperson === this.props.user.id
    );

    let seg;

    let segmentationList = this.getSegmentationList(segmentation);

    if (!currentOrderLog) {
      const list = this.getSegmentationOrder(segmentationList);

      const payload = {
        project: this.props.project.id,
        salesperson: this.props.user.id,
        data: list
      };

      const response = await this.props.saveOrderLog(payload);

      currentOrderLog = response.data;
    } else {
      /**
       * Revisar cuando esté el servidor de staging por qué genera eror
       * al armar el order log. (Mirar cómo llegan los datos)
       */
      seg = this.getSegmentationList(segmentation);

      const { data } = currentOrderLog;

      // segmentationList = {
      //   pool: seg.pool.sort(
      //     (a, b) => data.pool[a.id].index - data.pool[b.id].index
      //   ),
      //   low: seg.low.sort(
      //     (a, b) => data.low[a.id].index - data.low[b.id].index
      //   ),
      //   medium: seg.medium.sort(
      //     (a, b) => data.medium[a.id].index - data.medium[b.id].index
      //   ),
      //   high: seg.high.sort(
      //     (a, b) => data.high[a.id].index - data.high[b.id].index
      //   ),
      //   sold: seg.sold.sort(
      //     (a, b) => data.sold[a.id].index - data.sold[b.id].index
      //   )
      // };
    }

    this.setState({
      customers: seg,
      segmentation: segmentationList,
      availableUnities: availableUnities.map(unity => ({
        value: unity.id,
        label: unity.name
      })),
      currentOrderLog,
      sectionLoaded: true
    });
  }

  getSegmentationOrder = segmentationList => {
    return Object.keys(segmentationList).reduce((acc, current) => {
      return {
        ...acc,
        [current]: segmentationList[current].reduce((a, c, i) => {
          return { ...a, [c.id]: { index: i } };
        }, {})
      };
    }, {});
  };

  updateSegmentationOrder = () => {
    const payload = {
      pk: this.state.currentOrderLog.pk,
      project: this.props.project.id,
      salesperson: this.props.user.id,
      data: this.getSegmentationOrder(this.state.segmentation)
    };

    this.props.updateOrderLog(payload);
  };

  getSegmentationList = list => {
    return {
      pool: list.filter(el => !el.assigned_to),
      low: list.filter(
        el => el.segment === "low" && el.assigned_to && !el.sold
      ),
      medium: list.filter(
        el => el.segment === "medium" && el.assigned_to && !el.sold
      ),
      high: list.filter(
        el => el.segment === "high" && el.assigned_to && !el.sold
      ),
      sold: list.filter(el => el.sold && el.assigned_to)
    };
  };

  onSearchPerson = async data => {
    this.setState({ isSearching: true });

    const response = await this.props.searchPerson(data);

    this.setState({ isSearching: false });

    const personsSearch = response.map(person => {
      const personName = `${person.first_name} ${person.last_name}`;

      return { value: person.id, label: personName };
    });

    this.setState({ personsSearch });
  };

  toggleModal = type => {
    this.setState(prevState => {
      return {
        ...prevState,
        [type]: !prevState[type],
        customerSelected: {}
      };
    });
  };

  move = result => {
    const source = result.source.droppableId;
    const destination = result.destination.droppableId;

    if (source === destination) {
      const newList = this.reorder(
        this.state.segmentation[source],
        result.source.index,
        result.destination.index
      );

      this.setState(prevState => ({
        ...prevState,
        segmentation: {
          ...prevState.segmentation,
          [source]: newList
        }
      }));

      this.updateSegmentationOrder();

      return;
    }

    if (destination === "pool") return;

    const currentCustomer = {
      ...this.state.segmentation[source].find(
        el => el.id === result.draggableId
      ),
      segment: destination
    };

    const customerToSend = {
      ...currentCustomer,
      adquisition_channel: currentCustomer.adquisition_channel || "other"
    };

    this.changeSegment(
      customerToSend,
      destination,
      source,
      result.destination.index
    );

    this.updateSegmentationOrder();
  };

  changeSegment = async (
    currentCustomer,
    destination,
    source,
    destinationIndex = 0
  ) => {
    const { updateCustomer, user } = this.props;
    const newCustomer = { ...currentCustomer, assigned_to: user.id };

    if (destination === "sold") {
      newCustomer.segment = "";
      newCustomer.sold = true;
    }

    this.setState(prevState => {
      let destinationList = Array.from(prevState.segmentation[destination]);

      destinationList.splice(destinationIndex, 0, newCustomer);

      return {
        ...prevState,
        segmentation: {
          ...prevState.segmentation,
          [source]: prevState.segmentation[source].filter(
            el => el.id !== currentCustomer.id
          ),
          [destination]: destinationList
        },
        customerSelected: newCustomer
      };
    });

    await updateCustomer(currentCustomer.slug, newCustomer);
  };

  reorder = (list, startIndex, endIndex) => {
    const newList = Array.from(list);
    const [removed] = newList.splice(startIndex, 1);

    newList.splice(endIndex, 0, removed);

    return newList;
  };

  onDragEnd = result => {
    this.move(result);
  };

  onSaveCustomer = async payload => {
    const results = await this.props.saveCustomer({
      ...payload,
      unity_of_interest: payload.unity_of_interest.value
    });

    this.setState(prevState => {
      return {
        ...prevState,
        segmentation: {
          ...prevState.segmentation,
          [payload.segment]: [
            ...prevState.segmentation[payload.segment],
            results
          ]
        }
      };
    });
  };

  onSelectPerson = search => {
    if (!search) return;

    const { high, low, medium, pool, sold } = this.state.customers;

    const allCustomers = [...high, ...low, ...medium, ...pool, ...sold];

    const customer = allCustomers.find(c => c.id === search.value);

    if (customer) {
      this.onOpenCustomerInfo(customer);
    }
  };

  onOpenCustomerInfo = customer => {
    this.setState({ customerInfoOpen: true, customerSelected: customer });
  };

  onOpenEditCustomer = async (event, selected) => {
    if (event) {
      event.stopPropagation();
    }

    let unity = {};

    if (selected.unity_of_interest) {
      unity = await this.props.getUnity(selected.unity_of_interest);
    }

    const customer = {
      ...selected,
      unity_of_interest: { value: unity.id, label: unity.name },
      segment: customersData.segmentList.find(
        el => el.value === selected.segment
      ),
      adquisition_channel: customersData.adquisitionChannelList.find(
        el => el.value === selected.adquisition_channel
      )
    };

    this.setState({ customerSelected: customer, modalOpen: true });
  };

  onEditCustomer = async payload => {
    const results = await this.props.updateCustomer(
      this.state.customerSelected.slug,
      payload
    );

    this.setState(prevState => {
      return {
        ...prevState,
        segmentation: {
          ...prevState.segmentation,
          [payload.segment]: prevState.segmentation[payload.segment].map(
            customer => {
              if (customer.id === results.id) {
                return results;
              }

              return customer;
            }
          )
        }
      };
    });
  };

  completeActivity = activity => {
    if (activity.done) return;

    const newActivity = {
      ...activity,
      done: true,
      checked_as_done_timestamp: moment.now()
    };

    this.setState(prevState => ({
      ...prevState,
      customerSelected: {
        ...prevState.customerSelected,
        activities: prevState.customerSelected.activities.map(act => {
          if (act.id === activity.id) {
            return newActivity;
          }

          return act;
        })
      }
    }));

    this.props.updateActivity({
      ...newActivity,
      person: this.state.customerSelected.id,
      project: this.props.project.id
    });
  };

  addNoteToTheList = note => {
    this.setState(prevState => ({
      ...prevState,
      customerSelected: {
        ...prevState.customerSelected,
        notes: [note, ...prevState.customerSelected.notes]
      }
    }));
  };

  addActivityToTheList = activity => {
    this.setState(prevState => ({
      ...prevState,
      customerSelected: {
        ...prevState.customerSelected,
        activities: [...prevState.customerSelected.activities, activity]
      }
    }));
  };

  onCustomerEditFromInfo = (event, customer) => {
    this.setState({ customerInfoOpen: false });

    this.onOpenEditCustomer(event, customer);
  };

  render() {
    const {
      segmentation,
      personsSearch,
      availableUnities,
      customerSelected,
      modalOpen,
      customerInfoOpen,
      isSearching,
      sectionLoaded
    } = this.state;

    const {
      user,
      project,
      saveNote,
      saveActivity,
      showNotificationPopup
    } = this.props;

    return (
      <WithLoading isFetching={!sectionLoaded}>
        <section className="customers">
          <div className="customers__header">
            <a
              className="customers__add v-align"
              onClick={() => this.toggleModal("modalOpen")}
            >
              <i className="mdi mdi-plus" />
              <span>Agregar persona</span>
            </a>

            <div className="customers__search">
              <i className="mdi mdi-magnify" />
              <Select
                placeholder="Buscar Personas"
                noOptionsMessage={() => "No se encontraron resultados"}
                styles={this.styles}
                options={personsSearch}
                isLoading={isSearching}
                loadingMessage={() => "Buscando..."}
                classNamePrefix="customers-search"
                onInputChange={this.onSearchPerson}
                onChange={this.onSelectPerson}
              />
            </div>
          </div>

          <DragDropContext onDragEnd={this.onDragEnd}>
            <Droppable droppableId="lista">
              {provided => {
                return (
                  <div
                    className="customers__boards-list"
                    ref={provided.innerRef}
                  >
                    <Board
                      items={segmentation.pool}
                      title="Pool"
                      droppableId="pool"
                      onOpenCustomerInfo={this.onOpenCustomerInfo}
                      onOpenEditCustomer={this.onOpenEditCustomer}
                    />

                    <Board
                      items={segmentation.low}
                      title="Poco Probable"
                      droppableId="low"
                      onOpenCustomerInfo={this.onOpenCustomerInfo}
                      onOpenEditCustomer={this.onOpenEditCustomer}
                    />

                    <Board
                      items={segmentation.medium}
                      title="Medianamente Probable"
                      droppableId="medium"
                      onOpenCustomerInfo={this.onOpenCustomerInfo}
                      onOpenEditCustomer={this.onOpenEditCustomer}
                    />

                    <Board
                      items={segmentation.high}
                      title="Altamente Probable"
                      droppableId="high"
                      onOpenCustomerInfo={this.onOpenCustomerInfo}
                      onOpenEditCustomer={this.onOpenEditCustomer}
                    />

                    <Board
                      items={segmentation.sold}
                      title="Vendido"
                      droppableId="sold"
                      onOpenCustomerInfo={this.onOpenCustomerInfo}
                      onOpenEditCustomer={this.onOpenEditCustomer}
                    />
                  </div>
                );
              }}
            </Droppable>
          </DragDropContext>

          <Modal open={modalOpen} onClose={() => this.toggleModal("modalOpen")}>
            <div className="add-customer">
              <div className="add-customer__form-container">
                <div className="add-customer__form-header">
                  {customerSelected.id
                    ? "EDITAR CLIENTE"
                    : "AGREGAR NUEVO CLIENTE"}
                </div>

                <CustomerForm
                  availableUnities={availableUnities}
                  project={project}
                  onSaveCustomer={this.onSaveCustomer}
                  onEditCustomer={this.onEditCustomer}
                  toggleModal={this.toggleModal}
                  initialValues={customerSelected}
                  showNotificationPopup={showNotificationPopup}
                />
              </div>

              <div className="add-customer__picture show-for-large" />

              <i
                className="action-hover mdi mdi-close"
                onClick={() => this.toggleModal("modalOpen")}
              />
            </div>
          </Modal>

          <Modal open={customerInfoOpen}>
            <CustomerInfo
              user={user}
              customer={customerSelected}
              project={project}
              saveNote={saveNote}
              saveActivity={saveActivity}
              completeActivity={this.completeActivity}
              changeSegment={this.changeSegment}
              addNoteToTheList={this.addNoteToTheList}
              addActivityToTheList={this.addActivityToTheList}
              onCustomerEditFromInfo={this.onCustomerEditFromInfo}
              onClose={() => this.toggleModal("customerInfoOpen")}
            />
          </Modal>
        </section>
      </WithLoading>
    );
  }
}

const mapStateToProps = ({ project, user }) => {
  return {
    project,
    user
  };
};

const mapDispatchToProps = {
  getSegmentation: customersOperations.getSegmentation,
  searchPerson: customersOperations.searchPerson,
  saveCustomer: customersOperations.saveCustomer,
  getOrderLog: customersOperations.getOrderLog,
  saveOrderLog: customersOperations.saveOrderLog,
  updateOrderLog: customersOperations.updateOrderLog,
  showNotificationPopup: notificationPopupActions.showNotificationPopup,
  updateCustomer: customersOperations.updateCustomer,
  saveNote: customersOperations.saveNote,
  saveActivity: customersOperations.saveActivity,
  updateActivity: customersOperations.updateActivity,
  getUnity: unitiesOperations.getUnity,
  getAvailableUnities: unitiesOperations.getAvailableUnities
};

export default connect(mapStateToProps, mapDispatchToProps)(Customers);
