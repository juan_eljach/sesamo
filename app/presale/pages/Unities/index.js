import React, { Component } from "react";
import { connect } from "react-redux";
import classnames from "classnames";
import moment from "moment";
import uuid from "uuid/v4";
import { debounce as debounceFunc } from "throttle-debounce";
import { CSSTransition, TransitionGroup } from "react-transition-group";

import Table, {
  TableHead,
  TableBody,
  TableRow,
  TableCell
} from "core/components/Table";
import Searcher from "core/components/Searcher";
import InputField from "core/components/InputField";
import Button from "core/components/Button";
import formatValue from "core/helpers/formatValue";
import Initials from "core/components/Initials";
import NormalQuotationForm from "./components/NormalQuotationForm";
import QuickQuotationForm from "./components/QuickQuotationForm";
import WithLoading from "core/components/WithLoading";
import { unityStatusList } from "core/data/unities";
import {
  getFloorAndApto,
  getUnitiessByTowerAndFloor,
  getUnitiesNumber
} from "core/helpers/unities";
import * as unitiesActions from "core/redux/unities/operations";
import * as customersActions from "core/redux/customers/operations";
import Loading from "core/components/Loading";

class Unities extends Component {
  constructor(props) {
    super(props);

    this.state = {
      unitiesByTowerAndFloor: {},
      unitiesNumber: [],
      selectedTower: { label: "Todas", value: "all" },
      selectedUnity: {},
      selectedSidebarMenu: "features",
      personsResults: [],
      selectedPerson: {},
      customerSearchValue: "",
      interestedPersons: [],
      quotationInitialValues: {},
      quotationTabActive: "normal",
      quotationDocument: {},
      successMessageActive: "",
      sidebarOpen: false,
      isSaving: false,
      isSearching: false,
      sectionLoaded: false
    };

    this.menuItems = [
      {
        value: "features",
        text: "Características",
        icon: "../../../../static/img/starf.svg"
      },
      {
        value: "interestedPersons",
        text: "Clientes interesados",
        icon: "../../../../static/img/peoplef.svg"
      },
      {
        value: "virtualBooked",
        text: "Nueva separación tentativa",
        icon: "../../../../static/img/starf.svg"
      },
      {
        value: "newQuotation",
        text: "Nueva cotización",
        icon: "../../../../static/img/editf.svg"
      }
    ];

    this.quotationTabs = [
      { value: "normal", text: "Cotización normal" },
      { value: "quick", text: "Cotización rápida" }
    ];

    this.onCustomerSearch = debounceFunc(500, this.onCustomerSearch.bind(this));
  }

  async componentDidMount() {
    const unities = await this.props.getUnities();

    this.unitiesByTowerAndFloor = getUnitiessByTowerAndFloor(unities);
    this.unitiesNumber = getUnitiesNumber(this.unitiesByTowerAndFloor);

    this.setState({
      unitiesByTowerAndFloor: this.unitiesByTowerAndFloor,
      unitiesNumber: this.unitiesNumber,
      sectionLoaded: true
    });
  }

  getShortValueFromMillions = value => {
    const valueToString = value.toString();
    const wholeValue = valueToString.substring(0, valueToString.length - 6);

    const decimalValue = valueToString.substr(wholeValue.length, 1);
    return `${wholeValue},${decimalValue}`;
  };

  onTowerChange = tower => {
    if (tower.value === "all") {
      return this.setState(prevState => {
        return {
          ...prevState,
          unitiesByTowerAndFloor: this.unitiesByTowerAndFloor,
          selectedTower: tower
        };
      });
    }

    this.setState(prevState => {
      return {
        ...prevState,
        unitiesByTowerAndFloor: {
          [tower.value]: this.unitiesByTowerAndFloor[tower.value]
        },
        selectedTower: tower
      };
    });
  };

  onOpenSidebar = unity => {
    this.setState(prevState => {
      return {
        ...prevState,
        selectedUnity: unity,
        sidebarOpen: true,
        quotationDocument: {},
        selectedSidebarMenu: "features"
      };
    });
  };

  onCloseSidebar = () => {
    this.setState(prevState => {
      return { ...prevState, selectedUnity: {}, sidebarOpen: false };
    });
  };

  getUnityName = name => {
    if (!name) return "";

    const splited = name.split("-");

    return `${splited[0]} Apto ${splited[1]}`;
  };

  getTowerOptions = () => {
    if (!this.unitiesByTowerAndFloor) return [];

    return [
      { label: "Todas", value: "all" },
      ...Object.keys(this.unitiesByTowerAndFloor).map((t, index) => ({
        label: `Torre ${t.replace(/t/g, "")}`,
        value: t
      }))
    ];
  };

  onSelectSidebarMenu = item => {
    if (item === "interestedPersons") {
      this.getVirtualBooking();
    }

    this.setState(prevState => {
      if (item === "newQuotation") {
        return {
          selectedSidebarMenu: item,
          selectedPerson: {},
          customerSearchValue: "",
          personsResults: [],
          quotationInitialValues: {
            months_to_deliver: moment(
              prevState.selectedUnity.delivery_date
            ).diff(moment(), "months"),
            price: String(prevState.selectedUnity.price)
          }
        };
      }

      return {
        selectedSidebarMenu: item,
        selectedPerson: {},
        customerSearchValue: "",
        personsResults: []
      };
    });
  };

  onCustomersChangeInput = search => {
    this.setState({ customerSearchValue: search });

    this.onCustomerSearch(search);
  };

  onCustomerSearch = async search => {
    if (!search) return;

    this.setState({ isSearching: true });

    const results = await this.props.searchPerson(search);

    const personsResults = results.map(person => {
      const personName = `${person.first_name} ${person.last_name}`;

      return { ...person, value: person.id, label: personName };
    });

    this.setState({
      personsResults,
      successMessageActive: "",
      isSearching: false
    });
  };

  onCustomerSelect = value => {
    if (Array.isArray(value)) return;

    this.setState({ selectedPerson: value });
  };

  cleanCustomer = () => {
    this.setState({ selectedPerson: {} });
  };

  getVirtualBooking = async () => {
    const response = await this.props.getVirtualBooking(
      this.state.selectedUnity.id
    );

    this.setState({ interestedPersons: response });
  };

  saveVirtuallyBooked = async () => {
    const { selectedPerson, selectedUnity } = this.state;
    const { saveVirtualBooked } = this.props;

    this.setState({ isSaving: true });

    const payload = {
      assigned_to: null,
      notification_cellphone: selectedPerson.cellphone,
      notification_email: selectedPerson.email,
      person: selectedPerson.id,
      unity: selectedUnity.id
    };

    const response = await saveVirtualBooked(payload);

    if (response) {
      this.setState({
        isSaving: false,
        selectedPerson: {},
        customerSearchValue: "",
        personsResults: [],
        successMessageActive: "virtualBooked"
      });
    }

    setTimeout(() => this.setState({ successMessageActive: "" }), 5000);
  };

  onSelectQuotationTab = tab => {
    this.setState({ quotationTabActive: tab });
  };

  onSaveNormalQuotation = async values => {
    const { selectedPerson, selectedUnity } = this.state;

    const payload = {
      ...values,
      person: selectedPerson.id,
      unity: selectedUnity.id
    };

    const response = await this.props.saveNormalQuotation(payload);

    if (response) {
      this.setState({ quotationDocument: { id: response.id } });
    }
  };

  onSaveQuickQuotation = async values => {
    const { selectedPerson, selectedUnity } = this.state;

    const payload = {
      ...values,
      person: selectedPerson.id,
      unity: selectedUnity.id
    };

    const response = await this.props.saveQuickQuotation(payload);

    if (response) {
      this.setState({ quotationDocument: { id: response.id } });
    }
  };

  onNewQuotation = () => {
    this.setState({
      quotationDocument: {},
      selectedPerson: {},
      quotationInitialValues: {}
    });
  };

  onUnityStatusChange = async status => {
    try {
      const { selectedUnity } = this.state;
      const { updateUnity } = this.props;

      let payload = {
        ...selectedUnity,
        status: status.value
      };

      const response = await updateUnity(payload);

      const slugSplited = response.slug.split("-");
      const tower = slugSplited[0];
      const { floor } = getFloorAndApto(slugSplited[1]);

      this.setState(prevState => ({
        ...prevState,
        selectedUnity: { ...prevState.selectedUnity, ...response },
        unitiesByTowerAndFloor: {
          ...prevState.unitiesByTowerAndFloor,
          [tower]: {
            ...prevState.unitiesByTowerAndFloor[tower],
            [floor]: prevState.unitiesByTowerAndFloor[tower][floor].map(
              unity => {
                if (unity.id === selectedUnity.id) {
                  return response;
                }

                return unity;
              }
            )
          }
        }
      }));
    } catch (error) {
      // error
    }
  };

  handleSendEmail = () => {
    console.log("send email");
  };

  render() {
    const {
      unitiesByTowerAndFloor,
      unitiesNumber,
      selectedUnity,
      selectedTower,
      sidebarOpen,
      personsResults,
      selectedPerson,
      customerSearchValue,
      selectedSidebarMenu,
      interestedPersons,
      quotationTabActive,
      quotationInitialValues,
      quotationDocument,
      successMessageActive,
      isSearching,
      isSaving,
      sectionLoaded
    } = this.state;

    const towerOptions = this.getTowerOptions();
    const unityStatus = unityStatusList.find(
      s => s.value === selectedUnity.status
    );
    const selectedSidebarMenuItem = this.menuItems.find(
      item => item.value === selectedSidebarMenu
    );

    return (
      <WithLoading isFetching={!sectionLoaded}>
        <section className="unities">
          <CSSTransition
            in={sidebarOpen}
            appear={true}
            timeout={250}
            classNames="slide-right"
            unmountOnExit
          >
            <div className="unities__sidebar">
              <i
                className="mdi mdi-arrow-right"
                onClick={this.onCloseSidebar}
              />

              <div className="unities__sidebar-header">
                <h4>{this.getUnityName(selectedUnity.name)}</h4>

                <div className="unities__sidebar-header-info v-align space-between">
                  <div className="unities__sidebar-header-price">
                    ${formatValue(selectedUnity.price)}
                  </div>
                  <div className="unities__sidebar-header-status">
                    <InputField
                      type="select"
                      value={{
                        value: selectedUnity.status,
                        label: unityStatus && unityStatus.label
                      }}
                      isSearchable={false}
                      isDisabled={false}
                      options={unityStatusList.filter(statusItem => {
                        return statusItem.value !== "sold";
                      })}
                      onChange={this.onUnityStatusChange}
                      rounded
                    />
                  </div>
                </div>
              </div>

              <div className="unities__sidebar-menu">
                <ul>
                  {this.menuItems.map((item, index) => {
                    const sidebarMenuItemClassName = classnames({
                      "unities__sidebar-menu--selected":
                        item.value === selectedSidebarMenu
                    });

                    return (
                      <li
                        key={index}
                        className={sidebarMenuItemClassName}
                        onClick={() => this.onSelectSidebarMenu(item.value)}
                      >
                        <img src={item.icon} />
                        <span>{item.text}</span>
                      </li>
                    );
                  })}
                </ul>
              </div>

              {selectedSidebarMenu !== "newQuotation" && (
                <div className="unities__sidebar-menu-icon">
                  <div className="unities__sidebar-menu-icon-round">
                    <img src={selectedSidebarMenuItem.icon} />
                  </div>
                </div>
              )}

              {selectedSidebarMenu === "features" && (
                <div className="unities__sidebar-features">
                  <h5>Apartamento tipo 1</h5>

                  <div className="unities__sidebar-features-list">
                    <div>
                      <div>Habitaciones</div>
                      <div>{selectedUnity.bedrooms}</div>
                    </div>

                    <div>
                      <div>Baños</div>
                      <div>{selectedUnity.bathrooms}</div>
                    </div>

                    <div>
                      <div>Área construída</div>
                      <div>{selectedUnity.built_area}m2</div>
                    </div>

                    <div>
                      <div>Área privada</div>
                      <div>{selectedUnity.private_area}m2</div>
                    </div>
                  </div>
                </div>
              )}

              {selectedSidebarMenu === "interestedPersons" && (
                <div className="unities__sidebar-interested-persons">
                  <h5>Clientes interesados</h5>

                  <div className="unities__sidebar-interested-persons-list">
                    {interestedPersons.length ? (
                      interestedPersons.map((person, index) => {
                        return (
                          <div
                            className="unities__sidebar-interested-persons-item"
                            key={person.person.id}
                          >
                            <Initials initials={String(index + 1)} />

                            <div className="unities__sidebar-interested-persons-info">
                              <h6>{`${person.person.first_name} ${person.person.last_name}`}</h6>
                              <div>{person.notification_cellphone}</div>
                              <span>
                                Separado el{" "}
                                {moment(person.timestamp).format("DD MMM YYYY")}
                              </span>
                              <span>
                                Vence{" "}
                                {moment(person.timestamp)
                                  .add(8, "days")
                                  .fromNow()}
                              </span>
                            </div>
                          </div>
                        );
                      })
                    ) : (
                      <div className="unities__no-results">
                        Por el momento no hay clientes interesados
                      </div>
                    )}
                  </div>
                </div>
              )}

              {selectedSidebarMenu === "virtualBooked" && (
                <div className="unities__sidebar-virtual-booked">
                  <h5>Nueva Separación tentativa</h5>

                  <div className="unities__sidebar-virtual-booked-search">
                    <Searcher
                      inputValue={customerSearchValue}
                      options={personsResults}
                      selected={selectedPerson}
                      onInputChange={this.onCustomersChangeInput}
                      onSearch={this.onCustomerSearch}
                      onChange={this.onCustomerSelect}
                      onCloseChip={this.cleanCustomer}
                      isLoading={isSearching}
                      showChip={!!Object.keys(selectedPerson).length}
                      chipLabel={`${selectedPerson.first_name} ${selectedPerson.last_name}`}
                      isClearable
                      debounce
                    />
                  </div>

                  {successMessageActive === "virtualBooked" && (
                    <div className="unities__success">
                      La separación se ha realizado con éxito!
                    </div>
                  )}

                  {selectedPerson.label && (
                    <div className="unities__sidebar-virtual-booked-info">
                      <InputField
                        type="text"
                        value={selectedPerson.cellphone}
                        disabled
                      />
                      <InputField
                        type="text"
                        value={selectedPerson.email}
                        disabled
                      />

                      <div className="unities__sidebar-virtual-booked-button">
                        <Button
                          onClick={this.saveVirtuallyBooked}
                          isFetching={isSaving}
                        >
                          Guardar
                        </Button>
                      </div>
                    </div>
                  )}
                </div>
              )}

              {selectedSidebarMenu === "newQuotation" && (
                <div className="unities__sidebar-quotation">
                  <div className="unities__sidebar-quotation-header">
                    {this.quotationTabs.map((tab, index) => {
                      const quotationTabClassName = classnames(
                        "unities__sidebar-quotation-header-tab",
                        {
                          "unities__sidebar-quotation-header-tab--active":
                            tab.value === quotationTabActive
                        }
                      );

                      return (
                        <div
                          key={index}
                          className={quotationTabClassName}
                          onClick={() => this.onSelectQuotationTab(tab.value)}
                        >
                          {tab.text}
                        </div>
                      );
                    })}
                  </div>

                  {quotationTabActive === "normal" ? (
                    <div className="unities__sidebar-quotation-normal">
                      <Searcher
                        inputValue={customerSearchValue}
                        options={personsResults}
                        selected={selectedPerson}
                        onInputChange={this.onCustomersChangeInput}
                        onChange={this.onCustomerSelect}
                        onSearch={this.onCustomerSearch}
                        onCloseChip={this.cleanCustomer}
                        isClearable
                        isLoading={isSearching}
                        showChip={!!Object.keys(selectedPerson).length}
                        chipLabel={`${selectedPerson.first_name} ${selectedPerson.last_name}`}
                        withCustomOption
                        debounce
                      />

                      <NormalQuotationForm
                        initialValues={quotationInitialValues}
                        quotationDocument={quotationDocument}
                        onSaveNormalQuotation={this.onSaveNormalQuotation}
                        onNewQuotation={this.onNewQuotation}
                      />
                    </div>
                  ) : (
                    <div className="unities__sidebar-quotation-quick">
                      <QuickQuotationForm
                        initialValues={quotationInitialValues}
                        quotationDocument={quotationDocument}
                        onSaveQuickQuotation={this.onSaveQuickQuotation}
                        onNewQuotation={this.onNewQuotation}
                      />
                    </div>
                  )}
                </div>
              )}
            </div>
          </CSSTransition>

          <div className="unities__header v-align">
            <div className="unities__header-towers">
              <InputField
                value={selectedTower}
                type="select"
                isSearchable={false}
                options={towerOptions}
                onChange={this.onTowerChange}
                rounded
              />
            </div>

            <div className="unities__header-status-indicators v-align">
              <div className="unities__header-status-indicator">
                Separado Tentativo
              </div>

              <div className="unities__header-status-indicator">Separado</div>

              <div className="unities__header-status-indicator">Vendido</div>
            </div>

            <div
              className="send-unities-email v-align"
              onClick={this.handleSendEmail}
            >
              <span>... </span>

              <i className="mdi mdi-email-outline"></i>
              <a>Enviar unidades disponibles</a>
            </div>
          </div>

          <div className="unities__tables">
            {Object.keys(unitiesByTowerAndFloor).map((tower, index) => (
              <div className="unities__table" key={uuid()}>
                <h5 className="unities__table-tower">Torre {index + 1}</h5>

                <div className="unities__table-container">
                  <Table type="unities">
                    <TableHead>
                      <TableRow>
                        {unitiesNumber.map(n => (
                          <TableCell key={uuid()}>{n}</TableCell>
                        ))}
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {Object.keys(unitiesByTowerAndFloor[tower]).map(floor => {
                        return (
                          <TableRow key={uuid()}>
                            <TableCell>{floor}</TableCell>
                            {unitiesByTowerAndFloor[tower][floor].map(unity => {
                              const price = this.getShortValueFromMillions(
                                unity.price
                              );

                              const unityClassName = classnames(
                                "unities__table-unity",
                                {
                                  "unities__table-unity--sold":
                                    unity.status === "sold"
                                },
                                {
                                  "unities__table-unity--virtually-booked":
                                    unity.status === "virtually_booked"
                                },
                                {
                                  "unities__table-unity--booked":
                                    unity.status === "booked_not_formalized"
                                },
                                {
                                  "unities__table-unity--selected":
                                    unity.id === selectedUnity.id
                                }
                              );

                              return (
                                <TableCell key={unity.id}>
                                  <div
                                    className={unityClassName}
                                    onClick={() => this.onOpenSidebar(unity)}
                                  >
                                    {price}
                                  </div>
                                </TableCell>
                              );
                            })}
                          </TableRow>
                        );
                      })}
                    </TableBody>
                  </Table>
                </div>
              </div>
            ))}
          </div>
        </section>
      </WithLoading>
    );
  }
}

const mapStateToProps = ({ user }) => {
  console.log(user);
  return {
    user
  };
};

export default connect(mapStateToProps, {
  getUnities: unitiesActions.getUnities,
  getVirtualBooking: unitiesActions.getVirtualBooking,
  saveVirtualBooked: unitiesActions.saveVirtualBooked,
  saveNormalQuotation: unitiesActions.saveNormalQuotation,
  saveQuickQuotation: unitiesActions.saveQuickQuotation,
  updateUnity: unitiesActions.updateUnity,
  searchPerson: customersActions.searchPerson
})(Unities);
