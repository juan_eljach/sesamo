import React, { Component } from "react";
import { Field, reduxForm } from "redux-form";

import InputField from "core/components/InputField";
import Button from "core/components/Button";

class QuickQuotationForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isFetching: false
    };
  }

  static validate(fields) {
    const errors = {};
    const errorName = "Campo requerido";

    if (!fields.first_name) errors.first_name = errorName;
    if (!fields.last_name) errors.last_name = errorName;
    if (!fields.savings) errors.savings = errorName;
    if (!fields.email) errors.email = errorName;
    else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(fields.email)) {
      errors.email = "Email inválido";
    }

    return errors;
  }

  cleanValue = value => {
    return String(value).replace(/\./g, "");
  };

  onNewQuotation = () => {
    this.resetForm();
    this.props.onNewQuotation();
  };

  onSubmit = async values => {
    this.resetForm = this.props.reset;

    this.setState({ isFetching: true });

    const valuesCleaned = Object.keys(values).reduce((acc, current) => {
      if (current === "email") return { ...acc, [current]: values[current] };

      return { ...acc, [current]: this.cleanValue(values[current]) };
    }, {});

    this.props.onSaveQuickQuotation(valuesCleaned);

    this.setState({ isFetching: false });
  };

  render() {
    const { isFetching } = this.state;
    const {
      handleSubmit,
      quotationDocument,
      onFieldChange,
      onNewQuotation
    } = this.props;

    return (
      <form
        className="unities__sidebar-quotation-normal-form"
        onSubmit={handleSubmit(this.onSubmit)}
        ref={element => (this.form = element)}
      >
        <div className="unities__sidebar-quotation-form-container">
          <Field
            name="first_name"
            type="text"
            label="Nombres"
            component={InputField}
          />

          <Field
            name="last_name"
            type="text"
            label="Apellidos"
            component={InputField}
          />

          <Field
            name="email"
            type="text"
            label="Email"
            component={InputField}
          />

          <Field
            name="savings"
            type="text"
            label="Ahorros"
            component={InputField}
            withPriceIcon
            withPoints
          />

          <Field
            name="price"
            type="text"
            label="Precio"
            component={InputField}
            withPriceIcon
            withPoints
            disabled
          />

          <Field
            name="months_to_deliver"
            type="number"
            label="Meses para entrega"
            component={InputField}
            disabled
          />
        </div>

        {quotationDocument.id ? (
          <div className="unities__sidebar-quotation-document">
            <a
              href={`/download/quotation/${quotationDocument.id}`}
              target="_blank"
            >
              Ver cotización en pdf
            </a>

            <a onClick={this.onNewQuotation}>Realizar nueva cotización</a>
          </div>
        ) : (
          <div className="unities__sidebar-quotation-form-button">
            <Button isFetching={isFetching}>Cotizar</Button>
          </div>
        )}
      </form>
    );
  }
}

export default reduxForm({
  form: "QuickQuotationForm",
  validate: QuickQuotationForm.validate,
  keepDirtyOnReinitialize: true,
  enableReinitialize: true
})(QuickQuotationForm);
