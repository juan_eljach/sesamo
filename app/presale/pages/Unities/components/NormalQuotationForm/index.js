import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';

import InputField from 'core/components/InputField';
import Button from 'core/components/Button';

class NormalQuotationForm extends Component {
  constructor (props) {
    super(props);

    this.state = {
      isFetching: false
    };
  }

  static validate (fields) {
    const errors = {};
    const errorName = 'Campo requerido';

    if (!fields.subsidy) errors.subsidy = errorName;
    if (!fields.severance) errors.severance = errorName;
    if (!fields.savings) errors.savings = errorName;
    if (!fields.others) errors.others = errorName;
    if (!fields.monthly_income) errors.monthly_income = errorName;

    return errors;
  }

  cleanValue = (value) => {
    return String(value).replace(/\./g, '');
  }

  onNewQuotation = () => {
    this.resetForm();
    this.props.onNewQuotation();
  }

  onSubmit = async (values) => {
    this.resetForm = this.props.reset;

    this.setState({isFetching: true});

    const valuesCleaned = Object.keys(values).reduce((acc, current) => {
      return {...acc, [current]: this.cleanValue(values[current])};
    }, {});

    this.props.onSaveNormalQuotation(valuesCleaned);

    this.setState({isFetching: false});
  }

  render () {
    const { isFetching } = this.state;
    const { handleSubmit, quotationDocument, onFieldChange, onNewQuotation } = this.props;

    return (
      <form
        className="unities__sidebar-quotation-normal-form"
        onSubmit={handleSubmit(this.onSubmit)}
        ref={(element) => (this.form = element)}>

        <div className="unities__sidebar-quotation-form-container">
          <Field
            name="subsidy"
            type="text"
            label="Subsidios"
            component={InputField}
            withPriceIcon
            withPoints
          />

          <Field
            name="severance"
            type="text"
            label="Cesantías"
            component={InputField}
            withPriceIcon
            withPoints
          />

          <Field
            name="savings"
            type="text"
            label="Ahorros"
            component={InputField}
            withPriceIcon
            withPoints
          />

          <Field
            name="others"
            type="text"
            label="Otros"
            component={InputField}
            withPriceIcon
            withPoints
          />

          <Field
            name="monthly_income"
            type="text"
            label="Ingresos"
            component={InputField}
            withPriceIcon
            withPoints
          />

          <Field
            name="months_to_deliver"
            type="number"
            label="Meses para entrega"
            component={InputField}
            disabled
          />
        </div>

        {
          quotationDocument.id ? (
            <div className="unities__sidebar-quotation-document">
              <a href={`/download/quotation/${quotationDocument.id}`} target="_blank">Ver cotización en pdf</a>

              <a onClick={this.onNewQuotation}>Realizar nueva cotización</a>
            </div>
          ) : (
            <div className="unities__sidebar-quotation-form-button">
              <Button isFetching={isFetching}>Cotizar</Button>
            </div>
          )
        }
      </form>
    );
  }
}

export default reduxForm({
  form: 'NormalQuotationForm',
  validate: NormalQuotationForm.validate,
  keepDirtyOnReinitialize: true,
  enableReinitialize: true
})(NormalQuotationForm);
