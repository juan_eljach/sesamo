import React, { Component } from 'react';
import { connect } from 'react-redux';

import Checkbox from 'core/components/Checkbox';
import Loading from 'core/components/Loading';
import moment from 'moment';
import * as activitiesActions from 'core/redux/activities/operations';

class Home extends Component {
  constructor (props) {
    super(props);

    this.state = {
      activities: [],
      tabActive: 1,
      sectionLoaded: false
    };
  }

  async componentDidMount () {
    const activities = await this.props.getActivities();

    this.allActivities = activities.reverse().filter(activity => {
      const expired = this.verifyExpiration(activity.scheduled_date);

      return !activity.done;
    });

    this.setState({ activities: this.allActivities, sectionLoaded: true });
  }

  setTabActive (tab) {
    if (tab === 2) {
      const expiredActivities = this.state.activities.filter(
        (activity, index) => {
          const expired = this.verifyExpiration(activity.scheduled_date);

          return !expired && !activity.done;
        }
      );

      this.setState({ tabActive: tab, activities: expiredActivities });

      return;
    }

    this.setState({ tabActive: tab, activities: this.allActivities });
  }

  verifyExpiration = date => {
    return moment(date).isBefore(moment());
  };

  renderActivityStatus = activity => {
    const scheduledDay = moment(activity.scheduled_date).date();
    const today = moment().date();

    const expired = this.verifyExpiration(activity.scheduled_date);

    if (activity.done) {
      return (
        <div className="home__activity--done">
          Realizado{' '}
          {moment(activity.checked_as_done_timestamp)
            .locale('es')
            .fromNow()}
        </div>
      );
    }

    if (expired) {
      return (
        <div className="home__activity--expired">
          venció{' '}
          {moment(activity.scheduled_date)
            .locale('es')
            .fromNow()}
        </div>
      );
    }

    if (scheduledDay === today) {
      return (
        <div>
          Programado para hoy a las{' '}
          {moment(activity.scheduled_date)
            .locale('es')
            .format('h:mma')}
        </div>
      );
    }

    return (
      <div>
        Programado para el{' '}
        {moment(activity.scheduled_date)
          .locale('es')
          .format('DD MMM YYYY')}
      </div>
    );
  };

  /**
   * @description: Complete and update the activity
   * @param: {object} activity
   */
  completeActivity = async activity => {
    this.setState(prevState => {
      return {
        ...prevState,
        activities: prevState.activities.map(act => {
          if (act.id === activity.id) {
            return {
              ...act,
              done: true,
              checked_as_done_timestamp: moment()
            };
          }

          return act;
        })
      };
    });

    const payload = {
      ...activity,
      person: activity.person_id,
      done: true
    };

    this.props.updateActivity(payload, this.props.projectSlug);
  };

  render () {
    const { activities, tabActive, sectionLoaded } = this.state;

    if (!sectionLoaded) return <Loading />;

    const completed = activities.filter(act => act.done);

    return (
      <section className="home">
        <h1>{this.props.user.first_name}, ESTO ES LO QUE TIENES PARA HOY</h1>

        <div className="home__activities">
          <div className="home__activities-header v-align space-between">
            <div className="home__activities-info">
              <h2>ACTIVIDADES POR REALIZAR</h2>
              {/* <span>
                {completed.length} completadas de {activities.length}
              </span> */}
            </div>
            <div className="home__activities-icon">
              <i className="mdi mdi-calendar-check" />
            </div>
          </div>

          <div className="activities__tab">
            <div
              className={tabActive === 1 ? 'activities__tab--active' : ''}
              onClick={() => this.setTabActive(1)}
            >
              Todas
            </div>
            <div
              className={tabActive === 2 ? 'activities__tab--active' : ''}
              onClick={() => this.setTabActive(2)}
            >
              Por completar
            </div>
          </div>

          <div className="home__activities-list">
            {activities.map(activity => {
              return (
                <div className="home__activity v-align" key={activity.id}>
                  <Checkbox
                    checked={activity.done}
                    disabled={activity.done}
                    onChange={() => this.completeActivity(activity)}
                  />

                  <div>
                    <div className="home__activity-description">
                      {activity.description}
                    </div>
                    <div className="home__activity-client v-align space-between">
                      <div>
                        Cliente: <span>{activity.person}</span>
                      </div>
                      {this.renderActivityStatus(activity)}
                    </div>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </section>
    );
  }
}

const mapStateToProps = ({ user, activities }) => {
  return {
    user,
    projectSlug: user.projectSlug,
    activities
  };
};

export default connect(
  mapStateToProps,
  {
    getActivities: activitiesActions.getActivities,
    updateActivity: activitiesActions.updateActivity
  }
)(Home);
