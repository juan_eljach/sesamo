import React, { Component } from "react";
import { Switch, HashRouter, Route } from "react-router-dom";

// Pages
import Home from "./pages/Home";
import Customers from "./pages/Customers";
import Unities from "./pages/Unities";

class AppRouter extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <HashRouter basename="/">
        <section className="presale">
          {this.props.children}

          <section className="main">
            <Switch>
              <Route exact path="/" component={Home} />
              <Route exact path="/customers" component={Customers} />
              <Route exact path="/unities" component={Unities} />
            </Switch>
          </section>
        </section>
      </HashRouter>
    );
  }
}

export default AppRouter;
