import React, { Fragment } from "react";
import { render } from "react-dom";
import { Provider } from "react-redux";
import { MuiThemeProvider } from "@material-ui/core/styles";

import AppRouter from "./routes";
import NotificationPopup from "core/components/NotificationPopup";
import Header from "core/components/Header";
import Drawer from "core/components/Drawer";
import Navbar from "core/components/Navbar";
import muiTheme from "core/muiTheme";
import store from "./store";
import "core";
import { presaleMenuList } from "core/data/user";

const PresaleApp = (
  <MuiThemeProvider theme={muiTheme}>
    <Provider store={store}>
      <AppRouter>
        <Fragment>
          <NotificationPopup />
          <Header store={store} />
          <div className="hide-for-large">
            <Drawer menuItems={presaleMenuList} store={store} />
          </div>

          <div className="show-for-large">
            <Navbar menuItems={presaleMenuList} />
          </div>
        </Fragment>
      </AppRouter>
    </Provider>
  </MuiThemeProvider>
);

render(PresaleApp, document.querySelector("#app"));
