const path = require("path");
const webpack = require("webpack");
const webpackMerge = require("webpack-merge");
const baseConfig = require("./webpack.base");

const config = (env = {}) => {
  const plugins = [
    new webpack.DefinePlugin({
      "process.env.APP": JSON.stringify("presale")
    })
  ];

  return webpackMerge(baseConfig, {
    entry: {
      presale: "./presale/index.js"
    },
    output: {
      path: path.resolve(
        process.cwd(),
        env.production ? "../assets/app/" : "../static/app/"
      ),
      filename: "js/[name].js"
    },
    devtool: env.production ? "none" : "source-map",
    mode: env.production ? "production" : "development",
    plugins
  });
};

module.exports = config;
