import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { debounce as debounceFunc } from "throttle-debounce";
import classnames from "classnames";

import Searcher from "core/components/Searcher";
import Button from "core/components/Button";
import Tooltip from "core/components/Tooltip";
import Table, {
  TableHead,
  TableBody,
  TableRow,
  TableCell
} from "core/components/Table";
import Loading from "core/components/Loading";
import * as formalizationActions from "core/redux/formalization/operations";
import * as menuActions from "core/redux/menu/actions";

class PaymentPlans extends Component {
  constructor(props) {
    super(props);
    this.state = {
      paymentPlans: [],
      count: 0,
      searchValue: "",
      isSearching: false,
      sectionLoaded: false,
      currentPaymentPlan: {},
      actionsListOpen: false,
      actionsListCoor: {
        x: 0,
        y: 0
      }
    };

    this.searchPaymentPlans = debounceFunc(
      500,
      this.searchPaymentPlans.bind(this)
    );
  }

  async componentDidMount() {
    const response = await this.props.getPaymentPlans();

    this.setState({
      paymentPlans: response.results,
      isSearching: false,
      sectionLoaded: true,
      count: response.count
    });

    document.addEventListener("mousedown", this.handleClickOutside);
  }

  componentWillUnmount() {
    document.removeEventListener("mousedown", this.handleClickOutside);
  }

  handleClickOutside = event => {
    if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
      this.setState({ actionsListOpen: false });
    }
  };

  onInputChange = async value => {
    this.setState({ searchValue: value });

    this.searchPaymentPlans(value);
  };

  searchPaymentPlans = async value => {
    this.setState({ searchValue: value, isSearching: true });

    const response = await this.props.getPaymentPlans(value);

    this.setState({
      paymentPlans: response.results,
      count: response.count,
      isSearching: false
    });
  };

  onPageChange = async page => {
    const response = await this.props.getPaymentPlans("", page);

    this.setState({
      paymentPlans: response.results,
      count: response.count
    });
  };

  setWrapperRef = node => {
    this.wrapperRef = node;
  };

  handleList = (event, paymentPlan, index) => {
    const { clientY, clientX } = event;

    const x = index > 12 ? clientX - 220 : clientX - 200;
    const y = index > 12 ? clientY - 150 : clientY + 10;

    this.setState(prevState => {
      return {
        ...prevState,
        currentPaymentPlan: paymentPlan,
        actionsListOpen: true,
        actionsListCoor: {
          x,
          y
        }
      };
    });
  };

  render() {
    const {
      sectionLoaded,
      paymentPlans,
      count,
      searchValue,
      isSearching,
      currentPaymentPlan,
      actionsListCoor,
      actionsListOpen
    } = this.state;

    if (!sectionLoaded) {
      return <Loading />;
    }

    return (
      <section className="payment-plans wrap-container">
        <div className="payment-plans__header v-align">
          <Searcher
            inputValue={searchValue}
            onInputChange={this.onInputChange}
            menuIsOpen={false}
            isSearchable={true}
            isLoading={isSearching}
            placeholder="Buscar por persona o apto"
          />

          <Link to="/payment-plan/create">
            <Button>NUEVO PLAN DE PAGO</Button>
          </Link>
        </div>

        {!paymentPlans.length && sectionLoaded ? (
          <div className="no-results">No se encontraron resultados</div>
        ) : (
          <Table
            type="data-grid"
            totalItems={count}
            onPageChange={this.onPageChange}
          >
            <TableHead>
              <TableRow>
                <TableCell>Cliente</TableCell>
                <TableCell>Apartamento</TableCell>
                <TableCell>Celular</TableCell>
                <TableCell>Teléfono de notificación</TableCell>
                <TableCell>Fecha de creación</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {paymentPlans.map((paymentPlan, index) => {
                const person = paymentPlan.persons[0] || {};
                const personName = `${person.first_name} ${person.last_name}`;
                return (
                  <TableRow key={paymentPlan.id}>
                    <TableCell>{personName}</TableCell>
                    <TableCell>{paymentPlan.unity}</TableCell>
                    <TableCell>{person.cellphone}</TableCell>
                    <TableCell>{person.notification_phone}</TableCell>
                    <TableCell>{paymentPlan.timestamp}</TableCell>
                    {/* <TableCell>
                      <Link to={`/payments/${paymentPlan.id}`}>
                        Ver negocio
                      </Link>
                    </TableCell>
                    <TableCell>
                      <Link to={`/payment-plan/${paymentPlan.id}`}>
                        Ver Plan
                      </Link>
                    </TableCell> */}
                    <TableCell>
                      <Tooltip title="Ver acciones">
                        <i
                          className="mdi mdi-dots-vertical"
                          onClick={event =>
                            this.handleList(event, paymentPlan, index)
                          }
                        ></i>
                      </Tooltip>
                    </TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        )}

        {actionsListOpen && (
          <div
            ref={this.setWrapperRef}
            className="actions-list"
            style={{ top: actionsListCoor.y, left: actionsListCoor.x }}
          >
            <ul>
              <li>
                <a
                  onClick={() => {
                    this.props.setMenu("payments");
                    this.props.history.push(
                      `/payments/${currentPaymentPlan.id}`
                    );
                  }}
                >
                  Ver negocio
                </a>
              </li>
              <li>
                <Link to={`/payment-plan/${currentPaymentPlan.id}`}>
                  Ver Plan
                </Link>
              </li>

              <li>
                <a
                  href={`/api/projects/${this.props.projectSlug}/formalizations/payment-agreements/${currentPaymentPlan.id}/download/?type=acuerdo`}
                  target="_blank"
                >
                  Descargar Plan de pago
                </a>
              </li>

              <li>
                <a
                  href={`/api/projects/${this.props.projectSlug}/formalizations/payment-agreements/${currentPaymentPlan.id}/download/?type=pagos`}
                  target="_blank"
                >
                  Descargar Todos los pagos
                </a>
              </li>

              <li>
                <a
                  href={`/api/projects/${this.props.projectSlug}/formalizations/payment-agreements/${currentPaymentPlan.id}/download/?type=cuotas`}
                  target="_blank"
                >
                  Descargar Tabla de cuotas
                </a>
              </li>
            </ul>
          </div>
        )}
      </section>
    );
  }
}

const mapStateToProps = ({ project }) => {
  return { projectSlug: project.slug };
};

export default connect(mapStateToProps, {
  getPaymentPlans: formalizationActions.getPaymentPlans,
  setMenu: menuActions.setMenu
})(PaymentPlans);
