import React, { Component } from "react";
import { connect } from "react-redux";
import { debounce as debounceFunc } from "throttle-debounce";
import moment from "moment";
import classnames from "classnames";

import Searcher from "core/components/Searcher";
import SearchPersonForm from "./SearchPersonForm";
import getChipInitials from "core/helpers/getChipInitials";

class AddClient extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: "small-update",
      customerSearchValue: "",
      isSearching: false,
      foundCustomers: [],
      selectedCustomer: {}
    };

    this.civilStatus = {
      soltero: "Soltero",
      casado: "Casado",
      viudo: "Viudo",
      "union-libre": "Union Libre"
    };

    this.searchCustomer = debounceFunc(500, this.searchCustomer.bind(this));
  }

  onSelectTab = tab => {
    if (tab === "create") {
      this.removeCustomer();
    }

    this.setState({ activeTab: tab });
  };

  onChangeInput = value => {
    this.setState({ customerSearchValue: value });

    this.searchCustomer(value);
  };

  searchCustomer = async value => {
    this.setState({ isSearching: true });

    const response = await this.props.searchCustomers(value);

    const customers = response.filter(customer => {
      const found = this.props.addedClients.find(c => c.id === customer.id);

      return !found;
    });

    this.setState({
      foundCustomers: customers.map(customer => ({
        ...customer,
        value: customer.id,
        label: `${customer.first_name} ${customer.last_name}`
      })),
      isSearching: false
    });
  };

  selectCustomer = customer => {
    this.setState({
      selectedCustomer: {
        ...customer,
        birthday: customer.birthday ? moment(customer.birthday) : null,
        civil_status: customer.civil_status
          ? {
              value: customer.civil_status,
              label: this.civilStatus[customer.civil_status]
            }
          : null
      }
    });
  };

  removeCustomer = () => {
    this.setState({ selectedCustomer: {}, customerSearchValue: "" });
  };

  saveCustomer = async values => {
    const { activeTab } = this.state;
    const {
      updateFormalizationPerson,
      saveFormalizationPerson,
      showNotificationPopup,
      onCloseCustomerModal,
      onAddClient
    } = this.props;

    let payload = {
      first_name: values.first_name,
      last_name: values.last_name,
      identity: values.identity,
      expedition_place: values.expedition_place,
      email: values.email,
      birthday: values.birthday.format("YYYY-MM-DD"),
      cellphone: values.cellphone,
      phone: values.phone,
      civil_status: values.civil_status.value,
      notification_phone: values.notification_phone,
      notification_address: values.notification_address,
      assigned_to: values.assigned_to,
      adquisition_channel: "office",
      slug: values.slug,
      project: this.props.projectId,
      is_client: true
    };

    let response;

    if (activeTab === "small-update") {
      response = await updateFormalizationPerson(payload);
    } else {
      response = await saveFormalizationPerson(payload);
    }

    onAddClient(response.data);

    onCloseCustomerModal();

    showNotificationPopup({
      message: "El cliente se ha guardado correctamente!"
    });
  };

  render() {
    const {
      customerSearchValue,
      isSearching,
      foundCustomers,
      selectedCustomer,
      activeTab
    } = this.state;
    const { onCloseCustomerModal } = this.props;

    const editCustomerTabClassName = classnames("add-customer__tab", {
      "add-customer__tab--active": activeTab === "small-update"
    });
    const addCustomerTabClassName = classnames("add-customer__tab", {
      "add-customer__tab--active": activeTab === "create"
    });

    const chipInitials = getChipInitials(selectedCustomer.label);

    return (
      <div className="add-customer">
        <h2>AGREGAR CLIENTE</h2>

        <i
          className="mdi mdi-close action-hover"
          onClick={onCloseCustomerModal}
        ></i>

        <div className="add-customer__tabs v-align">
          <div
            className={editCustomerTabClassName}
            onClick={() => this.onSelectTab("small-update")}
          >
            Busca una persona existente
          </div>

          <div
            className={addCustomerTabClassName}
            onClick={() => this.onSelectTab("create")}
          >
            Agrega una persona nueva
          </div>
        </div>

        {activeTab === "small-update" && (
          <div className="add-customer__form-searcher">
            <Searcher
              inputValue={customerSearchValue}
              options={foundCustomers}
              isLoading={isSearching}
              onInputChange={this.onChangeInput}
              onChange={this.selectCustomer}
              onCloseChip={this.removeCustomer}
              showChip={!!selectedCustomer.label}
              chipInitials={chipInitials}
              chipLabel={`${selectedCustomer.label}`}
            />
          </div>
        )}

        <div className="add-customer__current-form">
          {activeTab === "small-update" && !selectedCustomer.id ? null : (
            <SearchPersonForm
              activeTab={activeTab}
              initialValues={selectedCustomer}
              selectedCustomer={selectedCustomer}
              saveCustomer={this.saveCustomer}
            />
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ project }) => {
  return {
    projectId: project.id
  };
};

export default connect(mapStateToProps)(AddClient);
