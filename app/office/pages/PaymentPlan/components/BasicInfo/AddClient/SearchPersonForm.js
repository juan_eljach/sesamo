import React, { Component } from "react";
import { Field, reduxForm } from "redux-form";
import PropTypes from "prop-types";

import InputField from "core/components/InputField";
import Button from "core/components/Button";

class SearchPersonForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isFetching: false
    };
  }

  static validate(fields) {
    const errors = {};
    const errorName = "Campo requerido";

    if (!fields.first_name) errors.first_name = errorName;
    if (!fields.last_name) errors.last_name = errorName;
    if (!fields.identity) errors.identity = errorName;
    if (!fields.expedition_place) errors.expedition_place = errorName;
    if (!fields.email) errors.email = errorName;
    else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(fields.email)) {
      errors.email = "Email inválido";
    }
    if (!fields.birthday) errors.birthday = errorName;
    if (!fields.cellphone) errors.cellphone = errorName;
    if (!fields.phone) errors.phone = errorName;
    if (!fields.notification_phone) errors.notification_phone = errorName;
    if (!fields.notification_address) errors.notification_address = errorName;
    if (!fields.civil_status) errors.civil_status = errorName;

    return errors;
  }

  onSubmit = async values => {
    try {
      this.setState({ isFetching: true });

      await this.props.saveCustomer(values);

      this.setState({ isFetching: false });
    } catch (error) {
      // error
    }
  };

  render() {
    const { isFetching } = this.state;
    const { handleSubmit, activeTab } = this.props;

    return (
      <form
        className="add-customer__form"
        onSubmit={handleSubmit(this.onSubmit)}
      >
        <div className="add-customer__form-container">
          <Field
            name="first_name"
            type="text"
            label="Nombres"
            placeholder="Escribir nombres"
            component={InputField}
          />
          <Field
            name="last_name"
            type="text"
            label="Apellidos"
            placeholder="Escribir apellidos"
            component={InputField}
          />
          <Field
            name="identity"
            type="number"
            label="Cédula"
            placeholder="Escribir cédula"
            component={InputField}
          />
          <Field
            name="expedition_place"
            type="text"
            label="Lugar de expedición"
            placeholder="Escribir lugar de expedición"
            component={InputField}
          />
          <Field
            name="email"
            type="text"
            label="Correo Electrónico"
            placeholder="Escribir correo electrónico"
            component={InputField}
          />
          <Field
            value={this.props.initialValues.birthday}
            name="birthday"
            type="date"
            label="Fecha de Nacimiento"
            placeholder="Seleccionar fecha de nacimiento"
            component={InputField}
            disableYounger
          />
          <Field
            name="cellphone"
            type="number"
            label="Celular"
            placeholder="Escribir celular"
            component={InputField}
          />
          <Field
            name="phone"
            type="number"
            label="Teléfono Fijo"
            placeholder="Escribir teléfono fijo"
            component={InputField}
          />
          <Field
            name="notification_phone"
            type="number"
            label="Teléfono de notificación"
            placeholder="Escribir teléfono de notificación"
            component={InputField}
          />
          <Field
            name="notification_address"
            type="text"
            label="Dirección de notificación"
            placeholder="Escribir dirección de notificación"
            component={InputField}
          />

          <Field
            name="civil_status"
            type="select"
            options={[
              { value: "soltero", label: "Soltero" },
              { value: "casado", label: "Casado" },
              { value: "viudo", label: "Viudo" },
              { value: "union-libre", label: "Unión Libre" }
            ]}
            label="Estado civil"
            placeholder="Seleccionar estado civil"
            component={InputField}
          />
        </div>

        <div className="add-customer__form-button">
          <Button isFetching={isFetching}>GUARDAR</Button>
        </div>
      </form>
    );
  }
}

SearchPersonForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  availableUnities: PropTypes.arrayOf(
    PropTypes.shape({ value: PropTypes.number, label: PropTypes.text })
  )
};

export default reduxForm({
  form: "SearchPersonForm",
  validate: SearchPersonForm.validate,
  keepDirtyOnReinitialize: true,
  enableReinitialize: true
})(SearchPersonForm);
