import React, { Component } from "react";
import { debounce as debounceFunc } from "throttle-debounce";

import InputField from "core/components/InputField";
import AddClient from "./AddClient";
import Searcher from "core/components/Searcher";
import Modal from "core/components/Modal";
import Chip from "core/components/Chip";

class BasicInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      unitySearchValue: "",
      unitiesFound: [],
      isSearching: false,
      customerModalOpen: false,
      teamList: [],
      otherSellerActive: false
    };

    this.searchUnities = debounceFunc(500, this.searchUnities.bind(this));
  }

  onInputChange = value => {
    this.setState({ unitySearchValue: value });

    this.searchUnities(value);
  };

  searchUnities = async value => {
    this.setState({ isSearching: true });

    const response = await this.props.searchUnities(
      value,
      "&paymentagreement__isnull=true"
    );

    this.setState({
      unitiesFound: response
        .slice(0, 10)
        .map(unity => ({ ...unity, value: unity.id, label: unity.name })),
      isSearching: false
    });
  };

  onRemoveUnity = () => {
    if (!this.props.isCreatePaymentPlan) return;

    this.setState({ unitySearchValue: "", unitiesFound: [] });

    this.props.removeUnity();
  };

  onOpenAddClient = () => {
    this.setState({ customerModalOpen: true });
  };

  onCloseCustomerModal = () => {
    this.setState({ customerModalOpen: false });
  };

  render() {
    const addedClientsLength = !!this.props.addedClients.length;

    const {
      isSearching,
      unitySearchValue,
      unitiesFound,
      customerModalOpen
    } = this.state;

    const {
      searchCustomers,
      updateFormalizationPerson,
      saveFormalizationPerson,
      selectUnity,
      selectedUnity,
      addedClients,
      isCreatePaymentPlan,
      onAddClient,
      onRemoveClient,
      showNotificationPopup
    } = this.props;

    return (
      <div className="basic-info payment-plan-section">
        <h3>INFORMACIÓN BÁSICA</h3>

        <div className="basic-info__customer">
          <div className="basic-info__customer-search">
            <label>Unidad</label>
            <Searcher
              inputValue={unitySearchValue}
              options={unitiesFound}
              onInputChange={this.onInputChange}
              onChange={selectUnity}
              onCloseChip={this.onRemoveUnity}
              isLoading={isSearching}
              placeholder="Ej: T2-201"
              showChip={!!selectedUnity.label}
              chipLabel={selectedUnity.label}
              isDisabled={!isCreatePaymentPlan}
            />
          </div>

          <div className="basic-info__unity-customers">
            <label>Clientes de la Unidad</label>
            <div className="basic-info__unity-customers-container v-align">
              {isCreatePaymentPlan && (
                <a onClick={this.onOpenAddClient}>Agregar nuevo cliente +</a>
              )}
              <div className="basic-info__unity-customers-list v-align">
                {addedClients.map((client, index) => (
                  <Chip
                    key={client.id}
                    initials={`${index + 1}`}
                    label={`${client.first_name} ${client.last_name}`}
                    onCloseChip={() => {
                      if (!isCreatePaymentPlan) return;

                      onRemoveClient(client);
                    }}
                  />
                ))}
              </div>
            </div>
          </div>
        </div>

        <div className="other-data">
          {addedClientsLength && (
            <InputField
              name="seller"
              value={this.props.seller}
              type="select"
              label="Vendedor"
              placeholder="Seleccionar vendedor"
              options={this.props.teamList}
              disabled={!isCreatePaymentPlan}
              onChange={value => {
                this.props.updateBasicInfo("seller", value);
              }}
            />
          )}

          {this.props.seller.value === "other" && addedClientsLength && (
            <InputField
              value={this.props.otherSeller}
              type="text"
              label="Nombre"
              placeholder="Ingresar nombre del vendedor"
              onChange={event =>
                this.props.updateBasicInfo("otherSeller", event.target.value)
              }
              disabled={!isCreatePaymentPlan}
            />
          )}

          {addedClientsLength && (
            <InputField
              value={this.props.referralInfo || ""}
              name="referral_info"
              type="textarea"
              label="Referido"
              placeholder="Escribir información de referido"
              onChange={event =>
                this.props.updateBasicInfo("referral_info", event.target.value)
              }
              disabled={!isCreatePaymentPlan}
            />
          )}
        </div>

        <Modal open={customerModalOpen}>
          <AddClient
            searchCustomers={searchCustomers}
            updateFormalizationPerson={updateFormalizationPerson}
            saveFormalizationPerson={saveFormalizationPerson}
            showNotificationPopup={showNotificationPopup}
            onCloseCustomerModal={this.onCloseCustomerModal}
            addedClients={addedClients}
            onAddClient={onAddClient}
          />
        </Modal>
      </div>
    );
  }
}

export default BasicInfo;
