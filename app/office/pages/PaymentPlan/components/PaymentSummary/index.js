import React, { Component } from "react";

import InputField from "core/components/InputField";
import Checkbox from "core/components/Checkbox";
import formatValue from "core/helpers/formatValue";

class PaymentSummary extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const {
      selectedUnity,
      applyToInitialFee,
      campaigns,
      paymentSummary,
      otherPayments,
      isCreatePaymentPlan,
      onCampaignChange,
      onChangeSummaryField,
      onChangeApplyOtherPayment
    } = this.props;
    const {} = this.state;

    return (
      <div className="payment-summary payment-plan-section">
        <h3>RESUMEN DE PAGO</h3>

        <div className="payment-summary__summary">
          {isCreatePaymentPlan && (
            <InputField
              type="select"
              selectPlaceholder="Selecciona una campaña"
              options={campaigns}
              onChange={onCampaignChange}
              disabled={!selectedUnity.id}
            />
          )}

          <div className="payment-summary__summary-inputs">
            <InputField
              value={formatValue(paymentSummary.unityValue)}
              label="Valor del apto"
              type="number"
              withPriceIcon
              disabled
            />

            <InputField
              value={formatValue(paymentSummary.initialFee)}
              label="Cuota inicial"
              type="number"
              withPriceIcon
              disabled
            />

            {isCreatePaymentPlan && (
              <InputField
                value={formatValue(paymentSummary.initialFeePercentage)}
                label="% de cuota inicial"
                type="number"
                withPriceIcon
                disabled
              />
            )}

            <InputField
              value={formatValue(paymentSummary.bookingPrice)}
              label="Separación"
              type="number"
              withPriceIcon
              disabled
            />

            <InputField
              value={formatValue(paymentSummary.creditToApply)}
              label="Crédito a solicitar"
              type="number"
              withPriceIcon
              disabled
            />
          </div>
        </div>

        <h3>OTROS PAGOS ACORDADOS</h3>

        {isCreatePaymentPlan && (
          <div className="agreements-legend">
            Marca el <Checkbox label="" style="square" disabled /> para aplicar
            a cuota inicial
          </div>
        )}

        <div className="payment-summary__others">
          <div className="payment-summary__others-item">
            <label>Ahorro programado</label>
            <div className="payment-summary__others-item-field v-align">
              <Checkbox
                style="square"
                checked={applyToInitialFee.includes("scheduledSavings")}
                onChange={event =>
                  onChangeApplyOtherPayment(
                    "scheduledSavings",
                    event.target.value
                  )
                }
                disabled={!isCreatePaymentPlan}
              />
              <InputField
                type="number"
                value={otherPayments.scheduledSavings}
                onChange={event =>
                  onChangeSummaryField(
                    "otherPayments",
                    "scheduledSavings",
                    event.target.value
                  )
                }
                disabled={!isCreatePaymentPlan}
                withPriceIcon
              />
            </div>
          </div>

          <div className="payment-summary__others-item">
            <label>Cesantías</label>
            <div className="payment-summary__others-item-field v-align">
              <Checkbox
                style="square"
                checked={applyToInitialFee.includes("severance")}
                onChange={event =>
                  onChangeApplyOtherPayment("severance", event.target.value)
                }
                disabled={!isCreatePaymentPlan}
              />
              <InputField
                type="number"
                value={otherPayments.severance}
                onChange={event =>
                  onChangeSummaryField(
                    "otherPayments",
                    "severance",
                    event.target.value
                  )
                }
                disabled={!isCreatePaymentPlan}
                withPriceIcon
              />
            </div>
          </div>

          <div className="payment-summary__others-item">
            <label>Subsidio de vivienda</label>
            <div className="payment-summary__others-item-field v-align">
              <Checkbox
                style="square"
                checked={applyToInitialFee.includes("housingAllowance")}
                onChange={event =>
                  onChangeApplyOtherPayment(
                    "housingAllowance",
                    event.target.value
                  )
                }
                disabled={!isCreatePaymentPlan}
              />
              <InputField
                type="number"
                value={otherPayments.housingAllowance}
                onChange={event =>
                  onChangeSummaryField(
                    "otherPayments",
                    "housingAllowance",
                    event.target.value
                  )
                }
                disabled={!isCreatePaymentPlan}
                withPriceIcon
              />
            </div>
          </div>

          <div className="payment-summary__others-item">
            <label>Otros</label>
            <div className="payment-summary__others-item-field v-align">
              <Checkbox
                style="square"
                checked={applyToInitialFee.includes("others")}
                onChange={event =>
                  onChangeApplyOtherPayment("others", event.target.value)
                }
                disabled={!isCreatePaymentPlan}
              />
              <InputField
                type="number"
                value={otherPayments.others}
                onChange={event =>
                  onChangeSummaryField(
                    "otherPayments",
                    "others",
                    event.target.value
                  )
                }
                disabled={!isCreatePaymentPlan}
                withPriceIcon
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default PaymentSummary;
