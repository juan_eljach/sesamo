import React, { Component } from 'react';

import Button from 'core/components/Button';

class CommentInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: ''
    };
  }

  onValueChange = (event) => {
    this.setState({value: event.target.value});
  }

  render() {
    const { onSave, onCloseComment } = this.props;

    return (
      <div className="payment-fees__comments-input">
        <i className="mdi mdi-close action-hover" onClick={onCloseComment}></i>
        <textarea name="" rows="3" placeholder="Escribir comentario" onChange={this.onValueChange}></textarea>
        <Button onClick={() => onSave(this.state.value)}>Guardar</Button>
      </div>
    );
  }
}

export default CommentInput;
