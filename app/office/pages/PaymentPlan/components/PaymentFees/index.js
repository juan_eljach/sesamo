import React, { Component, Fragment } from "react";
import classnames from "classnames";

import InputField from "core/components/InputField";
import Button from "core/components/Button";
import Comment from "core/components/Comment";
import formatValue from "core/helpers/formatValue";
import CommentInput from "./CommentInput";
import DatePicker from "core/components/DatePicker";
import Table, {
  TableHead,
  TableBody,
  TableRow,
  TableCell
} from "core/components/Table";

class PaymentFees extends Component {
  constructor(props) {
    super(props);
    this.state = {
      commentOpen: "",
      commentPopupOpen: ""
    };
  }

  onOpenComment = (type, value) => {
    this.setState({ [type]: value });
  };

  onCloseComment = (type, event) => {
    event.stopPropagation();

    this.setState({ [type]: null });
  };

  renderComment = () => {
    <div className="payment-fees__comments">
      <div className="payment-fees__comments-list" />
    </div>;
  };

  getFixedFees = campaign => {
    var foo = [];

    for (var i = 1; i <= campaign.number_of_paymentfees; i++) {
      foo.push({ value: i, label: i });
    }

    return foo;
  };

  render() {
    const { commentOpen, commentPopupOpen } = this.state;
    const {
      user,
      paymentFees,
      remainingMonths,
      remainingInitialFee,
      onSelectFirstDate,
      onChangeExtraordinaryFee,
      extraordinayInitialFee,
      NPV,
      NPV2,
      isCreatePaymentPlan,
      onSaveComment
    } = this.props;

    const feeValue = isCreatePaymentPlan
      ? paymentFees[1] && paymentFees[1].date === null
        ? 0
        : Number.parseInt(
            (remainingInitialFee - extraordinayInitialFee) / remainingMonths
          )
      : paymentFees[1].fee;

    return (
      <div className="payment-fees payment-plan-section">
        {true && (
          <Fragment>
            <h3>PLAN DE PAGO</h3>

            <h4>CUOTA INICIAL RESTANTE: {formatValue(remainingInitialFee)}</h4>
            <h4>
              CUOTA INICIAL RESTANTE EXTRAORDINARIA:{" "}
              {formatValue(remainingInitialFee - extraordinayInitialFee)}
            </h4>
            <h4>NPV: {formatValue(NPV)}</h4>
            <h4>NPV2: {formatValue(NPV2)}</h4>

            <h4>Número de cuotas: {Object.keys(paymentFees).length}</h4>
          </Fragment>
        )}

        {this.props.currentCampaign &&
          this.props.currentCampaign.campaign_type === "FIJA" && (
            <Fragment>
              <h4>Número de cuotas:</h4>

              <div className="select-fees">
                <InputField
                  defaultValue={{
                    value: this.props.currentCampaign.number_of_paymentfees,
                    label: this.props.currentCampaign.number_of_paymentfees
                  }}
                  type="select"
                  selectPlaceholder="Selecciona la cantidad de cuotas"
                  options={this.getFixedFees(this.props.currentCampaign)}
                  onChange={value =>
                    this.props.updateRemainingMonths(value.value)
                  }
                />
              </div>
            </Fragment>
          )}

        <Table>
          <TableHead>
            <TableRow>
              <TableCell>No. Cuota</TableCell>
              <TableCell>Fecha</TableCell>
              <TableCell>Valor de la cuota</TableCell>
              <TableCell>Cuota extraordinaria</TableCell>
              <TableCell>Comentarios</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow>
              <TableCell>1</TableCell>
              <TableCell>
                <div className="input-field date-input">
                  <DatePicker
                    value={paymentFees[1].date}
                    defaultValue={paymentFees[1].date}
                    onChange={onSelectFirstDate}
                    disabled={!isCreatePaymentPlan}
                  />
                </div>

                {/* <InputField
									type="date"
									value={paymentFees[1].date}
									onChange={onSelectFirstDate}
									disabled={!isCreatePaymentPlan}
									disabledPastDate
								/> */}
              </TableCell>
              <TableCell>
                <InputField
                  type="number"
                  value={feeValue}
                  disabled
                  withPriceIcon
                />
              </TableCell>
              <TableCell>
                <InputField
                  type="number"
                  value={paymentFees[1].extraordinaryFeeValue}
                  onChange={event =>
                    onChangeExtraordinaryFee(1, event.target.value)
                  }
                  disabled={!isCreatePaymentPlan}
                  withPriceIcon
                />
              </TableCell>
              <TableCell>
                {!paymentFees[1].comment ? (
                  isCreatePaymentPlan ? (
                    <div
                      className="payment-fees__comment"
                      style={{ position: "relative" }}
                      onClick={() =>
                        this.onOpenComment("commentOpen", "initial")
                      }
                    >
                      Comentar
                      {commentOpen === "initial" && (
                        <CommentInput
                          onSave={value => onSaveComment(1, value)}
                          onCloseComment={event =>
                            this.onCloseComment("commentOpen", event)
                          }
                        />
                      )}
                    </div>
                  ) : (
                    "-"
                  )
                ) : (
                  <Comment
                    label="Comentario"
                    user={`${user.first_name} ${user.last_name}`}
                    comment={paymentFees[1].comment}
                    popupOpen={commentPopupOpen === 1}
                    onOpen={() => this.onOpenComment("commentPopupOpen", 1)}
                    onClose={event =>
                      this.onCloseComment("commentPopupOpen", event)
                    }
                  />
                )}
              </TableCell>
            </TableRow>

            {Object.keys(paymentFees)
              .slice(1)
              .map(feeNumber => {
                const paymentFee = paymentFees[feeNumber];

                return (
                  <TableRow key={feeNumber}>
                    <TableCell>{feeNumber}</TableCell>
                    <TableCell>
                      <InputField
                        value={paymentFee.date}
                        type="date"
                        disabled
                      />
                    </TableCell>
                    <TableCell>
                      <InputField
                        type="number"
                        value={feeValue}
                        disabled
                        withPriceIcon
                      />
                    </TableCell>
                    <TableCell>
                      <InputField
                        type="number"
                        value={paymentFee.extraordinaryFeeValue}
                        onChange={event =>
                          onChangeExtraordinaryFee(
                            feeNumber,
                            event.target.value
                          )
                        }
                        disabled={!isCreatePaymentPlan}
                        withPriceIcon
                      />
                    </TableCell>
                    <TableCell>
                      {!paymentFees[feeNumber].comment ? (
                        isCreatePaymentPlan ? (
                          <div
                            className="payment-fees__comment"
                            style={{ position: "relative" }}
                            onClick={() =>
                              this.onOpenComment("commentOpen", feeNumber)
                            }
                          >
                            Comentar
                            {commentOpen === feeNumber && (
                              <CommentInput
                                onSave={value =>
                                  onSaveComment(feeNumber, value)
                                }
                                onCloseComment={event =>
                                  this.onCloseComment("commentOpen", event)
                                }
                              />
                            )}
                          </div>
                        ) : (
                          <div>-</div>
                        )
                      ) : (
                        <Comment
                          label="Comentario"
                          user={`${user.first_name} ${user.last_name}`}
                          comment={paymentFees[feeNumber].comment}
                          popupOpen={commentPopupOpen === feeNumber}
                          onOpen={() =>
                            this.onOpenComment("commentPopupOpen", feeNumber)
                          }
                          onClose={event =>
                            this.onCloseComment("commentPopupOpen", event)
                          }
                        />
                      )}
                    </TableCell>
                  </TableRow>
                );
              })}
          </TableBody>
        </Table>
      </div>
    );
  }
}

export default PaymentFees;
