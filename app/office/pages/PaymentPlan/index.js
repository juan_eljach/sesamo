import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { debounce as debounceFunc } from "throttle-debounce";
import moment from "moment";
import Finance from "financejs";

import BasicInfo from "./components/BasicInfo";
import PaymentSummary from "./components/PaymentSummary";
import PaymentFees from "./components/PaymentFees";
import Modal from "core/components/Modal";
import Button from "core/components/Button";
import Loading from "core/components/Loading";
import * as unitiesActions from "core/redux/unities/operations";
import * as customersActions from "core/redux/customers/operations";
import * as projectActions from "core/redux/project/operations";
import * as formalizationActions from "core/redux/formalization/operations";
import * as notificationPopupActions from "core/redux/notificationPopup/actions";
import * as paymentsActions from "core/redux/payments/operations";

class PaymentPlan extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedUnity: {},
      addedClients: [],
      remainingInitialFee: 0,
      extraordinayInitialFee: 0,
      campaigns: [],
      seller: "",
      referral_info: null,
      teamList: [],
      defaultSeller: null,
      otherSeller: null,
      selectedCampaign: {},
      remainingMonths: 0,
      paymentSummary: {
        unityValue: 0,
        initialFee: 0,
        initialFeePercentage: 0,
        bookingPrice: 0,
        creditToApply: 0
      },
      otherPayments: {
        scheduledSavings: 0,
        severance: 0,
        housingAllowance: 0,
        others: 0
      },
      applyToInitialFee: [],
      paymentFees: {
        1: { date: null, feeValue: 0, extraordinaryFeeValue: 0, comment: "" }
      },
      currentCampaign: null,
      NPV: 0,
      NPV2: 0,
      validationPopupOpen: false,
      validPaymentPlan: false,
      modification: {
        is_new_version: false,
        modification_date: null
      },
      isFetching: false,
      sectionLoaded: false
    };

    this.onChangeExtraordinaryFee = debounceFunc(
      500,
      this.onChangeExtraordinaryFee.bind(this)
    );
    this.interest = 1.5;
    this.finance = new Finance();
    this.action = this.props.match.params.action;
    this.isCreatePaymentPlan = this.action === "create";
  }

  componentDidMount = async () => {
    const campaignsResponse = await this.props.getCampaigns();

    await this.props.getProjectTeam();

    let teamList = [{ value: "other", label: "Otro" }];

    Object.keys(this.props.team).forEach(el => {
      const data = this.props.team[el];

      teamList.unshift({
        value: data.id,
        label: `${data.user.first_name} ${data.user.last_name}`
      });
    });

    if (this.isCreatePaymentPlan) {
      this.setState({
        campaigns: [
          { value: "", label: "Selecciona una campaña" },
          ...campaignsResponse.data.map(c => ({
            ...c,
            value: c.slug,
            label: c.name
          }))
        ],
        sectionLoaded: true,
        teamList
      });

      return;
    }

    const otherPaymentsMap = {
      "ahorro-programado": "scheduledSavings",
      cesantias: "severance",
      "subsidio-vivienda": "housingAllowance",
      "otro-pago-acordado": "others"
    };

    const unitiesResponse = await this.props.searchUnities("");

    const paymentAgreement = await this.props.getPaymentAgreementsDetail(
      this.action
    );

    const unity = unitiesResponse.find(u => u.name === paymentAgreement.unity);

    let modification = {
      is_new_version: paymentAgreement.is_new_version,
      modification_date: paymentAgreement.modification_date,
      old_version_copy: paymentAgreement.old_version_copy
    };

    let applyToInitialFee = [];

    const otherPayments = Object.keys(paymentAgreement.agreed_payments).reduce(
      (acc, current) => {
        const currentPayment = paymentAgreement.agreed_payments[current];
        const key = otherPaymentsMap[currentPayment.type_of_payment];

        if (currentPayment.applied_to_initialfee) {
          applyToInitialFee.push(key);
        }

        return { ...acc, [key]: currentPayment.fee };
      },
      {}
    );

    const paymentFees = paymentAgreement.payment_fees
      .sort((a, b) => {
        return new Date(a.date_to_pay) - new Date(b.date_to_pay);
      })
      .reduce((acc, current, index) => {
        return {
          ...acc,
          [index + 1]: {
            ...current,
            feeValue: current.fee,
            date: moment(current.date_to_pay),
            extraordinaryFeeValue: current.extraordinary_fee
          }
        };
      }, {});

    this.setState(prevstate => {
      let newState = {
        ...prevstate,
        selectedUnity: { ...unity, label: paymentAgreement.unity },
        addedClients: paymentAgreement.persons,
        applyToInitialFee,
        otherPayments,
        paymentFees,
        paymentSummary: {
          ...prevstate.paymentSummary,
          creditToApply: paymentAgreement.required_mortgage,
          bookingPrice: paymentAgreement.booking_price,
          unityValue: paymentAgreement.unity_price,
          initialFee: paymentAgreement.initial_fee
        },
        remainingMonths: paymentAgreement.length,
        modification,
        referral_info: paymentAgreement.referral_info,
        teamList,
        sectionLoaded: true
      };

      if (paymentAgreement.another_seller) {
        newState.seller = { value: "other", label: "Otro" };
        newState.otherSeller = paymentAgreement.another_seller;
      } else {
        newState.seller = {
          value: paymentAgreement.seller,
          label: paymentAgreement.seller
        };
      }

      return newState;
    });
  };

  updateBasicInfo = (type, data) => {
    this.setState({ [type]: data });
  };

  selectUnity = unity => {
    this.setState({ selectedUnity: unity });

    this.onChangeUnity(unity);
  };

  removeUnity = () => {
    this.setState({ selectedUnity: {} });
  };

  setRemainingInitialFee = remainingInitialFee => {
    this.setState({ remainingInitialFee });
  };

  onChangeUnity = unity => {
    if (unity && unity.id) {
      const initialFee = (unity.price * this.props.initialFeePercentage) / 100;
      const remainingMonths = moment(unity.delivery_date).diff(
        moment(moment(moment.now()).format("YYYY-MM-DD")),
        "months"
      );

      this.setState(
        prevState => ({
          ...prevState,
          paymentSummary: {
            ...prevState.paymentSummary,
            initialFee,
            unityValue: unity.price,
            initialFeePercentage: this.props.initialFeePercentage,
            bookingPrice: this.props.bookingPrice
          },
          remainingMonths: Math.abs(remainingMonths)
        }),
        () => {
          // This is executed after the state update of credit to apply changes.

          this.getCreditToApply();
          this.onRemainingInitialFeeChange();

          setTimeout(() => {
            const NPV = this.calculateNPV();

            this.setState(prevState => {
              return {
                ...prevState,
                NPV,
                NPV2: NPV
              };
            });
          }, 0);
        }
      );

      return;
    }

    // Clean the values when there is no unity
    this.setState({
      paymentSummary: {
        unityValue: 0,
        initialFee: 0,
        initialFeePercentage: 0,
        bookingPrice: 0,
        creditToApply: 0
      },
      otherPayments: {
        scheduledSavings: 0,
        severance: 0,
        housingAllowance: 0,
        others: 0
      }
    });
  };

  onAddClient = client => {
    this.setState(
      prevState => {
        return {
          ...prevState,
          addedClients: [...prevState.addedClients, client]
        };
      },
      () => {
        let defaultSeller =
          this.state.addedClients[0] &&
          this.state.teamList.find(
            s => s.value === this.state.addedClients[0].assigned_to
          );

        if (defaultSeller) {
          this.updateBasicInfo("seller", defaultSeller);
        }
      }
    );
  };

  onRemoveClient = client => {
    this.setState(prevState => ({
      ...prevState,
      addedClients: prevState.addedClients.filter(c => c.id !== client.id)
    }));
  };

  /**
   * Handle Campign change
   * @param {object} value - The campaign info
   */
  onCampaignChange = value => {
    if (!value.initial_fee_percentage) {
      return this.onChangeUnity(this.state.selectedUnity);
    }

    this.setState(
      prevState => {
        // Aplica descuentos de la campaña
        const unityPrice = prevState.selectedUnity.price;

        let unityPriceReal;

        if (value.percentage_off || value.fixed_price_off) {
          unityPriceReal = value.percentage_off
            ? unityPrice - (unityPrice * value.percentage_off) / 100
            : unityPrice - value.fixed_price_off;
        } else {
          unityPriceReal = unityPrice;
        }

        const initialFee =
          (unityPriceReal * value.initial_fee_percentage) / 100;

        let newState = {
          ...prevState,
          paymentSummary: {
            ...prevState.paymentSummary,
            unityValue: unityPriceReal,
            initialFee,
            initialFeePercentage: value.initial_fee_percentage,
            bookingPrice: value.booking_price
          },
          currentCampaign: value,
          remainingMonths:
            value.campaign_type === "FIJA"
              ? value.number_of_paymentfees
              : prevState.remainingMonths
        };

        if (
          prevState.currentCampaign &&
          value.slug !== prevState.currentCampaign.slug
        ) {
          newState.paymentFees = {
            1: {
              date: null,
              feeValue: 0,
              extraordinaryFeeValue: 0,
              comment: ""
            }
          };
        }

        return newState;
      },
      () => {
        // This is executed after the state update of other payments.
        this.getCreditToApply();
        this.onRemainingInitialFeeChange();

        setTimeout(() => {
          const NPV = this.calculateNPV();

          this.setState({ NPV });
        }, 0);
      }
    );
  };

  updateRemainingMonths = months => {
    let paymentFees;

    this.setState(
      prevState => {
        paymentFees = prevState.paymentFees;

        return {
          ...prevState,
          remainingMonths: months,
          paymentFees: {
            1: {
              date: null,
              feeValue: 0,
              extraordinaryFeeValue: 0,
              comment: ""
            }
          }
        };
      },
      () => {
        if (paymentFees[1].date) this.onSelectFirstDate(paymentFees[1].date);
      }
    );
  };

  // Calcula la cuota inicial restante
  onRemainingInitialFeeChange = () => {
    const { paymentSummary } = this.state;

    const totalInitialFee =
      paymentSummary.initialFee - paymentSummary.bookingPrice;

    // total of other payments - {number}
    const otherPaymentsValue = this.getOtherPaymentsValue(true);

    const remainingInitialFee = totalInitialFee - otherPaymentsValue;

    this.setRemainingInitialFee(remainingInitialFee);
  };

  getCreditToApply = () => {
    const { selectedUnity, applyToInitialFee, paymentSummary } = this.state;

    // total of other payments - {number}
    const otherPaymentsNotAppliedValue = this.getOtherPaymentsValue() || 0;

    const creditToApply =
      selectedUnity.price -
      paymentSummary.initialFee -
      otherPaymentsNotAppliedValue;

    if (!creditToApply) return;

    this.setState(prevState => ({
      ...prevState,
      paymentSummary: {
        ...prevState.paymentSummary,
        creditToApply
      }
    }));
  };

  /**
   * Updates the state of the fields based on the section and type of field
   * @param {string} section - The payment summary section.
   * @param {string} field - The field key on the state.
   */
  onChangeSummaryField = (section, field, value) => {
    this.setState(
      prevState => {
        return {
          ...prevState,
          [section]: {
            ...prevState.otherPayments,
            [field]: value
          }
        };
      },
      () => {
        // This is executed after the state update of other payments.
        this.getCreditToApply();
        this.onRemainingInitialFeeChange();

        setTimeout(() => {
          const NPV = this.calculateNPV();

          this.setState({ NPV });
        }, 0);
      }
    );
  };

  /** Maneja el onChange de los checks de otros pagos */
  onChangeApplyOtherPayment = field => {
    // Another agreed payment to the initial fee applies
    this.setState(
      prevState => {
        if (this.state.applyToInitialFee.includes(field)) {
          return {
            ...prevState,
            applyToInitialFee: prevState.applyToInitialFee.filter(
              f => f !== field
            )
          };
        }

        return {
          ...prevState,
          applyToInitialFee: [...prevState.applyToInitialFee, field]
        };
      },
      () => {
        // This is executed after the state update of other payments.
        this.getCreditToApply();
        this.onRemainingInitialFeeChange();

        setTimeout(() => {
          const NPV = this.calculateNPV();

          this.setState({ NPV });
        }, 0);
      }
    );
  };

  /**
   * Returns the total value of other agreed payments
   * @param {boolean} applied - The payment summary section.
   */
  getOtherPaymentsValue = applied => {
    const { otherPayments, applyToInitialFee } = this.state;

    const total = Object.keys(otherPayments).reduce((acc, current) => {
      const currentValue =
        otherPayments[current] === "" ? 0 : otherPayments[current];

      if (applied && applyToInitialFee.includes(current)) {
        return Number(acc) + Number(currentValue);
      }

      if (!applied && !applyToInitialFee.includes(current)) {
        return Number(acc) + Number(currentValue);
      }

      return acc;
    }, 0);

    return total;
  };

  calculateNPV = () => {
    const { remainingMonths, remainingInitialFee } = this.state;

    const feeList = [...Array(remainingMonths).keys()];

    // NPV calculation
    const feesNumber = feeList.map(
      fee => remainingInitialFee / remainingMonths
    );

    const NPV = this.finance.NPV(this.interest, 0, ...feesNumber);

    return NPV;
  };

  /**
   * Update date and value of the first date
   * and set remaining fees values
   */
  onSelectFirstDate = firstDate => {
    this.setState(prevState => {
      const { remainingInitialFee, remainingMonths } = prevState;

      if (remainingInitialFee < 0) {
        return alert(
          "El valor de los pagos no puede ser mayor al de la cuota inicial."
        );
      }

      const prevDate = firstDate.clone();

      const feeList = [...Array(remainingMonths).keys()];

      const fees = feeList.reduce((acc, current) => {
        const currentNumber = current + 1;
        const date =
          currentNumber === 1
            ? firstDate
            : moment(prevDate.add(1, "M").format("YYYY-MM-DD"));

        return {
          ...acc,
          [currentNumber]: {
            ...this.state.paymentFees[currentNumber],
            date
          }
        };
      }, {});

      return {
        ...prevState,
        paymentFees: {
          ...prevState.paymentFees,
          ...fees
        }
      };
    });
  };

  onChangeExtraordinaryFee = (feeNumber, value) => {
    const extraordinayValue = Number(value);

    this.setState(
      prevState => {
        const extraordinayInitialFeeSum = Object.keys(
          prevState.paymentFees
        ).reduce((acc, current) => {
          if (Number(feeNumber) === Number(current)) {
            return acc + extraordinayValue;
          }

          return (
            acc + (prevState.paymentFees[current].extraordinaryFeeValue || 0)
          );
        }, 0);

        return {
          ...prevState,
          paymentFees: {
            ...prevState.paymentFees,
            [feeNumber]: {
              ...prevState.paymentFees[feeNumber],
              extraordinaryFeeValue: value === "" ? "" : extraordinayValue
            }
          },
          extraordinayInitialFee: extraordinayInitialFeeSum
        };
      },
      () => {
        // This is executed after the state update.
        // NPV2 calculation

        const {
          remainingInitialFee,
          extraordinayInitialFee,
          paymentFees,
          remainingMonths
        } = this.state;
        const feeList = [...Array(remainingMonths).keys()];
        const feeValue = Number.parseInt(
          (remainingInitialFee - extraordinayInitialFee) / remainingMonths
        );

        // Suma los valores de la cuota + los valores de cuota extraordinaria
        const feesSumList = feeList.map(feeItem => {
          const extraordinaryFeeValue =
            paymentFees[feeItem + 1] &&
            paymentFees[feeItem + 1].extraordinaryFeeValue;

          if (extraordinaryFeeValue) {
            if (Number(feeNumber) === Number(feeItem + 1)) {
              return feeValue + extraordinayValue;
            }

            return feeValue + paymentFees[feeNumber].extraordinaryFeeValue;
          }

          return feeValue;
        });

        const NPV2 = this.finance.NPV(this.interest, 0, ...feesSumList);

        this.setState({ NPV2 });
      }
    );
  };

  onSaveComment = (feeNumber, comment) => {
    this.setState(prevState => {
      return {
        ...prevState,
        paymentFees: {
          ...prevState.paymentFees,
          [feeNumber]: {
            ...prevState.paymentFees[feeNumber],
            comment
          }
        }
      };
    });
  };

  closeValidationPopup = () => {
    this.setState({ validationPopupOpen: false });
  };

  validatePaymentPlan = () => {
    const { NPV, NPV2, seller, otherSeller } = this.state;

    if (seller.value === "other" && !otherSeller) {
      return alert("El campo nombre del vendedor no puede estar vacío");
    }

    if (NPV2 > NPV)
      return alert("Plan de pago inválido: NPV2 no puede ser mayor a NPV");

    return this.setState({
      validationPopupOpen: true,
      validPaymentPlan: true
    });
  };

  savePaymentPlan = async () => {
    const {
      addedClients,
      otherPayments,
      applyToInitialFee,
      paymentSummary,
      paymentFees,
      remainingInitialFee,
      extraordinayInitialFee,
      remainingMonths
    } = this.state;

    const feeValue = Number.parseInt(
      (remainingInitialFee - extraordinayInitialFee) / remainingMonths
    );

    const otherPaymentsMap = {
      scheduledSavings: "ahorro-programado",
      severance: "cesantias",
      housingAllowance: "subsidio-vivienda",
      others: "otro-pago-acordado"
    };

    const feeList = [...Array(remainingMonths).keys()];

    let payload = {
      persons: addedClients.map(client => client.id),
      booking_price: paymentSummary.bookingPrice,
      agreed_payments: Object.keys(otherPayments)
        .filter(oP => {
          return Number(otherPayments[oP]) || applyToInitialFee.includes(oP);
        })
        .map(otherPayment => {
          return {
            type_of_payment: otherPaymentsMap[otherPayment],
            fee: Number(otherPayments[otherPayment]),
            applied_to_initialfee: applyToInitialFee.includes(otherPayment)
          };
        }),
      payment_fees: feeList.reduce((acc, current, index) => {
        const fee = paymentFees[index + 1];

        if (fee && fee.extraordinaryFeeValue) {
          return [
            ...acc,
            {
              date_to_pay: fee.date.format("YYYY-MM-DD"),
              fee: feeValue,
              extraordinary_fee: fee.extraordinaryFeeValue
            }
          ];
        }

        return [
          ...acc,
          {
            fee: feeValue,
            date_to_pay: fee.date.format("YYYY-MM-DD"),
            extraordinary_fee: 0
          }
        ];
      }, []),
      unity: this.state.selectedUnity.id,
      initial_fee: this.state.paymentSummary.initialFee,
      required_mortgage: this.state.paymentSummary.creditToApply
    };

    if (this.state.seller && this.state.seller.value === "other") {
      payload.another_seller = this.state.otherSeller;
    } else {
      payload.seller = this.state.seller.value;
    }

    if (this.state.referral_info) {
      payload.referral_info = this.state.referral_info;
    }

    this.setState({ isFetching: true });

    const response = await this.props.savePaymentPlan(payload);

    this.setState({ isFetching: false });

    if (response) {
      this.props.history.push("/");

      this.props.showNotificationPopup({
        message: "El Plan de pago se ha guardado correctamente!"
      });
    }
  };

  handleGoBack = () => {
    let confirm;

    if (
      this.props.isCreatePaymentPlan &&
      this.state.selectedUnity &&
      this.state.selectedUnity.id
    ) {
      confirm = window.confirm(
        "Al regresar se borrarán los datos del nuevo Plan de pago. \n ¿Deseas regresar?"
      );

      if (confirm) return this.props.history.goBack();
    }

    this.props.history.goBack();
  };

  render() {
    const {
      selectedUnity,
      addedClients,
      remainingInitialFee,
      extraordinayInitialFee,
      applyToInitialFee,
      campaigns,
      paymentSummary,
      otherPayments,
      remainingMonths,
      paymentFees,
      NPV,
      NPV2,
      validationPopupOpen,
      validPaymentPlan,
      modification,
      isFetching,
      sectionLoaded
    } = this.state;

    const {
      user,
      searchUnities,
      searchCustomers,
      updateFormalizationPerson,
      saveFormalizationPerson,
      showNotificationPopup,
      bookingPrice,
      initialFeePercentage
    } = this.props;

    if (!sectionLoaded) {
      return <Loading />;
    }

    return (
      <section className="payment-plan wrap-container">
        <a className="payment-plan__back v-align" onClick={this.handleGoBack}>
          <i className="mdi mdi-arrow-left" />
          <span>Regresar</span>
        </a>
        <div className="payment-plan__header">
          <div className="align-center space-between">
            <h2>
              {this.isCreatePaymentPlan
                ? "NUEVO PLAN DE PAGOS"
                : "VER PLAN DE PAGO"}{" "}
              {modification.is_new_version &&
                `(Modificado el: ${moment(
                  modification.modification_date
                ).format("DD-MM-YYY")})`}
            </h2>

            {modification.is_new_version && (
              <a
                href={modification.old_version_copy}
                target="_blank"
                className="v-align"
              >
                <i className="mdi mdi-arrow-down" />
                Descargar versión anterior
              </a>
            )}
          </div>
        </div>

        <BasicInfo
          searchUnities={searchUnities}
          searchCustomers={searchCustomers}
          addedClients={addedClients}
          team={this.props.team}
          seller={this.state.seller}
          otherSeller={this.state.otherSeller}
          referralInfo={this.state.referral_info}
          teamList={this.state.teamList}
          defaultSeller={this.state.defaultSeller}
          updateBasicInfo={this.updateBasicInfo}
          updateFormalizationPerson={updateFormalizationPerson}
          saveFormalizationPerson={saveFormalizationPerson}
          selectUnity={this.selectUnity}
          selectedUnity={selectedUnity}
          removeUnity={this.removeUnity}
          onAddClient={this.onAddClient}
          onRemoveClient={this.onRemoveClient}
          showNotificationPopup={showNotificationPopup}
          isCreatePaymentPlan={this.isCreatePaymentPlan}
          getProjectTeam={this.props.getProjectTeam}
        />

        <PaymentSummary
          campaigns={campaigns}
          selectedUnity={selectedUnity}
          bookingPrice={bookingPrice}
          paymentSummary={paymentSummary}
          otherPayments={otherPayments}
          applyToInitialFee={applyToInitialFee}
          isCreatePaymentPlan={this.isCreatePaymentPlan}
          initialFeePercentage={initialFeePercentage}
          setRemainingInitialFee={this.setRemainingInitialFee}
          onCampaignChange={this.onCampaignChange}
          onChangeSummaryField={this.onChangeSummaryField}
          onChangeApplyOtherPayment={this.onChangeApplyOtherPayment}
        />

        <PaymentFees
          user={user}
          paymentFees={paymentFees}
          remainingInitialFee={remainingInitialFee}
          extraordinayInitialFee={extraordinayInitialFee}
          remainingMonths={remainingMonths}
          currentCampaign={this.state.currentCampaign}
          NPV={NPV}
          NPV2={NPV2}
          isCreatePaymentPlan={this.isCreatePaymentPlan}
          onSelectFirstDate={this.onSelectFirstDate}
          onChangeExtraordinaryFee={this.onChangeExtraordinaryFee}
          onSaveComment={this.onSaveComment}
          updateRemainingMonths={this.updateRemainingMonths}
        />

        {this.isCreatePaymentPlan && (
          <div className="payment-plan__button">
            <Button
              color="green"
              disabled={
                !NPV ||
                !addedClients.length ||
                this.state.paymentFees[1].date === null
              }
              onClick={this.validatePaymentPlan}
            >
              VALIDAR
            </Button>
          </div>
        )}

        <Modal open={validationPopupOpen} onClose={this.closeValidationPopup}>
          <div className="payment-plan__validation-popup">
            <i
              className="mdi mdi-close action-hover"
              onClick={this.closeValidationPopup}
            />
            {validPaymentPlan ? (
              <div>
                <img src="/static/img/emoji-lengua.png" alt="" />
                <p>El plan de pago es válido. Esa platica ya es segura</p>
                <Button onClick={this.savePaymentPlan} isFetching={isFetching}>
                  GUARDA Y ASEGURA!
                </Button>
              </div>
            ) : (
              <div>
                <img src="/static/img/emoji-triste.png" alt="" />
                <p>El plan de pago no es válido. Esa platica no es segura</p>
                <Button color="grey" onClick={this.closeValidationPopup}>
                  REGRESAR
                </Button>
              </div>
            )}
          </div>
        </Modal>
      </section>
    );
  }
}

const mapStateToProps = ({ project, user }) => {
  return {
    user,
    bookingPrice: project.booking_price,
    initialFeePercentage: project.initial_fee_percentage,
    team: project.team
  };
};

export default connect(mapStateToProps, {
  searchUnities: unitiesActions.searchUnities,
  searchCustomers: customersActions.searchPerson,
  updateFormalizationPerson: customersActions.updateFormalizationPerson,
  saveFormalizationPerson: customersActions.saveFormalizationPerson,
  getPaymentPlans: formalizationActions.getPaymentPlans,
  getPaymentAgreementsDetail: paymentsActions.getPaymentAgreementsDetail,
  getCampaigns: projectActions.getCampaigns,
  getProjectTeam: projectActions.getProjectTeam,
  savePaymentPlan: formalizationActions.savePaymentPlan,
  showNotificationPopup: notificationPopupActions.showNotificationPopup
})(PaymentPlan);
