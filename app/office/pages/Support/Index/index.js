import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import classnames from 'classnames';

import Table, { TableHead, TableBody, TableRow, TableCell } from 'core/components/Table';
import InputField from 'core/components/InputField';
import Button from 'core/components/Button';
import Initials from 'core/components/Initials';
import { colorsList } from 'core/data/colors';
import Icon from 'core/components/Icon';
import Tooltip from 'core/components/Tooltip';
import { typeList, typeListMap, statusList, priorityList, priorityListMap, filterTicketsList } from 'core/data/support';
import * as supportActions from 'core/redux/support/operations';

class Support extends PureComponent {
	constructor(props) {
		super(props);

		this.state = {
			allTickets: [],
			administratorTickets: [],
			collaboratorTickets: [],
			unassignedTickets: [],
			selectedFilter: 'all',
			statusFilterValue: '',
			typeFilterValue: '',
			priorityFilterValue: '',
			selectFilters: []
		};
	}

	async componentDidMount() {
		const tickets = await this.props.getTickets();

		const myTicketsAdministrator = await this.props.getMyTicketsAdministrator();
		const myTicketsCollaborator = await this.props.getMyTicketsCollaborator();
		const unassignedTickets = await this.props.getUnassignedTickets();

		this.setState({
			allTickets: tickets.map(t => ({ ...t, color: this.getRandomColor() })),
			administratorTickets: myTicketsAdministrator.map(t => ({ ...t, color: this.getRandomColor() })),
			collaboratorTickets: myTicketsCollaborator.map(t => ({ ...t, color: this.getRandomColor() })),
			unassignedTickets: unassignedTickets.map(t => ({ ...t, color: this.getRandomColor() }))
		});
	}

	onSelectTicketFilter = filter => {
		this.setState({ selectedFilter: filter });
	};

	getRandomColor = () => {
		return colorsList[Math.floor(Math.random() * colorsList.length)];
	};

	updatePriority = async (priority, ticket) => {
		if (priority.value !== ticket.priority) {
			this.setState(prevState => {
				return {
					...prevState,
					[`${prevState.selectedFilter}Tickets`]: prevState[`${prevState.selectedFilter}Tickets`].map(t => {
						if (t.slug === ticket.slug) {
							return { ...t, priority: priority.value };
						}

						return t;
					})
				};
			});

			const payload = {
				administrators: ticket.administrators.map(a => a.id),
				collaborators: ticket.collaborators.map(a => a.id),
				client: ticket.client.id,
				priority: priority.value,
				ticket_type: ticket.ticket_type,
				status: ticket.status
			};

			await this.props.updateTicket(ticket.slug, payload);
		}
	};

	takeTicket = async ticket => {
		const { user, updateTicket } = this.props;

		this.setState(prevState => {
			let newTicket = { ...ticket, administrators: [{ id: user.id, user_type: user.user_type, user }] };

			return {
				...prevState,
				[`${prevState.selectedFilter}Tickets`]: prevState[`${prevState.selectedFilter}Tickets`].map(ticket => {
					return newTicket;
				}),
				// allTickets: [...prevState.allTickets, newTicket],
				administratorTickets: [...prevState.administratorTickets, newTicket],
				unassignedTickets: prevState.unassignedTickets.filter(t => t.slug !== ticket.slug)
			};
		});

		const payload = {
			administrators: [this.props.user.id],
			collaborators: [],
			client: ticket.client.id,
			priority: ticket.priority,
			ticket_type: ticket.ticket_type,
			status: ticket.status
		};

		const response = await updateTicket(ticket.slug, payload);
	};

	onFilterTicket = (filterType, filterValue) => {
		this.setState(prevState => {
			const selectFilters =
				filterValue.value === '' && prevState.selectFilters.includes(filterType)
					? prevState.selectFilters.filter(t => t !== filterType)
					: [...prevState.selectFilters, filterType];

			return {
				...prevState,
				selectFilters,
				[`${filterType}FilterValue`]: filterValue
			};
		});
	};

	render() {
		const {
			allTickets,
			administratorTickets,
			collaboratorTickets,
			unassignedTickets,
			selectedFilter,
			statusFilterValue,
			typeFilterValue,
			priorityFilterValue,
			selectFilters
		} = this.state;

		const tickets = this.state[`${selectedFilter}Tickets`].filter(ticket => {
			if (!selectFilters.length) return true;

			return selectFilters.reduce((acc, current) => {
				const ticketType = current === 'type' ? 'ticket_type' : current;

				return acc && ticket[ticketType] === this.state[`${current}FilterValue`].value;
			}, true);
		});

		return (
			<section className="support">
				<div className="support__main">
					<div className="support__header space-between">
						<div className="support-filters v-align">
							<div className="v-align">
								<label htmlFor="">Estado:</label>
								<InputField
									value={statusFilterValue}
									type="select"
									placeholder="Filtrar"
									options={[{ label: 'Seleccionar', value: '' }, ...statusList]}
									onChange={value => this.onFilterTicket('status', value)}
									rounded
								/>
							</div>

							<div className="v-align">
								<label htmlFor="">Tipo:</label>
								<InputField
									value={typeFilterValue}
									type="select"
									placeholder="Filtrar"
									options={[{ label: 'Seleccionar', value: '' }, ...typeList]}
									onChange={value => this.onFilterTicket('type', value)}
									rounded
								/>
							</div>

							<div className="v-align">
								<label htmlFor="">Prioridad:</label>
								<InputField
									value={priorityFilterValue}
									type="select"
									placeholder="Filtrar"
									options={[{ label: 'Seleccionar', value: '' }, ...priorityList]}
									onChange={value => this.onFilterTicket('priority', value)}
									rounded
								/>
							</div>
						</div>

						<Link to="/support/create-ticket">
							<Button>NUEVO TICKET</Button>
						</Link>
					</div>

					{(typeFilterValue.value || priorityFilterValue.value) && (
							<p className="support__filters-legend">
								Filtrando por tickets de tipo <span>{typeListMap[typeFilterValue.value]}</span> de
								prioridad <span>{priorityListMap[priorityFilterValue.value]}</span>
							</p>
						)}

					<div className="support__table">
						<Table type="data-grid">
							<TableHead>
								<TableRow>
									<TableCell>Solicitante</TableCell>
									<TableCell>Tipo</TableCell>
									<TableCell>Título</TableCell>
									<TableCell>Administrado por</TableCell>
									<TableCell>Prioridad</TableCell>
									<TableCell />
								</TableRow>
							</TableHead>
							<TableBody>
								{tickets.map(ticket => {
									const clientFirstName = ticket.client.first_name;
									const clientLastName = ticket.client.last_name;
									const selectedPriority = priorityList.find(p => p.value === ticket.priority);

									const administrator = ticket.administrators[0];

									return (
										<TableRow key={ticket.slug}>
											<TableCell>
												<Initials
													initials={`${clientFirstName[0]}${clientLastName[0]}`}
													color={ticket.color}
												/>
												<div className="support__table-applicant">
													<div>{`${clientFirstName} ${clientLastName}`}</div>
													<span>{ticket.unity}</span>
												</div>
											</TableCell>
											<TableCell>{typeListMap[ticket.ticket_type]}</TableCell>
											<TableCell>{ticket.title}</TableCell>
											<TableCell>
												{administrator ? (
													`${administrator.user.first_name} ${administrator.user.last_name}`
												) : (
													<a onClick={() => this.takeTicket(ticket)}>Tomar ticket</a>
												)}
											</TableCell>
											<TableCell>
												<div className="support__table-priority">
													<InputField
														value={selectedPriority}
														type="select"
														options={priorityList}
														onChange={value => this.updatePriority(value, ticket)}
														rounded
													/>
												</div>
											</TableCell>
											<TableCell>
												<Tooltip title="Ver ticket">
													<Link to={`/support/tickets/${ticket.slug}`}>
														<Icon icon="Eye" />
													</Link>
												</Tooltip>
											</TableCell>
										</TableRow>
									);
								})}
							</TableBody>
						</Table>
					</div>
				</div>

				<div className="support-tickets">
					<div className="support-tickets__header">
						<div>Tickets</div>
					</div>

					<div className="support-tickets__list">
						<ul>
							{filterTicketsList.map(filterTicket => {
								const filterTicketClassName = classnames(
									'support-tickets__item v-align space-between',
									{ 'support-tickets__item--active': selectedFilter === filterTicket.value }
								);

								return (
									<li
										key={filterTicket.value}
										className={filterTicketClassName}
										onClick={() => this.onSelectTicketFilter(filterTicket.value)}
									>
										<div>{filterTicket.label}</div>
										<span>{this.state[`${filterTicket.value}Tickets`].length}</span>
									</li>
								);
							})}
						</ul>
					</div>
				</div>
			</section>
		);
	}
}

const mapStateToProps = ({ user }) => {
	return {
		user
	};
};

export default connect(mapStateToProps, {
	getTickets: supportActions.getTickets,
	getMyTicketsAdministrator: supportActions.getMyTicketsAdministrator,
	getMyTicketsCollaborator: supportActions.getMyTicketsCollaborator,
	getUnassignedTickets: supportActions.getUnassignedTickets,
	updateTicket: supportActions.updateTicket
})(Support);
