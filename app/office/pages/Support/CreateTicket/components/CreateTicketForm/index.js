import React, { Component } from "react";
import { Field, reduxForm } from "redux-form";
import { Link } from "react-router-dom";
import { debounce as debounceFunc } from "throttle-debounce";

import InputField from "core/components/InputField";
import Searcher from "core/components/Searcher";
import Icon from "core/components/Icon";
import Button from "core/components/Button";
import CustomerCard from "core/components/CustomerCard";
import { typeList, statusList, priorityList } from "core/data/support";

class CreateTicketForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      customersFound: [],
      selectedCustomer: {},
      unitiesFound: [],
      clientSearchValue: "",
      collaboratorSearchValue: "",
      administratorSearchValue: "",
      selectedUnity: "",
      descriptionFileName: "",
      collaboratorFound: [],
      administratorFound: [],
      collaboratorSelected: [],
      administratorSelected: [],
      clientSearching: false,
      collaboratorSearching: false,
      administratorSearching: false
    };

    this.searchCustomers = debounceFunc(500, this.searchCustomers.bind(this));
    this.searchTeam = debounceFunc(500, this.searchTeam.bind(this));
  }

  onInputChange = (type, value) => {
    this.setState({
      [`${type}SearchValue`]: value,
      [`${type}Searching`]: true
    });

    if (type === "client") {
      return this.searchCustomers(type, value);
    }

    this.searchTeam(type, value);
  };

  searchCustomers = async (type, value) => {
    const response = await this.props.searchPerson(`${value}&is_client=True`);

    this.setState({
      customersFound: response.map(customer => {
        return {
          ...customer,
          value: customer.id,
          label: `${customer.first_name} ${customer.last_name}`
        };
      }),
      [`${type}Searching`]: false
    });
  };

  searchTeam = async (type, value) => {
    const response = await this.props.getTeam();

    this.setState(prevState => {
      const administrators = prevState.administratorSelected.map(a => a.value);
      const collaborators = prevState.collaboratorSelected.map(c => c.value);

      if (type === "collaborator") {
        return {
          ...prevState,
          [`${type}Found`]: response
            .filter(c => !collaborators.includes(c.id))
            .map(el => {
              return {
                ...el,
                value: el.id,
                label: `${el.user.first_name} ${el.user.last_name}`
              };
            }),
          [`${type}Searching`]: false
        };
      }

      return {
        ...prevState,
        [`${type}Found`]: response
          .filter(c => !administrators.includes(c.id))
          .map(el => {
            return {
              ...el,
              value: el.id,
              label: `${el.user.first_name} ${el.user.last_name}`
            };
          }),
        [`${type}Searching`]: false
      };
    });
  };

  onSelectCustomer = async customer => {
    this.setState({ selectedCustomer: customer });

    if (customer.identity) {
      const response = await this.props.getClientIdentity(customer.identity);

      this.setState({
        unitiesFound: response.map(unity => ({
          ...unity,
          value: unity.id,
          label: unity.name
        })),
        selectedUnity: { value: response[0].id, label: response[0].name }
      });
    }
  };

  onRemoveCustomer = () => {
    this.setState({
      selectedCustomer: {},
      clientSearchValue: "",
      customersFound: [],
      selectedUnity: {},
      unitiesFound: []
    });
  };

  onChangeDescriptionFile = event => {
    this.setState({ descriptionFileName: event.target.files[0].name });
  };

  onAddTeam = (type, value) => {
    this.setState(prevState => {
      return {
        ...prevState,
        [`${type}Selected`]: [...prevState[`${type}Selected`], value],
        [`${type}Found`]: [],
        [`${type}SearchValue`]: ""
      };
    });
  };

  onRemoveTeam = (type, value) => {
    this.setState(prevState => {
      return {
        ...prevState,
        [`${type}Selected`]: prevState[`${type}Selected`].filter(
          el => Number(el.value) !== Number(value)
        )
      };
    });
  };

  static validate(fields) {
    const errors = {};
    const errorName = "Campo requerido";

    if (!fields.title) errors.title = errorName;
    if (!fields.type) errors.type = errorName;
    if (!fields.status) errors.status = errorName;
    if (!fields.priority) errors.priority = errorName;

    return errors;
  }

  onSubmit = async values => {
    try {
      const {
        selectedCustomer,
        selectedUnity,
        administratorSelected,
        collaboratorSelected
      } = this.state;
      const { saveTicket, showNotificationPopup } = this.props;

      let formData = new window.FormData(this.newTicketForm);

      let blob = new Blob([JSON.stringify(null)], {
        type: "multipart/form-data"
      });

      formData.append("client", selectedCustomer.id);
      formData.append("unity", selectedUnity.value);
      formData.append("title", values.title);
      formData.append("description", values.description);
      formData.append("attached_file", this.attachedFile.files[0] || blob);
      formData.append("ticket_type", values.type.value);
      formData.append("status", values.status.value);
      formData.append("priority", values.priority.value);

      if (administratorSelected.length) {
        formData.append("administrators", administratorSelected.map(a => a.id));
      }

      if (collaboratorSelected.length) {
        formData.append("collaborators", collaboratorSelected.map(a => a.id));
      }

      this.setState({ isFetching: true });

      const response = await saveTicket(formData);

      if (response) {
        this.props.history.push("/support");

        showNotificationPopup({
          message: "El Ticket se ha guardado correctamente!"
        });
      }
    } catch (error) {
      // error
    }
  };

  render() {
    const {
      clientSearchValue,
      clientSearching,
      selectedCustomer,
      customersFound,
      unitiesFound,
      selectedUnity,
      descriptionFileName,
      collaboratorFound,
      administratorFound,
      collaboratorSearchValue,
      administratorSearchValue,
      collaboratorSelected,
      administratorSelected,
      collaboratorSearching,
      administratorSearching,
      isFetching
    } = this.state;

    const { handleSubmit } = this.props;

    return (
      <div className="create-ticket__form">
        <form
          ref={element => (this.newTicketForm = element)}
          onSubmit={handleSubmit(this.onSubmit)}
        >
          <div className="create-ticket__form-group">
            <h4 className="v-align">
              <i className="mdi mdi-account" />
              <span>Detalles del solicitante</span>
            </h4>

            <div className="create-ticket__applicant">
              <div>
                <label>Cliente</label>
                <Searcher
                  inputValue={clientSearchValue}
                  options={customersFound}
                  onInputChange={value => this.onInputChange("client", value)}
                  onChange={this.onSelectCustomer}
                  onCloseChip={this.onRemoveCustomer}
                  placeholder="Ej: T2-201"
                  showChip={!!selectedCustomer.label}
                  chipLabel={selectedCustomer.label}
                  isSearchable={true}
                  isLoading={clientSearching}
                  placeholder="Buscar por persona o apto"
                />
              </div>

              <InputField
                type="select"
                value={selectedUnity}
                label="Apto:"
                options={unitiesFound}
                disabled={unitiesFound.length <= 1}
              />

              <InputField
                type="text"
                value={selectedCustomer.email}
                label="Correo:"
                disabled
              />

              <InputField
                type="number"
                value={selectedCustomer.cellphone}
                label="Celular"
                disabled
              />
            </div>
          </div>

          <div className="create-ticket__form-group">
            <h4 className="v-align">
              <i className="mdi mdi-pencil" />
              <span>Descripción del ticket</span>
            </h4>

            <div className="create-ticket__description">
              <Field
                type="text"
                name="title"
                label="Título"
                component={InputField}
              />

              <div>
                <Field
                  type="textarea"
                  name="description"
                  label="Descripción"
                  component={InputField}
                />

                <div className="create-ticket__description-file v-align">
                  <label htmlFor="newTicketAttachedFile" className="v-align">
                    <Icon icon="AttachmentFile" />
                    <span>Subir archivo</span>
                    <input
                      ref={element => (this.attachedFile = element)}
                      type="file"
                      id="newTicketAttachedFile"
                      onChange={this.onChangeDescriptionFile}
                    />
                  </label>

                  <div className="create-ticket__description-file-name">
                    {descriptionFileName}
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="create-ticket__form-group">
            <h4 className="v-align">
              <i className="mdi mdi-ticket" />
              <span>Detalles del ticket</span>
            </h4>

            <div className="create-ticket__details">
              <Field
                type="select"
                name="type"
                label="Tipo:"
                options={typeList}
                component={InputField}
              />

              <Field
                type="select"
                name="status"
                label="Estado:"
                options={statusList}
                component={InputField}
              />

              <Field
                type="select"
                name="priority"
                label="Prioridad:"
                options={priorityList}
                component={InputField}
              />
            </div>
          </div>

          <div className="create-ticket__form-group">
            <h4 className="v-align">
              <i className="mdi mdi-account-multiple" />
              <span>Administradores ó Colaboradores</span>
            </h4>

            <div className="create-ticket__management">
              <div>
                <label>Colaboradores</label>
                <Searcher
                  inputValue={collaboratorSearchValue}
                  options={collaboratorFound}
                  onInputChange={value =>
                    this.onInputChange("collaborator", value)
                  }
                  onChange={value => this.onAddTeam("collaborator", value)}
                  isSearchable={true}
                  isLoading={collaboratorSearching}
                  placeholder="Buscar Colaborador"
                />

                <div className="create-ticket__management-list">
                  {collaboratorSelected.map(c => {
                    return (
                      <CustomerCard
                        key={c.value}
                        name={c.label}
                        initials={`${c.user && c.user.first_name[0]} ${c.user &&
                          c.user.last_name[0]}`}
                        onClose={() =>
                          this.onRemoveTeam("collaborator", c.value)
                        }
                      />
                    );
                  })}
                </div>
              </div>

              <div>
                <label>Administradores</label>
                <Searcher
                  inputValue={administratorSearchValue}
                  options={administratorFound}
                  onInputChange={value =>
                    this.onInputChange("administrator", value)
                  }
                  onChange={value => this.onAddTeam("administrator", value)}
                  isSearchable={true}
                  isLoading={administratorSearching}
                  placeholder="Buscar Administrador"
                />

                <div className="create-ticket__management-list">
                  {administratorSelected.map(a => {
                    return (
                      <CustomerCard
                        key={a.value}
                        name={a.label}
                        initials={`${a.user && a.user.first_name[0]} ${a.user &&
                          a.user.last_name[0]}`}
                        onClose={() =>
                          this.onRemoveTeam("administrator", a.value)
                        }
                      />
                    );
                  })}
                </div>
              </div>
            </div>
          </div>

          <div className="create-ticket__form-button">
            <Button isFetching={isFetching}>GUARDAR TICKET</Button>
          </div>
        </form>
      </div>
    );
  }
}

export default reduxForm({
  form: "CreateTicketForm",
  validate: CreateTicketForm.validate,
  keepDirtyOnReinitialize: true,
  enableReinitialize: true
})(CreateTicketForm);
