import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import CreateTicketForm from './components/CreateTicketForm';
import * as customersActions from 'core/redux/customers/operations';
import * as userActions from 'core/redux/user/operations';
import * as supportActions from 'core/redux/support/operations';
import * as notificationPopupActions from 'core/redux/notificationPopup/actions';

class CreateTicket extends Component {
  constructor (props) {
    super(props);
  }

  render () {
    const {
      searchPerson,
      getClientIdentity,
      getTeam,
      saveTicket,
      showNotificationPopup
    } = this.props;

    return (
      <section className="create-ticket">
        <div className="create-ticket__header v-align">
          <div className="create-ticket__header-back">
            <Link to="/support" className="v-align action-hover">
              <i className="mdi mdi-arrow-left"></i>
              <div>Regresar</div>
            </Link>
          </div>

          <h2 className="create-ticket__header-title">Crea un nuevo Ticket</h2>
        </div>

        <CreateTicketForm
          searchPerson={searchPerson}
          getClientIdentity={getClientIdentity}
          getTeam={getTeam}
          saveTicket={saveTicket}
          showNotificationPopup={showNotificationPopup}
          history={this.props.history}
        />
      </section>
    );
  }
}

export default connect(null, {
  searchPerson: customersActions.searchPerson,
  getClientIdentity: customersActions.getClientIdentity,
  getTeam: userActions.getTeam,
  saveTicket: supportActions.saveTicket,
  showNotificationPopup: notificationPopupActions.showNotificationPopup
})(CreateTicket);
