import React, { Component } from 'react';
import Collapsible from 'react-collapsible';
import { debounce as debounceFunc } from 'throttle-debounce';
import moment from 'moment';

import InputField from 'core/components/InputField';
import Initials from 'core/components/Initials';
import Searcher from 'core/components/Searcher';
import Icon from 'core/components/Icon';
import CustomerCard from 'core/components/CustomerCard';
import { typeList, statusList, priorityList } from 'core/data/support';

class Sidebar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedManagement: '',
      collaboratorSearchValue: '',
      administratorSearchValue: '',
      collaboratorFound: [],
      administratorFound: [],
      collaboratorSelected: [],
      administratorSelected: [],
      collaboratorSearching: false,
      administratorSearching: false
    };

    this.searchTeam = debounceFunc(500, this.searchTeam.bind(this));
  }

  componentDidMount () {
    const { administrators, collaborators } = this.props.ticket;

    let administratorSelected = administrators.map(a => ({ ...a, label: `${a.user.first_name} ${a.user.last_name}`, value: a.id }));
    let collaboratorSelected = collaboratorSelected = collaborators.map(c => ({ ...c, label: `${c.user.first_name} ${c.user.last_name}`, value: c.id }));

    this.setState({
      administratorSelected,
      collaboratorSelected
    });
  }

  onManagementOpenChange = (value) => {
    this.setState(prevState => {
      if (value === prevState.selectedManagement) {
        return { selectedManagement: '' };
      }

      return { selectedManagement: value };
    });
  }

  onInputChange = (type, value) => {
    this.setState({ [`${type}SearchValue`]: value, [`${type}Searching`]: true });

    if (type === 'client') {
      return this.searchCustomers(type, value);
    }

    this.searchTeam(type, value);
  }

  searchTeam = async (type, value) => {
    const response = await this.props.getTeam();

    this.setState(prevState => {
      const administrators = prevState.administratorSelected.map(a => a.value);
      const collaborators = prevState.collaboratorSelected.map(c => c.value);

      if (type === 'collaborator') {
        return {
          ...prevState,
          [`${type}Found`]: response.filter(c => !collaborators.includes(c.id)).map(el => {
            return { ...el, value: el.id, label: `${el.user.first_name} ${el.user.last_name}` }
          }),
          [`${type}Searching`]: false
        }
      }

      return {
        ...prevState,
        [`${type}Found`]: response.filter(c => !administrators.includes(c.id)).map(el => {
          return { ...el, value: el.id, label: `${el.user.first_name} ${el.user.last_name}` }
        }),
        [`${type}Searching`]: false
      }
    });

  }

  onAddTeam = (type, value) => {
    this.setState(prevState => {
      return {
        ...prevState,
        [`${type}Selected`]: [...prevState[`${type}Selected`], value],
        [`${type}Found`]: [],
        [`${type}SearchValue`]: ''
      }
    });

    this.props.addTeam(`${type}s`, value.id);
  }

  onRemoveTeam = (type, value) => {
    this.setState(prevState => {
      return {
        ...prevState,
        [`${type}Selected`]: prevState[`${type}Selected`].filter(el => Number(el.value) !== Number(value))
      };
    });

    this.props.removeTeam(`${type}s`, value);
  }

  render() {
    const {
      selectedManagement,
      collaboratorSearchValue,
      administratorSearchValue,
      collaboratorSelected,
      administratorSelected,
      collaboratorSearching,
      administratorSearching,
      administratorFound,
      collaboratorFound
    } = this.state;

    const { ticket, onSelectChange } = this.props;

    return (
      <div className="ticket-detail-sidebar">
        <div className="ticket-detail-sidebar__applicant">
          <h5>Detalles del solicitante</h5>
          <div className="v-align">
            <Initials initials={`${ticket.client.first_name[0]} ${ticket.client.last_name[0]}`} />
            <div className="ticket-detail-sidebar__applicant-name">
              <div>{`${ticket.client.first_name} ${ticket.client.last_name}`}</div>
              <div>{ticket.unity}</div>
            </div>
          </div>

          <div className="ticket-detail-sidebar__applicant-info">
            <div>Correo: {ticket.client.email}</div>
            <div>Celular: {ticket.client.cellphone}</div>
          </div>
        </div>

        <div className="ticket-detail-sidebar__details">
          <h5>Detalles del ticket</h5>

          <div className="ticket-detail-sidebar__details-date v-align space-between">
            <div>Fecha de creación:</div>
            <div>
              <div>{moment(ticket.timestamp).format('DD MMMM YYYY')}</div>
              <div>({moment(ticket.timestamp).fromNow()})</div>
            </div>
          </div>

          <div className="ticket-detail-sidebar__details-selects">
            <div className="v-align space-between">
              <label>Tipo:</label>
              <InputField
                type="select"
                value={typeList.find(el => el.value === ticket.ticket_type)}
                options={typeList}
                onChange={(el) => onSelectChange('ticket_type', el.value)}
                rounded
              />
            </div>

            <div className="v-align space-between">
              <label>Estado:</label>
              <InputField
                type="select"
                value={statusList.find(el => el.value === ticket.status)}
                options={statusList}
                onChange={(el) => onSelectChange('status', el.value)}
                rounded
              />
            </div>

            <div className="v-align space-between">
              <label>Prioridad:</label>
              <InputField
                type="select"
                value={priorityList.find(el => el.value === ticket.priority)}
                options={priorityList}
                onChange={(el) => onSelectChange('priority', el.value)}
                rounded
              />
            </div>
          </div>
        </div>

        <div className="ticket-detail-sidebar__management">
          <Collapsible
            open={selectedManagement === 'administrators'}
            trigger={
              <div
                className="ticket-detail-sidebar__management-trigger v-align space-between"
                onClick={() => this.onManagementOpenChange('administrators')}>

                <div className="v-align">
                  <Icon icon="AssignPerson" />
                  <div>Administradores: {administratorSelected.length}</div>
                </div>
                <i className={`mdi mdi-chevron-${selectedManagement === 'administrators' ? 'up' : 'down'}`}></i>
              </div>
            }
            triggerDisabled
            transitionTime={200}>

            <div>
              <Searcher
                inputValue={administratorSearchValue}
                options={administratorFound}
                onInputChange={(value) => this.onInputChange('administrator', value)}
                onChange={(value) => this.onAddTeam('administrator', value)}
                isSearchable={true}
                isLoading={administratorSearching}
                placeholder="Buscar Administrador"
              />

              <div className="create-ticket__management-list">
                {
                  administratorSelected.map(a => {
                    return (
                      <CustomerCard
                        key={a.value}
                        name={a.label}
                        initials={`${a.user && a.user.first_name[0]} ${a.user && a.user.last_name[0]}`}
                        onClose={() => this.onRemoveTeam('administrator', a.value)}
                      />
                    );
                  })
                }
              </div>
            </div>

          </Collapsible>

          <Collapsible
            open={selectedManagement === 'collaborators'}
            trigger={
              <div
                className="ticket-detail-sidebar__management-trigger v-align space-between"
                onClick={() => this.onManagementOpenChange('collaborators')}>

                <div className="v-align">
                  <Icon icon="AddPerson" />
                  <div>Colaboradores: {collaboratorSelected.length}</div>
                </div>
                <i className={`mdi mdi-chevron-${selectedManagement === 'collaborators' ? 'up' : 'down'}`}></i>
              </div>
            }
            triggerDisabled
            transitionTime={200}>

            <Searcher
              inputValue={collaboratorSearchValue}
              options={collaboratorFound}
              onInputChange={(value) => this.onInputChange('collaborator', value)}
              onChange={(value) => this.onAddTeam('collaborator', value)}
              isSearchable={true}
              isLoading={collaboratorSearching}
              placeholder="Buscar Colaborador"
            />

            <div className="create-ticket__management-list">
              {
                collaboratorSelected.map(a => {
                  return (
                    <CustomerCard
                      key={a.value}
                      name={a.label}
                      initials={`${a.user && a.user.first_name[0]} ${a.user && a.user.last_name[0]}`}
                      onClose={() => this.onRemoveTeam('collaborator', a.value)}
                    />
                  );
                })
              }
            </div>

          </Collapsible>
        </div>

      </div>
    );
  }
}

export default Sidebar;
