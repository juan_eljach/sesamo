import React, { Component } from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';

import InputField from 'core/components/InputField';
import Icon from 'core/components/Icon';
import Button from 'core/components/Button/index';
import Tooltip from 'core/components/Tooltip';

class Write extends Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    const {
      tabsInput,
      selectedTabInput,
      onSelectTab,
      setFileElement,
      onFileChange,
      onInputChange,
      writeValue,
      isSending,
      file,
      saveComunication
      } = this.props;

    return (
      <div className="ticket-detail-write">
        <div className="ticket-detail-write__header v-align">
          {
            tabsInput.map(tab => {
              const tabItemClassName = classnames(
                { 'ticket-detail__tab-item--active': tab.value === selectedTabInput }
              );

              return (
                <div key={tab.value} className={tabItemClassName} onClick={() => onSelectTab('selectedTabInput', tab.value)}>
                  <Icon icon={tab.icon} />
                  <span>{tab.label}</span>
                </div>
              );
            })
          }
        </div>

        <InputField
          value={writeValue}
          type="textarea"
          placeholder={selectedTabInput === 'answer' ? 'Describe tu respuesta' : 'Describe la nota'}
          onChange={onInputChange}
        />

        <div className="ticket-detail-write__footer v-align space-between">
          {
            selectedTabInput === 'answer' && (
              <div className="ticket-detail-write__file v-align">
                <Tooltip title="Adjuntar archivo">
                  <label htmlFor="writeFile">
                    <Icon icon="AttachmentFile" />
                    <input id="writeFile" type="file" ref={(el) => setFileElement(el)} onChange={onFileChange} />
                  </label>
                </Tooltip>

                <div>{file.name}</div>
              </div>
            )
          }
          <Button isFetching={isSending} onClick={saveComunication}>ENVIAR</Button>
        </div>
      </div>
    );
  }
}

export default Write;
