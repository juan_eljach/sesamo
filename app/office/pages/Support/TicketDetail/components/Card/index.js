import React, { Component } from 'react';
import classnames from 'classnames';
import moment from 'moment';
import PropTypes from 'prop-types';

import Icon from 'core/components/Icon';

class Card extends Component {
  constructor(props) {
    super(props);
  }

  render () {
    const { type, data, role } = this.props;

    const cardClassName = classnames(
      'ticket-detail-card',
      type === 'answer' ? 'ticket-detail-card__answer' : 'ticket-detail-card__note'
    );

    return (
      <div className={cardClassName}>
        <div className="ticket-detail-card__icon v-align h-align">
          <Icon icon={type === 'answer' ? 'Response' : 'Note'} />
        </div>
        <div className="ticket-detail-card__date">
          <span>{moment(data.timestamp).format('DD MMM YYYY')}</span>
          <span>({moment(data.timestamp).fromNow()})</span>
        </div>
        <div className="ticket-detail-card__autor v-align">
          <div>{`${data.written_by.user.first_name} ${data.written_by.user.last_name}`}</div>
          {
            role && <div>({role})</div>
          }
        </div>
        <p>
          {data[`${type}_text`]}
        </p>

        {
          type === 'answer' && data.answer_attached_file && <a href={data.answer_attached_file} target="_blank">{data.answer_attached_file.split('/')[5]}</a>
        }
      </div>
    );
  }
}

Card.propTypes = {
  type: PropTypes.string,
  data: PropTypes.object
};

export default Card;
