import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import moment from 'moment';
import classnames from 'classnames';

import Icon from 'core/components/Icon';
import Button from 'core/components/Button/index';
import Card from './components/Card';
import Write from './components/Write';
import Sidebar from './components/Sidebar';
import Loading from 'core/components/Loading/index';
import * as supportData from 'core/data/support';
import * as userActions from 'core/redux/user/operations';
import * as supportActions from 'core/redux/support/operations';
import * as notificationPopupActions from 'core/redux/notificationPopup/actions';

class TicketDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ticket: {},
      isSending: false,
      writeValue: '',
      selectedTab: 'all',
      selectedTabInput: 'answer',
      file: {},
      pageLoaded: false
    };

    this.tabs = [
      { value: 'all', label: 'Todas', icon: null},
      { value: 'answers', label: 'Respuestas', icon: 'Response' },
      { value: 'notes', label: 'Notas internas', icon: 'Note' }
    ];

    this.tabsInput = [
      { value: 'answer', label: 'Respuesta', icon: 'Response' },
      { value: 'note', label: 'Nota interna', icon: 'Note' },
    ];

    this.ticketSlug = this.props.match.params.ticketSlug;
  }

  async componentDidMount () {
    const response = await this.props.getTicket(this.ticketSlug);
    console.log(response);
    this.setState({ ticket: response, pageLoaded: true});
  }

  onSelectTab = (type, tab) => {
    this.setState({[type]: tab});
  }

  onFileChange = (event) => {
    this.setState({file: event.target.files[0]})
  }

  setFileElement = (el) => {
    this.fileElement =  el;
  }

  onInputChange = (event) => {
    this.setState({ writeValue: event.target.value });
  }

  onSelectChange = (type, value) => {
    this.setState(prevState => {
      let payload = {
        ...prevState.ticket,
        [type]: value,
        client: prevState.ticket.client.id,
        administrators: prevState.ticket.administrators.map(a => a.id),
        collaborators: prevState.ticket.collaborators.map(a => a.id)
      };

      this.props.updateTicket(prevState.ticket.slug, payload);

      return {
        ...prevState,
        ticket: {
          ...prevState.ticket, [type]: value
        }
      };
    });

    this.props.showNotificationPopup({
      message: `El ticket ha actualizado su ${supportData.listMap[type === 'ticket_type' ? 'type' : type]} a ${supportData[`${type === 'ticket_type' ? 'type' : type}List`].find(el => el.value === value).label}`
    });
  }

  getRole = (ticket) => {
    const { user } = this.props;

    const isCollaborator = ticket.collaborators.find(c => c.id === user.id);
    const isAdministrator = ticket.administrators.find(a => a.id === user.id);

    if (isCollaborator && isAdministrator) return 'Administrador y Colaborador';
    if (isAdministrator) return 'Administrador';
    if (isCollaborator) return 'Colaborador';
  }

  addTeam = (type, id) => {
    const { ticket } = this.state;

    this.props.updateTicket(ticket.slug, {
      ...ticket,
      administrators: ticket.administrators.map(a => a.id),
      collaborators: ticket.collaborators.map(c => c.id),
      client: ticket.client.id,
      [type]: [...ticket[type].map(t => t.id), id]
    });
  }

  removeTeam = (type, id) => {
    this.setState(prevState => {
      const { ticket } = prevState;

      this.props.updateTicket(ticket.slug, {
        ...ticket,
        administrators: ticket.administrators.map(a => a.id),
        collaborators: ticket.collaborators.map(c => c.id),
        client: ticket.client.id,
        [type]: ticket[type].filter(t => t.id !== id)
      });
    })
  }

  saveComunication = async () => {
    const { selectedTabInput, writeValue } = this.state;
    const { user, createAnswer, createNote, showNotificationPopup } = this.props;

    if (selectedTabInput === 'answer') {
      if (!this.fileElement.files.length) return;

      this.setState({ isSending: true });

      const formData = new FormData()

      formData.append('answer_attached_file', this.fileElement.files[0]);
      formData.append('answer_text', writeValue);

      const response = await this.props.createAnswer(this.ticketSlug, formData);

      if (response) {
        showNotificationPopup({ message: 'La respuesta se ha guardado correctamente!' });

        this.setState(prevState => {
          return {
            ...prevState,
            ticket: {...prevState.ticket, answers: [...prevState.ticket.answers, {...response, written_by: {user: {...user}}, timestamp: moment()}]},
            file: {},
            writeValue: '',
            isSending: false
          };
        })

        return;
      }
    }

    this.setState({ isSending: true });

    const response = await createNote(this.ticketSlug, { note_text: writeValue});

    if (response) {
      this.setState(prevState => {
        return {
          ...prevState,
          ticket: { ...prevState.ticket, notes: [...prevState.ticket.notes, { ...response, written_by: { user: { ...user } }, timestamp: moment()}]},
          writeValue: '',
          isSending: false
        };
      })

      showNotificationPopup({ message: 'La nota se ha guardado correctamente!' });
    }
  }

  render() {
    const { pageLoaded, ticket, selectedTab, selectedTabInput, file, writeValue, isSending } = this.state;
    const { user, getTeam } = this.props;

    if (!pageLoaded) return <Loading />;

    return (
      <div className="ticket-detail">
        <div className="ticket-detail__main">
          <div className="ticket-detail__header">
            <div className="ticket-detail__ticket-close">
              <span>Ticket {ticket.unity}</span>
              <Link to="/support">
                <i className="mdi mdi-close action-hover"></i>
              </Link>
            </div>
          </div>

          <div className="ticket-detail__main">
            <div className="ticket-detail__ticket-info-list">
              <div className="ticket-detail__ticket-info v-align">
                <label>Título:</label>
                <p>{ticket.title}</p>
              </div>

              <div className="ticket-detail__ticket-info v-align">
                <label>Descripción:</label>
                <p>{ticket.description}</p>
              </div>

              <div className="ticket-detail__ticket-info v-align">
                <label>Documento:</label>
                <a href={ticket.attached_file} target="_blank">{ticket.attached_file.split('/')[5]}</a>
              </div>
            </div>

            <div className="ticket-detail__tab v-align">
              {
                this.tabs.map(tab => {
                  const tabItemClasName = classnames(
                    'ticket-detail__tab-item v-align',
                    {'ticket-detail__tab-item--active': tab.value === selectedTab}
                  );

                  return (
                    <div key={tab.value} className={tabItemClasName} onClick={() => this.onSelectTab('selectedTab', tab.value)}>
                      {
                        tab.icon && <Icon icon={tab.icon} />
                      }
                      <span></span>
                      {tab.label}
                    </div>
                  );
                })
              }
            </div>

            <div className="ticket-detail__list">
              {
                (selectedTab === 'all' || selectedTab === 'answers') && ticket.answers.map((answer, index) => (
                  <Card
                    key={index}
                    type="answer"
                    role={this.getRole(ticket)}
                    data={answer}
                  />
                ))
              }

              {
                (selectedTab === 'all' || selectedTab === 'notes') && ticket.notes.map((note, index) => (
                  <Card
                    key={index}
                    type="note"
                    role={this.getRole(ticket)}
                    data={note}
                  />
                ))
              }

            </div>

            <Write
              tabsInput={this.tabsInput}
              selectedTabInput={selectedTabInput}
              onSelectTab={this.onSelectTab}
              writeValue={writeValue}
              isSending={isSending}
              setFileElement={this.setFileElement}
              onFileChange={this.onFileChange}
              onInputChange={this.onInputChange}
              file={file}
              saveComunication={this.saveComunication}
            />
          </div>
        </div>

        <Sidebar
          ticket={ticket}
          getTeam={getTeam}
          addTeam={this.addTeam}
          removeTeam={this.removeTeam}
          onSelectChange={this.onSelectChange}
        />
      </div>
    );
  }
}

const mapStateToProps = ({ user }) => {
  return {
    user
  };
};

export default connect(mapStateToProps, {
  getTeam: userActions.getTeam,
  saveTicket: supportActions.saveTicket,
  updateTicket: supportActions.updateTicket,
  getTicket: supportActions.getTicket,
  createAnswer: supportActions.createAnswer,
  createNote: supportActions.createNote,
  showNotificationPopup: notificationPopupActions.showNotificationPopup
})(TicketDetail);

