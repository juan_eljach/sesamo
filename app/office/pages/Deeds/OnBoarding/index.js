import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from "react-router-dom";
import classnames from 'classnames';

import InputField from 'core/components/InputField';
import Button from "core/components/Button";
import { getUnitiessByTowerAndFloor } from 'core/helpers/unities';
import * as unitiesActions from 'core/redux/unities/operations';


class OnBoarding extends Component {
  constructor(props) {
    super(props);
    this.state = {
      step: 1,
      towers: [],
      selectedTower: {}
    };
  }

  async componentDidMount () {
    const unities = await this.props.getUnities();

    const towers = Object.keys(getUnitiessByTowerAndFloor(unities)).map(t => {
      return { label: t.replace(/t/, "Torre "), value: Number(t.replace(/t/, "")) };
    });

    this.setState({towers, selectedTower: towers[0]});
  }

  onStepChange = (step) => {
    this.setState({step});
  }

  onSelectTower = (tower) => {
    this.setState({selectedTower: tower});
  }

  renderDots (current) {
    const steps = 2;

    return (
      <div className="step-indicator">
        {
          [...Array(2).keys()].map(step => {
            const stepIndicatorClassName = classnames(
              'step-indicator',
              { 'step-indicator--active': (step + 1) === current}
            );

            return <span key={step} className={stepIndicatorClassName}></span>;
          })
        }

      </div>
    );
  }

  render() {
    const { step, towers, selectedTower } = this.state;

    return <section className="onboarding">
        {step === 1 ? <div className="onboarding-select">
            <h4>Hola David!</h4>

            <p>¿Qué torre vas a escriturar de Villa paraíso?</p>

            <InputField
              value={selectedTower}
              type="select"
              options={towers}
              onChange={this.onSelectTower}
            />

            <div className="onboarding__button">
              <Button onClick={() => this.onStepChange(2)}>
                SIGUIENTE
              </Button>
            </div>

            {this.renderDots(1)}
          </div> : <div className="onboarding-confirm">
            <div className="onboarding-confirm__back">
              <Button onClick={() => this.onStepChange(1)}>
                <div className="v-align">
                  <i className="mdi mdi-arrow-left" />
                  <span>Atrás</span>
                </div>
              </Button>
            </div>

            <img src="/static/img/deeds-onboard.png" alt="" />

            <p>
              Estás a punto de iniciar el proceso de escrituracion para la
              Torre 2. <br/> El primer paso es generar y enviar notificaciones
              masivas a los clientes alertando del inicio de dicho proceso.
              Da click a continuació para iniciar
            </p>

            <div className="onboarding__button">
              <Link to={`/deeds/send-notification/${selectedTower.value}`}>
                <Button>INICIAR PROCESO</Button>
              </Link>
            </div>

            {this.renderDots(2)}
          </div>}
      </section>;
  }
}

export default connect(null, {
  getUnities: unitiesActions.getUnities
})(OnBoarding);
