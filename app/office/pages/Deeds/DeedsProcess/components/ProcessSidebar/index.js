import React, { Component } from 'react';
import classnames from 'classnames';

import Initials from 'core/components/Initials';
import Progress from 'core/components/Progress';
import { tasksList } from 'core/data/deeds';

class ProcessSidebar extends Component {
  constructor (props) {
    super(props);
    this.state = {  }
  }
  render () {
    const { person, unity, currentStep, completedSteps, onSelectStep } = this.props;
    const percentage = String((completedSteps.length * 100) / tasksList.length).slice(0, 2);

    return (
      <div className="process-sidebar">
        <div className="process-sidebar__client">
          <Initials initials={`${person.first_name[0]} ${person.last_name[0]}`} />
          <div>{`${person.first_name} ${person.last_name}`}</div>
          <span>Apto {unity.name}</span>
        </div>

        <div className="process-sidebar__client-info">
          <div>Información de contacto</div>

          <ul>
            <li className="v-align space-between">
              <span>Celular:</span>
              <span>{person.cellphone}</span>
            </li>
            <li className="v-align space-between">
              <span>Teléfono:</span>
              <span>{person.phone}</span>
            </li>
            <li className="v-align space-between">
              <span>Correo:</span>
              <span>{person.email}</span>
            </li>
          </ul>
        </div>

        <div className="process-sidebar__tab v-align space-between">
          <div className="process-sidebar__tab-item process-sidebar__tab-item--active">Tareas</div>
          <div className="process-sidebar__tab-item">Comentarios</div>
        </div>

        <div className="process-sidebar__tasks">
          <div className="process-sidebar__tasks-progress">
            <div className="v-align space-between">
              <div>Progreso</div>
              <div className="process-sidebar__tasks-progress-percent">{percentage}%</div>
            </div>
            <Progress
              percent={percentage}
            />
          </div>

          <div className="process-sidebar__tasks-list">
            <ul>
              {
                tasksList.map((task, index) => {
                  const isCompleted = completedSteps.includes(task.step);
                  const itemClassName = classnames(
                    'process-sidebar__tasks-item v-align',
                    { 'process-sidebar__tasks-item--active': currentStep === task.step },
                    { 'process-sidebar__tasks-item--completed': isCompleted }
                  );

                  return (
                    <li key={index} className={itemClassName} onClick={() => onSelectStep(task.step)}>
                      {
                        isCompleted && <i className="mdi mdi-check-circle"></i>
                      }
                      {
                        !isCompleted && <div className="process-sidebar__tasks-item-number v-align h-align">{task.step}</div>
                      }
                      <div className="process-sidebar__tasks-item-label">{task.label}</div>
                    </li>
                  );
                })
              }

              <li className="process-sidebar__tasks-finish v-align">
                <div className="process-sidebar__tasks-finish-circle v-align h-align">
                  <span></span>
                </div>
                <div className="process-sidebar__tasks-item-label">Finalización del proceso</div>
              </li>
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

export default ProcessSidebar;
