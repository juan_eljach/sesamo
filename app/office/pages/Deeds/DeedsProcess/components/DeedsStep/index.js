import React, { Component } from 'react';
import { connect } from 'react-redux';
import moment from 'moment';

import ProcessFrame from '../ProcessFrame';
import Checkbox from 'core/components/Checkbox';
import InputField from 'core/components/InputField';
import Icon from 'core/components/Icon';

import * as deedsActions from 'core/redux/deeds/operations';

class DeedsStep extends Component {
  constructor (props) {
    super(props);

    this.notaries = [
      {value: '', label: 'Seleccionar'},
      {value: 'notaria_piedecuesta', label: 'Notaria de Piedecuesta'},
      {value: 'notaria_tercera', label: 'Notaria Tercera'},
    ];

    this.notariesMap = {'notaria_piedecuesta': 'Notaria de Piedecuesta', 'notaria_tercera': 'Notaria Tercera'};

    this.state = {
      stepRequired: true,
      deed_sent_to_notary: false,
      deed_sent_to_notary_date: null,
      notary: this.notaries[0],
      notary_date_and_time_checking: false,
      notary_date_and_time_checking_date: null,
      deed_signed: false,
      deed_signed_date: null
    };
  }

  async componentDidMount () {
    const response = await this.props.getDeedsStep(this.props.tower, this.props.unity.slug);

    if (response) {
      this.setState({
        stepRequired: response.required,
        deed_sent_to_notary: response.deed_sent_to_notary,
        deed_sent_to_notary_date: response.deed_sent_to_notary_date ? moment(response.deed_sent_to_notary_date) : null,
        notary: {value: response.notary, label: this.notariesMap[response.notary]},
        notary_date_and_time_checking: response.notary_date_and_time_checking,
        notary_date_and_time_checking_date: response.notary_date_and_time_checking_date ? moment(response.notary_date_and_time_checking_date) : null,
        deed_signed: response.deed_signed,
        deed_signed_date: response.deed_signed_date ? moment(this.deed_signed_date) : null,
        pk: response.pk
      });
    }
  }

  onRequireChange = async () => {
    const { tower, unity } = this.props;

    this.setState({ stepRequired: !this.state.stepRequired });

    let payload = {
      unity: unity.id,
      required: !this.state.stepRequired,
      notary: this.state.notary.value,
    };

    let operation = this.state.pk ? 'put' : 'post';

    await this.props.saveDeedsStep(operation, tower, unity.slug, payload);
  }

  onCheckChange = (type) => {
    this.setState({[type]: !this.state[type]});
  }

  onDateChange = (type, date) => {
    this.setState({[type]: date});
  }

  onNotaryChange = (notary) => {
    console.log(notary);
    this.setState({notary});
  }

  onSave = async () => {
    this.setState({isFetching: true});

    let payload = {
      required: this.state.stepRequired,
      unity: this.props.unity.id,
      deed_sent_to_notary: this.state.deed_sent_to_notary,
      deed_sent_to_notary_date: this.state.deed_sent_to_notary_date && this.state.deed_sent_to_notary_date.format('YYYY-MM-DD'),
      notary: this.state.notary.value,
      notary_date_and_time_checking: this.state.notary_date_and_time_checking,
      notary_date_and_time_checking_date: this.state.notary_date_and_time_checking_date && this.state.notary_date_and_time_checking_date.format('YYYY-MM-DD'),
      deed_signed: this.state.deed_signed,
      deed_signed_date: this.state.deed_signed_date && this.state.deed_signed_date.format('YYYY-MM-DD')
    };

    let operation = this.state.pk ? 'put' : 'post';

    const response = await this.props.saveDeedsStep(operation, this.props.tower, this.props.unity.slug, payload);

    if (response) {
      this.setState({pk: response.pk, isFetching: false});

      this.props.showNotificationPopup({ message: 'La información se ha guardado correctamente!' });
    }
  }

  render () {
    const { stepRequired, isFetching } = this.state;
    const { currentStep, onSelectStep } = this.props;

    return (
      <ProcessFrame
        stepText="Orden de escrituración"
        stepNumber={currentStep}
        stepRequired={stepRequired}
        onNext={() => onSelectStep(currentStep + 1)}
        onBack={() => onSelectStep(currentStep - 1)}
        onRequireChange={this.onRequireChange}
        onSave={this.onSave}
        isSaving={isFetching}
      >

        <div className="bank-step deeds-process__container">
          <div className="deeds-info-container">
            <img src="/static/img/deeds-secondstep-order.png" width="170" />
            <p>
              En este paso tendrás que realizar las siguientes tareas y marcarlas
              a medida que las realices para poder avanzar.
            </p>
          </div>

          <div className="deeds-task-container">
            <h5>Tareas:</h5>

            <div className="deeds-task-item">
              <div className="deeds-task-item__check v-align">
                <Checkbox color="blue" checked={this.state.deed_sent_to_notary} onChange={() => this.onCheckChange('deed_sent_to_notary')} />
                <p>Envío de orden de escrituración a la Notaria.</p>
              </div>

              <div className="deeds-task-item__content v-align">
                <div className="deeds-process__icon-input">
                  <Icon icon="Calendar" />
                  <InputField
                    value={this.state.deed_sent_to_notary_date}
                    type="date"
                    onChange={(date) => this.onDateChange('deed_sent_to_notary_date', date)}
                    disabledFutureDate
                  />
                </div>

                <div className="deeds-process__select-input">
                  <InputField
                    value={this.state.notary}
                    type="select"
                    options={this.notaries}
                    onChange={this.onNotaryChange}
                  />
                </div>
              </div>
            </div>

            <div className="deeds-task-item">
              <div className="deeds-task-item__check v-align">
                <Checkbox color="blue" checked={this.state.notary_date_and_time_checking} onChange={() => this.onCheckChange('notary_date_and_time_checking')} />
                <p>Revisión de fecha y hora estípulada por la Notaría para que el cliente firme.</p>
              </div>

              <div className="deeds-task-item__content">
                <div className="deeds-process__icon-input">
                  <Icon icon="Calendar" />
                  <InputField
                    type="date"
                    value={this.state.notary_date_and_time_checking_date}
                    onChange={(date) => this.onDateChange('notary_date_and_time_checking_date', date)}
                    disabledFutureDate
                  />
                </div>
              </div>
            </div>

            <div className="deeds-task-item">
              <div className="deeds-task-item__check v-align">
                <Checkbox color="blue" checked={this.state.deed_signed} onChange={() => this.onCheckChange('deed_signed')} />
                <p>Firma de la escritura por parte del cliente.</p>
              </div>

              <div className="deeds-task-item__content">
                <div className="deeds-process__icon-input">
                  <Icon icon="Calendar" />
                  <InputField
                    type="date"
                    value={this.state.deed_signed_date}
                    onChange={(date) => this.onDateChange('deed_signed_date', date)}
                    disabledFutureDate
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </ProcessFrame>
    );
  }
}

export default connect(null, {
  getDeedsStep: deedsActions.getDeedsStep,
  saveDeedsStep: deedsActions.saveDeedsStep
})(DeedsStep);
