import React, { Component } from 'react';

import ProcessFrame from '../ProcessFrame';
import Button from 'core/components/Button';

class OthersStep extends Component {
  constructor(props) {
    super(props);
    this.state = {
      stepRequired: true
    };
  }

  onRequireChange = () => {
    this.setState({ stepRequired: !this.state.stepRequired });
  }

  render () {
    const { stepRequired } = this.state;
    const { tower, unity, currentStep, onSelectStep } = this.props;

    return (
      <ProcessFrame
        stepText="Realización de Otrosí"
        stepNumber={currentStep}
        stepRequired={stepRequired}
        onNext={() => onSelectStep(currentStep + 1)}
        onBack={() => onSelectStep(currentStep - 1)}
        onRequireChange={this.onRequireChange}
      >

        <div className="others-step deeds-process__container">
          <div className="others-step__container">
            <img src="/static/img/deeds-secondstep-another.jpg" width="200" />
            <p>Te generamos dos documentos con la información necesaria para que completes el documento Otrosí:
              </p>

            <Button type="secondary">
              <a className="v-align" href={`/api/projects/villa-paraiso/deeds/tower/${tower}/unity/${unity.slug}/generate-otrosi/?document=payment_fees`}>
                <i className="mdi mdi-arrow-down"></i>
                <span>Descargar Informacion de Pagos</span>
              </a>
            </Button>

            <Button type="secondary">
              <a className="v-align" href={`/api/projects/villa-paraiso/deeds/tower/${tower}/unity/${unity.slug}/generate-otrosi/?document=data_table`}>
                <i className="mdi mdi-arrow-down"></i>
                <span>Descargar información del Acuerdo</span>
              </a>
            </Button>
          </div>
        </div>
      </ProcessFrame>
    );
  }
}

export default OthersStep;
