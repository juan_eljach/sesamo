import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import Modal from 'core/components/Modal';
import ProcessFrame from '../ProcessFrame';
import PaymentInfo from './components/PaymentInfo';
import PaymentPlan from './components/PaymentPlan';
import PaymentPlanEdit from './components/PaymentPlanEdit';

import * as formalizationActions from 'core/redux/formalization/operations';
import * as paymentsActions from 'core/redux/payments/operations';
import * as unitiesActions from 'core/redux/unities/operations';
import * as notificationPopupActions from 'core/redux/notificationPopup/actions';

class PaymentsStep extends Component {
	constructor (props) {
		super(props);
		this.state = {
			stepRequired: true,
			feesOpen: '',
			editOpen: false,
			initialFeeValue: 0,
			totalPaid: 0,
			creditValue: 0
		};

		this.unity = this.props.match.params.unity;
	}

	async componentDidMount () {
		const initialFeeValue = this.props.unity.price * this.props.project.initial_fee_percentage / 100;

		const totalPaid = this.props.paymentAgreement.payments.reduce((acc, current) => {
			return acc + current.amount;
		}, 0);

		const creditValue = this.props.unity.price - (initialFeeValue + this.props.project.booking_price);

		this.setState({ initialFeeValue, totalPaid, creditValue });
	}

	onRequireChange = () => {
		this.setState({ stepRequired: !this.state.stepRequired });
	};

	onFeesOpenChange = item => {
		this.setState(prevState => {
			if (prevState.feesOpen === item) {
				return { feesOpen: '' };
			}

			return { feesOpen: item };
		});
	};

	onToggleEdit = () => {
		this.setState({ editOpen: !this.state.editOpen });
	};

	updatePaymentAgreement = paymentAgreement => {
		this.setState(prevState => {
			return {
				...prevState,
				paymentAgreement
			};
		});
	};

	render() {
		const {
			stepRequired,
			feesOpen,
			editOpen,
			initialFeeValue,
			totalPaid,
			creditValue
		} = this.state;
		const { paymentAgreement, unity, currentStep, onSelectStep } = this.props;

		return (
			<ProcessFrame
				stepText="Verificación del Plan de pago"
				stepNumber={currentStep}
				stepRequired={stepRequired}
				unity={this.unity}
        extra={paymentAgreement.is_new_version && `Plan de Pagos (Modificado el: ${paymentAgreement.modification_date})`}
        onNext={() => onSelectStep(currentStep + 1)}
        onBack={() => onSelectStep(currentStep - 1)}
				onRequireChange={this.onRequireChange}
				onSave={this.onSavePayments}
			>
				<div className="payments-step deeds-process__container">
					<PaymentInfo
						currentUnity={unity}
						initialFeeValue={initialFeeValue}
						paymentAgreement={paymentAgreement}
						totalPaid={totalPaid}
						creditValue={creditValue}
					/>

					<PaymentPlan
						feesOpen={feesOpen}
						paymentFees={paymentAgreement.payment_fees}
						onToggleEdit={this.onToggleEdit}
						onFeesOpenChange={this.onFeesOpenChange}
					/>

					<Modal open={editOpen}>
						<PaymentPlanEdit
							onToggleEdit={this.onToggleEdit}
							currentUnity={unity}
							initialFeeValue={initialFeeValue}
							paymentAgreement={paymentAgreement}
							totalPaid={totalPaid}
							creditValue={creditValue}
							project={this.props.project}
							savePaymentPlan={this.props.savePaymentPlan}
							updatePaymentAgreement={this.updatePaymentAgreement}
							showNotificationPopup={this.props.showNotificationPopup}
						/>
					</Modal>
				</div>
			</ProcessFrame>
		);
	}
}

const mapStateToProps = ({ project }) => {
	return {
		project
	};
};

export default withRouter(
	connect(mapStateToProps, {
		getPaymentAgreementsDetail: paymentsActions.getPaymentAgreementsDetail,
		searchUnities: unitiesActions.searchUnities,
		savePaymentPlan: formalizationActions.savePaymentPlan,
		showNotificationPopup: notificationPopupActions.showNotificationPopup
	})(PaymentsStep)
);
