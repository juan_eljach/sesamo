import React, { Component } from 'react';
import Collapsible from 'react-collapsible';

import Table, { TableHead, TableBody, TableRow, TableCell } from 'core/components/Table';
import formatValue from 'core/helpers/formatValue';

class PaymentPlan extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}
	render() {
		const { feesOpen, paymentFees, onFeesOpenChange, onToggleEdit } = this.props;

		const paidFees = paymentFees.filter(f => f.paid);
		const unpaidFees = paymentFees.filter(f => !f.paid);

		return (
			<div className="payments-step-plan">
				<div className="payments-step-plan__header v-align space-between">
					<div className="payments-step-plan__header-title">Plan de Pago</div>

					<div className="payments-step-plan__header-edit v-align" onClick={onToggleEdit}>
						<i className="mdi mdi-pencil" />
						<span>Modificar plan de pago</span>
					</div>
				</div>

				<Collapsible
					open={feesOpen === 'canceled'}
					trigger={
						<div
							className="payments-step-plan__trigger space-between"
							onClick={() => onFeesOpenChange('canceled')}
						>
							<div className="payments-step-plan__trigger-label payments-step-plan__trigger-label--canceled">
								<i className="mdi mdi-arrow-down-bold-circle" />
								<span>Cuotas canceladas: {paidFees.length}</span>
							</div>

							{feesOpen === 'canceled' ? (
								<i className="mdi mdi-chevron-up" />
							) : (
								<i className="mdi mdi-chevron-down" />
							)}
						</div>
					}
					triggerDisabled
					transitionTime={200}
				>
					<div className="payments-step-plan__content">
						<Table type="data-grid">
							<TableHead>
								<TableRow>
									<TableCell># cuota</TableCell>
									<TableCell>Fecha</TableCell>
									<TableCell>Valor cuota</TableCell>
									<TableCell>Cuota extraordinaria</TableCell>
									<TableCell>Saldo pendiente</TableCell>
								</TableRow>
							</TableHead>
							<TableBody>
								{paidFees.map((fee, index) => {
									const feeNumber = index + 1;
									return (
										<TableRow key={feeNumber}>
											<TableCell>{feeNumber}</TableCell>
											<TableCell>{fee.date_to_pay}</TableCell>
											<TableCell>${formatValue(fee.fee)}</TableCell>
											<TableCell>${formatValue(fee.extraordinary_fee)}</TableCell>
											<TableCell>${formatValue(fee.debt)}</TableCell>
										</TableRow>
									);
								})}
							</TableBody>
						</Table>
					</div>
				</Collapsible>

				<Collapsible
					open={feesOpen === 'nocanceled'}
					triggerDisabled
					trigger={
						<div
							className="payments-step-plan__trigger space-between"
							onClick={() => onFeesOpenChange('nocanceled')}
						>
							<div className="payments-step-plan__trigger-label payments-step-plan__trigger-label--no-canceled">
								<i className="mdi mdi-close-circle" />
								<span>Cuotas sin cancelar: {unpaidFees.length}</span>
							</div>

							{feesOpen === 'nocanceled' ? (
								<i className="mdi mdi-chevron-up" />
							) : (
								<i className="mdi mdi-chevron-down" />
							)}
						</div>
					}
					transitionTime={200}
				>
					<div className="payments-step-plan__content">
						<Table type="data-grid">
							<TableHead>
								<TableRow>
									<TableCell># cuota</TableCell>
									<TableCell>Fecha</TableCell>
									<TableCell>Valor cuota</TableCell>
									<TableCell>Cuota extraordinaria</TableCell>
									<TableCell>Saldo pendiente</TableCell>
								</TableRow>
							</TableHead>
							<TableBody>
								{unpaidFees.map((fee, index) => {
									const feeNumber = index + 1;
									return (
										<TableRow key={feeNumber}>
											<TableCell>{feeNumber}</TableCell>
											<TableCell>{fee.date_to_pay}</TableCell>
											<TableCell>${formatValue(fee.fee)}</TableCell>
											<TableCell>${formatValue(fee.extraordinary_fee)}</TableCell>
											<TableCell>${formatValue(fee.debt)}</TableCell>
										</TableRow>
									);
								})}
							</TableBody>
						</Table>
					</div>
				</Collapsible>
			</div>
		);
	}
}

export default PaymentPlan;
