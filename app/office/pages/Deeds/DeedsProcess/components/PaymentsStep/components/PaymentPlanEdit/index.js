import React, { Component } from 'react';
import moment from 'moment';

import PaymentInfo from '../PaymentInfo';
import InputField from 'core/components/InputField';
import Table, { TableHead, TableBody, TableRow, TableCell } from 'core/components/Table';
import Button from 'core/components/Button';
import formatValue from 'core/helpers/formatValue';

class PaymentPlanEdit extends Component {
	constructor(props) {
		super(props);
		this.state = {
			otherPayments: {
				scheduledSavings: 0,
				severance: 0,
				housingAllowance: 0,
				others: 0
			},
			applyToCI: ['others'],
			numberFees: null,
			newFees: {
				1: { date: null, value: 0 }
			},
			isFetching: false
		};

		this.numberFees = [...Array(15).keys()].map(v => {
			return { value: v + 1, label: v + 1 };
		});
	}

	onOtherPaymentsChange = (type, value) => {
		this.setState(
			prevState => {
				return {
					...prevState,
					otherPayments: {
						...prevState.otherPayments,
						[type]: value
					}
				};
			},
			() => {
				if (this.state.applyToCI.includes(type)) {
					const fees = this.getFees(this.state, moment(this.state.newFees[1].date));

					this.setState(prevState => {
						return {
							...prevState,
							newFees: fees
						};
					});
				}
			}
		);
	};

	onNumberFeesChange = v => {
		this.setState(
			prevState => {
				return {
					...prevState,
					numberFees: v.value
				};
			},
			() => {
				const firstDate = this.state.newFees[1].date;

				if (firstDate) {
					const fees = this.getFees(this.state, firstDate);

					this.setState({ newFees: fees });
				}
			}
		);
	};

	onApplyToCIChange = value => {
		this.setState(
			prevState => {
				if (prevState.applyToCI.includes(value)) {
					return {
						...prevState,
						applyToCI: prevState.applyToCI.filter(i => i !== value)
					};
				}

				return {
					...prevState,
					applyToCI: [...prevState.applyToCI, value]
				};
			},
			() => {
				if (this.state.newFees[1].date) {
					const fees = this.getFees(this.state, moment(this.state.newFees[1].date));

					this.setState(prevState => {
						return {
							...prevState,
							newFees: fees
						};
					});
				}
			}
		);
	};

	getFees = (prevState, firstDate) => {
		const otherPaymentsAppliedToCI = Object.keys(prevState.otherPayments).reduce((acc, current) => {
			if (prevState.applyToCI.includes(current)) {
				return acc + Number(prevState.otherPayments[current]);
			}

			return acc;
		}, 0);

		const initialFeeValue =
			Number(this.props.initialFeeValue) - Number(otherPaymentsAppliedToCI) - this.props.totalPaid;

		const feeValue = Math.ceil(initialFeeValue / prevState.numberFees);

		const feeList = [...Array(prevState.numberFees).keys()];

		const prevDate = firstDate.clone();

		const fees = feeList.reduce((acc, current) => {
			const currentNumber = current + 1;
			const date = currentNumber === 1 ? firstDate : moment(prevDate.add(1, 'M').format('YYYY-MM-DD'));

			return {
				...acc,
				[currentNumber]: {
					...prevState.otherPayments[currentNumber],
					value: feeValue,
					date
				}
			};
		}, {});

		return fees;
	};

	onFeeDateChange = firstDate => {
		this.setState(prevState => {
			const fees = this.getFees(prevState, firstDate);

			return {
				...prevState,
				newFees: fees
			};
		});
	};

	onSavePaymentAgreement = async () => {
		const { numberFees, otherPayments, applyToCI, newFees } = this.state;

		const otherPaymentsMap = {
			scheduledSavings: 'ahorro-programado',
			severance: 'cesantias',
			housingAllowance: 'subsidio-vivienda',
			others: 'otro-pago-acordado'
		};

		const feeList = [...Array(numberFees).keys()];

		const payload = {
			paid: true,
			persons: this.props.paymentAgreement.persons.map(person => person.id),
			booking_price: this.props.project.booking_price,
			agreed_payments: Object.keys(otherPayments)
				.filter(oP => {
					return applyToCI.includes(oP);
				})
				.map(otherPayment => {
					return {
						type_of_payment: otherPaymentsMap[otherPayment],
						fee: otherPayment === 'others' ? this.props.totalPaid : Number(otherPayments[otherPayment]),
						applied_to_initialfee: applyToCI.includes(otherPayment)
					};
				}),
			payment_fees: feeList.reduce((acc, current, index) => {
				const fee = newFees[index + 1];

				return [...acc, { fee: fee.value, date_to_pay: fee.date.format('YYYY-MM-DD'), extraordinary_fee: 0 }];
			}, []),
			unity: this.props.currentUnity.id,
			initial_fee: this.props.initialFeeValue
		};

		this.setState({ isFetching: true });

		const response = await this.props.savePaymentPlan(payload);

		this.props.updatePaymentAgreement(response);

		this.props.showNotificationPopup({ message: 'El Plan de pago se ha guardado correctamente!' });

		this.setState({ isFetching: false });

		this.props.onToggleEdit();
	};

	render() {
		const { otherPayments, numberFees, firstDate, newFees, applyToCI, isFetching } = this.state;
		const { onToggleEdit, ...rest } = this.props;

		return (
			<div className="payments-step-edit">
				<div className="payments-step-edit__header-container">
					<div className="payments-step-edit__header">
						<h4>Modificar plan de Pago</h4>
						<i className="mdi mdi-close action-hover" onClick={onToggleEdit} />
					</div>

					<PaymentInfo
						{...rest}
						otherPayments={otherPayments}
						applyToCI={applyToCI}
						onOtherPaymentsChange={this.onOtherPaymentsChange}
						onApplyToCIChange={this.onApplyToCIChange}
						modify
					/>
				</div>

				<div className="payments-step-edit__plan">
					<h5>Nuevo Plan de Pago</h5>

					<div className="payments-step-edit__plan-container">
						<div className="payments-step-edit__plan-inputs">
							<InputField
								type="select"
								label="Número de cuotas"
								options={this.numberFees}
								onChange={this.onNumberFeesChange}
							/>
						</div>

						<div className="payments-step-edit__plan-table">
							<Table type="data-grid">
								<TableHead>
									<TableRow>
										<TableCell># cuota</TableCell>
										<TableCell>Fecha</TableCell>
										<TableCell>Valor cuota</TableCell>
									</TableRow>
								</TableHead>
								<TableBody>
									<TableRow>
										<TableCell>1</TableCell>
										<TableCell>
											<InputField
												type="date"
												value={newFees[1].date}
												onChange={this.onFeeDateChange}
												disabled={!numberFees}
												disabledPastDate
											/>
										</TableCell>
										<TableCell>$ {formatValue(newFees[1].value)}</TableCell>
									</TableRow>

									{Object.keys(newFees)
										.slice(1)
										.map(feeNumber => {
											const fee = newFees[feeNumber];

											return (
												<TableRow key={feeNumber}>
													<TableCell>{feeNumber}</TableCell>
													<TableCell>{fee.date.format('DD-MM-YYYY')}</TableCell>
													<TableCell>$ {formatValue(fee.value)}</TableCell>
												</TableRow>
											);
										})}
								</TableBody>
							</Table>
						</div>
					</div>

					<div className="h-align">
						<Button onClick={this.onSavePaymentAgreement} isFetching={isFetching}>
							GUARDAR
						</Button>
					</div>
				</div>
			</div>
		);
	}
}

export default PaymentPlanEdit;
