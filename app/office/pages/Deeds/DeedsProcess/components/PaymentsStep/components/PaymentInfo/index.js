import React, { Component } from 'react';

import InputField from 'core/components/InputField';
import Checkbox from 'core/components/Checkbox';
import OthersStep from '../../../OthersStep/index';
import formatValue from 'core/helpers/formatValue';

class PaymentInfo extends Component {
	constructor (props) {
		super(props);
		this.state = {};
	}

	render () {
		const {
			otherPayments,
			onOtherPaymentsChange,
			modify,
			currentUnity,
			initialFeeValue,
			paymentAgreement,
			totalPaid,
			creditValue,
			applyToCI,
			onApplyToCIChange
		} = this.props;

		const prevPaymentAgreement = {
			scheduledSavings: paymentAgreement.agreed_payments.find(p => p.type_of_payment === 'ahorro-programado'),
			severance: paymentAgreement.agreed_payments.find(p => p.type_of_payment === 'cesantias'),
			housingAllowance: paymentAgreement.agreed_payments.find(p => p.type_of_payment === 'subsidio-vivienda'),
			others: paymentAgreement.agreed_payments.find(p => p.type_of_payment === 'otro-pago-acordado')
		};

		return (
			<div className="payments-step-info">
				<div className="payments-step-info__type">
					<h4>Datos generales</h4>
					<div className="payments-step-info__container">
						<div className="payments-step-info__item">
							<div>Valor del apto</div>
							<div>${formatValue(currentUnity.price)}</div>
						</div>

						<div className="payments-step-info__item">
							<div>Valor de la cuota inicial</div>
							<div>${formatValue(initialFeeValue)}</div>
						</div>

						<div className="payments-step-info__item">
							<div>Pagos realizados a la fecha</div>
							<div>${formatValue(totalPaid)}</div>
						</div>

						<div className="payments-step-info__item">
							<div>Valor del crédito</div>
							<div>${formatValue(creditValue)}</div>
						</div>
					</div>
				</div>

				{modify ? (
					<div className="payments-step-info__type">
						<h4>Otros pagos</h4>
						<div className="payments-step-info__container">
							<div className="payments-step-info__item">
								<div>Cesantías:</div>
								<div>
									<div>
										Valor anterior:{' '}
										{prevPaymentAgreement.severance
											? formatValue(prevPaymentAgreement.severance.fee)
											: 0}
									</div>
									<span>
										{prevPaymentAgreement.severance &&
										prevPaymentAgreement.severance.applied_to_initialfee
											? 'Aplicado a CI'
											: 'No Aplicado a CI'}
									</span>
								</div>

								<div className="payment-summary__others-item">
									<div className="payment-summary__others-item-field v-align">
										<Checkbox
											style="square"
											checked={applyToCI.includes('severance')}
											disabled={false}
											onChange={event => onApplyToCIChange('severance')}
										/>
										<InputField
											type="number"
											value={otherPayments.severance}
											onChange={event => onOtherPaymentsChange('severance', event.target.value)}
											disabled={false}
											withPriceIcon
										/>
									</div>
								</div>
							</div>

							<div className="payments-step-info__item">
								<div>Subsidio:</div>
								<div>
									<div>
										Valor anterior:{' '}
										{prevPaymentAgreement.housingAllowance
											? formatValue(prevPaymentAgreement.housingAllowance.fee)
											: 0}
									</div>
									<span>
										{prevPaymentAgreement.housingAllowance &&
										prevPaymentAgreement.housingAllowance.applied_to_initialfee
											? 'Aplicado a CI'
											: 'No Aplicado a CI'}
									</span>
								</div>
								<div className="payment-summary__others-item">
									<div className="payment-summary__others-item-field v-align">
										<Checkbox
											style="square"
											checked={applyToCI.includes('housingAllowance')}
											disabled={false}
											onChange={event => onApplyToCIChange('housingAllowance')}
										/>
										<InputField
											type="number"
											value={otherPayments.housingAllowance}
											onChange={event =>
												onOtherPaymentsChange('housingAllowance', event.target.value)}
											disabled={false}
											withPriceIcon
										/>
									</div>
								</div>
							</div>

							<div className="payments-step-info__item">
								<div>Ahorros:</div>
								<div>
									<div>
										Valor anterior:{' '}
										{prevPaymentAgreement.scheduledSavings
											? formatValue(prevPaymentAgreement.scheduledSavings.fee)
											: 0}
									</div>
									<span>
										{prevPaymentAgreement.scheduledSavings &&
										prevPaymentAgreement.scheduledSavings.applied_to_initialfee
											? 'Aplicado a CI'
											: 'No Aplicado a CI'}
									</span>
								</div>
								<div className="payment-summary__others-item">
									<div className="payment-summary__others-item-field v-align">
										<Checkbox
											style="square"
											checked={applyToCI.includes('scheduledSavings')}
											disabled={false}
											onChange={event => onApplyToCIChange('scheduledSavings')}
										/>
										<InputField
											type="number"
											value={otherPayments.scheduledSavings}
											onChange={event =>
												onOtherPaymentsChange('scheduledSavings', event.target.value)}
											disabled={false}
											withPriceIcon
										/>
									</div>
								</div>
							</div>

							<div className="payments-step-info__item">
								<div>Otros:</div>
								<div>
									<div>
										Valor anterior:{' '}
										{prevPaymentAgreement.others ? formatValue(prevPaymentAgreement.others.fee) : 0}
									</div>
									<span>
										{prevPaymentAgreement.others &&
										prevPaymentAgreement.others.applied_to_initialfee
											? 'Aplicado a CI'
											: 'No Aplicado a CI'}
									</span>
								</div>
								<div className="payment-summary__others-item">
									<div className="payment-summary__others-item-field v-align">
										<Checkbox
											style="square"
											checked={applyToCI.includes('others')}
											disabled={true}
										/>
										<InputField
											type="number"
											value={totalPaid}
											disabled={true}
											withPriceIcon
										/>
									</div>
								</div>
							</div>
						</div>
					</div>
				) : (
					<div className="payments-step-info__type">
						<h4>Otros pagos (Modificado)</h4>
						<div className="payments-step-info__container">
							<div className="payments-step-info__item">
								<div>
									Cesantías: <span>Aplicado a CI</span>
								</div>
								<div className="payments-step-info__fee-label payments-step-info__fee-label-canceled v-align">
									<i className="mdi mdi-arrow-down-bold-circle" />
									<span>Valor cancelado</span>
								</div>
								<div>
									${prevPaymentAgreement.severance
										? formatValue(
												prevPaymentAgreement.severance.fee - prevPaymentAgreement.severance.debt
											)
										: 0}
								</div>
								<div className="payments-step-info__fee-label payments-step-info__fee-label-nocanceled v-align">
									<i className="mdi mdi-close-circle" />
									<span>Valor sin cancelar</span>
								</div>
								<div>
									${prevPaymentAgreement.severance
										? formatValue(prevPaymentAgreement.severance.debt)
										: 0}
								</div>
							</div>

							<div className="payments-step-info__item">
								<div>
									Subsidio: <span>Aplicado a CI</span>
								</div>
								<div className="payments-step-info__fee-label payments-step-info__fee-label-canceled v-align">
									<i className="mdi mdi-arrow-down-bold-circle" />
									<span>Valor cancelado</span>
								</div>
								<div>
									${prevPaymentAgreement.housingAllowance
										? formatValue(
												prevPaymentAgreement.housingAllowance.fee -
													prevPaymentAgreement.housingAllowance.debt
											)
										: 0}
								</div>
								<div className="payments-step-info__fee-label payments-step-info__fee-label-nocanceled v-align">
									<i className="mdi mdi-close-circle" />
									<span>Valor sin cancelar</span>
								</div>
								<div>
									${prevPaymentAgreement.housingAllowance
										? formatValue(prevPaymentAgreement.housingAllowance.debt)
										: 0}
								</div>
							</div>

							<div className="payments-step-info__item">
								<div>
									Ahorros: <span>No Aplicado a CI</span>
								</div>
								<div className="payments-step-info__fee-label payments-step-info__fee-label-canceled v-align">
									<i className="mdi mdi-arrow-down-bold-circle" />
									<span>Valor cancelado</span>
								</div>
								<div>
									${prevPaymentAgreement.scheduledSavings
										? formatValue(
												prevPaymentAgreement.scheduledSavings.fee -
													prevPaymentAgreement.scheduledSavings.debt
											)
										: 0}
								</div>
								<div className="payments-step-info__fee-label payments-step-info__fee-label-nocanceled v-align">
									<i className="mdi mdi-close-circle" />
									<span>Valor sin cancelar</span>
								</div>
								<div>
									${prevPaymentAgreement.housingAllowance
										? formatValue(prevPaymentAgreement.housingAllowance.debt)
										: 0}
								</div>
							</div>

							<div className="payments-step-info__item">
								<div>
									Otros: <span>Aplicado a CI</span>
								</div>
								<div className="payments-step-info__fee-label payments-step-info__fee-label-canceled v-align">
									<i className="mdi mdi-arrow-down-bold-circle" />
									<span>Valor cancelado</span>
								</div>
								<div>
									${prevPaymentAgreement.others
										? formatValue(
												prevPaymentAgreement.others.fee - prevPaymentAgreement.others.debt
											)
										: 0}
								</div>
								<div className="payments-step-info__fee-label payments-step-info__fee-label-nocanceled v-align">
									<i className="mdi mdi-close-circle" />
									<span>Valor sin cancelar</span>
								</div>
								<div>
									${prevPaymentAgreement.others ? formatValue(prevPaymentAgreement.others.debt) : 0}
								</div>
							</div>
						</div>
					</div>
				)}
			</div>
		);
	}
}

export default PaymentInfo;
