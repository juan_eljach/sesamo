import React, { Component } from 'react';
import { connect } from 'react-redux';
import moment from 'moment';

import ProcessFrame from '../ProcessFrame';
import Checkbox from 'core/components/Checkbox';
import InputField from 'core/components/InputField';
import Icon from 'core/components/Icon';

import { mediumChoicesMap } from 'core/data/payments';
import * as deedsActions from 'core/redux/deeds/operations';

class BankStep extends Component {
	constructor(props) {
		super(props);
		this.state = {
			stepRequired: true,
			documentsSentToBank: false,
			clientIsOkWithBank: false,
			sentToBankDate: null,
			isOkWithBankDate: null,
			isFetching: false
		};
	}

	async componentDidMount() {
		const bankResponse = await this.props.getBankStep(this.props.tower, this.props.unity.slug);
		const creditResponse = await this.props.getCredit(this.props.tower, this.props.unity.slug);

		if (bankResponse) {
			this.setState({
				stepRequired: bankResponse.required,
				documentsSentToBank: bankResponse.documents_sent_to_bank,
				clientIsOkWithBank: bankResponse.client_is_ok_with_bank,
				sentToBankDate: bankResponse.documents_sent_to_bank_date
					? moment(bankResponse.documents_sent_to_bank_date)
					: null,
				isOkWithBankDate: bankResponse.client_is_ok_with_bank_date
					? moment(bankResponse.client_is_ok_with_bank_date)
					: null,
				bank: mediumChoicesMap[creditResponse.bank],
				pk: bankResponse.pk
			});
		}
	}

	onRequireChange = async () => {
		const { tower, unity } = this.props;

		this.setState({ stepRequired: !this.state.stepRequired });

		let payload = {
			unity: unity.id,
			required: !this.state.stepRequired
		};

		let operation = this.state.pk ? 'put' : 'post';

		await this.props.saveBankStep(operation, tower, unity.slug, payload);
	};

	onDateChange = (type, date) => {
		this.setState({ [type]: date });
	};

	onCheckChange = type => {
		this.setState({ [type]: !this.state[type] });
	};

	onSave = async () => {
		const { documentsSentToBank, sentToBankDate, clientIsOkWithBank, isOkWithBankDate } = this.state;
		const { tower, unity } = this.props;

		this.setState({ isFetching: true });

		let payload = {
			required: this.state.stepRequired,
			unity: unity.id,
			documents_sent_to_bank: documentsSentToBank,
			documents_sent_to_bank_date: sentToBankDate && sentToBankDate.format('YYYY-MM-DD'),
			client_is_ok_with_bank: clientIsOkWithBank,
			client_is_ok_with_bank_date: isOkWithBankDate && isOkWithBankDate.format('YYYY-MM-DD')
		};

		let operation = this.state.pk ? 'put' : 'post';

		const response = await this.props.saveBankStep(operation, tower, unity.slug, payload);

		this.setState({ pk: response.pk, isFetching: false });

		this.props.showNotificationPopup({ message: 'La información se ha guardado correctamente!' });
	};

	render() {
		const {
			stepRequired,
			sentToBankDate,
			isOkWithBankDate,
			documentsSentToBank,
			clientIsOkWithBank,
			bank,
			isFetching
		} = this.state;
		const { currentStep, onSelectStep } = this.props;

		return (
			<ProcessFrame
				stepText="Confirmación con Banco"
				stepNumber={currentStep}
				stepRequired={stepRequired}
				onNext={() => onSelectStep(currentStep + 1)}
				onBack={() => onSelectStep(currentStep - 1)}
				onRequireChange={this.onRequireChange}
				onSave={this.onSave}
				isSaving={isFetching}
			>
				<div className="bank-step deeds-process__container">
					<div className="deeds-info-container">
						<img src="/static/img/deeds-secondstep-bank.png" width="200" />
						<p>
							En este paso tendrás que realizar las siguientes tareas y marcarlas a medida que las
							realices para poder avanzar.
						</p>
					</div>

					<div className="deeds-task-container">
						<h5>Tareas:</h5>

						<div className="deeds-task-item">
							<div className="deeds-task-item__check v-align">
								<Checkbox
									color="blue"
									checked={documentsSentToBank}
									onChange={() => this.onCheckChange('documentsSentToBank')}
								/>
								<p>
									Envío de promesa de compraventa, certificado del crédito y fotocopia de la cédula al
									Banco <span>{bank}</span>.
								</p>
							</div>

							<div className="deeds-task-item__content">
								<div className="deeds-process__icon-input">
									<Icon icon="Calendar" />
									<InputField
										value={sentToBankDate}
										type="date"
										onChange={date => this.onDateChange('sentToBankDate', date)}
										disabledFutureDate
									/>
								</div>
							</div>
						</div>

						<div className="deeds-task-item">
							<div className="deeds-task-item__check v-align">
								<Checkbox
									color="blue"
									checked={clientIsOkWithBank}
									onChange={() => this.onCheckChange('clientIsOkWithBank')}
								/>
								<p>
									El cliente se encuentra a paz y salvo con el Banco <span>{bank}</span>.
								</p>
							</div>

							<div className="deeds-task-item__content">
								<div className="deeds-process__icon-input">
									<Icon icon="Calendar" />
									<InputField
										value={isOkWithBankDate}
										type="date"
										onChange={date => this.onDateChange('isOkWithBankDate', date)}
										disabledFutureDate
									/>
								</div>
							</div>
						</div>
					</div>
				</div>
			</ProcessFrame>
		);
	}
}

export default connect(null, {
	getBankStep: deedsActions.getBankStep,
	saveBankStep: deedsActions.saveBankStep,
	getCredit: deedsActions.getCredit
})(BankStep);
