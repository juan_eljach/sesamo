import React, { Component } from 'react';

import Switch from 'core/components/Switch';
import Button from 'core/components/Button/index';

class ProcessFrame extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	render() {
		const { children, stepNumber, stepText, extra, onSave, isSaving, onNext, onBack, stepRequired, onRequireChange } = this.props;

		return (
			<div className="process-frame">
				<div className="process-frame__header v-align">
					<div className="process-frame__number-step h-align v-align">{stepNumber}</div>
					<p>{stepText}</p>

					<div className="process-frame__require v-align">
						<Switch checked={stepRequired} onChange={onRequireChange} />
						<span>{stepRequired ? 'Requerido' : 'No Requerido'}</span>
					</div>

          {extra && <div className="process-frame__extra">{extra}</div>}
				</div>

				<div className="process-frame__content">
					{stepRequired ? (
						children
					) : (
						<div className="process-frame__require-message">
							<img src="/static/img/deeds-secondstep-norequired.png" alt="" />
							<p>
								Este paso está marcado como no requerido, si lo requieres sólo cambia al modo activado
							</p>
						</div>
					)}
				</div>

				<div className="process-frame__footer v-align space-between">
					<a className="v-align" onClick={onBack}>
						<i className="mdi mdi-arrow-left" />
						<span>Ir al paso anterior</span>
					</a>
					<Button onClick={onSave} isFetching={isSaving}>
						GUARDAR
					</Button>

					<a className="v-align" onClick={onNext}>
						<span>Ir al siguiente paso</span>
						<i className="mdi mdi-arrow-right" />
					</a>
				</div>
			</div>
		);
	}
}

export default ProcessFrame;
