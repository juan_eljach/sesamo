import React, { Component } from 'react';
import moment from 'moment';
import { withRouter } from 'react-router-dom';

import ProcessFrame from '../ProcessFrame';
import InputField from 'core/components/InputField';
import Button from 'core/components/Button';
import Icon from 'core/components/Icon';
import { mediumChoicesMap } from 'core/data/payments';

class CreditStep extends Component {
	constructor(props) {
		super(props);
		this.state = {
			stepRequired: true,
			selectedDocument: {},
			expeditionDate: null,
			bankValue: '',
			creditValue: 0,
			isSaving: false
		};

		this.banksList = Object.keys(mediumChoicesMap).map(b => ({ value: b, label: mediumChoicesMap[b] }));
	}

	async componentDidMount() {
		const response = await this.props.getCredit(this.props.tower, this.props.unity.slug);

		if (response) {
			this.setState({
				pk: response.pk,
				stepRequired: response.required,
				selectedDocument: { name: response.document.split('/')[5] },
				expeditionDate: moment(response.expedition_timestamp),
				bankValue: { value: response.bank, label: mediumChoicesMap[response.bank] },
				creditValue: response.amount
			});
		}
	}

	onRequireChange = async () => {
		const { tower, unity } = this.props;
		const { pk, stepRequired, selectedDocument, expeditionDate, bankValue, creditValue } = this.state;

		this.setState({ stepRequired: !this.state.stepRequired });

		if (pk) {
			let payload = {
				unity: unity.id,
				required: !this.state.stepRequired,
				expedition_timestamp: expeditionDate.format('YYYY-MM-DD'),
				amount: creditValue,
				bank: bankValue.value
			};

			this.props.updateCredit(tower, unity.slug, payload);
		}
	};

	onDocumentChange = event => {
		this.setState({ selectedDocument: event.target.files[0] });
	};

	onDateChange = date => {
		this.setState({ expeditionDate: date });
	};

	onBankChange = bank => {
		this.setState({ bankValue: bank });
	};

	onValueChange = event => {
		this.setState({ creditValue: event.target.value });
	};

	saveCredit = async () => {
		const { pk, stepRequired, selectedDocument, expeditionDate, bankValue, creditValue } = this.state;

		const { tower, unity } = this.props;

		let formData = new FormData(this.creditForm);

		formData.append('unity', unity.id);
		formData.append('amount', creditValue);
		formData.append('bank', bankValue.value);
		formData.append('expedition_timestamp', expeditionDate.format('YYYY-MM-DD'));
		formData.append('required', stepRequired);

		if (selectedDocument.type) {
			formData.append('document', selectedDocument);
		}

		this.setState({ isSaving: true });

		let response;

		if (pk) {
			response = await this.props.updateCredit(tower, unity.slug, formData);
		} else {
			response = await this.props.saveCredit(tower, unity.slug, formData);
		}

		this.setState({ isSaving: false });

		if (response) {
			this.props.showNotificationPopup({ message: 'La información se ha guardado correctamente!' });
		}
	};

	render() {
		const { stepRequired, selectedDocument, expeditionDate, bankValue, creditValue, isSaving } = this.state;
		const { currentStep, onSelectStep } = this.props;

		return (
			<ProcessFrame
				stepText="Certificado de aprobación del crédito"
				stepNumber={currentStep}
				stepRequired={stepRequired}
				onRequireChange={this.onRequireChange}
				onSave={this.saveCredit}
        onNext={() => onSelectStep(currentStep + 1)}
        onBack={() => this.props.history.push('/deeds')}
				isSaving={isSaving}
			>
				<div className="credit-step deeds-process__container">
					<form ref={el => (this.creditForm = el)}>
						<div className="credit-step__file">
							{selectedDocument.name ? (
								<div className="credit-step__selected-document v-align space-between">
									<div className="v-align">
										<Icon icon="PdfDocument" />
										<p>Certificado adjunto: {selectedDocument.name}</p>
									</div>

									<div>
										<Button type="secondary">
											<label htmlFor="creditFileInput">
												Cambiar certificado
												<input
													id="creditFileInput"
													type="file"
													ref={el => (this.inputFile = el)}
													onChange={this.onDocumentChange}
												/>
											</label>
										</Button>
									</div>
								</div>
							) : (
								<label htmlFor="creditFileInput" className="v-align">
									<Icon icon="CloudUp" />
									<a>Agregar certificado del crédito</a>
									<input
										id="creditFileInput"
										type="file"
										ref={el => (this.inputFile = el)}
										onChange={this.onDocumentChange}
									/>
								</label>
							)}
						</div>

						<div className="credit-step__inputs">
							<div className="deeds-process__icon-input">
								<Icon icon="Calendar" />

								<InputField
									value={expeditionDate}
									type="date"
									label="Fecha de expedición"
									placeholder="Fecha de expedición"
									disabledFutureDate
									onChange={this.onDateChange}
								/>
							</div>

							<div className="deeds-process__icon-input">
								<Icon icon="Bank" />

								<InputField
									value={bankValue}
									type="select"
									label="Banco del crédito"
									placeholder="Seleccionar"
									options={this.banksList}
									onChange={this.onBankChange}
								/>
							</div>

							<div className="deeds-process__icon-input">
								<Icon icon="Value" />

								<InputField
									value={creditValue}
									type="number"
									label="Valor"
									placeholder="Monto del crédito"
									onChange={this.onValueChange}
									showNumberIcon={false}
									withPriceIcon
								/>
							</div>
						</div>
					</form>
				</div>
			</ProcessFrame>
		);
	}
}

export default withRouter(CreditStep);
