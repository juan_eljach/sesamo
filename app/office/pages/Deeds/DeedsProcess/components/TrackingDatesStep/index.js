import React, { Component } from 'react';
import { connect } from 'react-redux';
import moment from 'moment';

import ProcessFrame from '../ProcessFrame';
import Checkbox from 'core/components/Checkbox';
import InputField from 'core/components/InputField';
import Icon from 'core/components/Icon';

import * as deedsActions from 'core/redux/deeds/operations';

class TrackingDatesStep extends Component {
  constructor (props) {
    super(props);
    this.state = {
      stepRequired: true,
      deed_signed_by_legal_representative: false,
      deed_signed_by_bank: false,
      deed_arrives_to_office: false,
      bank_cash_outlay: false,
      deed_signed_by_legal_representative_date: null,
      deed_signed_by_bank_date: null,
      deed_arrives_to_office_date: null,
      bank_cash_outlay_date: null
    };
  }

  async componentDidMount () {
    const response = await this.props.getTrackingStep(this.props.tower, this.props.unity.slug);

    if (response) {
      this.setState({
        deed_signed_by_legal_representative_date: response.deed_signed_by_legal_representative ? moment(response.deed_signed_by_legal_representative) : null,
        deed_signed_by_bank_date: response.deed_signed_by_bank ? moment(response.deed_signed_by_bank) : null,
        deed_arrives_to_office_date: response.deed_arrives_to_office ? moment(response.deed_arrives_to_office) : null,
        bank_cash_outlay_date: response.bank_cash_outlay ? moment(response.bank_cash_outlay) : null,
        pk: response.pk
      });
    }
  }

  onRequireChange = async () => {
    const { tower, unity } = this.props;

    this.setState({ stepRequired: !this.state.stepRequired });

    let payload = {
      unity: unity.id,
      required: !this.state.stepRequired
    };

    let operation = this.state.pk ? 'put' : 'post';

    await this.props.saveTrackingStep(operation, tower, unity.slug, payload);
  }

  onDateChange = (type, date) => {
    this.setState({[type]: date});
  }

  onCheckChange = (type) => {
    this.setState({[type]: !this.state[type]});
  }

  onSave = async () => {
    const { tower, unity } = this.props;

    this.setState({isFetching: true});

    let operation = this.state.pk ? 'put' : 'post';

    let payload = {
      unity: unity.id,
      deed_signed_by_legal_representative: this.state.deed_signed_by_legal_representative_date && this.state.deed_signed_by_legal_representative_date.format('YYYY-MM-DD'),
      deed_signed_by_bank: this.state.deed_signed_by_bank_date && this.state.deed_signed_by_bank_date.format('YYYY-MM-DD'),
      deed_arrives_to_office: this.state.deed_arrives_to_office_date && this.state.deed_arrives_to_office_date.format('YYYY-MM-DD'),
      bank_cash_outlay: this.state.bank_cash_outlay_date && this.state.bank_cash_outlay_date.format('YYYY-MM-DD')
    };

    const response = await this.props.saveTrackingStep(operation, tower, unity.slug, payload);

    if (response) {
      this.setState({pk: response.pk, isFetching: false});

      this.props.showNotificationPopup({ message: 'La información se ha guardado correctamente!' });
    }
  }

  render () {
    const { currentStep, onSelectStep } = this.props;
    const { stepRequired } = this.state;

    return (
      <ProcessFrame
        stepText="Seguimiento de fechas interno"
        stepNumber={currentStep}
        stepRequired={stepRequired}
        onBack={() => onSelectStep(currentStep - 1)}
        onRequireChange={this.onRequireChange}
        onSave={this.onSave}
        isSaving={this.state.isFetching}
      >

        <div className="bank-step deeds-process__container">
          <div className="deeds-info-container">
            <img src="/static/img/deeds-secondstep-datetracking.png" width="220" />
            <p>
              En este paso tendrás que realizar las siguientes tareas y marcarlas
              a medida que las realices para poder avanzar.
            </p>
          </div>

          <div className="deeds-task-container">
            <h5>Tareas:</h5>

            <div className="deeds-task-item">
              <div className="deeds-task-item__check v-align">
                <Checkbox color="blue" checked={this.state.deed_signed_by_legal_representative} onChange={() => this.onCheckChange('deed_signed_by_legal_representative')} />
                <p>Firma de escritura por parte del Representante Legal</p>
              </div>

              <div className="deeds-task-item__content v-align">
                <div className="deeds-process__icon-input">
                  <Icon icon="Calendar" />
                  <InputField
                    value={this.state.deed_signed_by_legal_representative_date}
                    type="date"
                    onChange={(date) => this.onDateChange('deed_signed_by_legal_representative_date', date)}
                    disabledFutureDate
                  />
                </div>
              </div>
            </div>

            <div className="deeds-task-item">
              <div className="deeds-task-item__check v-align">
                <Checkbox color="blue" checked={this.state.deed_signed_by_bank} onChange={() => this.onCheckChange('deed_signed_by_bank')} />
                <p>Firma de escritura por parte del Banco</p>
              </div>

              <div className="deeds-task-item__content">
                <div className="deeds-process__icon-input">
                  <Icon icon="Calendar" />
                  <InputField
                    value={this.state.deed_signed_by_bank_date}
                    type="date"
                    onChange={(date) => this.onDateChange('deed_signed_by_bank_date', date)}
                    disabledFutureDate
                  />
                </div>
              </div>
            </div>

            <div className="deeds-task-item">
              <div className="deeds-task-item__check v-align">
                <Checkbox color="blue" checked={this.state.deed_arrives_to_office_date} onChange={() => this.onCheckChange('deed_arrives_to_office_date')} />
                <p>Escrituras llegaron a la oficina</p>
              </div>

              <div className="deeds-task-item__content">
                <div className="deeds-process__icon-input">
                  <Icon icon="Calendar" />
                  <InputField
                    value={this.state.deed_arrives_to_office_date}
                    type="date"
                    onChange={(date) => this.onDateChange('deed_arrives_to_office_date', date)}
                    disabledFutureDate
                  />
                </div>
              </div>
            </div>

            <div className="deeds-task-item">
              <div className="deeds-task-item__check v-align">
                <Checkbox color="blue" checked={this.state.bank_cash_outlay_date} onChange={() => this.onCheckChange('bank_cash_outlay_date')} />
                <p>Desembolso del dinero del banco</p>
              </div>

              <div className="deeds-task-item__content">
                <div className="deeds-process__icon-input">
                  <Icon icon="Calendar" />
                  <InputField
                    value={this.state.bank_cash_outlay_date}
                    type="date"
                    onChange={(date) => this.onDateChange('bank_cash_outlay_date', date)}
                    disabledFutureDate
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </ProcessFrame>
    );
  }
}

export default connect(null, {
  getTrackingStep: deedsActions.getTrackingStep,
  saveTrackingStep: deedsActions.saveTrackingStep
})(TrackingDatesStep);
