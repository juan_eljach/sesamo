import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

// Steps
import ProcessSidebar from './components/ProcessSidebar';
import CreditStep from './components/CreditStep';
import PaymentsStep from './components/PaymentsStep';
import OthersStep from './components/OthersStep';
import BankStep from './components/BankStep';
import DeedsStep from './components/DeedsStep';
import TrackingDatesStep from './components/TrackingDatesStep';
import Loading from 'core/components/Loading';

import * as formalizationActions from 'core/redux/formalization/operations';
import * as deedsActions from 'core/redux/deeds/operations';
import * as unitiesActions from 'core/redux/unities/operations';
import * as notificationPopupActions from 'core/redux/notificationPopup/actions';

class DeedsProcess extends Component {
	constructor(props) {
		super(props);
		this.state = {
			currentStep: 1,
			completedSteps: [],
			unity: {},
			paymentAgreement: {
				persons: [],
				agreed_payments: [],
				payment_fees: []
			},
			pageLoaded: false
		};

		this.unitySplited = this.props.match.params.unity.split('-');
		this.tower = this.unitySplited[0][1];
	}

	async componentDidMount() {
		const unity = await this.props.getUnityByTowerAndName(this.tower, this.props.match.params.unity);
		const paymentAgreementResponse = await this.props.getPaymentPlansById(unity.id);
		const paymentAgreement = paymentAgreementResponse.results[0];

		this.setState({ unity, paymentAgreement, pageLoaded: true });
	}

	onSelectStep = step => {
		this.setState({ currentStep: step });
	};

	updateData = type => {};

	render() {
		const { unity, paymentAgreement, currentStep, completedSteps, pageLoaded } = this.state;

		const { saveCredit, getCredit, updateCredit, showNotificationPopup } = this.props;

		if (!pageLoaded) return <Loading />;

		return (
			<section className="deeds-process">
				<div className="deeds-process__main">
					<div className="deeds-process__header v-align">
						<Link to="/deeds">Todos los clientes</Link>
						<p> > Proceso de la {unity.slug.toUpperCase()}</p>
					</div>

					{currentStep === 1 && (
						<CreditStep
							tower={this.tower}
							unity={unity}
							currentStep={currentStep}
							onRequireChange={this.onRequireChange}
							onSelectStep={this.onSelectStep}
							saveCredit={saveCredit}
							getCredit={getCredit}
							updateCredit={updateCredit}
							showNotificationPopup={showNotificationPopup}
						/>
					)}

					{currentStep === 2 && (
						<PaymentsStep
							tower={this.tower}
							unity={unity}
							paymentAgreement={paymentAgreement}
							currentStep={currentStep}
							onSelectStep={this.onSelectStep}
							onRequireChange={this.onRequireChange}
							showNotificationPopup={showNotificationPopup}
						/>
					)}

					{currentStep === 3 && (
						<OthersStep
							tower={this.tower}
							unity={unity}
							currentStep={currentStep}
							onRequireChange={this.onRequireChange}
							onSelectStep={this.onSelectStep}
						/>
					)}

					{currentStep === 4 && (
						<BankStep
							currentStep={currentStep}
							tower={this.tower}
							unity={unity}
							onSelectStep={this.onSelectStep}
							onRequireChange={this.onRequireChange}
							showNotificationPopup={showNotificationPopup}
						/>
					)}

					{currentStep === 5 && (
						<DeedsStep
							currentStep={currentStep}
							tower={this.tower}
							unity={unity}
							onSelectStep={this.onSelectStep}
							onRequireChange={this.onRequireChange}
							showNotificationPopup={showNotificationPopup}
						/>
					)}

					{currentStep === 6 && (
						<TrackingDatesStep
							currentStep={currentStep}
							tower={this.tower}
							unity={unity}
							onSelectStep={this.onSelectStep}
							onRequireChange={this.onRequireChange}
							showNotificationPopup={showNotificationPopup}
						/>
					)}
				</div>

				<ProcessSidebar
					person={paymentAgreement.persons[0] || {}}
					unity={unity}
					currentStep={currentStep}
					completedSteps={completedSteps}
					onSelectStep={this.onSelectStep}
				/>
			</section>
		);
	}
}

export default connect(null, {
	getCredit: deedsActions.getCredit,
	saveCredit: deedsActions.saveCredit,
	updateCredit: deedsActions.updateCredit,
	getUnityByTowerAndName: unitiesActions.getUnityByTowerAndName,
	getPaymentPlansById: formalizationActions.getPaymentPlansById,
	showNotificationPopup: notificationPopupActions.showNotificationPopup
})(DeedsProcess);
