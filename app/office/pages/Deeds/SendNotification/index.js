import React, { Component } from 'react';
import { connect } from 'react-redux';

import SendNotificationFrame from './components/SendNotificationFrame';
import SendNotificationTable from './components/SendNotificationTable';
import NotificationsMessage from 'core/components/NotificationsMessage';
import SendNotificationConfirm from './components/SendNotificationConfirm';
import SendingNotificationPopup from './components/SendingNotificationPopup';
import Modal from 'core/components/Modal';
import Loading from 'core/components/Loading';
import { getUnitiessByTowerAndFloor, getUnitiesNumber } from 'core/helpers/unities';
import { html } from 'core/data/deeds';
import * as unitiesActions from 'core/redux/unities/operations';
import * as deedsActions from 'core/redux/deeds/operations';

class SendNotification extends Component {
	constructor(props) {
		super(props);
		this.state = {
			step: 1,
			unitiesByTowerAndFloor: {},
			unitiesNumber: [],
			addedUnities: [],
			addedFloors: [],
			notifiedUnits: [],
			html,
			sectionLoaded: false,
			notificationPopupOpen: false,
			notificationDone: false
		};

		this.tower = this.props.match.params.towerId;
		this.towerAlfa = `t${this.tower}`;
	}

	async componentDidMount() {
		const unities = await this.props.getUnities();

		const unitiesByTowerAndFloor = getUnitiessByTowerAndFloor(unities);
		const unitiesNumber = getUnitiesNumber(unitiesByTowerAndFloor);

		const deeds = await this.props.getDeeds(this.tower);

		const notifiedUnits = deeds.filter(u => u.unitynotificationlog).map(u => u.id);

		this.setState({
			unitiesByTowerAndFloor: unitiesByTowerAndFloor[this.towerAlfa],
			unitiesNumber,
			notifiedUnits,
			sectionLoaded: true
		});
	}

	onSelectUnity = (unity, floor) => {
		if (!unity.paymentagreement || this.state.notifiedUnits.includes(unity.id)) return;

		this.setState(prevState => {
			const unityFound = prevState.addedUnities.find(u => u === unity.id);
			const floorFound = prevState.addedFloors.includes(floor);

			if (unityFound) {
				if (floorFound) {
					return {
						...prevState,
						addedUnities: prevState.addedUnities.filter(u => u !== unity.id),
						addedFloors: prevState.addedFloors.filter(f => f !== floor)
					};
				}

				return {
					...prevState,
					addedUnities: prevState.addedUnities.filter(u => u !== unity.id)
				};
			}

			return {
				...prevState,
				addedUnities: [...prevState.addedUnities, unity.id]
			};
		});
	};

	onSelectFloor = floor => {
		this.setState(prevState => {
			if (prevState.addedFloors.includes(floor)) {
				return {
					...prevState,
					addedUnities: prevState.addedUnities.filter(unity => {
						const found = prevState.unitiesByTowerAndFloor[floor].find(u => u.id === unity);

						return !found;
					}),
					addedFloors: prevState.addedFloors.filter(f => f !== floor)
				};
			}

			return {
				...prevState,
				addedFloors: [...prevState.addedFloors, floor],
				addedUnities: [
					...prevState.addedUnities,
					...prevState.unitiesByTowerAndFloor[floor]
						.filter(u => {
							return (
								!prevState.addedUnities.includes(u.id) &&
								u.paymentagreement &&
								!prevState.notifiedUnits.includes(u.id)
							);
						})
						.map(unity => unity.id)
				]
			};
		});
	};

	onNext = () => {
		this.setState(prevState => {
			if (prevState.step === 3) {
				// Validations
				return this.sendNotification();
			}

			return {
				...prevState,
				step: prevState.step + 1
			};
		});
	};

	onBack = () => {
		if (this.state.step === 1) {
			return this.props.history.push('/deeds');
		}

		this.setState(prevState => {
			return {
				...prevState,
				step: prevState.step - 1
			};
		});
	};

	onContentEditableChange = html => {
		this.setState({ html });
	};

	getUnitiesAmount = unities => {
		return Object.keys(unities).reduce((acc, current) => {
			return acc + unities[current].length;
		}, 0);
	};

	goToStep = step => {
		this.setState({ step });
	};

	sendNotification = async () => {
		const { addedUnities, html } = this.state;

		const payload = {
			unities: addedUnities,
			html_snippet: html,
			tower_number: this.tower
		};

		const response = await this.props.sendNotifications(payload);

		if (response) {
			this.setState({ notificationPopupOpen: true });

			this.notificationStatusTimer = window.setInterval(
				() => this.checkNotificationsStatus(response.task_id),
				2000
			);
		}
	};

	checkNotificationsStatus = async taskId => {
		const response = await this.props.checkNotificationsStatus({ task_id: taskId });

		if (response.data.message === 'SUCCESS') {
			window.open(response.data.filename.url_path, '_blank');
			window.clearInterval(this.notificationStatusTimer);

			this.setState({ notificationDone: true });
		}
	};

	onCloseNotificationPopup = () => {
		this.setState({ notificationPopupOpen: false });

		this.props.history.push('/deeds');
	};

	render() {
		const {
			step,
			unitiesByTowerAndFloor,
			unitiesNumber,
			addedFloors,
			addedUnities,
			notifiedUnits,
			html,
			sectionLoaded,
			notificationPopupOpen,
			notificationDone
		} = this.state;
		const { getUnities } = this.props;

		if (!sectionLoaded) {
			return <Loading />;
		}

		return (
			<section className="send-notification">
				<SendNotificationFrame
					step={step}
					tower={this.tower}
					onNext={this.onNext}
					onBack={this.onBack}
					goToStep={this.goToStep}
					sendNotification={this.sendNotification}
				>
					{step === 1 && (
						<SendNotificationTable
							tower={'t2'}
							unitiesByTowerAndFloor={unitiesByTowerAndFloor}
							unitiesNumber={unitiesNumber}
							notifiedUnits={notifiedUnits}
							addedFloors={addedFloors}
							addedUnities={addedUnities}
							onSelectUnity={this.onSelectUnity}
							onSelectFloor={this.onSelectFloor}
						/>
					)}

					{step === 2 && (
						<NotificationsMessage html={html} onContentEditableChange={this.onContentEditableChange} />
					)}

					{step === 3 && (
						<SendNotificationConfirm
							recipients={addedUnities.length}
							tower={this.tower}
							goToStep={this.goToStep}
						/>
					)}
				</SendNotificationFrame>

				<SendingNotificationPopup
					notificationPopupOpen={notificationPopupOpen}
					notificationDone={notificationDone}
					sent={addedUnities.length}
					pending={this.getUnitiesAmount(unitiesByTowerAndFloor) - addedUnities.length}
					onCloseNotificationPopup={this.onCloseNotificationPopup}
				/>
			</section>
		);
	}
}

export default connect(null, {
	getUnities: unitiesActions.getUnities,
	sendNotifications: deedsActions.sendNotifications,
	checkNotificationsStatus: deedsActions.checkNotificationsStatus,
	getDeeds: deedsActions.getDeeds
})(SendNotification);
