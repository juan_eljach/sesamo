import React, { Component } from 'react';

import Modal from 'core/components/Modal';
import Button from 'core/components/Button';
import Loading from 'core/components/Loading';

class SendingNotificationPopup extends Component {
	constructor(props) {
		super(props);
	}
	render() {
		const { notificationPopupOpen, notificationDone, sent, pending, onCloseNotificationPopup } = this.props;

		return (
			<Modal open={notificationPopupOpen}>
				<div className="send-notification-popup">
					{notificationDone ? (
						<div className="send-notification-popup__done">
							<i className="mdi mdi-close action-hover" onClick={onCloseNotificationPopup} />
							<img src="/static/img/deeds-onboard-done.png" alt="" />
							<div>
								<p>¡Enviaste {sent} notificaciones!</p>
								<p>Pendientes: {pending} clientes sin notificar</p>
							</div>
						</div>
					) : (
						<div className="send-notification-popup__sending">
							<Loading />
							<p>Enviando tus notificaciones...</p>
							<p>Este proceso puede tardar unos minutos.</p>
						</div>
					)}
				</div>
			</Modal>
		);
	}
}

export default SendingNotificationPopup;
