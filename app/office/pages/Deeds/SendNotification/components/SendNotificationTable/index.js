import React, { Component } from 'react';
import uuid from 'uuid/v4';
import classnames from 'classnames';

import Tooltip from 'core/components/Tooltip';
import Table, { TableHead, TableBody, TableRow, TableCell } from 'core/components/Table';

class SendNotificationTable extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	renderUnity = (unity, floor, unityClassName) => {
		return (
			<div className={unityClassName} onClick={() => this.props.onSelectUnity(unity, floor)}>
				<i className="mdi mdi-check-circle" />

				{unity.name}
			</div>
		);
	};

	render() {
		const {
			tower,
			unitiesByTowerAndFloor,
			unitiesNumber,
			addedFloors,
			addedUnities,
			notifiedUnits,
			onSelectUnity,
			onSelectFloor
		} = this.props;

		return (
			<div className="send-notification-table">
				<Table type="unities">
					<TableHead>
						<TableRow>{unitiesNumber.map(n => <TableCell key={uuid()}>{n}</TableCell>)}</TableRow>
					</TableHead>
					<TableBody>
						{Object.keys(unitiesByTowerAndFloor).map(floor => {
							const floorAdded = addedFloors.includes(floor);
							const floorClassName = classnames('send-notification-table__unity-floor', {
								'send-notification-table__unity-floor--added': floorAdded
							});

							return (
								<TableRow key={uuid()}>
									<TableCell>
										<div className={floorClassName} onClick={() => onSelectFloor(floor)}>
											{floorAdded ? (
												<i className="mdi mdi-check-circle" />
											) : (
												<i className="mdi mdi-radiobox-blank" />
											)}
											<span>{floor}</span>
										</div>
									</TableCell>

									{unitiesByTowerAndFloor[floor].map(unity => {
										const unityAdded = addedUnities.includes(unity.id);
										const unityNotified = notifiedUnits.includes(unity.id);
										const unityClassName = classnames('send-notification-table__unity', {
											'send-notification-table__unity--added': unityAdded,
											'send-notification-table__unity--notified': unityNotified,
											'send-notification-table__unity--disabled':
												!unity.paymentagreement || unityNotified
										});

										return (
											<TableCell key={unity.id}>
												{!unity.paymentagreement ? (
													<Tooltip title={'Sin Acuerdo de Pago'}>
														{this.renderUnity(unity, floor, unityClassName)}
													</Tooltip>
												) : unityNotified ? (
													<Tooltip title={'Unidad notificada'}>
														{this.renderUnity(unity, floor, unityClassName)}
													</Tooltip>
												) : (
													this.renderUnity(unity, floor, unityClassName)
												)}
											</TableCell>
										);
									})}
								</TableRow>
							);
						})}
					</TableBody>
				</Table>
			</div>
		);
	}
}

export default SendNotificationTable;
