import React, { Component } from 'react';
import {savePayment} from '../../../../../../core/redux/payments/operations'

class SendNotificationConfirm extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { tower, recipients, goToStep } = this.props;

    return <div className="send-notification-confirm">
        <div className="send-notification-confirm__item v-align">
          <div className="v-align">
            <i className="mdi mdi-checkbox-marked-circle" />
            <div className="send-notification-confirm__item-info">
              <h5>Torre</h5>
              <p>Torre {tower}</p>
            </div>
          </div>
        </div>

        <div className="send-notification-confirm__item v-align">
          <div className="v-align">
            <i className="mdi mdi-checkbox-marked-circle" />
            <div className="send-notification-confirm__item-info">
              <h5>Destinatarios</h5>
            <p>{recipients}</p>
            </div>
          </div>

          <div className="send-notification-confirm__item-edit v-align" onClick={() => goToStep(1)}>
            <i className="mdi mdi-pencil"></i>
            <span>Editar</span>
          </div>
        </div>

        <div className="send-notification-confirm__item v-align">
          <div className="v-align">
            <i className="mdi mdi-checkbox-marked-circle" />
            <div className="send-notification-confirm__item-info">
              <h5>Mensaje de la notificación</h5>
            </div>
          </div>

          <div className="send-notification-confirm__item-edit v-align" onClick={() => goToStep(2)}>
            <i className="mdi mdi-pencil"></i>
            <span>Editar</span>
          </div>
        </div>
      </div>;
  }
}


export default SendNotificationConfirm;
