import React, { Component } from 'react';
import classnames from 'classnames';

import Button from "core/components/Button";

class SendNotificationFrame extends Component {
  constructor(props) {
    super(props);
    this.state = {  }

    this.stepsData = {
      1: { text: 'Haz click sobre la unidad para seleccionarla', icon: 'check-circle' },
      2: { text: 'Edita el mensaje de la notificación', icon: 'email-outline' },
      3: { text: 'Confirma el envío', icon: 'check' }
    };

    this.navList = [
      { text: '1. Selecciona los apartamentos >', value: 1},
      { text: '2. Valida el template y envía >', value: 2 },
      { text: '3. Confirmar', value: 3 }
    ]
  }
  render () {
    const { children, step, tower, onNext, onBack, goToStep, sendNotification } = this.props;

    return (
      <div className="send-notification-frame">
        <div className="send-notification-frame__header">
          <div className="send-notification-frame__header-tower">Torre {tower}</div>
          <div className="send-notification-frame__header-title v-align">
            <i className={`mdi mdi-${this.stepsData[step].icon}`}></i>
            <span>{this.stepsData[step].text}</span>
          </div>
        </div>

        <div className="send-notification-frame__content">
          { children }
        </div>

        <div className="send-notification-frame__footer">
          <Button type="secondary" onClick={onBack}>
            <div className="v-align h-align">
              <i className="mdi mdi-arrow-left"></i>
              <span>{step === 1 ? 'CANCELAR' : 'REGRESAR'}</span>
            </div>
          </Button>

          <nav className="send-notification-frame__footer-nav">
            {
              this.navList.map((item, index) => {
                const itemClassName = classnames(
                  'send-notification-frame__footer-nav-item',
                  {'send-notification-frame__footer-nav-item--active': item.value === step}
                );

                return (
                  <a className={itemClassName} key={index} onClick={() => goToStep(item.value)}>{item.text}</a>
                )
              })
            }
          </nav>

          <Button onClick={step === 3 ? sendNotification : onNext}>
            <div className="v-align h-align">
              <span>{step === 3 ? 'ENVIAR' : 'SIGUIENTE'}</span>
              {
                step !== 3 && <i className="mdi mdi-arrow-right"></i>
              }
            </div>
          </Button>
        </div>
      </div>
    );
  }
}

export default SendNotificationFrame;
