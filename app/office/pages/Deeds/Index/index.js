import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import InputField from 'core/components/InputField';
import Loading from 'core/components/Loading';
import Modal from 'core/components/Modal';
import Button from 'core/components/Button';
import Initials from 'core/components/Initials';
import Checkbox from 'core/components/Checkbox';
import Tooltip from 'core/components/Tooltip';
import Icon from 'core/components/Icon';
import NotificationsMessage from 'core/components/NotificationsMessage';
import SendingNotificationPopup from '../SendNotification/components/SendingNotificationPopup';
import Table, { TableHead, TableBody, TableRow, TableCell } from 'core/components/Table';
import { html } from 'core/data/deeds';
import { getUnitiessByTowerAndFloor } from 'core/helpers/unities';
import * as unitiesActions from 'core/redux/unities/operations';
import * as deedsActions from 'core/redux/deeds/operations';

class Deeds extends Component {
	constructor(props) {
		super(props);
		this.state = {
			towers: [],
			pageLoaded: false,
			selectedTower: {},
			notificationAlert: { open: false },
			html,
			addedUnities: [],
			notificationsOpen: false,
			sendingNotificationsPopupOpen: false,
			notificationsDone: false
		};
	}

	componentDidCatch(error, info) {
		console.log(error, info);
	}

	async componentDidMount() {
		try {
			const unities = await this.props.getUnities();

			const unitiesByTowerAndFloor = getUnitiessByTowerAndFloor(unities);

			const towers = Object.keys(unitiesByTowerAndFloor);

			const notifiedTowers = await this.checkTowers(towers);

			if (!notifiedTowers.length) {
				return this.props.history.push('/deeds/onboarding');
			}

			const selectedTower = { value: notifiedTowers[0], label: `Torre ${notifiedTowers[0]}` };

			const deeds = await this.props.getDeeds(Number(notifiedTowers[0]));

			this.setState({
				unities,
				deeds,
				towers: towers.map(tower => ({ label: tower.replace(/t/, 'Torre '), value: tower.replace(/t/, '') })),
				notifiedTowers,
				selectedTower,
				pageLoaded: true
			});
		} catch (error) {
			console.log(error);
		}
	}

	checkTowers = towers => {
		return new Promise((resolve, reject) => {
			var indexEl = 0;
			let notifiedTowers = [];

			towers.forEach(async tower => {
				try {
					const towerNumber = tower.replace(/t/, '');
					const response = await this.props.checkNotificationsTower(towerNumber);

					if (response && response.data.massive_notifications_sent) {
						notifiedTowers.push(towerNumber);

						indexEl = indexEl + 1;
					}

					if (towers.length === indexEl) {
						resolve(notifiedTowers);
					}
				} catch (error) {
					indexEl = indexEl + 1;

					if (towers.length === indexEl) {
						resolve(notifiedTowers);
					}
				}
			});
		});
	};

	onCloseNotificationAlert = () => {
		this.setState(prevState => {
			return {
				...prevState,
				notificationAlert: {
					...prevState.notificationAlert,
					open: false
				}
			};
		});
	};

	onTowerChange = async tower => {
		if (!this.state.notifiedTowers.includes(tower.value)) {
			return this.setState(prevState => {
				return {
					...prevState,
					notificationAlert: {
						...prevState.notificationAlert,
						open: true,
						tower: tower.value,
						onConfirm: () => {
							this.props.history.push(`/deeds/send-notification/${tower.value}`);
						},
						onClose: this.onCloseNotificationAlert
					}
				};
			});
		}

		const deeds = await this.props.getDeeds(Number(tower.value));

		this.setState(prevState => {
			return {
				...prevState,
				deeds,
				selectedTower: tower
			};
		});
	};

	onFloorChange = () => {};

	onToggleSendNotifications = () => {
		this.setState({ notificationsOpen: !this.state.notificationsOpen });
	};

	onSelectAllUnities = () => {
		this.setState(prevState => {
			if (prevState.addedUnities.length === prevState.deeds.length) {
				return {
					...prevState,
					addedUnities: []
				};
			}

			return {
				...prevState,
				addedUnities: prevState.deeds.map(deed => deed.name)
			};
		});
	};

	onCloseSendingNotificationsPopup = () => {
		this.setState({ sendingNotificationsPopupOpen: false });
	};

	onAddUnity = deed => {
		this.setState(prevState => {
			if (prevState.addedUnities.includes(deed.name)) {
				return {
					...prevState,
					addedUnities: prevState.addedUnities.filter(d => d !== deed.name)
				};
			}

			return {
				...prevState,
				addedUnities: [...prevState.addedUnities, deed.name]
			};
		});
	};

	onContentEditableChange = html => {
		this.setState({ html });
	};

	sendNotifications = async () => {
		const { addedUnities, html, selectedTower } = this.state;

		const payload = {
			unities: addedUnities,
			html_snippet: html,
			tower_number: selectedTower.value
		};

		const response = await this.props.sendNotifications(payload);

		if (response) {
			this.setState({ sendingNotificationsPopupOpen: true });

			this.notificationStatusTimer = window.setInterval(
				() => this.checkNotificationsStatus(response.task_id),
				2000
			);
		}
	};

	checkNotificationsStatus = async taskId => {
		const response = await this.props.checkNotificationsStatus({ task_id: taskId });

		if (response.data.message === 'SUCCESS') {
			window.open(response.data.filename.url_path);
			window.clearInterval(this.notificationStatusTimer);

			this.setState({ notificationDone: true });
		}
	};

	render() {
		const {
			pageLoaded,
			deeds,
			towers,
			selectedTower,
			addedUnities,
			notificationAlert,
			notificationsOpen,
			sendingNotificationsPopupOpen,
			notificationsDone
		} = this.state;

		if (!pageLoaded) return <Loading />;

		return (
			<section className="deeds wrap-container">
				<div className="deeds__header v-align space-between">
					<div className="deeds__header-tower-filter v-align">
						<label>Filtrar Por:</label>
						<InputField
							value={selectedTower}
							type="select"
							options={towers}
							onChange={this.onTowerChange}
						/>

						<InputField value={null} type="select" options={[]} onChange={this.onFloorChange} />
					</div>

					<Button onClick={() => this.props.history.push(`/deeds/send-notification/${selectedTower.value}`)}>
						Enviar Notificaciones
					</Button>
				</div>

				<div className="deeds__table">
					<Table type="data-grid">
						<TableHead>
							<TableRow>
								<TableCell>Apartamento</TableCell>
								<TableCell>Cliente</TableCell>
								<TableCell>Paso Actual</TableCell>
								<TableCell>Completado</TableCell>
								<TableCell>Última modificación</TableCell>
								<TableCell>Documento</TableCell>
								<TableCell />
							</TableRow>
						</TableHead>
						<TableBody>
							{deeds.map((deed, index) => {
								const clientName = deed.clients[0];
								const clientNameSplit = clientName.split(' ');
								const initials = `${clientNameSplit[0][0]} ${clientNameSplit[1][0]}`;

								return (
									<TableRow key={deed.id}>
										<TableCell>
											<div className="deeds__table-unity">
												<Link to={`/deeds/process/${deed.name}`}>{deed.name}</Link>
											</div>
										</TableCell>
										<TableCell>
											<div className="v-align">
												<Initials initials={initials} />
												<span>{clientName}</span>
											</div>
										</TableCell>
										<TableCell>Verificación de pagos</TableCell>
										<TableCell>0%</TableCell>
										<TableCell>
											{deed.unitynotificationlog.email_notification_sent_timestamp}
										</TableCell>
										<TableCell>
											<a
												href={deed.unitynotificationlog.document}
												target="_blank"
												className="deeds__table-document v-align"
											>
												<span className="v-align">
													<i className="mdi mdi-arrow-down" />
													Descargar Documento
												</span>
											</a>
										</TableCell>
										<TableCell>
											<div className="deeds__table-in">
												<Link to={`/deeds/process/${deed.name}`}>
													<Tooltip title="Ingresar a detalle de escrituración">
														<i className="mdi mdi-arrow-right" />
													</Tooltip>
												</Link>
											</div>
										</TableCell>
									</TableRow>
								);
							})}
						</TableBody>
					</Table>
				</div>

				<Modal open={notificationAlert.open}>
					<div className="deeds__notification-alert">
						<p>
							La torre <span>{notificationAlert.tower}</span> no está notificada
						</p>
						<Button onClick={notificationAlert.onConfirm}>Notificar</Button>

						<i className="mdi mdi-close action-hover" onClick={notificationAlert.onClose} />
					</div>
				</Modal>
			</section>
		);
	}
}

export default connect(null, {
	getUnities: unitiesActions.getUnities,
	checkNotificationsTower: deedsActions.checkNotificationsTower,
	getDeeds: deedsActions.getDeeds,
	sendNotifications: deedsActions.sendNotifications,
	checkNotificationsStatus: deedsActions.checkNotificationsStatus
})(Deeds);
