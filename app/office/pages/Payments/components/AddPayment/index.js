import React, { Component } from "react";
import { Field, reduxForm } from "redux-form";
import { debounce as debounceFunc } from "throttle-debounce";
import PropTypes from "prop-types";

import Searcher from "core/components/Searcher";
import InputField from "core/components/InputField";
import SelectField from "core/components/InputField";
import Button from "core/components/Button";
import { mediumChoices, paymentsTypes } from "core/data/payments";

class AddPayment extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isFetching: false,
      searchValue: "",
      unitiesFound: [],
      typeOfPaymentValue: ""
    };

    this.searchUnities = debounceFunc(500, this.searchUnities.bind(this));
    this.mediumChoices = Object.keys(mediumChoices).map(el => ({
      label: el,
      value: mediumChoices[el]
    }));
    this.paymentsTypes = Object.keys(paymentsTypes).map(el => ({
      label: el,
      value: paymentsTypes[el]
    }));

    this.surplusToChoices = [
      { label: "Cuota Mensual", value: "cuota-mensual" },
      { label: "Superavit", value: "surplus-model" }
    ];
  }

  onInputChange = value => {
    this.setState({ searchValue: value });

    this.searchUnities(value);
  };

  searchUnities = async value => {
    this.setState({ isSearching: true });

    const response = await this.props.searchUnities(
      value,
      "&paymentagreement__isnull=false"
    );

    this.setState({
      unitiesFound: response
        .slice(0, 10)
        .map(unity => ({ ...unity, value: unity.id, label: unity.name })),
      isSearching: false
    });
  };

  static validate(fields) {
    const errors = {};
    const errorName = "Campo requerido";

    if (!fields.date) errors.date = errorName;
    if (!fields.amount) errors.amount = errorName;
    if (!fields.type_of_payment) errors.type_of_payment = errorName;
    if (!fields.medium) errors.medium = errorName;

    return errors;
  }

  onSubmit = async values => {
    try {
      const {
        paymentAgreement,
        addPayment,
        onCloseAddPayment,
        showNotificationPopup
      } = this.props;

      const payload = {
        ...values,
        date: values.date.format("YYYY-MM-DD"),
        amount: Number(values.amount),
        medium: values.medium.value,
        type_of_payment: values.type_of_payment.value,
        payment_agreement: paymentAgreement.id
      };

      if (values.apply_surplus_to) {
        payload.apply_surplus_to = values.apply_surplus_to.value;
      }

      this.setState({ isFetching: true });

      const response = await this.props.savePayment(payload);

      onCloseAddPayment();

      if (addPayment) {
        addPayment(response);
      }

      await this.props.updatePaymentsPlan();

      this.setState({ isFetching: false });

      showNotificationPopup({
        message: "El Pago se ha guardado correctamente!"
      });
    } catch (error) {
      // error
    }
  };

  onTypeChange = type => {
    this.setState({ typeOfPaymentValue: type.value });
  };

  render() {
    const {
      isFetching,
      isSearching,
      searchValue,
      typeOfPaymentValue,
      unitiesFound
    } = this.state;
    const {
      selectUnity,
      selectedUnity,
      agreedPayments,
      handleSubmit,
      onCloseAddPayment
    } = this.props;

    return (
      <div className="add-payment">
        <i className="mdi mdi-close action-hover" onClick={onCloseAddPayment} />
        <h4>Nuevo Pago</h4>
        <form onSubmit={handleSubmit(this.onSubmit)}>
          <div className="add-payment__container">
            <Searcher
              inputValue={searchValue}
              options={unitiesFound}
              onInputChange={this.onInputChange}
              onChange={selectUnity}
              onCloseChip={this.onRemoveUnity}
              isLoading={isSearching}
              placeholder="Ej: T2-201"
              showChip={!!selectedUnity.label}
              chipLabel={selectedUnity.label}
              isDisabled={selectedUnity.disabled}
            />

            <Field
              name="date"
              type="date"
              label="Fecha"
              placeholder="Seleccionar fecha"
              component={InputField}
              disabledFutureDate
            />

            <Field
              name="amount"
              type="number"
              label="Valor del pago"
              placeholder="Escribir valor del pago"
              component={InputField}
              withPriceIcon
            />

            <Field
              name="type_of_payment"
              type="select"
              label="Tipo"
              placeholder="Selecciona el tipo de pago"
              options={this.paymentsTypes}
              component={InputField}
              onChange={this.onTypeChange}
              isOptionDisabled={option => {
                if (
                  agreedPayments &&
                  !agreedPayments[option.value] &&
                  [
                    "ahorro-programado",
                    "cesantias",
                    "otro-pago-acordado",
                    "subsidio-vivienda"
                  ].includes(option.value)
                ) {
                  return (option.disabled = "yes");
                }
              }}
            />

            {[
              "ahorro-programado",
              "cesantias",
              "subsidio-vivienda",
              "otro-pago-acordado",
              "separacion"
            ].includes(typeOfPaymentValue) && (
              <Field
                name="apply_surplus_to"
                type="select"
                label="En caso de excedente, agregarlo a:"
                placeholder="Selecciona el tipo de pago"
                options={this.surplusToChoices}
                component={InputField}
              />
            )}

            <Field
              name="medium"
              type="select"
              label="Medio"
              placeholder="Escribir correo electrónico"
              options={this.mediumChoices}
              component={InputField}
            />

            <Field
              name="comment"
              type="text"
              label="Observaciones"
              placeholder=""
              component={InputField}
            />
          </div>
          <div className="add-customer__form-button">
            <Button isFetching={isFetching}>GUARDAR</Button>
          </div>
        </form>
      </div>
    );
  }
}

AddPayment.propTypes = {
  handleSubmit: PropTypes.func.isRequired
};

export default reduxForm({
  form: "AddPayment",
  validate: AddPayment.validate,
  keepDirtyOnReinitialize: true,
  enableReinitialize: true
})(AddPayment);
