import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { debounce as debounceFunc } from 'throttle-debounce';
import classnames from 'classnames';
import moment from 'moment';

import Searcher from 'core/components/Searcher';
import Button from 'core/components/Button';
import Table, { TableHead, TableBody, TableRow, TableCell } from 'core/components/Table';
import Loading from 'core/components/Loading';
import Modal from 'core/components/Modal';
import AddPayment from './components/AddPayment';
import colors from 'core/data/colors';
import formatValue from 'core/helpers/formatValue';
import * as formalizationActions from 'core/redux/formalization/operations';
import * as unitiesActions from 'core/redux/unities/operations';
import * as paymentsActions from 'core/redux/payments/operations';
import * as notificationPopupActions from 'core/redux/notificationPopup/actions';

class Payments extends Component {
	constructor(props) {
		super(props);
		this.state = {
			paymentPlans: [],
			totalPlans: 0,
			unities: [],
			searchValue: '',
			selectedUnity: {},
			paymentAgreement: {},
			isSearching: false,
			sectionLoaded: false,
			unityDisabled: false
		};

		this.searchPaymentPlans = debounceFunc(500, this.searchPaymentPlans.bind(this));
	}

	async componentDidMount() {
		const unities = await this.props.getUnities();
		const plans = await this.props.getPaymentPlans();

		this.setState({
			paymentPlans: plans.results,
			totalPlans: plans.count,
			unities,
			sectionLoaded: true
		});
	}

	onInputChange = async value => {
		this.setState({ searchValue: value, isSearching: true });

		this.searchPaymentPlans(value);
	};

	searchPaymentPlans = async value => {
		this.setState({ searchValue: value });

		const response = await this.props.getPaymentPlans(value);

		this.setState({
			paymentPlans: response.results,
			totalPlans: response.count,
			isSearching: false
		});
	};

	selectUnity = async unity => {
		this.setState({ selectedUnity: unity });

		const response = await this.props.getPaymentPlansById(unity.id);

		if (response && response.results) {
			this.setState({ paymentAgreement: response.results[0] });
		}
	};

	onRemoveUnity = () => {
		this.setState({ selectedUnity: {}, searchValue: '' });
	};

	onOpenAddPayment = unityName => {
		this.setState({ addPaymentOpen: true, unityDisabled: true });

		if (unityName) {
			const selectedUnity = this.state.unities.find(u => u.name === unityName);

			this.selectUnity({ ...selectedUnity, label: selectedUnity.name, disabled: true } || {});
		}
	};

	onCloseAddPayment = () => {
		this.setState({ addPaymentOpen: false, selectedUnity: {} });
	};

	onPageChange = async page => {
		const response = await this.props.getPaymentPlans('', page);

		this.setState({
			paymentPlans: response.results
		});
	};

	getPaymentStatus = paymentPlan => {
		let paid = paymentPlan.payment_fees.reduce((acc, current) => {
			return acc && current.paid;
		}, true);

		let paymentStatus = {};

		if (paid) {
			paymentStatus.text = 'Cancelado';
			paymentStatus.color = colors.green;
		} else if (paymentPlan.paymentfees_enmora) {
			paymentStatus.text = 'En Mora';
			paymentStatus.color = colors.red;
		} else if (!paid && !paymentPlan.paymentfees_enmora) {
			paymentStatus.text = 'Al día';
			paymentStatus.color = colors.greyDark;
		}

		return paymentStatus;
	};

	getDebtValue = paymentPlan => {
		let currentDate = moment().format('YYYY-MM-DD');
		let currentDateCount = moment(currentDate).diff(currentDate);

		const debtValue = paymentPlan.payment_fees.reduce(
			(acc, current) => {
				const dateToPay = current.date_to_pay;
				let dateToPayCount = moment(dateToPay).diff(currentDate, 'days');

				if (dateToPayCount < currentDateCount) {
					return {
						...acc,
						value: acc.value + current.fee,
						time: acc.time + Math.abs(dateToPayCount)
					};
				}

				return acc;
			},
			{ value: 0, time: 0 }
		);

		return debtValue;
	};

	render() {
		const {
			sectionLoaded,
			paymentPlans,
			totalPlans,
			searchValue,
			isSearching,
			addPaymentOpen,
			unities,
			selectedUnity,
			paymentAgreement,
			unityDisabled
		} = this.state;

		const { searchUnities, getPaymentPlansById, savePayment, showNotificationPopup } = this.props;

		if (!sectionLoaded) {
			return <Loading />;
		}

		return (
			<section className="payments wrap-container">
				<div className="payments__header v-align">
					<Searcher
						inputValue={searchValue}
						onInputChange={this.onInputChange}
						menuIsOpen={false}
						isSearchable={true}
						isLoading={isSearching}
						placeholder="Buscar por persona o apto"
					/>

					<Button onClick={this.onOpenAddPayment}>AGREGAR PAGO</Button>
				</div>

				{!paymentPlans.length && sectionLoaded ? (
					<div className="no-results">No se encontraron resultados</div>
				) : (
					<Table type="data-grid" totalItems={totalPlans} onPageChange={this.onPageChange}>
						<TableHead>
							<TableRow>
								<TableCell>Cliente</TableCell>
								<TableCell>Apartamento</TableCell>
								<TableCell>Estado</TableCell>
								<TableCell>Cuota en mora</TableCell>
								<TableCell>Valor en mora</TableCell>
								<TableCell>Tiempo en mora</TableCell>
								<TableCell />
								<TableCell />
							</TableRow>
						</TableHead>
						<TableBody>
							{paymentPlans.map((paymentPlan, index) => {
								const person = paymentPlan.persons[0] || {};
								const personName = `${person.first_name} ${person.last_name}`;
								const debt = this.getDebtValue(paymentPlan);
								const paymentStatus = this.getPaymentStatus(paymentPlan);

								return (
									<TableRow key={paymentPlan.id}>
										<TableCell>{personName}</TableCell>
										<TableCell>{paymentPlan.unity}</TableCell>
										<TableCell>
											<span style={{ color: paymentStatus.color }}>{paymentStatus.text}</span>
										</TableCell>
										<TableCell>{paymentPlan.paymentfees_enmora}</TableCell>
										<TableCell>${formatValue(debt.value)}</TableCell>
										<TableCell>
											{debt.time
												? moment()
														.subtract(debt.time, 'days')
														.fromNow()
												: '-'}
										</TableCell>
										<TableCell>
											<Link to={`/payments/${paymentPlan.id}`}>Ver negocio</Link>
										</TableCell>
										<TableCell>
											<a onClick={() => this.onOpenAddPayment(paymentPlan.unity)}>Agregar pago</a>
										</TableCell>
									</TableRow>
								);
							})}
						</TableBody>
					</Table>
				)}

				<Modal open={addPaymentOpen} onClose={this.onCloseAddPayment}>
					<AddPayment
						unities={unities}
						searchUnities={searchUnities}
						selectUnity={this.selectUnity}
						selectedUnity={selectedUnity}
						paymentAgreement={paymentAgreement}
						unityDisabled={unityDisabled}
						getPaymentPlansById={getPaymentPlansById}
						savePayment={savePayment}
						onCloseAddPayment={this.onCloseAddPayment}
						showNotificationPopup={showNotificationPopup}
					/>
				</Modal>
			</section>
		);
	}
}

const mapStateToProps = ({ project }) => {
	return {};
};

export default connect(mapStateToProps, {
	getPaymentPlans: formalizationActions.getPaymentPlans,
	getPaymentPlansById: formalizationActions.getPaymentPlansById,
	getUnities: unitiesActions.getUnities,
	searchUnities: unitiesActions.searchUnities,
	savePayment: paymentsActions.savePayment,
	showNotificationPopup: notificationPopupActions.showNotificationPopup
})(Payments);
