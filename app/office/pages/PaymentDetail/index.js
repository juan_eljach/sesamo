import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { debounce as debounceFunc } from "throttle-debounce";
import classnames from "classnames";

import Searcher from "core/components/Searcher";
import Button from "core/components/Button";
import Loading from "core/components/Loading";
import Modal from "core/components/Modal";
import AddPayment from "../Payments/components/AddPayment";
import Tab from "./components/Tab";
import PaymentsTable from "./components/PaymentsTable";
import PaymentPlanTable from "./components/PaymentPlanTable";
import AgreedPaymentsTable from "./components/AgreedPaymentsTable/index";
import SurplusTable from "./components/SurplusTable";
import { paymentsTypesMap } from "core/data/payments";
import formatValue from "core/helpers/formatValue";

import * as formalizationActions from "core/redux/formalization/operations";
import * as unitiesActions from "core/redux/unities/operations";
import * as paymentsActions from "core/redux/payments/operations";
import * as notificationPopupActions from "core/redux/notificationPopup/actions";

class PaymentDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      paymentAgreementDetail: {},
      paymentPlans: [],
      unities: [],
      searchValue: "",
      selectedUnity: {},
      paymentAgreement: {},
      isSearching: false,
      unityDisabled: false,
      selectedTab: "paymentsPlan",
      person: {},
      paymentsPlan: [],
      payments: [],
      surplus: [],
      agreedPayments: [],
      sectionLoaded: false
    };

    this.searchPaymentPlans = debounceFunc(
      500,
      this.searchPaymentPlans.bind(this)
    );

    this.paymentAgreementId = this.props.match.params.paymentAgreementId;
  }

  async componentDidMount() {
    const unities = await this.props.getUnities();
    await this.loadPaymentAgreement();

    this.setState({ unities, sectionLoaded: true });
  }

  loadPaymentAgreement = async () => {
    const response = await this.props.getPaymentAgreementsDetail(
      this.paymentAgreementId
    );

    const agreedPayments = response.agreed_payments.reduce((acc, current) => {
      return {
        ...acc,
        [current.type_of_payment]: current.fee
      };
    }, {});

    const { persons, payment_fees, payments, ...rest } = response;

    if (response) {
      const surplus = response.surplus ? response.surplus.surplus_payments : [];

      this.setState({
        paymentAgreement: response,
        person: response.persons[0],
        agreedPayments,
        agreedPaymentsFull: response.agreed_payments,
        paymentsPlan: response.payment_fees.sort((a, b) => {
          return new Date(a.date_to_pay) - new Date(b.date_to_pay);
        }),
        payments: [...response.payments, ...surplus].sort((a, b) => {
          return new Date(a.date_to_pay) - new Date(b.date_to_pay);
        }),
        surplus,
        paymentAgreementDetail: rest
      });
    }
  };

  onInputChange = async value => {
    this.setState({ searchValue: value, isSearching: true });

    this.searchPaymentPlans(value);
  };

  searchPaymentPlans = async value => {
    this.setState({ searchValue: value });

    const response = await this.props.getPaymentPlans(value);

    this.setState({
      paymentPlans: response.results,
      isSearching: false,
      sectionLoaded: true
    });
  };

  onRemoveUnity = () => {
    this.setState({ selectedUnity: {}, searchValue: "" });
  };

  onOpenAddPayment = () => {
    const selectedUnity = this.state.unities.find(
      u => u.name === this.state.paymentAgreementDetail.unity
    );

    this.setState({
      addPaymentOpen: true,
      selectedUnity: {
        ...selectedUnity,
        label: selectedUnity.name,
        disabled: true
      }
    });
  };

  onCloseAddPayment = () => {
    this.setState({ addPaymentOpen: false, selectedUnity: {} });
  };

  onSelecTab = tab => {
    this.setState({ selectedTab: tab });
  };

  addPayment = async () => {
    await this.loadPaymentAgreement();
  };

  updatePaymentsPlan = async () => {
    const response = await this.props.getPaymentAgreementsDetail(
      this.paymentAgreementId
    );

    this.setState({
      paymentsPlan: response.payment_fees.sort((a, b) => {
        return new Date(a.date_to_pay) - new Date(b.date_to_pay);
      })
    });
  };

  getInitialFeeDue = () => {
    if (!this.state.paymentAgreement.payments) return 0;

    const paymentFees = this.state.paymentAgreement.payment_fees.reduce(
      (acc, current) => {
        if (current.paid) return acc + current.fee;

        if (current.partially_paid) return acc + (current.fee - current.debt);

        return acc;
      },
      0
    );

    const agreedPayments = this.state.paymentAgreement.agreed_payments.reduce(
      (acc, current) => {
        if (current.applied_to_initialfee) {
          if (current.paid) return acc + current.fee;
          if (current.partially_paid) return acc + (current.fee - current.debt);
        }

        return acc;
      },
      0
    );

    return paymentFees + agreedPayments;
  };

  onPageChange = page => {};

  render() {
    const {
      sectionLoaded,
      paymentAgreementDetail,
      addPaymentOpen,
      unities,
      selectedUnity,
      selectedTab,
      person,
      payments,
      paymentsPlan,
      surplus,
      agreedPayments,
      agreedPaymentsFull
    } = this.state;

    const {
      searchUnities,
      getPaymentPlansById,
      savePayment,
      showNotificationPopup
    } = this.props;

    if (!sectionLoaded) {
      return <Loading />;
    }

    const bookingFee = paymentAgreementDetail.agreed_payments.find(
      p => p.type_of_payment === "separacion"
    );

    const totalPaid = payments.reduce((acc, current) => {
      return acc + current.amount;
    }, 0);

    const initialFeeValue = this.getInitialFeeDue();

    return (
      <section className="payment-detail">
        <div className="payment-detail__info">
          <div className="payment-detail__info-section v-align">
            <div className="payment-detail__info-user-name">
              <div>{`${person.first_name[0]} ${person.last_name[0]}`}</div>
              <div>{`${person.first_name} ${person.last_name}`}</div>
            </div>

            <div>
              <div className="payment-detail__info-title">
                <h4>INFORMACIÓN DE CONTACTO</h4>
              </div>

              <div className="v-align">
                <div className="payment-detail__info-item">
                  <div>Celular</div>
                  <div>{person.cellphone}</div>
                </div>
                <div className="payment-detail__info-item">
                  <div>Teléfono</div>
                  <div>{person.phone}</div>
                </div>
                <div className="payment-detail__info-item">
                  <div>Correo</div>
                  <div>{person.email}</div>
                </div>
              </div>
            </div>
          </div>

          <div className="payment-detail__info-section">
            <div className="payment-detail__info-title">
              <h4>TOTAL PAGADO: ${formatValue(totalPaid)}</h4>
            </div>

            <div className="payment-detail-data">
              <div className="payment-detail__info-item info-item-featured">
                <div>Cuota inicial por pagar</div>
                <div>
                  $
                  {formatValue(
                    paymentAgreementDetail.initial_fee - initialFeeValue
                  ) || 0}
                </div>
              </div>
              <div className="payment-detail__info-item info-item-featured">
                <div>Crédito a solicitar</div>
                <div>
                  ${formatValue(paymentAgreementDetail.required_mortgage) || 0}
                </div>
              </div>

              <div className="payment-detail__info-item info-item-featured">
                <div>Balance por pagar</div>
                <div>
                  $
                  {formatValue(
                    paymentAgreementDetail.unity_price - totalPaid
                  ) || 0}
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="payment-detail__unity-icon">
          {paymentAgreementDetail.unity}
        </div>

        <Tab selectedTab={selectedTab} onSelecTab={this.onSelecTab} />

        <div className="wrap-container">
          {selectedTab === "payments" && (
            <div className="payment-detail__header-top">
              <Button onClick={this.onOpenAddPayment}>AGREGAR PAGO</Button>
            </div>
          )}
          <div className="payment-detail__table-header v-align space-between">
            <h4>
              {`${this.state[selectedTab] && this.state[selectedTab].length} ${
                selectedTab === "paymentsPlan" ? `Planes.` : "Pagos."
              }`}
              {selectedTab === "paymentsPlan" && bookingFee && (
                <span>Valor de separación: ${formatValue(bookingFee.fee)}</span>
              )}
            </h4>
          </div>

          {selectedTab === "paymentsPlan" && (
            <PaymentPlanTable paymentsPlan={paymentsPlan} />
          )}

          {selectedTab === "payments" && <PaymentsTable payments={payments} />}

          {selectedTab === "surplus" && <SurplusTable surplus={surplus} />}

          {selectedTab === "agreedPaymentsFull" && (
            <AgreedPaymentsTable agreedPaymentsFull={agreedPaymentsFull} />
          )}
        </div>

        <Modal open={addPaymentOpen} onClose={this.onCloseAddPayment}>
          <AddPayment
            unities={unities}
            agreedPayments={agreedPayments}
            searchUnities={searchUnities}
            selectUnity={this.selectUnity}
            selectedUnity={selectedUnity}
            paymentAgreement={paymentAgreementDetail}
            getPaymentPlansById={getPaymentPlansById}
            savePayment={savePayment}
            updatePaymentsPlan={this.updatePaymentsPlan}
            onCloseAddPayment={this.onCloseAddPayment}
            showNotificationPopup={showNotificationPopup}
            addPayment={this.addPayment}
          />
        </Modal>
      </section>
    );
  }
}

const mapStateToProps = ({ project }) => {
  return {};
};

export default connect(mapStateToProps, {
  getPaymentPlans: formalizationActions.getPaymentPlans,
  getPaymentPlansById: formalizationActions.getPaymentPlansById,
  getUnities: unitiesActions.getUnities,
  searchUnities: unitiesActions.searchUnities,
  getPaymentAgreementsDetail: paymentsActions.getPaymentAgreementsDetail,
  savePayment: paymentsActions.savePayment,
  showNotificationPopup: notificationPopupActions.showNotificationPopup
})(PaymentDetail);
