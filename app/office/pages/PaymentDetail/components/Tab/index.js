import React, { Component } from 'react';
import Table, { TableHead, TableBody, TableRow, TableCell } from 'core/components/Table';
import classnames from 'classnames';

import Icon from 'core/components/Icon';

class Tab extends Component {
	constructor(props) {
		super(props);

		this.tabItems = [
			{ text: 'Cuotas mensuales', value: 'paymentsPlan', icon: 'PaymentPlan' },
			{ text: 'Otros pagos acordados', value: 'agreedPaymentsFull', icon: 'Payments' },
			{ text: 'Superavit', value: 'surplus', icon: 'Payments' },
			{ text: 'Todos los pagos', value: 'payments', icon: 'Payments' }
		];
	}

	render() {
		const { selectedTab, onSelecTab } = this.props;

		return (
			<div className="payment-plan-tab v-align">
				{this.tabItems.map(item => {
					const itemClassName = classnames('payment-plan-tab__item v-align', {
						'payment-plan-tab__item--active': item.value === selectedTab
					});
					return (
						<div key={item.value} className={itemClassName} onClick={() => onSelecTab(item.value)}>
							<Icon icon={item.icon} />

							<span>{item.text}</span>
						</div>
					);
				})}
			</div>
		);
	}
}

export default Tab;
