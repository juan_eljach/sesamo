import React, { Component } from "react";
import moment from "moment";

import Table, {
  TableHead,
  TableBody,
  TableRow,
  TableCell
} from "core/components/Table";
import Comment from "core/components/Comment";
import formatValue from "core/helpers/formatValue";
import colors from "core/data/colors";

class PaymentPlanTable extends Component {
  constructor(props) {
    super(props);

    this.state = {
      commentPopupOpen: null
    };
  }

  onOpenCommentPopup = value => {
    this.setState({ commentPopupOpen: value });
  };

  onCloseCommentPopup = value => {
    this.setState({ commentPopupOpen: "" });
  };

  getPaymentStatus = payment => {
    let currentDate = moment(Date.now()).format("YYYY-MM-DD");
    let payDate = payment.date_to_pay;

    let currentDateCount = moment(currentDate).diff(currentDate);
    let payDateCount = moment(payDate).diff(currentDate, "days");

    let paymentStatus = {};

    if (payment.paid) {
      paymentStatus.text = "Cancelado";
      paymentStatus.color = colors.green;
    } else if (payment.partially_paid && payDateCount > currentDateCount) {
      paymentStatus.text = "Pago Parcial";
      paymentStatus.color = colors.green;
    } else if (payment.paid === false && payDateCount < currentDateCount) {
      paymentStatus.text = "En Mora";
      paymentStatus.color = colors.red;
    } else if (
      payment.paid === false &&
      payment.partially_paid === false &&
      payDateCount > currentDateCount
    ) {
      paymentStatus.text = "Sin Cancelar";
      paymentStatus.color = colors.greyDark;
    }

    return paymentStatus;
  };

  render() {
    const { commentPopupOpen } = this.state;
    const { paymentsPlan } = this.props;

    return (
      <div className="payment-plan-table">
        <Table type="data-grid" totalItems={0} onPageChange={this.onPageChange}>
          <TableHead>
            <TableRow>
              <TableCell>Cuota</TableCell>
              <TableCell>Fecha</TableCell>
              <TableCell>Valor de la cuota</TableCell>
              <TableCell>Cuota extraordinaria</TableCell>
              <TableCell>Saldo</TableCell>
              <TableCell />
              <TableCell>Comentarios</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {paymentsPlan.map((paymentPlan, index) => {
              const paymentStatus = this.getPaymentStatus(paymentPlan);

              return (
                <TableRow key={index}>
                  <TableCell>{index + 1}</TableCell>
                  <TableCell>{paymentPlan.date_to_pay}</TableCell>
                  <TableCell>${formatValue(paymentPlan.fee)}</TableCell>
                  <TableCell>
                    ${formatValue(paymentPlan.extraordinary_fee)}
                  </TableCell>
                  <TableCell>${formatValue(paymentPlan.debt)}</TableCell>
                  <TableCell>
                    <span style={{ color: paymentStatus.color }}>
                      {paymentStatus.text}
                    </span>
                  </TableCell>
                  <TableCell>
                    {paymentPlan.comment ? (
                      <Comment
                        label="Comentario"
                        comment={paymentPlan.comment}
                        popupOpen={commentPopupOpen === index}
                        onOpen={() => this.onOpenCommentPopup(index)}
                        onClose={this.onCloseCommentPopup}
                      />
                    ) : (
                      "-"
                    )}
                  </TableCell>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </div>
    );
  }
}

export default PaymentPlanTable;
