import React, { Component } from 'react';
import moment from 'moment';

import Table, { TableHead, TableBody, TableRow, TableCell } from 'core/components/Table';
import Comment from 'core/components/Comment';
import formatValue from "core/helpers/formatValue";
import { paymentsTypesMap, mediumChoicesMap } from "core/data/payments";

class SurplusTable extends Component {
  constructor (props) {
    super(props);
  }

  render () {
    const {
      surplus
    } = this.props;

    return (
      <div className="surplus-table">
        <Table
          type="data-grid"
          totalItems={0}
          onPageChange={this.onPageChange}
        >
          <TableHead>
            <TableRow>
              <TableCell>No.</TableCell>
              <TableCell>Fecha</TableCell>
              <TableCell>Valor</TableCell>
              <TableCell>Origen</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {
              surplus.map((payment, index) => {
                return (
                  <TableRow key={index}>
                    <TableCell>{index + 1}</TableCell>
                    <TableCell>{moment(payment.timestamp).format('YYYY-MM-DD')}</TableCell>
                    <TableCell>${formatValue(payment.amount)}</TableCell>
                    <TableCell>{paymentsTypesMap[payment.origin]}</TableCell>
                  </TableRow>
                );
              })
            }
          </TableBody>
        </Table>
      </div>
    );
  }
}

export default SurplusTable;
