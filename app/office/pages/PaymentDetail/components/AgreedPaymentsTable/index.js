import React, { Component } from 'react';
import moment from 'moment';

import Table, { TableHead, TableBody, TableRow, TableCell } from 'core/components/Table';
import Comment from 'core/components/Comment';
import formatValue from 'core/helpers/formatValue';
import colors from 'core/data/colors';
import { paymentsTypesMap } from 'core/data/payments';

class AgreedPaymentsTable extends Component {
	constructor(props) {
		super(props);

		this.state = {
			commentPopupOpen: null
		};
	}

	onOpenCommentPopup = value => {
		this.setState({ commentPopupOpen: value });
	};

	onCloseCommentPopup = value => {
		this.setState({ commentPopupOpen: '' });
	};

	getPaymentStatus = payment => {
		let currentDate = moment(Date.now()).format('YYYY-MM-DD');
		let payDate = payment.date_to_pay;

		let currentDateCount = moment(currentDate).diff(currentDate);
		let payDateCount = moment(payDate).diff(currentDate, 'days');

		let paymentStatus = {};

		if (payment.paid) {
			paymentStatus.text = 'Cancelado';
			paymentStatus.color = colors.green;
		} else if (payment.partially_paid) {
			paymentStatus.text = 'Pago Parcial';
			paymentStatus.color = colors.green;
		} else if (payment.paid === false && payment.partially_paid === false) {
			paymentStatus.text = 'Sin Cancelar';
			paymentStatus.color = colors.greyDark;
		}

		return paymentStatus;
	};

	render() {
		const { commentPopupOpen } = this.state;
		const { agreedPaymentsFull } = this.props;

		return (
			<div className="agreed-payments-table">
				<Table type="data-grid" totalItems={0} onPageChange={this.onPageChange}>
					<TableHead>
						<TableRow>
							<TableCell>No.</TableCell>
							<TableCell>Tipo</TableCell>
							<TableCell>Valor</TableCell>
							<TableCell>Saldo</TableCell>
							<TableCell />
						</TableRow>
					</TableHead>
					<TableBody>
						{agreedPaymentsFull.map((paymentPlan, index) => {
							const paymentStatus = this.getPaymentStatus(paymentPlan);

							return (
								<TableRow key={index}>
									<TableCell>{index + 1}</TableCell>
									<TableCell>{paymentsTypesMap[paymentPlan.type_of_payment]}</TableCell>
									<TableCell>${formatValue(paymentPlan.fee)}</TableCell>
									<TableCell>${formatValue(paymentPlan.debt)}</TableCell>
									<TableCell>
										<span style={{ color: paymentStatus.color }}>{paymentStatus.text}</span>
									</TableCell>
								</TableRow>
							);
						})}
					</TableBody>
				</Table>
			</div>
		);
	}
}

export default AgreedPaymentsTable;
