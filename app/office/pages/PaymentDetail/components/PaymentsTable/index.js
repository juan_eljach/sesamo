import React, { Component } from 'react';
import moment from 'moment';

import Table, { TableHead, TableBody, TableRow, TableCell } from 'core/components/Table';
import Comment from 'core/components/Comment';
import formatValue from 'core/helpers/formatValue';
import { paymentsTypesMap, mediumChoicesMap } from 'core/data/payments';

class PaymentsTable extends Component {
	constructor(props) {
		super(props);
		this.state = {
			commentPopupOpen: null
		};
	}

	onOpenCommentPopup = value => {
		this.setState({ commentPopupOpen: value });
	};

	onCloseCommentPopup = value => {
		this.setState({ commentPopupOpen: '' });
	};

	render() {
		const { commentPopupOpen } = this.state;
		const { payments } = this.props;

		return (
			<div className="payments-table">
				<Table type="data-grid" totalItems={0} onPageChange={this.onPageChange}>
					<TableHead>
						<TableRow>
							<TableCell>No.</TableCell>
							<TableCell>Fecha</TableCell>
							<TableCell>Valor del pago</TableCell>
							<TableCell>Tipo</TableCell>
							<TableCell>Medio</TableCell>
							<TableCell>Observaciones</TableCell>
						</TableRow>
					</TableHead>
					<TableBody>
						{payments.map((payment, index) => {
							return (
								<TableRow key={index}>
									<TableCell>{index + 1}</TableCell>
									<TableCell>
										{payment.date || moment(payment.timestamp).format('YYYY-MM-DD')}
									</TableCell>
									<TableCell>${formatValue(payment.amount)}</TableCell>
									<TableCell>{paymentsTypesMap[payment.type_of_payment] || 'Superavit'}</TableCell>
									<TableCell>{mediumChoicesMap[payment.medium]}</TableCell>
									<TableCell>
										{payment.comment ? (
											<Comment
												label="Observación"
												comment={payment.comment}
												popupOpen={commentPopupOpen === index}
												onOpen={() => this.onOpenCommentPopup(index)}
												onClose={this.onCloseCommentPopup}
											/>
										) : (
											'-'
										)}
									</TableCell>
								</TableRow>
							);
						})}
					</TableBody>
				</Table>
			</div>
		);
	}
}

export default PaymentsTable;
