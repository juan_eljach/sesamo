import { createStore, applyMiddleware, compose, combineReducers } from "redux";
import thunk from "redux-thunk";

// Reducers
import getReducers from "core/redux/getReducers";
const reducerList = [
  "form",
  "burgerMenu",
  "user",
  "menu",
  "project",
  "notificationPopup"
];
const reducers = combineReducers(getReducers(reducerList));

let store;

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
  ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({ name: "officeStore" })
  : compose;

if (process.env.NODE_ENV === "production") {
  store = createStore(reducers, applyMiddleware(thunk));
} else {
  store = createStore(reducers, composeEnhancers(applyMiddleware(thunk)));
}

export default store;
