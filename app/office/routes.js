import React, { Component } from 'react';
import { Switch, HashRouter, Route } from 'react-router-dom';

// Pages
import PaymentPlans from './pages/PaymentPlans';
import PaymentPlan from './pages/PaymentPlan';
import Payments from './pages/Payments';
import PaymentDetail from './pages/PaymentDetail';
import Deeds from './pages/Deeds/Index';
import OnBoarding from './pages/Deeds/OnBoarding';
import SendNotification from './pages/Deeds/SendNotification';
import Support from './pages/Support/Index';
import CreateTicket from './pages/Support/CreateTicket';
import TicketDetail from './pages/Support/TicketDetail';
import DeedsProcess from './pages/Deeds/DeedsProcess';

class AppRouter extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<HashRouter basename="/">
				<section className="office">
					{this.props.children}

					<section className="main">
						<Switch>
							<Route exact path="/" component={PaymentPlans} />
							<Route exact path="/payment-plan/:action" component={PaymentPlan} />
							<Route exact path="/payments" component={Payments} />
							<Route exact path="/payments/:paymentAgreementId" component={PaymentDetail} />
							<Route exact path="/support" component={Support} />
							<Route exact path="/support/create-ticket" component={CreateTicket} />
							<Route exact path="/support/tickets/:ticketSlug" component={TicketDetail} />
							<Route exact path="/deeds" component={Deeds} />
							<Route exact path="/deeds/onboarding" component={OnBoarding} />
							<Route exact path="/deeds/send-notification/:towerId" component={SendNotification} />
							<Route exact path="/deeds/process/:unity" component={DeedsProcess} />
						</Switch>
					</section>
				</section>
			</HashRouter>
		);
	}
}

export default AppRouter;
