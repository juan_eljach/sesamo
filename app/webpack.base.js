const path = require('path');

module.exports = {
  resolve: {
    modules: [
      './',
      './node_modules/'
    ],
    alias: {
      xhr: path.resolve(__dirname, 'core/xhr'),
      static: path.resolve(__dirname, process.cwd(), '../static/'),
      presale: path.resolve(__dirname, 'presale'),
      office: path.resolve(__dirname, 'office'),
    }
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: [
              'env',
              'react'
            ],
            plugins: [
              'transform-object-rest-spread',
              'transform-class-properties',
              'transform-runtime'
            ]
          }
        }
      },
      {
        test: /\.css$/,
        use: [ 'style-loader', 'css-loader' ]
      }
    ]
  }
};
