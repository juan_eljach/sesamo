import axios from 'axios';
import getProjectSlug from 'core/helpers/getProjectSlug';

import getCookie from 'core/helpers/getCookie';
import settings from './settings';

const csrftoken = getCookie('csrftoken');

axios.defaults.baseURL = settings.api;
axios.defaults.headers.common['X-CSRFToken'] = csrftoken;

const xhrProject = axios.create({
  baseURL: `${settings.api}/projects/${getProjectSlug()}`,
  headers: {'X-CSRFToken': csrftoken}
});

export { xhrProject };
export default axios;
