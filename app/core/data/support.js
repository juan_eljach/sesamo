export const typeList = [
  { value: 'modifications', label: 'Modificaciones' },
  { value: 'warranty', label: 'Solicitudes de garantía' },
  { value: 'various', label: 'Varios' },
  { value: 'payment-agreements', label: 'Preguntas sobre planes de pago' },
  { value: 'complaints-claims', label: 'Quejas y reclamos' }
];

export const statusList = [
  { value: 'open', label: 'Abierto' },
  { value: 're-opened', label: 'Re abierto' },
  { value: 'resolved', label: 'Resuelto' },
  { value: 'closed', label: 'Cerrado' },
];

export const priorityList = [
  { value: 'low', label: 'Baja' },
  { value: 'very-low', label: 'Muy baja' },
  { value: 'high', label: 'Alta' },
  { value: 'critical', label: 'Crítica' },
];

export const typeListMap = {
  'modifications': 'Modificaciones',
  'warranty': 'Solicitudes de garantía',
  'various': 'Varios',
  'payment-agreements': 'Preguntas sobre planes de pago',
  'complaints-claims': 'Quejas y reclamos'
};

export const priorityListMap = {
  'low': 'Baja',
  'very-low': 'Muy baja',
  'high': 'Alta',
  'critical': 'Crítica',
};

export const filterTicketsList = [
  { value: 'all', label: 'Todos los Tickets' },
  { value: 'administrator', label: 'Tickets que administras' },
  { value: 'collaborator', label: 'Tickets en que eres colaborador' },
  { value: 'unassigned', label: 'Tickets sin asignar' }
];

export const listMap = { type: 'Tipo', status: 'Estado', priority: 'Prioridad' };
