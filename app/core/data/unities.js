const unityStatusList = [
  { value: "available", label: "Disponible" },
  { value: "virtually_booked", label: "Separado tent." },
  { value: "booked_not_formalized", label: "Separado" },
  { value: "sold", label: "Vendido" }
  // {value: 'legalized', label: 'Legalizado'}
];

const unityStatusListMap = {
  sold: "Vendido",
  virtually_booked: "Separado tent"
};

export { unityStatusList };
