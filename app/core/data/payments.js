const mediumChoices = {
  Bancolombia: "bancolombia",
  "Caja Social": "caja_social",
  BBVA: "bbva",
  "Banco de Bogotá": "banco-bogota",
  Davivienda: "davivienda",
  "Cheque de gerencia": "cheque-gerencia",
  Efectivo: "efectivo",
  "Fiduciaria Bogotá": "fiduciaria-bogota",
  Otro: "otro"
};

const mediumChoicesMap = {
  bancolombia: "Bancolombia",
  caja_social: "Caja Social",
  bbva: "BBVA",
  "banco-bogota": "Banco de Bogotá",
  davivienda: "Davivienda",
  "cheque-gerencia": "Cheque de gerencia",
  efectivo: "Efectivo",
  "fiduciaria-bogota": "Fiduciaria Bogotá",
  otro: "Otro"
};

const paymentsTypes = {
  "Cuota Mensual": "cuota-mensual",
  "Ahorro Programado": "ahorro-programado",
  Cesantías: "cesantias",
  "Subsidio de Vivienda": "subsidio-vivienda",
  "Otro Pago Acordado": "otro-pago-acordado",
  "Gastos Notariales": "gastos-notariales",
  "Otros Conceptos": "otros-conceptos",
  "Crédito Hipotecario": "credito-hipotecario",
  "Kit de Acabados": "kit-acabados",
  "Cambios/Modificaciones": "cambios-modificaciones",
  "Descuento Financiero": "descuento-financiero",
  Separación: "separacion"
};

const paymentsTypesMap = {
  "cuota-mensual": "Cuota Mensual",
  "ahorro-programado": "Ahorro Programado",
  cesantias: "Cesantías",
  "subsidio-vivienda": "Subsidio de Vivienda",
  separacion: "Separación",
  "otro-pago-acordado": "Otro Pago Acordado",
  "gastos-notariales": "Gastos Notariales",
  "otros-conceptos": "Otros Conceptos",
  "credito-hipotecario": "Crédito Hipotecario",
  "kit-acabados": "Kit de Acabados",
  "cambios-modificaciones": "Cambios/Modificaciones",
  "descuento-financiero": "Descuento Financiero"
};

export { mediumChoices, mediumChoicesMap, paymentsTypes, paymentsTypesMap };
