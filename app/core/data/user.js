const userTypeList = {
  gerente: 'GERENTE',
  vendedor: 'VENDEDOR',
};

const presaleMenuList = [
  {value: 'home', text: 'Inicio', img: '/static/app/img/diploma.png', link: '/'},
  {value: 'customers', text: 'Todos los clientes', img: '/static/app/img/payments.png', link: '/customers'},
  {value: 'unities', text: 'Tabla de unidades', img: '/static/app/img/keys.png', link: '/unities'}
];

const officeMenuList = [
  {value: 'formalization', text: 'Formalización', img: '/static/app/img/check.png', link: '/'},
  {value: 'payments', text: 'Pagos', img: '/static/app/img/payments.png', link: '/payments'},
  {value: 'support', text: 'Soporte', img: '/static/app/img/support.png', link: '/support'},
  {value: 'deeds', text: 'Escrituración', img: '/static/app/img/diploma.png', link: '/deeds'}
];

export { userTypeList, presaleMenuList, officeMenuList };
