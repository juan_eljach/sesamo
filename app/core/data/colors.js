const colors = {
  green: '#81C784',
  red: '#ff8a80',
  blue: '#00a8ff',
  yellow: '#ffd54f',
  purple: '#9575cd',
  fucsia: '#f06292',
  orange: '#FB8C55',
  grey: '#F9F8FD',
  greyDark: '#7e7e7e'
};

const colorsList = ['#81C784', '#ff8a80', '#00a8ff', '#ffd54f', '#9575cd', '#f06292', '#FB8C55'];

export { colorsList };
export default colors;
