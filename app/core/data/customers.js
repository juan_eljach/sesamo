const adquisitionChannelList = [
  {value: 'room', label: 'Sala de Ventas'},
  {value: 'fair', label: 'Feria'},
  {value: 'phone', label: 'Teléfono'},
  {value: 'press', label: 'Publicación en Prensa'},
  {value: 'office', label: 'Oficina'},
  {value: 'referal', label: 'Referido'},
  {value: 'web', label: 'Página Web'},
  {value: 'radio', label: 'Radio'},
  {value: 'otros', label: 'Otros'}
];

const segmentList = [
  {value: 'high', label: 'Altamente probable'},
  {value: 'medium', label: 'Medianamente probable'},
  {value: 'low', label: 'Poco probable'},
];

export { adquisitionChannelList, segmentList };
