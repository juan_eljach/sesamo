import moment from 'moment';

moment.locale('es');

export default {
  api: `${window.location.origin}/api`,
  app: process.env.APP
};
