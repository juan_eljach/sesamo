import React, { Component } from 'react';
import Select from 'react-select';
import classnames from 'classnames';
import PropTypes from 'prop-types';

import Chip from 'core/components/Chip';
import Initials from 'core/components/Initials';

class Searcher extends Component {
  constructor (props) {
    super(props);

    this.preventEvents = ['input-blur', 'menu-close'];
  }

  /**
   * Handler when the input changes
   * @param {string} value - The value of the input
   * @param {event} value - The event of the input given by react-select
   */
  onInputChange = (value, event) => {
    if (event.action === 'input-change') {
      this.props.onInputChange(value);
    }
  }

  getCustomOption = ({ data, ...props }) => {
    const initials = `${data.first_name[0]}${data.last_name[0]}`;
    const name = `${data.first_name} ${data.last_name}`;

    return (
      <div className="searcher__custom-option" onClick={() => props.selectOption(data)}>
        <Initials initials={initials} />
        <div className="searcher__custom-option-info">
          <div className="searcher__custom-option-name">{name}</div>
          <div>
            <span>{data.email}</span>
            <span className="searcher__custom-option-separator"> | </span>
            <span className="searcher__custom-option-cellphone">{data.cellphone}</span>
          </div>
        </div>
      </div>
    );
  }

  render () {
    const {
      inputValue,
      options,
      label,
      placeholder,
      onChange,
      onCloseChip,
      showChip,
      chipInitials,
      chipLabel,
      withCustomOption,
      isDisabled,
      ...rest
    } = this.props;

    const searcherClassName = classnames(
      'searcher',
      {'searcher--chip-open': showChip}
    );

    let restProps = {...rest};

    if (withCustomOption) restProps.components = {Option: this.getCustomOption};

    return (
      <div className={searcherClassName}>
        <Select
          {...restProps}
          value={inputValue}
          inputValue={inputValue}
          placeholder={placeholder}
          noOptionsMessage={() => 'No se encontraron resultados'}
          options={options}
          loadingMessage={() => 'Buscando...'}
          classNamePrefix="searcher-select"
          onInputChange={this.onInputChange}
          onChange={onChange}
          isDisabled={showChip || isDisabled}
        />

        {
          !!showChip && <Chip initials={chipInitials} label={chipLabel} onCloseChip={onCloseChip} />
        }

        <i className="mdi mdi-magnify"></i>
      </div>
    );
  }
}

Searcher.defaultProps = {
  options: [],
  placeholder: 'Buscar persona'
};

Searcher.propTypes = {
  placeholder: PropTypes.string,
  onInputChange: PropTypes.func.isRequired,
  onChange: PropTypes.func,
  showChip: PropTypes.bool,
  chipInitials: PropTypes.string,
  chipLabel: PropTypes.string
};

export default Searcher;
