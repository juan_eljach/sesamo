import React, { Component } from 'react';
import { Tooltip as Tippy } from 'react-tippy';
import PropTypes from 'prop-types';

class Tooltip extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		const { title, children, ...rest } = this.props;

		return (
			<Tippy title={title} animation="fade" {...rest}>
				{children}
			</Tippy>
		);
	}
}

Tooltip.propTypes = {
	title: PropTypes.string,
	children: PropTypes.object
};

export default Tooltip;
