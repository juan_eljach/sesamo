import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as notificationPopupActions from 'core/redux/notificationPopup/actions';

class NotificationPopup extends Component {
  constructor (props) {
    super(props);
  }

  render () {
    const { opened, options } = this.props;

    if (!opened) return null;

    setTimeout(this.props.hideNotificationPopup, 5000);

    return (
      <div className="notification-popup v-align">
        <div className="notification-popup__container v-align">
          <div className="notification-popup__icon">
            <i className="mdi mdi-check-circle-outline"></i>
          </div>

          <div className="notification-popup__message">
            {options.message}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({notificationPopup}) => {
  return {
    opened: notificationPopup.opened,
    options: notificationPopup.options
  };
};

const mapDispatchToProps = {
  hideNotificationPopup: notificationPopupActions.hideNotificationPopup
};

export default connect(mapStateToProps, mapDispatchToProps)(NotificationPopup);
