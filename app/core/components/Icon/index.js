import React, { Component } from 'react';
import PropTypes from 'prop-types';

import * as icons from './icons';


class Icon extends Component {
  constructor(props) {
    super(props);
  }
  render () {
    const { icon } = this.props;

    if (!icons[icon]) return null;

    return (
      icons[icon]()
    );
  }
}


Icon.propTypes = {
  icon: PropTypes.string.isRequired
};

export default Icon;
