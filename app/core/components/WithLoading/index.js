import React from 'react';
import Loading from 'core/components/Loading';
import PropTypes from 'prop-types';

const WithLoading = ({ children, isFetching }) => {
  if (isFetching) return <Loading mode="section" />;

  return children;
};

WithLoading.proptypes = {
  isFetching: PropTypes.bool
};

export default WithLoading;
