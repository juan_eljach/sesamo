import React, { Component } from 'react';
import PropTypes from 'prop-types';

import colors from 'core/data/colors';

class Initials extends Component {
	constructor(props) {
		super(props);
	}

	shouldComponentUpdate(nextProps) {
		return false;
	}

	render() {
		const { initials, color } = this.props;

		return (
			<div className="initials" style={{ backgroundColor: color }}>
				{initials.toUpperCase()}
			</div>
		);
	}
}

Initials.defaultProps = {
	color: colors.blue
};

Initials.propTypes = {
	initials: PropTypes.string.isRequired,
	color: PropTypes.string.isRequired
};

export default Initials;
