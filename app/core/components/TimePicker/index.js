import React, { Component } from 'react';
import ReactTimePicker from 'rc-time-picker/lib/TimePicker';
import moment from 'moment';
import PropTypes from 'prop-types';

import 'rc-time-picker/assets/index.css';


class TimePicker extends Component {
  constructor (props) {
    super(props);
  }

  render () {
    return (
      <div className="time-picker">
        <ReactTimePicker {...this.props} />

        <i className="mdi mdi-chevron-down"></i>
      </div>
    );
  }
}

TimePicker.defaultProps = {
  defaultValue: moment('14:00:00', 'HH:mm'),
  showSecond: false,
  use12Hours: true
};

TimePicker.propTypes = {
  defaultValue: PropTypes.object,
  showSecond: PropTypes.bool,
  use12Hours: PropTypes.bool
};

export default TimePicker;
