import React, { Component } from "react";
import locale from "rc-calendar/lib/locale/es_ES";
import PropTypes from "prop-types";
import moment from "moment";
import "rc-calendar/assets/index.css";

import Calendar from "rc-calendar/lib/Calendar";
import Picker from "rc-calendar/lib/Picker";

class DatePicker extends Component {
  renderDatePicker = (value, placeholder) => {
    return (
      <div className="date-picker v-align" tabIndex="0">
        <i className="mdi mdi-calendar" />
        {!value ? (
          <span className="date-picker__placeholder">{placeholder}</span>
        ) : (
          <span className="date-picker__value">{value}</span>
        )}
        <i className="mdi mdi-chevron-down" />
      </div>
    );
  };

  disableDate = (type, current) => {
    if (!current) {
      // allow empty select
      return false;
    }
    const date = moment();

    let init = type === "past" ? 0 : 24;

    date.hour(init);
    date.minute(init);
    date.second(init);

    if (type === "past") {
      return current.valueOf() < date.valueOf();
    }

    if (type === "future") {
      return current.valueOf() > date.valueOf();
    }
  };

  handleDisableYounger = current => {
    const allowedYear = moment().subtract(18, "years");

    return current.year() > allowedYear.year();
  };

  render() {
    const { placeholder, onChange, value, ...rest } = this.props;

    const calendar = (
      <Calendar
        locale={locale}
        className="date-picker__wrap"
        showToday={false}
        disabledDate={
          rest.disableYounger
            ? this.handleDisableYounger
            : (rest.disabledPastDate || rest.disabledFutureDate) &&
              (current =>
                this.disableDate(
                  rest.disabledPastDate ? "past" : "future",
                  current
                ))
        }
      />
    );

    return (
      <Picker calendar={calendar} onChange={onChange} animation="slide-up">
        {val => {
          return this.renderDatePicker(
            value && value.format("DD-MM-YYYY"),
            placeholder
          );
        }}
      </Picker>
    );
  }
}

DatePicker.propTypes = {
  defaultValue: PropTypes.object,
  placeholder: PropTypes.string,
  icon: PropTypes.bool
};

DatePicker.defaultProps = {
  placeholder: "Seleccionar",
  icon: true
};

export default DatePicker;
