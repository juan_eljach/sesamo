import React, { Component } from "react";
import { connect } from "react-redux";
import { action as toggleMenu } from "redux-burger-menu";
import PropTypes from "prop-types";

import { userTypeList } from "core/data/user";
import settings from "core/settings";
import * as userActions from "core/redux/user/operations";
import * as projectActions from "core/redux/project/operations";

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      menuOpen: false,
      menuMobileOpen: false,
      anchorEl: null
    };
  }

  async componentDidMount() {
    await this.props.getUser();
    await this.props.getProject();

    document.addEventListener("mousedown", this.handleShareClose, false);
  }

  componentWillUnmount = () => {
    document.removeEventListener("mousedown", this.handleShareClose, false);
  };

  handleShareClose = e => {
    if (!this.element) return;

    if (this.element.contains(e.target)) {
      return;
    }

    this.toggleUserMenu();
  };

  toggleMenuMobile = () => {
    this.setState({ menuMobileOpen: true });
  };

  toggleUserMenu = () => {
    this.setState({ menuOpen: !this.state.menuOpen });
  };

  openNavBar = () => {
    this.props.store.dispatch(toggleMenu(!this.props.isOpen));
  };

  render() {
    const { menuOpen } = this.state;
    const { user } = this.props;

    return (
      <header className="header v-align">
        <div
          className="header__show-menu v-align hide-for-large"
          onClick={this.openNavBar}
        >
          <div>Ver menu</div>
          <i className="mdi mdi-menu" />
        </div>

        <div
          className="header__logo show-for-large"
          onClick={() => {
            window.location.href = "/projects";
          }}
        >
          <h1>Sesamo</h1>
        </div>

        <div className="header__nav v-align">
          <div className="header__menu">
            <ul>
              <li>
                {settings.app === "presale" ? (
                  <a href={`/projects/${user.projectSlug}/office/#/`}>
                    Ir a Office
                  </a>
                ) : (
                  <a href={`/projects/${user.projectSlug}/presale/#/`}>
                    Ir a Preventa
                  </a>
                )}
              </li>
            </ul>
          </div>

          <div className="header__user" onClick={this.toggleUserMenu}>
            <div className="header__user-info v-align">
              <div className="header__user-info-profile">
                <div>
                  {user.first_name} {user.last_name}
                </div>
                <span>{userTypeList[user.user_type]}</span>
              </div>

              <i className="mdi mdi-menu-down" />
            </div>

            {menuOpen && (
              <div
                className="header__user-menu"
                ref={element => (this.element = element)}
              >
                <ul>
                  <li
                    className="v-align"
                    onClick={() => (window.location.href = "/projects/")}
                  >
                    <i className="mdi mdi-file-document-box-outline" />
                    <span>Proyectos</span>
                  </li>
                  <li
                    className="v-align"
                    onClick={() => (window.location.href = "/logout")}
                  >
                    <i className="mdi mdi-exit-to-app" />
                    <span>Cerrar sesión</span>
                  </li>
                </ul>
              </div>
            )}
          </div>
        </div>
      </header>
    );
  }
}

const mapStateToProps = ({ user, burgerMenu }) => {
  return {
    user,
    isOpen: burgerMenu.isOpen
  };
};

const HeaderConnected = connect(mapStateToProps, {
  getUser: userActions.getUser,
  getProject: projectActions.getProject
})(Header);

HeaderConnected.propTypes = {
  store: PropTypes.object.isRequired
};

export default HeaderConnected;
