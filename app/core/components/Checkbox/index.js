import React, { Component } from 'react';
import MaterialUICheckbox from '@material-ui/core/Checkbox';
import PropTypes from 'prop-types';

import colors from 'core/data/colors';

class Checkbox extends Component {
  constructor (props) {
    super(props);
    this.state = {};


  }

  renderCheckIcon = (done) => {
    const { style } = this.props;

    if (done) {
      return style === 'square' ? (
        <i className="mdi mdi-checkbox-marked"></i>
      ) : (
        <i className="mdi mdi-check-circle"></i>
      );
    }

    return style === 'square' ? (
      <i className="mdi mdi-checkbox-blank-outline"></i>
    ) : (
      <i className="mdi mdi-checkbox-blank-circle-outline"></i>
    );
  }

  render () {
    const {
      label,
      color,
      checked,
      disabled,
      onChange
    } = this.props;

    /**
     * TODO: Refactorizar el render del checkbox en una método(DRY).
     */

    if (!label) {
      return (
        <MaterialUICheckbox
          icon={this.renderCheckIcon(checked)}
          checkedIcon={this.renderCheckIcon(checked)}
          disabled={disabled}
          style={{ color: checked ? colors[color] : '#7e7e7e', width: 20}}
          onChange={onChange}
        />
      );
    }

    return (
      <label htmlFor="">
        <MaterialUICheckbox
          icon={this.renderCheckIcon(checked)}
          checkedIcon={this.renderCheckIcon(checked)}
          disabled={disabled}
          style={{color: checked ? colors[color] : '#7e7e7e', width: 20, marginRight: 3, height: 10}}
          onChange={onChange}
        />

        {label}
      </label>
    );
  }
}

Checkbox.propTypes = {
  label: PropTypes.string,
  checked: PropTypes.bool,
  color: PropTypes.PropTypes.oneOf(['green', 'blue']),
  disabled: PropTypes.bool,
  onChange: PropTypes.func,
  style: PropTypes.oneOf(['round', 'square'])
};

Checkbox.defaultProps = {
  style: 'round',
  color: 'green',
  disabled: false
};

export default Checkbox;
