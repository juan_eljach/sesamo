import React, { Component } from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import classnames from 'classnames';
import PropTypes from 'prop-types';

class Button extends Component {
  constructor (props) {
    super(props);
  }

  onClick = (event) => {
    const { onClick, disabled, isFetching } = this.props;

    if (disabled) return;

    if (isFetching) {
      return event.preventDefault();
    }

    if (onClick) {
      onClick();
    }
  }

  render () {
    const { children, type, color, disabled, isFetching } = this.props;
    const buttonClassName = classnames(
      'button',
      `button--${color}`,
      {'button--secondary': type === 'secondary'},
      {'button--disabled': disabled}
    );

    return (
      <button className={buttonClassName} onClick={this.onClick}>
        {
          isFetching ? (
            <CircularProgress size={20} style={{color: 'white'}} />
          ) : children
        }

      </button>
    );
  }
}

Button.defaultProps = {
  color: 'blue'
};

Button.propTypes = {
  children: PropTypes.any.isRequired,
  color: PropTypes.string,
  type: PropTypes.string,
  disabled: PropTypes.bool,
  isFetching: PropTypes.bool
};

export default Button;
