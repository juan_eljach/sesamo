import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Initials from 'core/components/Initials';

class CustomerCard extends Component {
  constructor(props) {
    super(props);
    this.state = {  }
  }
  render() {
    const { name, initials, onClose } = this.props;

    return (
      <div className="customer-card v-align">
        <Initials initials={initials} />
        <span>{name}</span>

        <i className="mdi mdi-close action-hover" onClick={onClose}></i>
      </div>
    );
  }
}

CustomerCard.propTypes = {
  name: PropTypes.string.isRequired,
  initials: PropTypes.string.isRequired,
  onClose: PropTypes.func
};

export default CustomerCard;
