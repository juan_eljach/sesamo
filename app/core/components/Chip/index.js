import React, { Component } from 'react';
import Initials from 'core/components/Initials';
import PropTypes from 'prop-types';

class Chip extends Component {
  constructor (props) {
    super(props);
  }
  render () {
    const { initials, label, onCloseChip } = this.props;

    return (
      <div className="chip v-align">
        { initials && <Initials initials={initials} color="cool-blue" />}
        <span>{label}</span>
        <i className="mdi mdi-close" onClick={onCloseChip}></i>
      </div>
    );
  }
}

Chip.propTypes = {
  initials: PropTypes.string,
  label: PropTypes.string.isRequired,
  onCloseChip: PropTypes.func
};

export default Chip;
