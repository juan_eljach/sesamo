import React, { Component } from "react";
import Select from "react-select";
import NumberFormat from "react-number-format";
import classnames from "classnames";
import PropTypes from "prop-types";

import formatValue from "core/helpers/formatValue";
import DatePickerComponent from "core/components/DatePicker";

class InputField extends Component {
  constructor(props) {
    super(props);
  }

  renderInputField = () => {
    const {
      type,
      label,
      placeholder,
      selectPlaceholder,
      input,
      withPriceIcon,
      showNumberIcon,
      withPoints,
      meta: { touched, error },
      ...rest
    } = this.props;

    const inputFieldClassName = classnames(
      "input-field",
      { "input-field--disabled": rest.disabled },
      { "input-field--with-price-icon": withPriceIcon },
      { "input-field--error": touched && error }
    );

    if (["text", "number", "password"].includes(type)) {
      let inputValues;

      if (withPoints) {
        inputValues = {
          ...input,
          value: formatValue(input.value.replace(/\./g, ""))
        };
      } else {
        inputValues = input || {};
      }

      const priceIconClassName = classnames("input-field__price-icon", {
        "input-field__price-icon-with-label": withPriceIcon && label
      });

      const {
        onBlur,
        onChange: onChangeInputValues,
        ...inputValuesRest
      } = inputValues;

      return (
        <div className={inputFieldClassName}>
          {label && <label className="input-field-label">{label}</label>}

          {showNumberIcon && withPriceIcon && (
            <div className={priceIconClassName}>$</div>
          )}

          {type === "number" && withPriceIcon ? (
            <NumberFormat
              thousandSeparator="."
              decimalSeparator=","
              // Decide whether to call component or redux form onChange
              onValueChange={(values, e) =>
                rest.onChange
                  ? rest.onChange({ ...e, target: { value: values.value } })
                  : onChangeInputValues(values.value)
              }
              value={rest.value}
              placeholder={placeholder}
              disabled={rest.disabled}
              {...inputValuesRest}
            />
          ) : (
            <input
              type={type}
              placeholder={placeholder}
              {...inputValues}
              {...rest}
            />
          )}

          {touched && error && (
            <div className="input-field__error">{error}</div>
          )}
        </div>
      );
    }

    if (type === "select") {
      const inputSelectClassName = classnames(
        "input-field-select",
        { "input-field-select--error": touched && error },
        { "input-field-select--rounded": rest.rounded }
      );

      return (
        <div className={inputSelectClassName}>
          {label && <label className="input-field-label">{label}</label>}
          <Select
            classNamePrefix="input-field-select"
            placeholder={selectPlaceholder}
            noOptionsMessage={() => "No se encontraron resultados"}
            isDisabled={rest.disabled}
            isSearchable={false}
            {...input}
            {...rest}
            onBlur={() => input && input.onBlur(input.value)}
          />

          {touched && error && (
            <div className="input-field__error">{error}</div>
          )}
        </div>
      );
    }

    if (type === "date") {
      const inputDateClassName = classnames(
        "input-field",
        "input-field-date",
        { "input-field-date--disabled": rest.disabled },
        { "input-field-date--error": touched && error }
      );

      const dateInput = input || rest || {};

      return (
        <div className={inputDateClassName}>
          {label && <label className="input-field-label">{label}</label>}

          <DatePickerComponent
            placeholder={placeholder}
            onChange={dateInput.onChange}
            {...rest}
            value={dateInput.value}
          />

          {touched && error && (
            <div className="input-field__error">{error}</div>
          )}
        </div>
      );
    }

    if (type === "textarea") {
      return (
        <div className="input-field input-field-textarea">
          {label && <label className="input-field-label">{label}</label>}

          <textarea rows="4" placeholder={placeholder} {...input} {...rest} />

          {touched && error && (
            <div className="input-field__error">{error}</div>
          )}
        </div>
      );
    }
  };

  render() {
    return this.renderInputField();
  }
}

InputField.defaultProps = {
  meta: {},
  selectPlaceholder: "Seleccionar",
  showNumberIcon: true
};

InputField.propTypes = {
  type: PropTypes.string.isRequired,
  label: PropTypes.string,
  placeholder: PropTypes.string,
  selectPlaceholder: PropTypes.string,
  showNumberIcon: PropTypes.bool,
  input: PropTypes.object,
  rest: PropTypes.object,
  withPriceIcon: PropTypes.bool
};

export default InputField;
