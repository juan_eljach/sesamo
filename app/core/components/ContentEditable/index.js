import React, { Component } from 'react';
import ReactContentEditable from "react-contenteditable";
import sanitizeHtml from "sanitize-html";
import PropTypes from 'prop-types';

class ContentEditable extends Component {
  constructor(props) {
    super(props);

    this.state = {
      editable: false
    };

    this.sanitizeConf = {
      allowedTags: ["b", "i", "em", "span", "strong", "a", "p", "h1"],
      allowedAttributes: { a: ["href"] }
    };

    this.tags = ["{{tower}}", "{{unity}}", "{{date}}", "{{project}}"];

    this.htmlEl = null;
  }

  sanitize = () => {
    this.setState({
      html: sanitizeHtml(this.state.html, this.sanitizeConf)
    });
  }

  onContentEditableChange = event => {
    const selection = window.getSelection();
    const range = document.createRange();

    const anchorNode = selection.anchorNode;

    if (anchorNode.data === " " || this.tags.includes(anchorNode.data)) {
      if (anchorNode.nodeName === "#text" && anchorNode.previousSibling) {
        let rangeStart;
        let rangeEnd;

        if (anchorNode.previousElementSibling && anchorNode.previousElementSibling.nodeName !== "#text") {
          rangeStart = anchorNode.previousSibling.previousSibling;
          rangeEnd = anchorNode.previousSibling.previousSibling.textContent.length;
        } else {
          rangeStart = anchorNode.previousSibling;
          rangeEnd = 1;
        }

        range.setStart(rangeStart, rangeEnd);
        range.collapse(false);
        selection.removeAllRanges();
        selection.addRange(range);

        return;
      }

      range.setStart(anchorNode, 0);
      range.collapse(true);
      selection.removeAllRanges();
      selection.addRange(range);

      return;
    }

    this.props.onChange(event.target.value);
  }

  onClickEdit = () => {
    this.setState({ editable: true }, () => this.htmlEl.focus());
  }

  onContentEditableBlur = event => {
    // this.props.onChange(sanitizeHtml(this.props.html, this.sanitizeConf));
  }


  render() {
    const { editable } = this.state;
    const { html } = this.props;

    return (
      <div className="content-editable">
        <div className="content-editable__header">
          <div className="content-editable__header-edit v-align" onClick={this.onClickEdit}>
            <i className="mdi mdi-pencil"></i>
            <span>Editar</span>
          </div>
        </div>

        <ReactContentEditable
          ref={(el) => (this.htmlEl = el && el.htmlEl)}
          className="editable"
          tagName="pre"
          html={html} // innerHTML of the editable div
          disabled={!editable} // use true to disable edition
          onChange={this.onContentEditableChange} // handle innerHTML change
          onBlur={this.onContentEditableBlur}
        />
      </div>
    );
  }
}

ContentEditable.defaultProps = {
  editable: true,
  html: ''
};

ContentEditable.propTypes = {
  html: PropTypes.string,
  onChange: PropTypes.func
};

export default ContentEditable;
