import React, { Component } from 'react';
import { Line } from 'rc-progress';
import PropTypes from 'prop-types';

import colors from 'core/data/colors';

class Progress extends Component {
  constructor(props) {
    super(props);
    this.state = {  }
  }
  render() {
    return (
      <Line
        {...this.props}
      />
    );
  }
}

Progress.defaultProps = {
  percent: 10,
  strokeColor: colors.blue,
  strokeWidth: 4,
  trailWidth: 4
};

Progress.propTypes = {
  percent: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]),
  strokeColor: PropTypes.string,
  strokeWidth: PropTypes.number,
  trailWidth: PropTypes.number
};

export default Progress;
