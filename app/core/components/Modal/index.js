import React, { Component } from 'react';
import Slide from '@material-ui/core/Slide';
import { CSSTransition, TransitionGroup } from 'react-transition-group';
import PropTypes from 'prop-types';

class Modal extends Component {
  constructor (props) {
    super(props);
  }

  render () {
    const { open, onClose } = this.props;

    return (
      <CSSTransition
          in={open}
          appear={true}
          timeout={250}
          classNames="fade"
          unmountOnExit>

          { state => {
            return (
              <div className="modal">
                <CSSTransition
                  in={state === 'entered'}
                  timeout={250}
                  classNames="slide"
                  unmountOnExit>

                  { this.props.children }
                </CSSTransition>
              </div>
            );
          } }
        </CSSTransition>
    );
  }
}

Modal.propTypes = {
  open: PropTypes.bool,
  onClose: PropTypes.func
};

export default Modal;
