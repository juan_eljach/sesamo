import React, { Component } from "react";
import { connect } from "react-redux";
import { slide as Menu } from "react-burger-menu";
import { decorator as reduxBurgerMenu } from "redux-burger-menu";
import { officeMenuList } from "core/data/user";

import Navbar from "core/components/Navbar";

class Drawer extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { isOpen, menuItems } = this.props;

    return (
      <Menu isOpen={isOpen} width={280}>
        <Navbar menuItems={menuItems} store={this.props.store} />
      </Menu>
    );
  }
}

const mapStateToProps = ({ burgerMenu }) => {
  return {
    isOpen: burgerMenu.isOpen
  };
};

const DrawerConnected = connect(mapStateToProps)(Drawer);

export default reduxBurgerMenu(DrawerConnected);
