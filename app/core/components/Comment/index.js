import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Comment extends Component {
  constructor(props) {
    super(props);
    this.state = {  }
  }
  render() {
    const { user, comment, label, popupOpen, onOpen, onClose } = this.props;

    return (
      <div className="comment">
        <div className="comment__label v-align">
          <div className="comment__label-text">
            {label}

            <span>1</span>
          </div>

          <a onClick={onOpen}>ver</a>
        </div>

        {
          popupOpen && (
            <div className="comment__popup">
              <i className="mdi mdi-close action-hover" onClick={onClose}></i>
              <p>
                {user && <span>{user}:</span>} {comment}
              </p>
            </div>
          )
        }
      </div>
    );
  }
}

Comment.defaultProps = {
  label: "Comentario"
};

Comment.propTypes = {
  label: PropTypes.string,
  popupOpen: PropTypes.bool,
  onOpen: PropTypes.func,
  onClose: PropTypes.func
};

export default Comment;
