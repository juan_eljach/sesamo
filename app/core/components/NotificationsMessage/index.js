import React, { Component } from 'react';

import ContentEditable from "core/components/ContentEditable";

class SendNotificationEdit extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { html, onContentEditableChange } = this.props;

    return (
      <div className="send-notification-edit">
        <ContentEditable
          html={html}
          onChange={onContentEditableChange}
        />
      </div>
    );
  }
}

export default SendNotificationEdit;
