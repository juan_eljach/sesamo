import React, { Component } from 'react';
import ReactSwitch from 'rc-switch'

class Switch extends Component {
  constructor(props) {
    super(props);
    this.state = {  }
  }

  render() {
    return (
      <ReactSwitch {...this.props} />
    );
  }
}

export default Switch;
