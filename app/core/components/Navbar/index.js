import React, { Component } from "react";
import { Link } from "react-router-dom";
import classnames from "classnames";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import * as menuActions from "core/redux/menu/actions";
import { withRouter } from "react-router";
import { action as toggleMenu } from "redux-burger-menu";

import settings from "core/settings";

class Navbar extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const activeMenu = sessionStorage.getItem("activeMenu");

    if (!activeMenu) {
      this.props.setMenu(settings.app === "presale" ? "home" : "formalization");
    } else {
      this.props.setMenu(activeMenu);
    }

    const handleHashChange = () => {
      const { pathname } = this.props.history.location;

      if (pathname === "/") return this.props.setMenu("formalization");

      this.props.setMenu(pathname.split("/")[1]);
    };

    window.addEventListener("hashchange", handleHashChange, false);
  }

  componentWillUnmount() {
    window.removeEventListener("hashchange", handleHashChange, false);
  }

  onSelectItem = value => {
    this.props.setMenu(value);

    this.saveMenuToStorage(value);

    this.props.store.dispatch(toggleMenu(!this.props.isMenuOpen));
  };

  saveMenuToStorage = activeMenu => {
    sessionStorage.setItem("activeMenu", activeMenu);
  };

  render() {
    const { menuItems, activeMenu } = this.props;

    return (
      <div className="navbar">
        <ul>
          {menuItems.map(item => {
            const itemClassName = classnames({
              "navbar--item-active": activeMenu === item.value
            });

            return (
              <li
                key={item.value}
                className={itemClassName}
                onClick={() => this.onSelectItem(item.value)}
              >
                <Link to={item.link} className="v-align">
                  <img src={item.img} alt={item.text} />
                  <span>{item.text}</span>
                </Link>
              </li>
            );
          })}
        </ul>
      </div>
    );
  }
}

Navbar.propTypes = {
  menuItems: PropTypes.arrayOf(PropTypes.object)
};

const mapStateToProps = ({ menu, burgerMenu }) => {
  return { activeMenu: menu.activeMenu, isMenuOpen: burgerMenu.isOpen };
};

export default connect(mapStateToProps, {
  setMenu: menuActions.setMenu
})(withRouter(Navbar));
