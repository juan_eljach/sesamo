import React, { Component } from 'react';

class Loading extends Component {
  constructor(props) {
    super(props);
    this.state = {  }
  }
  render() {
    return (
      <div className="loading">
        <div className="loading__showbox">
          <div className="loading__loader">
            <svg className="loading__circular" viewBox="25 25 50 50">
              <circle className="loading__path" cx="50" cy="50" r="20" fill="none" strokeWidth="3" strokeMiterlimit="10"/>
            </svg>
          </div>
        </div>
      </div>
    );
  }
}

export default Loading
