import React, { Component, createContext, Fragment } from 'react';
import classnames from 'classnames';
import Pagination from 'rc-pagination';
import 'rc-pagination/assets/index.css';

const TableContext = createContext();

const TableHead = ({ children }) => {
	let child = React.Children.map(children, c => {
		return React.cloneElement(c, { parent: 'head' });
	});

	return <thead className="table__head">{child}</thead>;
};

const TableBody = ({ children }) => {
	let child = React.Children.map(children, c => {
		return React.cloneElement(c, { parent: 'body' });
	});

	return <tbody className="table__body">{child}</tbody>;
};

const TableRow = ({ children, parent, section, ...rest }) => {
	return (
		<TableContext.Consumer>
			{type => {
				if (parent === 'head' && type === 'unities') {
					return (
						<tr className="table__row">
							<TableCell>
								<div className="table__head-indicators">
									<span>Piso</span>
									<span>Apto</span>
								</div>
							</TableCell>
							{children}
						</tr>
					);
				}

				return <tr className="table__row">{children}</tr>;
			}}
		</TableContext.Consumer>
	);
};

const TableCell = ({ children, ...rest }) => {
	return <td className="table__cell">{children}</td>;
};

class Table extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		const { children, type, totalItems, onPageChange } = this.props;
		const tableClassName = classnames('table', { 'table-data-grid': type === 'data-grid' });

		return (
			<TableContext.Provider value={type}>
				<table className={tableClassName}>{children}</table>
				{totalItems >= 15 && <Pagination total={totalItems} onChange={onPageChange} pageSize={15} />}
			</TableContext.Provider>
		);
	}
}

export { Table, TableHead, TableBody, TableRow, TableCell };

export default Table;
