const getChipInitials = (value) => {
  if (!value) return '';

  const splited = value.split(' ');
  const chipInitials = splited && `${splited[0][0]}${splited[1][0]}`;

  return chipInitials;
};

export default getChipInitials;
