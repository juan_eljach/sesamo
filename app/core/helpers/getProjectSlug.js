/**
 * @description: Get the project slug while looking for a better way to get it
 * @return: {string}
 * TODO: Refactor it
 */
const getProjectSlug = () => {
  return window.location.pathname.split('/')[2];
};

export default getProjectSlug;
