export const getFloorAndApto = (unit) => {
  let floor;
  let apto;

  if (unit && unit.length === 3) {
    floor = unit.substring(0, 1);
    apto = unit.substring(1, 3);
  }

  if (unit && unit.length === 4) {
    floor = unit.substring(0, 2);
    apto = unit.substring(2, 4);
  }

  return { floor, apto };
}

export const getUnitiessByTowerAndFloor = (unities) => {
  const unitiesOrdered = unities.sort((a, b) => a.id - b.id);

  return unitiesOrdered.reduce((acc, current) => {
    const c = current.slug.split('-');
    const t = c[0];
    const { floor } = getFloorAndApto(c[1]);

    if (acc[t]) {
      if (acc[t][floor]) {
        const isFound = acc[t][floor].find(u => u.slug === current.slug);

        if (!isFound) {
          return {
            ...acc,
            [t]: {
              ...acc[t],
              [floor]: [...acc[t][floor], current].sort((a, b) => a.id - b.id)
            }
          };
        }
      }

      return {
        ...acc,
        [t]: {
          ...acc[t],
          [floor]: [current]
        }
      };
    }

    return {
      ...acc,
      [t]: {
        [floor]: [current]
      }
    };
  }, {});
}

export const getUnitiesNumber = (unitiesByTowerAndFloor) => {
  const unitiesLength = Object.keys(unitiesByTowerAndFloor).reduce((accTower, currentTower) => {
    const aptoLength = Object.keys(unitiesByTowerAndFloor[currentTower]).reduce((accApto, currentApto) => {
      const apto = unitiesByTowerAndFloor[currentTower][currentApto];

      if (apto.length > accApto) return apto.length;

      return accApto;
    }, 0);

    if (aptoLength > accTower) return aptoLength;

    return accTower;
  }, 0);

  return [...Array(unitiesLength).keys()].map(n => n + 1);
}
