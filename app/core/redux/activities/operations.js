import { xhrProject as xhr } from 'xhr';

const getActivities = () => async (dispatch) => {
  const response = await xhr.get('/activities/today');

  return response.data;
};

const updateActivity = (payload, projectSlug) => async (dispatch) => {
  const response = await xhr.put(`/activities/update/${payload.id}/`, payload);

  return response.data;
};

export {
  getActivities,
  updateActivity
};
