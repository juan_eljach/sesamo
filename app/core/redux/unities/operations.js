import { xhrProject as xhr } from "xhr";

const getAvailableUnities = () => async () => {
  const response = await xhr.get("/available-unities");

  return response.data;
};

const getUnity = id => async () => {
  const response = await xhr.get(`/unities/${id}`);

  return response.data;
};

const getUnityByTowerAndName = (tower, unitySlug) => async () => {
  const response = await xhr.get(`/deeds/tower/${tower}/unity/${unitySlug}/`);

  return response.data;
};

const getUnities = () => async () => {
  const response = await xhr.get("/unities");

  return response.data;
};

const getVirtualBooking = unity => async () => {
  const response = await xhr.get(
    `/presales/unity/${unity}/virtual-booking-list/`
  );

  return response.data;
};

const saveVirtualBooked = payload => async () => {
  const response = await xhr.post("/presales/virtual-booking/create/", payload);

  return response.data;
};

const saveNormalQuotation = payload => async () => {
  const response = await xhr.post("/presales/quotation/create/", payload);

  return response.data;
};

const saveQuickQuotation = payload => async () => {
  const response = await xhr.post("/presales/quickquotation/create/", payload);

  return response.data;
};

const updateUnity = payload => async () => {
  const response = await xhr.put(`/unities/${payload.id}/`, payload);

  return response.data;
};

const searchUnities = (value, query) => async () => {
  const url = query
    ? `/search-unities/?search=${value}${query}`
    : `/search-unities/?search=${value}`;
  const response = await xhr.get(url);

  return response.data;
};

export {
  getUnity,
  getAvailableUnities,
  getUnities,
  getVirtualBooking,
  saveVirtualBooked,
  saveNormalQuotation,
  saveQuickQuotation,
  updateUnity,
  searchUnities,
  getUnityByTowerAndName
};
