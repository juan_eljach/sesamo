import * as types from './types';

const showNotificationPopup = (payload) => ({
  type: types.SHOW_NOTIFICATION_POPUP,
  payload
});

const hideNotificationPopup = (payload) => ({
  type: types.HIDE_NOTIFICATION_POPUP,
  payload
});

export {
  showNotificationPopup,
  hideNotificationPopup
};
