import * as types from './types';

const initial = {
  opened: false,
  options: {}
};

const notificationPopupReducer = ( state = initial, action ) => {
  switch ( action.type ) {
    case types.SHOW_NOTIFICATION_POPUP: {
      return {...state, opened: true, options: action.payload};
    }

    case types.HIDE_NOTIFICATION_POPUP: {
      return {...state, opened: false};
    }

    default:
      return state;
  }
};


export default notificationPopupReducer;
