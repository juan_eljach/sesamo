import { xhrProject as xhr } from 'xhr';

const getTickets = () => async (dispatch) => {
  const response = await xhr.get('/support/tickets/');

  return response.data;
};

const getTicket = (slug) => async (dispatch) => {
  const response = await xhr.get(`/support/tickets/${slug}`);

  return response.data;
};

const getMyTicketsAdministrator = () => async (dispatch) => {
  const response = await xhr.get('/support/tickets/filter/my_tickets_admin/');

  return response.data;
};

const getMyTicketsCollaborator = () => async (dispatch) => {
  const response = await xhr.get('/support/tickets/filter/my_tickets_collaborator/');

  return response.data;
};

const getUnassignedTickets = () => async (dispatch) => {
  const response = await xhr.get('/support/tickets/filter/unassigned/');

  return response.data;
};

const saveTicket = (payload) => async (dispatch) => {
  const response = await xhr.put('/support/tickets/', payload);

  return response.data;
};

const updateTicket = (ticketSlug, payload) => async (dispatch) => {
  const response = await xhr.put(`/support/tickets/${ticketSlug}/`, payload);

  return response.data;
};

const createAnswer = (slug, payload) => async (dispatch) => {
  const response = await xhr.post(`/support/tickets/${slug}/answers/create/`, payload);

  return response.data;
};

const createNote = (slug, payload) => async (dispatch) => {
  const response = await xhr.post(`/support/tickets/${slug}/ticket-notes/create/`, payload);

  return response.data;
};

export {
  saveTicket,
  getTickets,
  getTicket,
  getMyTicketsAdministrator,
  getMyTicketsCollaborator,
  getUnassignedTickets,
  updateTicket,
  createAnswer,
  createNote
};
