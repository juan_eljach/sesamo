import user from "./user";
import project from "./project";
import menu from "./menu";
import notificationPopup from "./notificationPopup";
import { reducer as formReducer } from "redux-form";
import { reducer as burgerMenu } from "redux-burger-menu";

const reducers = {
  user,
  project,
  menu,
  notificationPopup,
  form: formReducer,
  burgerMenu
};

const getReducers = reducersList => {
  const reducersMapped = reducersList.reduce((acc, current) => {
    return {
      ...acc,
      [current]: reducers[current]
    };
  }, {});

  return reducersMapped;
};

export default getReducers;
