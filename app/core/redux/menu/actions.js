import * as types from "./types";

const setMenu = payload => ({
  type: types.SET_MENU,
  payload
});

export { setMenu };
