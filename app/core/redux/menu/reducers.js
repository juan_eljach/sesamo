import * as types from "./types";

const initial = {
  activeMenu: ""
};

const projectReducer = (state = initial, action) => {
  switch (action.type) {
    case types.SET_MENU: {
      return { ...state, activeMenu: action.payload };
    }

    default:
      return state;
  }
};

export default projectReducer;
