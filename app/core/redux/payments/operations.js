import { xhrProject as xhr } from 'xhr';

const getPaymentAgreementsDetail = id => async () => {
	const response = await xhr.get(`/formalizations/payment-agreements/detail/${id}`);

	return response.data;
};

const savePayment = payload => async () => {
	const response = await xhr.post(`/payment/create/`, payload);

	return response.data;
};

export { getPaymentAgreementsDetail, savePayment };
