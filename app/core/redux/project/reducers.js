import * as types from "./types";

const projectReducer = (state = {}, action) => {
  switch (action.type) {
    case types.SET_PROJECT: {
      return { ...state, ...action.payload };
    }

    case types.SET_TEAM: {
      return { ...state, team: action.payload };
    }

    default:
      return state;
  }
};

export default projectReducer;
