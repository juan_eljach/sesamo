import * as types from "./types";

const setProject = payload => ({
  type: types.SET_PROJECT,
  payload
});

const setTeam = payload => ({
  type: types.SET_TEAM,
  payload
});

export { setProject, setTeam };
