const SET_PROJECT = "presale/user/SET_PROJECT";
const SET_TEAM = "presale/user/SET_TEAM";

export { SET_PROJECT, SET_TEAM };
