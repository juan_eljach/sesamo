import { xhrProject as xhr } from "xhr";
import * as actions from "./actions";

const getProject = () => async dispatch => {
  const response = await xhr.get("/info");

  dispatch(actions.setProject({ ...response.data }));
};

const getCampaigns = () => async dispatch => {
  const response = await xhr.get("/formalizations/campaigns/");

  return response;
};

const getProjectTeam = () => async dispatch => {
  const response = await xhr.get("/team");

  dispatch(actions.setTeam({ ...response.data }));
};

export { getProject, getCampaigns, getProjectTeam };
