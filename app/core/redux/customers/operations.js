import { xhrProject as xhr } from 'xhr';

const getSegmentation = () => async () => {
  const response = await xhr.get('/segmentation/');

  return response.data;
};

const getClientIdentity = (identity) => async () => {
  const response = await xhr.get(`/support/client/?identity=${identity}`);

  return response.data;
};

const searchPerson = (query) => async () => {
  const response = await xhr.get(`/persons?search=${query}`);

  return response.data;
};

const saveCustomer = (payload) => async () => {
  const response = await xhr.post('/persons/create/', payload);

  return response.data;
};

const updateCustomer = (slug, payload) => async () => {
  const response = await xhr.put(`/segmentation/${slug}/`, payload);

  return response.data;
};

const saveNote = (payload) => async () => {
  const response = await xhr.post('/notes/create/', payload);

  return response.data;
};

const saveActivity = (payload) => async () => {
  const response = await xhr.post('/activities/create/', payload);

  return response.data;
};

const updateActivity = (payload) => async () => {
  const response = await xhr.put(`/activities/update/${payload.id}/`, payload);

  return response.data;
};

const getOrderLog = () => async () => {
  const response = await xhr.get('/orderlog/');

  return response;
};

const saveOrderLog = (payload) => async () => {
  const response = await xhr.post('/orderlog/', payload);

  return response;
};

const updateOrderLog = (payload) => async () => {
  const response = await xhr.put(`/orderlog/${payload.pk}/`, payload);

  return response;
};

const updateFormalizationPerson = (payload) => async () => {
  const response = await xhr.put(`/formalizations/person/${payload.slug}/small-update/`, payload);

  return response;
};

const saveFormalizationPerson = (payload) => async () => {
  const response = await xhr.post('/formalizations/person/create/', payload);

  return response;
};

export {
  getSegmentation,
  getClientIdentity,
  searchPerson,
  saveCustomer,
  updateCustomer,
  saveNote,
  saveActivity,
  updateActivity,
  getOrderLog,
  saveOrderLog,
  updateOrderLog,
  updateFormalizationPerson,
  saveFormalizationPerson
};
