import xhr, { xhrProject } from 'xhr';

import getProjectSlug from 'core/helpers/getProjectSlug';
import * as actions from './actions';

const settings = {
	projectSlug: getProjectSlug()
};

const getUser = () => async dispatch => {
	const userProfileResponse = await xhr.get('/userprofile');
	const { user, ...rest } = userProfileResponse.data;

	let userInfo = {
		...user,
		...rest,
		...settings
	};

	dispatch(actions.setUser(userInfo));

	return userInfo;
};

const getTeam = () => async dispatch => {
	const response = await xhrProject.get('/team/');

	return response.data;
};

export { getUser, getTeam };
