import { xhrProject as xhr } from 'xhr';

const savePaymentPlan = payload => async () => {
	const response = await xhr.post(`/formalizations/payment-agreement/create/`, payload);

	return response.data;
};

const getPaymentPlans = (query, page = 1) => async () => {
	let url = `/formalizations/payment-agreements/?page=${page}`;

	if (query) {
		url = `${url}&search=${query}`;
	}

	const response = await xhr.get(url);

	return response.data;
};

const getPaymentPlansById = unityId => async () => {
	const response = await xhr.get(`/formalizations/payment-agreements/?unity=${unityId}`);

	return response.data;
};
export { getPaymentPlans, savePaymentPlan, getPaymentPlansById };
