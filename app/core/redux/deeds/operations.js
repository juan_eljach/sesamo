import xhr from "xhr";
import { xhrProject } from 'xhr';

const getDeeds = (tower) => async () => {
  const response = await xhrProject.get(`/deeds/tower/${tower}`);

  return response.data;
};

const checkNotificationsTower = (tower) => async () => {
  const response = await xhrProject.get(`/deeds/notifications/tracking/tower/${tower}`);

  return response;
};

const sendNotifications = (payload) => async () => {
  const response = await xhrProject.post('/deeds/send-notifications/', payload);

  return response.data;
};

const checkNotificationsStatus = (payload) => async () => {
  const response = await xhr.post('/tasks/status/', payload);

  return response;
};

const saveCredit = (tower, unity, payload) => async () => {
  const response = await xhrProject.post(`/deeds/tower/${tower}/unity/${unity}/credit/`, payload);

  return response.data;
};

const getCredit = (tower, unitySlug) => async () => {
  const response = await xhrProject.get(`/deeds/tower/${tower}/unity/${unitySlug}/credit/`);

  return response.data;
};

const updateCredit = (tower, unitySlug, payload) => async () => {
  const response = await xhrProject.put(`/deeds/tower/${tower}/unity/${unitySlug}/credit/`, payload);

  return response.data;
};

const getBankStep = (tower, unitySlug) => async () => {
  let url = `/deeds/tower/${tower}/unity/${unitySlug}/bank-confirmation/`;

  const response = await xhrProject.get(url);

  return response.data;
};

const saveBankStep = (operation, tower, unitySlug, payload) => async () => {
  let url = `/deeds/tower/${tower}/unity/${unitySlug}/bank-confirmation/`;

  const response = await xhrProject[operation](url, payload);

  return response.data;
};

const getDeedsStep = (tower, unitySlug) => async () => {
  let url = `/deeds/tower/${tower}/unity/${unitySlug}/deed-order/`;

  const response = await xhrProject.get(url);

  return response.data;
};

const saveDeedsStep = (operation, tower, unitySlug, payload) => async () => {
  let url = `/deeds/tower/${tower}/unity/${unitySlug}/deed-order/`;

  const response = await xhrProject[operation](url, payload);

  return response.data;
};

const getTrackingStep = (tower, unitySlug) => async () => {
  let url = `/deeds/tower/${tower}/unity/${unitySlug}/dates-tracking/`;

  const response = await xhrProject.get(url);

  return response.data;
};

const saveTrackingStep = (operation, tower, unitySlug, payload) => async () => {
  let url = `/deeds/tower/${tower}/unity/${unitySlug}/dates-tracking/`;

  const response = await xhrProject[operation](url, payload);

  return response.data;
};

export {
  sendNotifications,
  checkNotificationsStatus,
  checkNotificationsTower,
  getDeeds,
  saveCredit,
  updateCredit,
  getCredit,
  getBankStep,
  saveBankStep,
  getDeedsStep,
  saveDeedsStep,
  getTrackingStep,
  saveTrackingStep
};
