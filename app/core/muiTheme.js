import { createMuiTheme } from '@material-ui/core/styles';
import purple from '@material-ui/core/colors/purple';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#7e7e7e'
    },
    secondary: {
      main: '#00a8ff'
    }
  },
  status: {
    danger: '#F68284',
  },
});

export default theme;
