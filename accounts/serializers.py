from django.contrib.auth.models import User
from rest_framework import serializers
from .models import UserProfile

class UserDetailsSerializer(serializers.ModelSerializer):
	class Meta:
		model = User
		fields = ('username', 'email', 'first_name', 'last_name')

class UserProfileSerializer(serializers.ModelSerializer):
	user = UserDetailsSerializer()
	class Meta:
		model = UserProfile
		fields = (
			'id',
			'user',
			'user_type',
		)