from django.conf.urls import patterns, url
from django.core.urlresolvers import reverse_lazy
from userena.views import signin, signup
from .forms import SignupFormOnlyEmailExtra

urlpatterns = patterns('',
    url(r'^login/$', signin, name="login"),
    url(r'^logout/$', 'django.contrib.auth.views.logout_then_login', name="logout"),
    url(r'^signup/$', signup, {"signup_form": SignupFormOnlyEmailExtra, "success_url": reverse_lazy("projects")}, name="signup"),
)
