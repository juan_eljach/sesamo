from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.db import models
from userena.models import UserenaBaseProfile
from projects.models import Project

user_type_choices = (
		("comprador", "Comprador"),
		("cartera", "Cartera"),
		("gerente", "Gerente"),
		("ingeniero", "Ingeniero"),
		("soporte", "Soporte al Cliente"),
		("vendedor", "Vendedor"),
		("empleado","Empleado"),
	)

class UserProfile(UserenaBaseProfile):
	REQUIRED_FIELDS = ('user', 'user_type')
	user = models.OneToOneField(User, unique=True, related_name='user_profile')
	user_type = models.CharField(max_length=20, choices=user_type_choices, blank=False)

	def get_full_name(self):
		return self.user.get_full_name() or 'None'

	def __str__(self):
		return self.get_full_name()
