from django import forms
from django.utils.translation import ugettext_lazy as _
from collections import OrderedDict
from userena.forms import SignupFormOnlyEmail

user_type_choices = (
	("cartera", "Cartera"),
	("gerente", "Gerente"),
	("ingeniero", "Ingeniero"),
	("soporte", "Soporte al Cliente"),
	("vendedor", "Vendedor"),
)

class SignupFormOnlyEmailExtra(SignupFormOnlyEmail):
	first_name = forms.CharField(label=_(u'First name'), max_length=30, required=True)
	last_name = forms.CharField(label=_(u'Last name'), max_length=30, required=True)
	user_type = forms.ChoiceField(label=_(u'Role'), choices=user_type_choices, required=True)

	def __init__(self, *args, **kwargs):
		super(SignupFormOnlyEmailExtra, self).__init__(*args, **kwargs)
		original_fields = self.fields
		new_order = OrderedDict()
		for key in ['first_name', 'last_name', 'user_type', 'email', 'password1', 'password2']:
			new_order[key] = original_fields[key]
		self.fields = new_order
		self.fields["user_type"].widget.attrs.update({"class": "browser-default"})

	def save(self, *args, **kwargs):
		"""
		Override the save method to save the first and last name to the user
		field.

		"""
		# First save the parent form and get the user.
		new_user = super(SignupFormOnlyEmailExtra, self).save()
		new_user.first_name = self.cleaned_data['first_name']
		new_user.last_name = self.cleaned_data['last_name']
		new_user.save()

		# Get the profile, the `save` method above creates a profile for each
		# user because it calls the manager method `create_user`.
		# See: https://github.com/bread-and-pepper/django-userena/blob/master/userena/managers.py#L65
		user_profile = new_user.user_profile
		user_profile.user_type = self.cleaned_data['user_type']
		user_profile.save()

		# Userena expects to get the new user from this form, so return the new
		# user.
		return new_user
