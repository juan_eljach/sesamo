import calendar
import io
import locale
from io import BytesIO
import os
import zipfile
import django_filters
from django.conf import settings
from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404
from django.template.loader import get_template
from django.utils.translation import get_language, activate
from docx import Document
from celery.result import AsyncResult
from rest_framework import status
from rest_framework import generics
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.views import APIView
from formalizations.serializers import PaymentFeeSerializerExtended, AgreedPaymentSerializer
from sesamo.mixins import SlugManagerMixin
from .models import TowerNotificationsTracking, UnityNotificationLog, CreditCertificate, BankConfirmation, DeedOrder, DatesTracking
from .serializers import TemplateSerializer, SendNotificationSerializer, TowerNotificationsTrackingSerializer, UnitiyNotificationLogSerializer, UnityDeedsSerializer, UnityDeedsCheckPaymentsSerializer, CreditCertificateSerializer, BankConfirmationSerializer, DeedOrderSerializer, DatesTrackingSerializer
from .tasks import SendNotificationsTask
from units.models import Unity

class ReturnTemplate(APIView):
    #at the moment this templates are both the same. But this might change, so we specifie both of them as two separate templates
	notification_template = "deeds/notification_template.html"
	document_notification_template = "deeds/document_notification_template.html"

	def get(self, request, *args, **kwargs):
		template_param = request.GET.get("name")
		if template_param == "notification":
			html_template = get_template(self.notification_template)
		elif template_param == "document":
			html_template = get_template(self.document_notification_template)
		else:
			return Response(status=status.HTTP_404_NOT_FOUND)

		html_template = html_template.render().encode(encoding="UTF-8")
		data = {'code': html_template}
		serialized_data = TemplateSerializer(data)
		return Response(serialized_data.data)

class SendNotificationView(SlugManagerMixin, APIView):
	def post(self, request, *args, **kwargs):
		project = self.get_project()
		notification_serializer = SendNotificationSerializer(data=request.data)
		if notification_serializer.is_valid():
			unities_ids = notification_serializer.data["unities"]
			html_snippet = notification_serializer.data["html_snippet"]
			tower_number = notification_serializer.data["tower_number"]
			request_user_id = request.user.id
			project_id = project.id
			task = SendNotificationsTask.delay(request_user_id, project_id, unities_ids, html_snippet, tower_number)
			data = {"task_id":task.id}
			return Response(data=data, status=status.HTTP_200_OK)
		else:
			return Response(status= status.HTTP_400_BAD_REQUEST)


class UnityDeedsNotifications(SlugManagerMixin, generics.ListAPIView):
	model_class = Unity
	serializer_class = UnityDeedsSerializer

	def get_queryset(self):
		project = self.get_project()
		tower_number = int(self.kwargs.get("tower_number"))
		return self.model_class.objects.filter(project=project, paymentagreement__isnull=False, unitynotificationlog__tower_number=tower_number)

class UnityDetailDeedNotification(SlugManagerMixin, generics.RetrieveAPIView):
	model_class = Unity
	serializer_class = UnityDeedsSerializer

	def get_object(self):
		unity = self.get_unity()
		return unity

class TowerNotificationsTrackingView(SlugManagerMixin, generics.RetrieveAPIView):
	model = TowerNotificationsTracking
	serializer_class = TowerNotificationsTrackingSerializer
	lookup_field = "tower_number"
	lookup_url_kwarg = "tower_number"

	def get_queryset(self):
		project = self.get_project()
		return self.model.objects.filter(project=project)


class DownloadNotificationDocuments(SlugManagerMixin, APIView):
	def generate_zip_file(self, logs):
		zip_filename = "documentos-generados.zip"
		s = io.BytesIO()
		zf = zipfile.ZipFile(s, "w")

		for log in logs:
			fdir, fname = os.path.split(log.document.path)
			subdir = log.unity.name
			zip_path = os.path.join(subdir, fname)
			zf.write(log.document.path, zip_path)
		zf.close()

		resp = HttpResponse(s.getvalue(), content_type="application/x-zip-compressed")
		resp['Content-Disposition'] = 'attachment; filename=%s' % zip_filename

		return resp

	def get(self, request, *args, **kwargs):
		project = self.get_project()
		tower_number = int(self.kwargs.get("tower_number"))
		queryset = UnityNotificationLog.objects.filter(unity__project=project, tower_number=tower_number).exclude(document='')
		if queryset:
			zip_file = self.generate_zip_file(logs=queryset)
		else:
			zip_file = Response(status=status.HTTP_404_NOT_FOUND)
		return zip_file

class CreditCertificateViewSet(SlugManagerMixin, viewsets.ModelViewSet):
	model_class = CreditCertificate
	create_serializer = CreditCertificateSerializer
	serializer_class = CreditCertificateSerializer
	lookup_field = 'unity__slug'
	lookup_url_kwarg = 'unity_slug'

	def get_queryset(self):
		project = self.get_project()
		return self.model_class.objects.filter(unity__project=project)

	def perform_create(self, serializer):
		try:
			project = self.get_project()
			serializer.document = self.request.FILES.get('document')
			serializer.save()
		except Exception as err:
			print (err)

	def list(self, request, *args, **kwargs):
		queryset = self.get_queryset()
		serializer = self.serializer_class(queryset, many=True)
		return Response(serializer.data)

	def retrieve(self, request, *args, **kwargs):
		unity = self.get_unity()
		credit = get_object_or_404(CreditCertificate, unity=unity)
		serializer = self.serializer_class(credit)
		return Response(serializer.data)

	def perform_update(self, serializer):
		serializer.save()

	def get_serializer_class(self):
		if self.action == 'create' or self.action == 'update':
			return self.create_serializer
		return self.serializer_class

class UnityDeedsCheckPayments(SlugManagerMixin, generics.RetrieveAPIView):
	model_class = Unity
	serializer_class = UnityDeedsCheckPaymentsSerializer
	lookup_field = "slug__iexact"
	lookup_url_kwarg = "unity_slug"

	def get_queryset(self):
		project = self.get_project()
		return self.model_class.objects.filter(project=project)

	def retrieve(self, *args, **kwargs):
		unity = self.get_object()
		initial_fee = unity.paymentagreement.initial_fee
		payment_fees_in_debt = unity.paymentagreement.payment_fees.filter(paid=False)
		done_payments = unity.paymentagreement.payment_fees.filter(paid=True)
		fees_in_debt = [payment_fee.fee for payment_fee in payment_fees_in_debt]
		fees_in_debt_sum = sum(fees_in_debt)
		agreed_payments = unity.paymentagreement.agreed_payments.all()
		done_payments_fees = [payment_fee.fee for payment_fee in done_payments]
		done_payments_fees_sum = sum(done_payments_fees)
		current_debt = fees_in_debt_sum
		if (hasattr(unity, "credit")):
			credit = unity.credit.amount
		else:
			credit = 0
		total_debt = current_debt
		paymentfees_serialized = PaymentFeeSerializerExtended(payment_fees_in_debt, many=True)
		done_paymentfees_serialized = PaymentFeeSerializerExtended(done_payments, many=True)
		agreed_payments_serialized = AgreedPaymentSerializer(agreed_payments, many=True)
		data = {
			"paymentfees_indebt": paymentfees_serialized.data,
			"done_payments": done_paymentfees_serialized.data,
			"agreed_payments": agreed_payments_serialized.data,
			"done_payments_fees_sum": done_payments_fees_sum,
			"initial_fee": initial_fee,
			"unity_price": unity.price,
			"fees_in_debt_sum": fees_in_debt_sum,
			"credit": credit,
			"current_debt": current_debt,
			"total_debt":total_debt,
		}
		serializer = self.serializer_class(data=data)
		if serializer.is_valid():
			return Response(serializer.data)
		else:
			return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class GenerateOtroSi(SlugManagerMixin, APIView):
#	def get_month_name(month_no, locale):
#		with TimeEncoding(locale) as encoding:
#			s = month_name[month_no]
#			if encoding is not None:
#				s = s.decode(encoding)
#			return s

	def generate_data_table_file(self, unity, paymentagreement):
		document = Document()
		table = document.add_table(rows=1, cols=2)
		hdr_cells = table.rows[0].cells
		hdr_cells[0].text = 'Dato'
		hdr_cells[1].text = 'Valor'
		row_cells = table.add_row().cells
		row_cells[0].text = "[#APTO_NUMBER]"
		row_cells[1].text = unity.name
		persons = paymentagreement.persons.all()
		for idx, person in enumerate(persons):
			person_data_dict = {}
			person_data_dict["[#COMPRADOR_{}]".format(idx)] = person.get_person_full_name()
			person_data_dict["[#COMPRADOR_{}-CEDULA]".format(idx)] = person.identity
			person_data_dict["[#COMPRADOR_{}-CEDULA-EXPEDICION]".format(idx)] = person.expedition_place
			person_data_dict["[#COMPRADOR_{}-DIRECCION-NOTIFICACION]".format(idx)] = person.notification_address
			person_data_dict["[#COMPRADOR_{}-TELEFONO-NOTIFICACION]".format(idx)] = person.notification_phone
			for x,y in person_data_dict.items():
				row_cells = table.add_row().cells
				row_cells[0].text = x
				row_cells[1].text = y

		document.add_page_break()
		document_name = "{}-datatable-file.docx".format(unity.name)
		target_stream = BytesIO()
		try:
			document.save(target_stream)
		except Exception as err:
			print (err)
		length = target_stream.tell()
		target_stream.seek(0)
		response = HttpResponse(target_stream.getvalue(), content_type='application/vnd.openxmlformats-officedocument.wordprocessingml.document')
		response['Content-Disposition'] = 'attachment; filename=' + document_name
		response['Content-Length'] = length
		return response

	def generate_paymentfees_table_file(self, unity, fees):
		activate("es")
		document = Document()
		table = document.add_table(rows=1, cols=5)
		hdr_cells = table.rows[0].cells
		hdr_cells[0].text = 'Cuota'
		hdr_cells[1].text = 'Valor'
		hdr_cells[2].text = 'Mes'
		hdr_cells[3].text = 'Dia'
		hdr_cells[4].text = 'Año'
		for idx, fee in enumerate(fees, start=1):
			row_cells = table.add_row().cells
			row_cells[0].text = str(idx)
			row_cells[1].text = str(fee.fee)
			row_cells[2].text = str(fee.date_to_pay.strftime("%B"))
			row_cells[3].text = str(fee.date_to_pay.day)
			row_cells[4].text = str(fee.date_to_pay.year)

		document.add_page_break()
		document_name = "{}-feestable.docx".format(unity.name)
		file_path = os.path.join(settings.MEDIA_ROOT, "deeds", "documents", document_name)
		target_stream = BytesIO()
		try:
			document.save(target_stream)
		except Exception as err:
			print (err)
		length = target_stream.tell()
		target_stream.seek(0)
		response = HttpResponse(target_stream.getvalue(), content_type='application/vnd.openxmlformats-officedocument.wordprocessingml.document')
		response['Content-Disposition'] = 'attachment; filename=' + document_name
		response['Content-Length'] = length
		return response

		#zip_filename = "documentos-generados.zip"
		#s = io.BytesIO()
		#zf = zipfile.ZipFile(s, "w")

		#for log in logs:
		#	fdir, fname = os.path.split(log.document.path)
		#	subdir = log.unity.name
		#	zip_path = os.path.join(subdir, fname)
		#	zf.write(log.document.path, zip_path)
		#zf.close()

		#resp = HttpResponse(s.getvalue(), content_type="application/x-zip-compressed")
		#resp['Content-Disposition'] = 'attachment; filename=%s' % zip_filename

		#return resp

	def get(self, request, *args, **kwargs):
		document_param = request.GET.get("document")
		project = self.get_project()
		unity = self.get_unity()
		tower_number = int(self.kwargs.get("tower_number"))
		payment_fees = unity.paymentagreement.payment_fees.all().order_by("date_to_pay")
		print ("ACTUALLY GOT IN")
		if document_param == "payment_fees":
			if payment_fees:
				file = self.generate_paymentfees_table_file(unity=unity, fees=payment_fees)
			else:
				file = Response(status=status.HTTP_404_NOT_FOUND)
			return file
		elif document_param == "data_table":
			if unity and unity.paymentagreement:
				file = self.generate_data_table_file(unity=unity, paymentagreement=unity.paymentagreement)
			else:
				file = Response(status=status.HTTP_404_NOT_FOUND)
			return file
		else:
			return Response(status=status.HTTP_404_NOT_FOUND)


class BankConfirmationViewSet(SlugManagerMixin, viewsets.ModelViewSet):
	model_class = BankConfirmation
	queryset = BankConfirmation.objects.all()
	serializer_class = BankConfirmationSerializer
	lookup_field = 'unity__slug'
	lookup_url_kwarg = 'unity_slug'


class DeedOrderViewSet(SlugManagerMixin, viewsets.ModelViewSet):
	model_class = DeedOrder
	queryset = DeedOrder.objects.all()
	serializer_class = DeedOrderSerializer
	lookup_field = 'unity__slug'
	lookup_url_kwarg = 'unity_slug'

class DatesTracking(SlugManagerMixin, viewsets.ModelViewSet):
	model_class = DatesTracking
	queryset = DatesTracking.objects.all()
	serializer_class = DatesTrackingSerializer
	lookup_field = 'unity__slug'
	lookup_url_kwarg = 'unity_slug'
