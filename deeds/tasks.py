import datetime
import os
import io
import uuid
import time
import zipfile
from os.path import abspath, basename, dirname, join, normpath
from django import template
from django.conf import settings
from django.contrib.auth.models import User
from django.core.files.base import ContentFile
from django.core.files.uploadedfile import SimpleUploadedFile
from django.core.files.storage import default_storage
from celery.decorators import task
from celery.task.schedules import crontab
from weasyprint import HTML, CSS
from sesamo.tasks import SendEmail
from projects.models import Project
from units.models import Unity
from .models import UnityNotificationLog, TowerNotificationsTracking, OtroSi

@task
def SendNotificationsTask(request_user_id, project_id, unities, html_code, tower_number):
	tower_number = int(tower_number)
	user = User.objects.get(id=request_user_id)
	user_profile = user.user_profile
	project = Project.objects.get(id=project_id)
	unities_objects = [Unity.objects.get(id=unity) for unity in unities]
	for unity in unities_objects:
		print ("UNITY: {}".format(unity))
		if(hasattr(unity, 'paymentagreement')):
			print ("UNITY paymentagreement: ".format(unity.paymentagreement))
			clients_of_unity = unity.paymentagreement.persons.all()
			email_of_clients = [client.email for client in clients_of_unity]
			tower = unity.name.split("-")[0][1:]
			data = {"names": clients_of_unity, "unity": unity.name, "project":unity.project, "tower":tower}
			context = template.Context(data)
			html_template = template.Template(html_code)
			rendered_template = html_template.render(context).encode(encoding="ISO-8859-1")
			try:
				unity_notification_log, created = UnityNotificationLog.objects.get_or_create(unity=unity, tower_number=tower)
				print ("Unity not log:".format(unity_notification_log))
				print ("created:".format(created))
			except Exception as err:
				print (err)
			"""
			email_sent = SendEmail.delay(
				email_of_clients,
				None,
				settings.DEFAULT_EMAIL_SENDER,
				settings.DEFAULT_NAME_SENDER,
				"Notificacion de inicio de proceso de escrituracion",
				settings.DEEDS_NOTIFICATION_TEMPLATE_ID,
				rendered_template
			)
			if email_sent:
				unity_notification_log.email_notification_sent = True
				unity_notification_log.email_notification_sent_timestamp =  datetime.datetime.today().date()
				unity_notification_log.save()
				unities_notified.append(unity)
			"""
			print ("GONNA ENTER TRY TO GENERATE PDF")
			try:
				#styles = normpath(join(settings.SITE_ROOT, 'static/css/deeds-notifications.css'))
				#pdf_file = HTML(string=rendered_template).write_pdf(stylesheets=[CSS(filename=styles)])
				pdf_file = HTML(string=rendered_template).write_pdf()
			except Exception as err:
				print (err)
				pass
			document_name = "Notificacion - {}".format(unity.name)
			if pdf_file:
				try:
					unity_notification_log.document = SimpleUploadedFile("Notificacion-{}{}".format(unity.name, ".pdf"), pdf_file, content_type='application/pdf')
					unity_notification_log.save()
				except Exception as err:
					print (err)
		else:
			print ("DOES NOT HAVE PA")


	print ("MAKIA")
	try:
		tower_notifications_tracking, created = TowerNotificationsTracking.objects.get_or_create(project=project, tower_number=tower_number)
		print ("TOWER GOOD MI PERRO")
		tower_notifications_tracking.massive_notifications_sent = True
		tower_notifications_tracking.sent_by = user_profile
		tower_notifications_tracking.save()
	except Exception as err:
		print ("OCURRRIO UN ERRORRRR D:")
		print (err)

	print ("PASSED THE TRY")

	"""

	MODIFICAR LA LOGICA PARA QUE GENERE LOS DOCUMENTOS A LOS APTOS QUE UNO SELECCIONO. HAY QUE ARREGLAR LA QUERYSET PORQUE ESTA GENERICA
	"""
	zip_file_name = str(uuid.uuid1())
	s = io.BytesIO()
	zf = zipfile.ZipFile(s, "w")

	for log in UnityNotificationLog.objects.filter(unity__project=project, tower_number=tower_number).exclude(document=''):
		fdir, fname = os.path.split(log.document.path)
		subdir = log.unity.name
		zip_path = os.path.join(subdir, fname)
		zf.write(log.document.path, zip_path)
	zf.close()
	file_obj = s.getvalue()
	file = default_storage.save("generated-zips/{0}.zip".format(zip_file_name), ContentFile(file_obj))
	path_to_file = os.path.join(settings.MEDIA_ROOT, file)
	url_path_to_file = "{0}{1}".format(settings.MEDIA_URL, file)

	data = {"filename":zip_file_name, "url_path":url_path_to_file}
	print ("DATA: {}".format(data))
	return data

@task
def GenerateOtroSiTask(additional_payment_agreement):
	additional_pa = additional_payment_agreement
	parent_pa = additional_pa.parent_payment_agreement
	otrosi, created = OtroSi.objects.get_or_create(unity=additional_pa.unity)
	persons = [person for person in parent_pa.persons.all()]
	if created:
		otrosi.required = True
	data = {
		"unity" : otrosi.unity,
		"persons" : persons,
		"additional_payment_fees" : additional_pa.additional_payment_fees.all(),
	}

	html_string = render_to_string("formalizations/purchase-agreement-doc.html", data)
	pdf_file = HTML(string=html_string).write_pdf()
