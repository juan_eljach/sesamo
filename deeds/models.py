import uuid
from django.conf import settings
from django.db import models
from projects.models import Project
from units.models import Unity

BANK_CHOICES = (
	("caja_social", "Caja Social"),
	("davivienda", "Davivienda"),
	("colpatria", "Colpatria"),
	("av_villas", "AV Villas"),
	("bancolombia", "Bancolombia"),
	("banco_bogota", "Banco de Bogotá"),
	("fna", "Fondo Nacional del ahorro"),
	("credifamilia", "Credifamilia"),
)

NOTARY_CHOICES = (
	("notaria_piedecuesta", "Notaria de Piedecuesta"),
	("notaria_tercera", "Notaria Tercera"),
)

class TowerNotificationsTracking(models.Model):
	tower_number = models.IntegerField()
	project = models.ForeignKey(Project)
	massive_notifications_sent = models.BooleanField(default=False)
	massive_notifications_sent_by = models.ForeignKey(settings.AUTH_PROFILE_MODULE, blank=True, null=True)

	def __str__(self):
		return "Notifications Tracking for Tower {} in Project {}".format(self.tower_number, self.project)

class UnityNotificationLog(models.Model):
	unity = models.OneToOneField(Unity)
	email_notification_sent = models.BooleanField(default=False)
	email_notification_sent_timestamp = models.DateField(blank=True, null=True)
	document = models.FileField(upload_to="uploads/deeds/", blank=True, null=True)
	tower_number = models.IntegerField()

	def __str__(self):
		return "Notification Tracking for Unity: {}".format(self.unity.name)

class ZipFile(models.Model):
	filename = models.UUIDField(primary_key=True, default=uuid.uuid1, editable=False)
	file = models.FileField(upload_to="generated_zips/", blank=True, null=True)

class CreditCertificate(models.Model):
	required = models.BooleanField(default=False)
	marked_as_done = models.BooleanField(default=False)
	unity = models.OneToOneField(Unity, related_name="credit")
	amount = models.IntegerField()
	bank = models.CharField(max_length=50, choices=BANK_CHOICES)
	expedition_timestamp = models.DateField()
	document = models.FileField(upload_to="uploads/deeds/credits/", blank=True, null=True)

	def __str__(self):
		return "Certificado de credito de la unidad: {}".format(self.unity)

class OtroSi(models.Model):
	required = models.BooleanField(default=False)
	unity = models.OneToOneField(Unity, related_name="otrosi")
	document = models.FileField(upload_to="uploads/deeds/otrosis/", blank=True, null=True)

class BankConfirmation(models.Model):
	required = models.BooleanField(default=False)
	marked_as_done = models.BooleanField(default=False)
	unity = models.OneToOneField(Unity, related_name="bank_confirmation")
	documents_sent_to_bank = models.BooleanField(default=False)
	documents_sent_to_bank_date = models.DateField(blank=True, null=True)
	client_is_ok_with_bank = models.BooleanField(default=False)
	client_is_ok_with_bank_date = models.DateField(blank=True, null=True)

	def __str__(self):
		return "Bank Confirmation for Unity: {}".format(self.unity)

class DeedOrder(models.Model):
	required = models.BooleanField(default=False)
	marked_as_done = models.BooleanField(default=False)
	unity = models.OneToOneField(Unity, related_name="deed_order")
	deed_sent_to_notary = models.BooleanField(default=False)
	deed_sent_to_notary_date = models.DateField(blank=True, null=True)
	notary = models.CharField(max_length=50, choices=NOTARY_CHOICES)
	notary_date_and_time_checking = models.BooleanField(default=False)
	notary_date_and_time_checking_date = models.DateField(blank=True, null=True)
	deed_signed = models.BooleanField(default=False)
	deed_signed_date = models.DateField(blank=True, null=True)

class DatesTracking(models.Model):
	required = models.BooleanField(default=False)
	marked_as_done = models.BooleanField(default=False)
	unity = models.OneToOneField(Unity, related_name="dates_tracking")
	deed_signed_by_legal_representative = models.DateField(blank=True, null=True)
	deed_signed_by_bank = models.DateField(blank=True, null=True)
	deed_arrives_to_office = models.DateField(blank=True, null=True)
	bank_cash_outlay = models.DateField(blank=True, null=True)
