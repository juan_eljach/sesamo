from django.contrib import admin
from .models import UnityNotificationLog, TowerNotificationsTracking, CreditCertificate, DeedOrder

admin.site.register(TowerNotificationsTracking)
admin.site.register(UnityNotificationLog)
admin.site.register(CreditCertificate)
admin.site.register(DeedOrder)
