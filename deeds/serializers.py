from rest_framework import serializers
from formalizations.serializers import PaymentFeeSerializerExtended, AgreedPaymentSerializer
from units.serializers import UnitySmallSerializer
from units.models import Unity
from .models import TowerNotificationsTracking, UnityNotificationLog, CreditCertificate, BankConfirmation, DeedOrder, DatesTracking

class TemplateSerializer(serializers.Serializer):
	code = serializers.CharField(style={'base_template': 'textarea.html'})

class SendNotificationSerializer(serializers.Serializer):
	unities = serializers.ListField(child=serializers.IntegerField())
	tower_number = serializers.IntegerField()
	html_snippet = serializers.CharField()

class TowerNotificationsTrackingSerializer(serializers.ModelSerializer):
	class Meta:
		model = TowerNotificationsTracking
		fields = (
			'massive_notifications_sent',
			'massive_notifications_sent_by',
		)

class UnitiyNotificationLogSerializer(serializers.ModelSerializer):
	class Meta:
		model = UnityNotificationLog
		fields = (
			'email_notification_sent',
			'email_notification_sent_timestamp',
			'document'
		)

class UnityDeedsSerializer(serializers.ModelSerializer):
	unitynotificationlog = UnitiyNotificationLogSerializer()
	clients = serializers.SerializerMethodField()
	class Meta:
		model = Unity
		fields = (
			'id',
			'name',
			'price',
			'unitynotificationlog',
			'clients',
			'slug',
		)

	def get_clients(self, obj):
		try:
			clients = [person.get_person_full_name() for person in obj.paymentagreement.persons.all()]
		except:
			clients = None
			pass
		return clients


class CreditCertificateSerializer(serializers.ModelSerializer):
	pk = serializers.IntegerField(read_only=True)
	class Meta:
		model = CreditCertificate
		fields = (
			'pk',
			'required',
			'unity',
			'amount',
			'bank',
			'expedition_timestamp',
			'document',
		)

	def update(self, instance, validated_data):
		instance.required = validated_data.get("required", instance.required)
		instance.amount = validated_data.get("amount", instance.amount)
		instance.unity = validated_data.get("unity", instance.unity)
		instance.expedition_timestamp = validated_data.get("expedition_timestamp", instance.expedition_timestamp)
		instance.bank = validated_data.get("bank", instance.bank)
		new_document = validated_data.get("document")
		if new_document:
			instance.document = new_document
		instance.save()
		return instance

class UnityDeedsCheckPaymentsSerializer(serializers.Serializer):
	paymentfees_indebt = PaymentFeeSerializerExtended(many=True)
	done_payments = PaymentFeeSerializerExtended(many=True)
	agreed_payments = AgreedPaymentSerializer(many=True)
	done_payments_fees_sum = serializers.IntegerField()
	fees_in_debt_sum = serializers.IntegerField()
	credit = serializers.IntegerField()
	current_debt = serializers.IntegerField()
	total_debt = serializers.IntegerField()
	initial_fee = serializers.IntegerField()
	unity_price = serializers.IntegerField()


class BankConfirmationSerializer(serializers.ModelSerializer):
	pk = serializers.IntegerField(read_only=True)
	class Meta:
		model = BankConfirmation
		fields = (
			"pk",
			"required",
			"unity",
			"documents_sent_to_bank",
			"documents_sent_to_bank_date",
			"client_is_ok_with_bank",
			"client_is_ok_with_bank_date"
		)

class DeedOrderSerializer(serializers.ModelSerializer):
	pk = serializers.IntegerField(read_only=True)
	class Meta:
		model = DeedOrder
		fields = (
			"pk",
			"required",
			"unity",
			"deed_sent_to_notary",
			"deed_sent_to_notary_date",
			"notary",
			"notary_date_and_time_checking",
			"notary_date_and_time_checking_date",
			"deed_signed",
			"deed_signed_date"
		)

class DatesTrackingSerializer(serializers.ModelSerializer):
	pk = serializers.IntegerField(read_only=True)
	class Meta:
		model = DatesTracking
		fields = (
			"pk",
			"required",
			"unity",
			"deed_signed_by_legal_representative",
			"deed_signed_by_bank",
			"deed_arrives_to_office",
			"bank_cash_outlay",
		)
