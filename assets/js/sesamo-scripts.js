function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

$(document).ready(function(){
  $('a.add-project-btn').click(function() {
    AddUnityType(this);
  });
});

function AddUnityType(event){
   var csrftoken = getCookie('csrftoken');
   var type_name = $('#id_type_name').val();
   var private_area = $('#id_private_area').val();
   var built_area = $('#id_built_area').val();
   var url = "/item/add/";
   $.ajax({
       type: "POST",
       url: url,
       data: {
           'name': name,
           'description': description,
           'priority': priority,
           'meeting': meeting,
           'csrfmiddlewaretoken': csrftoken
       },
       success: function(data){
        $('#name').val("");
        $('#description').val("");
        var name = data['name'];
        var description = data['description'];
        var done = data['done'];
        $('.item-list').append("<li><a class='aIcon' id='check' href='#' onclick='ItemDone()'><i class='icon un-check'></i></a><a class='aIcon' id='delete' href='#'><i class='icon  fa-trash-o'></i></a><p>"+name+": "+description+" - "+done+"</p></li>");

       }
   });
}