from django.contrib import admin
from .models import Project, ProjectTeam, ProjectSettings

class ProjectAdmin(admin.ModelAdmin):
    exclude = ("slug",)

admin.site.register(Project, ProjectAdmin)
admin.site.register(ProjectTeam)
admin.site.register(ProjectSettings)