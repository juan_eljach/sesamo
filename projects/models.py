from __future__ import unicode_literals
import random
from django.conf import settings
from django.core.urlresolvers import reverse
from django.db import models
from django.template import defaultfilters
from hashlib import sha1
from sesamo.tasks import SendEmail

class Project(models.Model):
	name = models.CharField(max_length=100)
	booking_price = models.IntegerField()
	initial_fee_percentage = models.IntegerField()
	slug = models.CharField(max_length=100)

	def save(self, *args, **kwargs):
		if not self.pk:
			self.slug = defaultfilters.slugify(self.name)
			print (self.slug)
			try:
				project_exists = Project.objects.get(slug__iexact=self.slug)
				if project_exists:
					self.slug = "{0}-{1}".format(self.slug, sha1(str(random.random()).encode('utf-8')).hexdigest()[:5])
			except Project.DoesNotExist:
				super(Project, self).save(*args, **kwargs)

	def __str__(self, *args, **kwargs):
		return self.name

	def get_absolute_url(self):
		return reverse('project_detail', kwargs={'project_slug': self.slug})

class ProjectTeam(models.Model):
	project = models.OneToOneField(Project)
	users = models.ManyToManyField(settings.AUTH_PROFILE_MODULE)

	def __str__(self):
		return self.project.name


class ProjectSettings(models.Model):
	booking_price = models.IntegerField()
	initial_fee_percentage = models.IntegerField()
	project = models.OneToOneField(Project, related_name="settings")

	def __str__(self):
		return "Settings of {}".format(self.project.name)
