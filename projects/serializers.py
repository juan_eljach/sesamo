from rest_framework import serializers
from .models import Project, ProjectSettings

class ProjectSerializer(serializers.ModelSerializer):
	initial_fee_percentage = serializers.IntegerField(source="settings.initial_fee_percentage")
	booking_price = serializers.IntegerField(source="settings.booking_price")
	class Meta:
		model = Project
		fields = (
			'id',
			'name',
			'initial_fee_percentage',
			'booking_price',
			'slug',
		)

class ProjectSettingsSerializer(serializers.ModelSerializer):
	pk = serializers.IntegerField(read_only=True)
	class Meta:
		model = ProjectSettings
		fields = (
			'pk',
			'booking_price',
			'project',
			'initial_fee_percentage', 
	)
