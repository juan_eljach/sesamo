from django.conf.urls import patterns, url
from django.views.generic import RedirectView
from .views import ProjectCreateView, ProjectDetailView, ProjectListView, LandingTemplateView


urlpatterns = patterns('',
	url(r'^$', LandingTemplateView.as_view(), name='index'),
	url(r'^projects/$', ProjectListView.as_view(), name='projects'),
	url(r'^projects/create/$', ProjectCreateView.as_view(), name='project_create'),
	url(r'^projects/(?P<project_slug>[-\w]+)/$', ProjectDetailView.as_view(), name='project_detail'),
)