from rest_framework import permissions
from rest_framework import exceptions


class IsManagerAndIsOnTeamOrForbidden(permissions.BasePermission):
	def has_object_permission(self, request, view, obj):
		return request.user.user_profile.user_type == 'gerente' and request.user.user_profile in obj.project.projectteam.users.all()

class IsOnTeamOrForbidden(permissions.BasePermission):
	def has_object_permission(self, request, view, obj):
		return request.user.user_profile in obj.project.projectteam.users.all()

class HasPersonAssigned(permissions.BasePermission):
	def has_object_permission(self, request, view, obj):
		if request.method in permissions.SAFE_METHODS:
			return True

		return obj.assigned_to == request.user

class CanCreateActivityOnPerson(permissions.BasePermission):
	def has_object_permission(self, request, view, obj):
		if request.method in permissions.SAFE_METHODS:
			return True

		if obj.person.assigned_to == request.user.user_profile:
			return True
		else:
			raise exceptions.PermissionDenied