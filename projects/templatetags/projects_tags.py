from django import template
from django.shortcuts import get_object_or_404
from projects.models import Project
register = template.Library()

@register.assignment_tag(takes_context=True)
def get_project(context, project_slug):
    project = get_object_or_404(Project, slug__iexact=project_slug)
    return project

