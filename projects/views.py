from rest_framework import generics
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.shortcuts import render, redirect, get_object_or_404
from django.utils.decorators import method_decorator
from django.views.generic import CreateView, DetailView, RedirectView, ListView, TemplateView
from datetime import datetime
from sesamo.decorators import user_belongs_to_project
from sesamo.mixins import SlugManagerMixin
from .models import Project, ProjectTeam, ProjectSettings
from .permissions import IsManagerAndIsOnTeamOrForbidden, IsOnTeamOrForbidden
from .serializers import ProjectSettingsSerializer, ProjectSerializer

from rest_framework import viewsets
from rest_framework.response import Response

decorators = [login_required, user_belongs_to_project]

@method_decorator(login_required, name='dispatch')
class ProjectCreateView(CreateView):
	model = Project
	fields = ["name", "booking_price", "initial_fee_percentage"]
	template_name = "projects/create_first_project.html"

	def form_valid(self, form):
		self.object = form.save()
		try:
			project_team = ProjectTeam.objects.create(project=self.object)
			project_team.users.add(self.request.user.user_profile)
			project_team.save()
		except Exception as err:
			print (err)
		return super(ProjectCreateView, self).form_valid(form)


@method_decorator(decorators, name='dispatch')
class ProjectDetailView(DetailView):
	model = Project
	slug_url_kwarg = 'project_slug'
	queryset = Project.objects.all()

	def get(self, request, *args, **kwargs):
		self.object = self.get_object()
		redirect_kwargs = {'project_slug': self.object.slug}
		print (request.user.user_profile.user_type)
		if not self.object.unity_set.all():
			return redirect(reverse('unity_types_create', kwargs=redirect_kwargs))
			#context = self.get_context_data(object=self.object)
			#return self.render_to_response(context)
		if request.user.user_profile.user_type == "vendedor" or request.user.user_profile.user_type == "gerente" or request.user.user_profile.user_type == "empleado" :
			return redirect(reverse('presale_splash', kwargs=redirect_kwargs))
		elif request.user.user_profile.user_type == "soporte":
			return redirect(reverse('office_splash', kwargs=redirect_kwargs))
		#elif request.user.user_profile.user_type == "gerente":
		#	return redirect(reverse())

@method_decorator(login_required, name='dispatch')
class ProjectListView(ListView):
	context_object_name = "projects"
	model = Project
	template_name = "projects/projects_list.html"

	def get_queryset(self):
		profile = self.request.user.user_profile
		projects = [project_team.project for project_team in profile.projectteam_set.all()]
		return projects

	def get(self, request, *args, **kwargs):
		self.object_list = self.get_queryset()
		if not self.object_list:
			return redirect(reverse("project_create"))
		context = self.get_context_data()
		return self.render_to_response(context)


class ProjectSettingsViewSet(SlugManagerMixin, viewsets.ModelViewSet):
	model_class = ProjectSettings
	serializer_class = ProjectSettingsSerializer
	queryset = ProjectSettings.objects.all()
	lookup_field = "project__slug"
	lookup_url_kwarg = "project_slug"

	def get_permissions(self):
		if self.request.method == 'GET':
			self.permission_classes = [IsOnTeamOrForbidden,]
		elif self.request.method == 'PUT' or self.request.method == "POST":
			self.permission_classes = [IsManagerAndIsOnTeamOrForbidden,]
		return super(ProjectSettingsViewSet, self).get_permissions()

#class ProjectSettingsViewSet(viewsets.ViewSet):
#	def retrieve(self, request, *args, **kwargs):
#		project_slug = kwargs["project_slug"]
#		project = get_object_or_404(Project, slug__iexact=project_slug)
#		project_settings = project.settings
#		serializer = ProjectSettingsSerializer(project_settings, initial={"booking_price":project.booking_price})
#		return Response(serializer.data)

#	def create(self, request, *args, **kwargs):
#		project_slug = kwargs["project_slug"]
#		project = get_object_or_404(Project, slug__iexact=project_slug)
#		serializer = ProjectSettingsSerializer(initial={"booking_price":project.booking_price})
#		return Response(serializer.data)




class ProjectRetrieveAPIView(generics.RetrieveAPIView):
	model_class = Project
	queryset = Project.objects.all()
	serializer_class = ProjectSerializer
	lookup_field = "slug"
	lookup_url_kwarg = "project_slug"


#class ProjectTeamRetrieveAPIView(generics.RetrieveAPIView):
#	model_class = ProjectTeam
#	queryset = ProjectTeam.objects.all()
#	serializer_class = ProjectTeamSerializer
#	lookup_field = "project__slug"
#	lookup_url_kwarg = "project_slug"

class LandingTemplateView(TemplateView):
	template_name = "landing.html"
