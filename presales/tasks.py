import datetime
from django.conf import settings
from celery.decorators import task, periodic_task
from celery.task.schedules import crontab
from alerts.models import Alert
from persons.models import Person, adquisition_channel_choices, segment_choices
from projects.models import Project
from .models import VirtualBooking

#Plotting
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt; plt.rcdefaults()
from matplotlib.backends.backend_pdf import PdfPages
import numpy as np

@periodic_task(run_every=(crontab(minute=0, hour=6)), name="daily_check_of_virtualbookings_in_unities", ignore_result=True)
def VirtualBookingChecker():
    today = datetime.datetime.today().date()
    time_delta = datetime.timedelta(days=settings.DAYS_INTERVAL_FOR_UNITY_VIRTUALBOOKING)
    interval_of_days = today - time_delta
    for virtual_booking in VirtualBooking.objects.filter(unity__status="virtually_booked", timestamp__lt=interval_of_days):
        virtual_booking.closed = True
        virtual_booking.save()
        virtual_booking.unity.status = "available"
        virtual_booking.unity.save()
        try:
            virtual_booking.unity.create_alert(
                alert_type="virtualbooking_closed",
                unity=virtual_booking.unity,
                person=virtual_booking.person,
                created_for=virtual_booking.assigned_to,
            )
        except Exception as err:
            print (err)
            pass

def GenerateReportTask(project_id):
    project = Project.objects.get(id=project_id)
    matplotlib.rcParams.update({'font.size': 5})
    with PdfPages('multipage_pdf.pdf') as pdf:
        #Adquisition Channels Report
        objects = []
        performance = []
        for channel in adquisition_channel_choices:
            objects.append(channel[1])
            persons_in_channel = Person.objects.filter(project=project, adquisition_channel=channel[0])
            performance.append(len(persons_in_channel))
        objects = tuple(objects)
        y_pos = np.arange(len(objects))
        plt.bar(y_pos, performance, align='center', color="blue", alpha=0.5, width=0.5)
        plt.xticks(y_pos, objects)
        plt.ylabel('Cantidad de Personas')
        plt.title('Cantida de Personas que llegaron por punto de contacto/canal de adquisición')
        pdf.savefig()
        plt.close


        def autolabel(rects):
            for rect in rects:
                height = rect.get_height()

                if height > 0:
                    ax.text(rect.get_x() + rect.get_width()/2., 0.6 * height, '%d' % int(height), ha='center', va='bottom')

        #Leads Segmentation Report
        segments = []
        people_in_segments = []
        people_in_segments_assignedto_none = []
        for segment in segment_choices:
            segments.append(segment[1])
            people = Person.objects.filter(project=project, segment=segment[0])
            people_in_segments.append(len(people))
            people_assignedto_none = Person.objects.filter(project=project, segment=segment[0], assigned_to=None)
            people_in_segments_assignedto_none.append(len(people_assignedto_none))
        y_pos = np.arange(len(segments))

        fig, ax = plt.subplots()
        bar1 = ax.bar(y_pos, people_in_segments, color='blue', alpha=0.5, width=0.35)
        bar2 = ax.bar(y_pos, people_in_segments_assignedto_none, color='r', alpha=0.5, width=0.35, bottom=people_in_segments)

        ax.set_ylabel('Personas')
        ax.set_title('Cantidad total de Personas en un segmento junto a la cantidad que no tienen seguimiento')
        ax.set_xticks(y_pos)
        ax.legend((bar1[0], bar2[0]), ('Personas en el segmento','Personas en el segmento sin seguimiento'))
        plt.xticks(y_pos, segments)
        autolabel(bar1)
        pdf.savefig()
        plt.close()



#@periodic_task(run_every=(crontab(minute=0, hour=8)), name="daily_email_of_activites", ignore_result=True)
#def ActivitiesSender():
#    today = datetime.today().date()
#    from accounts.models import UserProfile
#    for user_profile in UserProfile.objects.filter(user_type="vendedor"):
#        for projectteam in user.projectteam_set.all():
#            project = projectteam.project
#            activities = Activity.objects.filter(scheduled_date__date=today, person__assigned_to=user_profile, project=project)
#    project = self.get_project()
#    user_profile = self.request.user.user_profile
#    today = datetime.today().date()
#    activities = Activity.objects.filter(scheduled_date__date=today, person__assigned_to=user_profile, project=project)
#    return activities
