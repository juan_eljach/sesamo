import rest_framework_filters as filters
from persons.models import Person

class PersonFilter(filters.FilterSet):
    #paymentagreement__isnull = filters.BooleanFilter(name="paymentagreement", lookup_expr="isnull")

    class Meta:
        model = Person
        fields = ("is_client",)
