from django.contrib import admin
from .models import Quotation, QuickQuotation, VirtualBooking

admin.site.register(Quotation)
admin.site.register(QuickQuotation)
admin.site.register(VirtualBooking)