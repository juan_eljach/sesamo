from django.conf import settings
from django.db import models
from django.template.loader import render_to_string
from sesamo.tasks import SendEmail
from persons.models import Person
from units.models import Unity
from accounts.models import UserProfile

class Quotation(models.Model):
	person = models.ForeignKey(Person, related_name="quotations", on_delete=models.CASCADE)
	unity = models.ForeignKey(Unity)
	subsidy = models.IntegerField(default=0, blank=True)
	savings = models.IntegerField(default=0, blank=True)
	severance = models.IntegerField(default=0, blank=True)
	others = models.IntegerField(default=0, blank=True)
	monthly_income = models.IntegerField(default=0, blank=True)
	months_to_deliver = models.IntegerField(default=0, blank=True)
	sent = models.BooleanField(default=False)

	def send_quotation(self, *args, **kwargs):
		to_email = self.person.email
		to_name = self.person.get_person_full_name()
		from_email = settings.DEFAULT_EMAIL_SENDER
		from_name = settings.DEFAULT_NAME_SENDER
		subject = "Aqui tienes tu cotizacion"
		template_name = settings.QUOTATION_TEMPLATE_ID
		html_text = render_to_string("presales/emails/quotation_email_template.html")
		substitutions = {
			"-name-" : [self.person.first_name], 
			"-pk-" : [str(self.pk)]
		}
		SendEmail.delay(
			to_email, 
			to_name, 
			from_email, 
			from_name, 
			subject, 
			template_name, 
			html_text, 
			substitutions, 
		)
		#HACER AQUI TRACKING DE ERRORES ANTES DE RETORNAR TRUE
		return True

	def save(self, *args, **kwargs):
		super(Quotation, self).save(*args, **kwargs)
		if not self.sent:
			print ("Not sent")
			try:
				quotation_sent = self.send_quotation()
				if quotation_sent:
					self.sent = True
					super(Quotation, self).save(*args, **kwargs)
			except:
				pass

	def __str__(self):
		return self.unity.name

class QuickQuotation(models.Model):
	email = models.EmailField()
	first_name = models.CharField(max_length=100)
	last_name = models.CharField(max_length=100)
	price = models.IntegerField()
	months_to_deliver = models.IntegerField()
	savings = models.IntegerField(default=0, blank=True)
	sent = models.BooleanField(default=False)
	unity = models.ForeignKey(Unity)

	def get_months_to_deliver(self, *args, **kwargs):
		return self.months_to_deliver

	def send_quotation(self, *args, **kwargs):
		to_email = self.email
		to_name = self.first_name
		from_email = settings.DEFAULT_EMAIL_SENDER
		from_name = settings.DEFAULT_NAME_SENDER
		subject = "Aqui tienes tu cotizacion"
		template_name = settings.QUOTATION_TEMPLATE_ID
		html_text = render_to_string("presales/emails/quickquotation_email_template.html")
		substitutions = {
			"-name-" : [to_name], 
			"-pk-" : [str(self.pk)]
		}
		#HACER MANEJO DE EXCEPCIONES, REAL, AQUI
		try:
			SendEmail.delay(
				to_email, 
				to_name,
				from_email, 
				from_name, 
				subject, 
				template_name, 
				html_text, 
				substitutions, 
			)
			return True
		except Exception as err:
			print  ("THERE WAS AN EXCEPTION WHILE CALLING THE TASK: ")
			print (err)

	def save(self, *args, **kwargs):
		if not self.pk:
			super(QuickQuotation, self).save(*args, **kwargs)
			try:
				Person.objects.create(first_name=self.first_name, last_name=self.last_name, email=self.email, project=self.unity.project)
			except Exception as err:
				print ("ERROR: {}".format(err))
		if not self.sent:
			try:
				quickquotation_sent = self.send_quotation()
				if quickquotation_sent:
					self.sent = True
					super(QuickQuotation, self).save(*args, **kwargs)
			except:
				pass

class VirtualBooking(models.Model):
	closed = models.BooleanField(default=False)
	assigned_to = models.ForeignKey(settings.AUTH_PROFILE_MODULE, blank=True, null=True)
	person = models.ForeignKey(Person)
	unity = models.ForeignKey(Unity)
	notification_cellphone = models.CharField(max_length=100)
	notification_email = models.EmailField()
	timestamp = models.DateField(auto_now_add=True)


class Sale(models.Model):
	unity = models.ForeignKey(Unity)
	salesperson = models.OneToOneField(UserProfile)
	sale_date = models.DateField()
	timestamp = models.DateField(auto_now=True)
	buyer = models.ForeignKey(Person)
	