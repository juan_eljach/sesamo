from datetime import datetime
from django.conf import settings
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse, Http404
from django.shortcuts import render, redirect, get_object_or_404
from django.utils.decorators import method_decorator
from django.views.generic import CreateView
from rest_framework import generics, filters
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.views import APIView
from .models import Quotation, QuickQuotation, VirtualBooking
from .forms import QuotationModelForm
from .serializers import QuotationSerializer, QuickQuotationSerializer, VirtualBookingSerializer, VirtualBookingSerializerWithPersonSerializer
from .filters import PersonFilter
from django.template import RequestContext
from django.template.loader import get_template

from sesamo.mixins import SlugManagerMixin

from weasyprint import HTML, CSS

from persons.models import Person
from persons.serializers import PersonSmallSerializer

@method_decorator(login_required, name='dispatch')
class VirtualBookingCreateView(generics.CreateAPIView):
    queryset = VirtualBooking.objects.all()
    serializer_class = VirtualBookingSerializer

@method_decorator(login_required, name='dispatch')
class VirtualBookingForUnityListAPIView(SlugManagerMixin, generics.ListAPIView):
	model_class = VirtualBooking
	serializer_class = VirtualBookingSerializerWithPersonSerializer

	def get_queryset(self):
		project = self.get_project()
		unity = self.kwargs.get("id")
		virtual_bookings = VirtualBooking.objects.filter(unity=unity, closed=False)
		return virtual_bookings

@method_decorator(login_required, name='dispatch')
class PersonSearchView(SlugManagerMixin, generics.ListAPIView):
    model_class = Person
    queryset = Person.objects.all()
    serializer_class = PersonSmallSerializer
    filter_backends = (filters.SearchFilter, DjangoFilterBackend)
    filter_class = PersonFilter
    search_fields = ("^first_name", "^last_name")

    def get_queryset(self):
        project = self.get_project()
        persons_in_project = Person.objects.filter(project=project)
        return persons_in_project

@method_decorator(login_required, name='dispatch')
class QuotationCreateView(SlugManagerMixin, CreateView):
	model = Quotation
	form_class = QuotationModelForm

	def form_valid(self, form):
		person = self.get_person()
		project = self.get_project()
		unity = self.get_unity()
		self.object = form.save(commit=False)
		self.object.person = person
		self.object.unity = unity
		self.object.save()
		super(QuotationCreateView, self).form_valid(form)
		return HttpResponseRedirect(reverse("person_tracking", kwargs={"project_slug": project.slug, "person_slug": person.slug}))

	def get_success_url(self):
		project = self.get_project()
		person = self.get_person()
		return reverse("person_tracking", kwargs={"project_slug": project.slug, "person_slug": person.slug})

def get_quotation(request, pk):
	html_template = get_template("presales/quotation_template.html")
	quotation = get_object_or_404(Quotation, pk=pk)
	booking_price_for_this_quotation = quotation.unity.project.booking_price
	thirty_percent = int(0.3 * int(quotation.unity.price))
	credit_to_request = int(int(quotation.unity.price) - thirty_percent)
	initial_fee_residue = thirty_percent - (int(quotation.savings) + int(booking_price_for_this_quotation) + int(quotation.severance) + int(quotation.subsidy) + int(quotation.others))
	monthly_fees = int(int(initial_fee_residue) / int(quotation.months_to_deliver))
	today = str(datetime.today().date())
	data_dict = {
		'pagesize':"A4",
		'title': 'Cotizacion de Apartamento {0}'.format(quotation.unity.name),
		'apartment': quotation.unity.name,
		'apartment_price': int(quotation.unity.price),
		'thirty_percent': thirty_percent,
		'booking_price': booking_price_for_this_quotation,
		'savings': int(quotation.savings),
		'severance': int(quotation.severance),
		'subsidy': int(quotation.subsidy),
		'initial_fee_residue': initial_fee_residue,
		'monthly_fees': monthly_fees,
		'months_to_deliver': quotation.months_to_deliver,
		'credit_to_request': credit_to_request,
		'today': today,
		'unity': quotation.unity,
		'others': quotation.others
	}

	rendered_html = html_template.render(RequestContext(request, data_dict)).encode(encoding="UTF-8")

	pdf_file = HTML(string=rendered_html).write_pdf(stylesheets=[CSS(settings.STATIC_ROOT + "/css/quotation.css")])

	http_response = HttpResponse(pdf_file, content_type='application/pdf')
	http_response['Content-Disposition'] = 'filename="cotizacion.pdf"'
	return http_response

@method_decorator(login_required, name='dispatch')
class QuotationCreateAPIView(generics.CreateAPIView):
	queryset = Quotation.objects.all()
	serializer_class = QuotationSerializer

	def get_serializer_context(self):
		return {
			'request': self.request,
			'format': self.format_kwarg,
			'view': self
		}

@method_decorator(login_required, name='dispatch')
class QuickQuotationCreateAPIView(generics.CreateAPIView):
	queryset = QuickQuotation.objects.all()
	serializer_class = QuickQuotationSerializer

	def get_serializer_context(self):
		return {
			'request': self.request,
			'format': self.format_kwarg,
			'view': self
		}

#QUICK-QUOTATIONS

def get_quickquotation(request, pk):
	html_template = get_template("presales/quickquotation_template.html")
	quickquotation = get_object_or_404(QuickQuotation, pk=pk)
	booking_price_for_this_quotation = quickquotation.unity.project.booking_price
	thirty_percent = int(0.3 * int(quickquotation.unity.price))
	credit_to_request = int(int(quickquotation.unity.price) - thirty_percent)
	initial_fee_residue = thirty_percent - (int(quickquotation.savings) + int(booking_price_for_this_quotation))
	monthly_fees = int(int(initial_fee_residue) / int(quickquotation.months_to_deliver))
	today = str(datetime.today().date())
	data_dict = {
		'pagesize':"A4",
		'title': 'Cotizacion de Apartamento {0}'.format(quickquotation.unity.name),
		'apartment': quickquotation.unity.name,
		'apartment_price': int(quickquotation.unity.price),
		'thirty_percent': thirty_percent,
		'booking_price': booking_price_for_this_quotation,
		'savings': int(quickquotation.savings),
		'initial_fee_residue': initial_fee_residue,
		'monthly_fees': monthly_fees,
		'months_to_deliver': quickquotation.months_to_deliver,
		'credit_to_request': credit_to_request,
		'today': today,
		'unity': quickquotation.unity,
	}

	rendered_html = html_template.render(RequestContext(request, data_dict)).encode(encoding="UTF-8")

	pdf_file = HTML(string=rendered_html).write_pdf(stylesheets=[CSS(settings.STATIC_ROOT + "/css/quotation.css")])

	http_response = HttpResponse(pdf_file, content_type='application/pdf')
	http_response['Content-Disposition'] = 'filename="cotizacion.pdf"'
	return http_response


class SendNotificationView(SlugManagerMixin, APIView):
	def post(self, request, *args, **kwargs):
		project = self.get_project()
		report_serializer = ReportSerializer(data=request.data)
		if report_serializer.is_valid():
			unities_ids = notification_serializer.data["unities"]
			html_snippet = notification_serializer.data["html_snippet"]
			tower_number = notification_serializer.data["tower_number"]
			request_user_id = request.user.id
			project_id = project.id
			task = SendNotificationsTask.delay(request_user_id, project_id, unities_ids, html_snippet, tower_number)
			data = {"task_id":task.id}
			return Response(data=data, status=status.HTTP_200_OK)
		else:
			return Response(status= status.HTTP_400_BAD_REQUEST)
