#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django import forms
from django.utils.translation import ugettext_lazy as _
from .models import Quotation

class IntegerCurrencyInput(forms.TextInput):
    def render(self, name, value, attrs=None):
        from django.contrib.humanize.templatetags.humanize import intcomma
        if value:
            value = "$%s" % intcomma(value)
        return super(IntegerCurrencyInput, self).render(name, value, attrs)

class IntegerCurrencyField(forms.IntegerField):
    widget = IntegerCurrencyInput
    
    def clean(self, value):
        if value:
            if value[0] == "$": value = value[1:] # Cut off the dollar sign
            value = value.replace('.', '') # Remove Commas
        print (value)
        value = super(IntegerCurrencyField, self).clean(value)
        return int(value) if value else value


class QuotationModelForm(forms.ModelForm):
	subsidy = IntegerCurrencyField()
	savings = IntegerCurrencyField()
	severance = IntegerCurrencyField()
	monthly_income = IntegerCurrencyField()
	others = IntegerCurrencyField()

	class Meta:
		model = Quotation
		fields = [
			"subsidy",
			"savings",
			"severance",
			"months_to_deliver",
			"monthly_income", 
			"others"
		]