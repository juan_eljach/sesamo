from django.conf.urls import patterns, url
from .views import QuotationCreateView, get_quotation, get_quickquotation

urlpatterns = patterns('',
	url(r'^projects/(?P<project_slug>[-\w]+)/person/(?P<person_slug>[-\w]+)/(?P<unity_slug>[-\w]+)/quotation/$', QuotationCreateView.as_view(), name='create_quotation'),
	url(r"^download/quotation/(?P<pk>[-\w]+)/$", get_quotation, name="download_quotation"),
	url(r"^download/quickquotation/(?P<pk>[-\w]+)/$", get_quickquotation, name="download_quickquotation")
)