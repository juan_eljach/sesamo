from rest_framework import serializers
from persons.serializers import PersonSmallSerializer
from .models import Quotation, QuickQuotation, VirtualBooking, Sale

class QuotationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Quotation
        fields = (
            "id", 
            "person", 
            "unity", 
            "subsidy", 
            "savings", 
            "severance", 
            "others", 
            "monthly_income", 
            "months_to_deliver", 
            "sent"
        )


class QuickQuotationSerializer(serializers.ModelSerializer):
    class Meta:
        model = QuickQuotation
        fields = (
            "id", 
            "email", 
            "first_name", 
            "last_name", 
            "price", 
            "months_to_deliver", 
            "savings", 
            "sent", 
            "unity", 
        )

class VirtualBookingSerializer(serializers.ModelSerializer):
    class Meta:
        model = VirtualBooking
        fields = ( 
            'closed', 
            'person', 
            'unity',
            'assigned_to', 
            'notification_cellphone',
            'notification_email',
            'timestamp', 
        )

    def create(self, validated_data):
        obj = VirtualBooking.objects.create(**validated_data)
        obj.unity.status = "virtually_booked"
        obj.unity.save()
        return obj

class VirtualBookingSerializerWithPersonSerializer(serializers.ModelSerializer):
    person = PersonSmallSerializer(read_only=True)
    class Meta:
        model = VirtualBooking
        fields = (
            'person',
            'unity', 
            'notification_cellphone', 
            'notification_email', 
            'timestamp'
        )


class SaleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sale

class ReportSerializer(serializers.Serializer):
    start_date = serializers.DateField()
    end_date = serializers.DateField()