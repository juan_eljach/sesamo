from django.core.exceptions import ObjectDoesNotExist, PermissionDenied
from django.core.urlresolvers import reverse
from django.http import HttpResponseForbidden, Http404
from django.shortcuts import render, redirect, get_object_or_404
from django.forms import BaseFormSet
from django.forms.formsets import formset_factory
from django.views.generic import TemplateView
from projects.models import Project, ProjectTeam
from userena import settings as userena_settings
from userena import signals as userena_signals
from .forms import InvitationForm
from .models import Invitation

class BaseInviteTeamFormSet(BaseFormSet):
	def add_fields(self, form, index):
		form.fields["guest_email"].label = "Email"
		form.fields["team_role"].label = "Rol en el equipo"
		form.fields["team_role"].widget.attrs.update({'class' : 'browser-default'})
		form.fields['team_role'].choices = form.fields["team_role"].choices[2:]
		form.fields["team_role"].initial = "cartera"
		super(BaseInviteTeamFormSet, self).add_fields(form, index)

def project_team_invitation(request, *args, **kwargs):
	user = request.user
	InvitationFormset = formset_factory(InvitationForm, formset=BaseInviteTeamFormSet, extra=1)
	project = get_object_or_404(Project, slug__iexact=kwargs.get("project_slug"))
	try:
		project_team = project.projectteam
	except ObjectDoesNotExist:
		project_team = ProjectTeam.objects.create(project=project)
		project_team.users.add(request.user.user_profile)
	except Exception as err:
		print ("Unexpected error: {}".format(err))

	if request.method == "POST":
		invitations_formset = InvitationFormset(request.POST)
		if invitations_formset.is_valid() and request.user.user_profile and project_team:
			for form in invitations_formset.forms:
				form_instance = form.save(commit=False)
				form_instance.inviter = request.user.user_profile
				form_instance.project_team = project_team
				try:
					form_instance.save()
					context = {"invitations_sent":"True"}
				except:
					context = {"invitations_sent":"False"}
					pass
		else:
			return HttpResponseForbidden() 
	else:
		context = {}
		invitations_formset = InvitationFormset()

	context["invitations_formset"] = invitations_formset
	context["user_profile"] = user
	context.update(kwargs)
	return render(request, "invitations/invite_team.html", context)

class ExtraContextTemplateView(TemplateView):
    """ Add extra context to a simple template view """
    extra_context = None

    def get_context_data(self, *args, **kwargs):
        context = super(ExtraContextTemplateView, self).get_context_data(*args, **kwargs)
        if self.extra_context:
            context.update(self.extra_context)
        return context

    # this view is used in POST requests, e.g. signup when the form is not valid
    post = TemplateView.get

def invitation_signup(request, signup_form=None,
           template_name='userena/signup_form.html', success_url=None,
           extra_context=None, *args, **kwargs):
	"""
	IMPORTANT: THIS IS AN OVERRIDING OF USERENA'S VIEW signup. THIS IS DONE
	IN ORDER TO KEEP THE SAME SIGNUP LOGIC THAT USRENA PROVIDES
	Signup of an account.
	Signup requiring a username, email and password. After signup a user gets
	an email with an activation link used to activate their account. After
	successful signup redirects to ``success_url``.
	:param signup_form:
	    Form that will be used to sign a user. Defaults to userena's
	    :class:`SignupForm`.
	:param template_name:
	    String containing the template name that will be used to display the
	    signup form. Defaults to ``userena/signup_form.html``.
	:param success_url:
	    String containing the URI which should be redirected to after a
	    successful signup. If not supplied will redirect to
	    ``userena_signup_complete`` view.
	:param extra_context:
	    Dictionary containing variables which are added to the template
	    context. Defaults to a dictionary with a ``form`` key containing the
		``signup_form``.
	**Context**
	``form``
	    Form supplied by ``signup_form``.
    """
    # If signup is disabled, return 403
	if userena_settings.USERENA_DISABLE_SIGNUP:
		raise PermissionDenied

	# If no usernames are wanted and the default form is used, fallback to the
	# default form that doesn't display to enter the username.
	if userena_settings.USERENA_WITHOUT_USERNAMES and (signup_form == SignupForm):
		signup_form = SignupFormOnlyEmail

	form = signup_form()

	try:
		invitation_key = kwargs.get("invitation_key")
		project_slug = kwargs.get("project_slug")
		team_role = kwargs.get("team_role")
	except:
		raise Http404()

	project = get_object_or_404(Project, slug__iexact=project_slug)

	if request.method == 'POST':
		form = signup_form(request.POST, request.FILES)
		if form.is_valid():
			guest_email = form.cleaned_data["email"]
			try:
				invitation = Invitation.objects.get(
					guest_email=guest_email, 
					project_team=project.projectteam, 
					key=invitation_key, 
					team_role=team_role, 
				)
			except ObjectDoesNotExist:
				raise PermissionDenied

			if not invitation.used:
				user = form.save()
				project.projectteam.users.add(user.user_profile)
				invitation.used = True
				invitation.save()
			else:
				return HttpResponseForbidden()
			userena_signals.signup_complete.send(sender=None, user=user)
			if success_url: redirect_to = success_url
			else: redirect_to = reverse('userena_signup_complete',
								kwargs={'username': user.username})
			if request.user.is_authenticated():
				logout(request)
			if (userena_settings.USERENA_SIGNIN_AFTER_SIGNUP and
				not userena_settings.USERENA_ACTIVATION_REQUIRED):
				user = authenticate(identification=user.email, check_password=False)
				login(request, user)

			return redirect(redirect_to)

	if not extra_context: extra_context = dict()
	extra_context['form'] = form
	return ExtraContextTemplateView.as_view(template_name=template_name,
											extra_context=extra_context)(request)