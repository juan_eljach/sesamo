from django import forms
from .models import Invitation

class InvitationForm(forms.ModelForm):
	class Meta:
		model = Invitation
		fields = ["guest_email", "team_role"]