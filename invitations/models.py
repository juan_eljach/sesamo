import datetime
import hashlib
import random
from django.conf import settings
from django.db import models
from django.template.loader import render_to_string
from hashlib import sha1 as sha_constructor
from sesamo.tasks import SendEmail
from projects.models import ProjectTeam

user_type_choices = (
		("comprador", "Comprador"),
		("cartera", "Cartera"),
		("gerente", "Gerente"),
		("ingeniero", "Ingeniero"),
		("soporte", "Soporte al Cliente"),
		("vendedor", "Vendedor"),
	)

class Invitation(models.Model):
	guest_email = models.EmailField(blank=False, null=False)
	inviter = models.ForeignKey(settings.AUTH_PROFILE_MODULE, blank=False, null=False)
	key = models.CharField(max_length=50)
	project_team = models.ForeignKey(ProjectTeam, blank=False, null=False)
	team_role = models.CharField(max_length=20, choices=user_type_choices, blank=False, null=False)
	timestamp = models.DateTimeField(auto_now_add=True)
	sent = models.BooleanField(default=False)
	used = models.BooleanField(default=False)

	def generate_key(self):
		#random_string = str(random.random())
		#random_string = random_string.encode("utf-8")
		#salt = sha_constructor(random_string).hexdigest()[:5]
		#salt_bytes = salt.encode("utf-8")
		#datetime_encoded = str(datetime.datetime.now()).encode("utf-8")
		#inviter_username_encoded = self.inviter.user.username.encode("utf-8")
#		whole_string = "{}{}{}".format(datetime_encoded, salt_bytes, inviter_username_encoded).encode("utf-8")
		key = hashlib.sha1(str(random.getrandbits(256)).encode('utf-8')).hexdigest()
		return (key)

	def send_invitation(self, *args, **kwargs):
		to_email = self.guest_email
		to_name = None
		from_email = settings.DEFAULT_EMAIL_SENDER
		from_name = settings.DEFAULT_NAME_SENDER
		subject = "{} te ha invitado a unirte a su proyecto en Sesamo.com".format(self.inviter.user.get_full_name())
		template_name = settings.INVITATION_TO_PROJECT_TEMPLATE_ID
		html_text = render_to_string("invitations/emails/invitation_email.html")
		text = """
		%inviter name% te ha invitado a unirte como %guest-role% al proyecto %project-name%
		Acepta la invitacion en este link: http://sesamo.co/%url%
		"""
		substitutions = {
			"%inviter-name%" : [self.inviter.user.get_full_name()], 
			"%guest-role%" : [self.get_team_role_display()],
			"%project-name%" : [self.project_team.project.name],
			"%url%" : [
						"{}/{}/{}".format(self.key, self.project_team.project.slug, self.team_role)
					  ]
			}

		SendEmail.delay(
			to_email, 
			to_name, 
			from_email, 
			from_name, 
			subject, 
			template_name, 
			html_text,
			substitutions, 
			text, 
			)

	def save(self, *args, **kwargs):
		if not self.key:
			self.key = self.generate_key()
		if not self.sent:
			try:
				#self.send_invitation()
				self.sent = True
			except Exception as err:
				print (err)
		super(Invitation, self).save(*args, **kwargs)

	def __str__(self):
		return self.guest_email

