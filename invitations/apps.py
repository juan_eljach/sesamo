from __future__ import unicode_literals

from django.apps import AppConfig


class InvitationsConfig(AppConfig):
    name = 'invitations'
