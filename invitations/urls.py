from django.conf.urls import patterns, url
from django.core.urlresolvers import reverse_lazy
from accounts.forms import SignupFormOnlyEmailExtra
from .views import project_team_invitation, invitation_signup

urlpatterns = patterns('',
	url(r'^projects/(?P<project_slug>[-\w]+)/team/invite/$', project_team_invitation, name='invite_team_mates'),
	url(r'^invitation/(?P<invitation_key>\w+)/(?P<project_slug>[-\w]+)/(?P<team_role>[-\w]+)$', 
			invitation_signup, 
			{
				"signup_form": SignupFormOnlyEmailExtra, 
				"success_url": reverse_lazy("projects")
			}, 
			name='invitation_signup'
		),
)
