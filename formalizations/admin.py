from django import forms
from django.contrib import admin
from searchableselect.widgets import SearchableSelect
from .choices import CampaignType
from .models import PaymentAgreement, PaymentFee, Campaign, AgreedPayment, AdditionalPaymentFee, AdditionalPaymentAgreement

class PaymentAgreementAdmin(admin.ModelAdmin):
	exclude = ("slug",)

class PaymentFeeAdmin(admin.ModelAdmin):
	list_filter = ('payment_agreement',)
	list_display = ['__str__', 'payment_agreement', 'fee', 'date_to_pay', 'paid', 'debt']

class AdditionalPaymentAgreementForm(forms.ModelForm):
    class Meta:
        model = AdditionalPaymentAgreement
        exclude = ()
        widgets = {
            'unity': SearchableSelect(model='units.Unity', search_field='name', many=False, limit=1)
        }

class AdditionalPaymentAgreementAdmin(admin.ModelAdmin):
    form = AdditionalPaymentAgreementForm


class CampaingAdminForm(forms.ModelForm):
    class Meta:
        model = Campaign
        fields = '__all__'

    def clean(self):
        fixed_price_off = self.cleaned_data.get('fixed_price_off')
        percentage_off = self.cleaned_data.get('percentage_off')

        if fixed_price_off is not None and percentage_off is not None:
            raise forms.ValidationError('Cannot specify both fixed price off and percentage off')

        start_date = self.cleaned_data.get('start_date')
        end_date = self.cleaned_data.get('end_date')
        if any([x is None for x in (start_date, end_date)]):
            raise forms.ValidationError("Las fechas de vigencia son invalidas")

        campaign_type = self.cleaned_data.get('campaign_type')
        number_of_paymentfees = self.cleaned_data.get('number_of_paymentfees')
        if campaign_type is None:
            raise forms.ValidationError("Debe especificar el tipo de camapaña")
        elif campaign_type == CampaignType.DYNAMIC:
            if number_of_paymentfees is not None:
                raise forms.ValidationError("No se puede especificar numero de cuotas cuando la campaña es dinamica")
        elif campaign_type == CampaignType.FIXED:
            if number_of_paymentfees is None:
                raise forms.ValidationError("Si la campaña es fija debe especificar el numero de cuotas")
        return self.cleaned_data


class CampaignAdmin(admin.ModelAdmin):
    form = CampaingAdminForm

admin.site.register(PaymentAgreement, PaymentAgreementAdmin)
admin.site.register(PaymentFee, PaymentFeeAdmin)
admin.site.register(Campaign, CampaignAdmin)
admin.site.register(AgreedPayment)
admin.site.register(AdditionalPaymentFee)
admin.site.register(AdditionalPaymentAgreement, AdditionalPaymentAgreementAdmin)
