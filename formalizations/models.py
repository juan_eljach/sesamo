import random
from hashlib import sha1
from django.core.files import File
from io import BytesIO
from sesamo.utils import render_to_pdf
from django.conf import settings
from django.core.mail import EmailMessage
from django.core.exceptions import ValidationError
from django.db import models
from django.template import defaultfilters
from persons.models import Person
from projects.models import Project, ProjectTeam
from units.models import Unity
from .choices import types_of_agreed_payments, CampaignType

class AgreedPayment(models.Model):
	type_of_payment = models.CharField(max_length=30, choices=types_of_agreed_payments)
	fee = models.IntegerField(blank=True, null=True)
	applied_to_initialfee = models.BooleanField()
	paid = models.BooleanField(default=False)
	partially_paid = models.BooleanField(default=False)
	debt = models.IntegerField(default=0)

	def __str__(self):
		return "Type: {}. Value: {}".format(self.type_of_payment, self.fee)

class PaymentAgreement(models.Model):
	project = models.ForeignKey(Project)
	persons = models.ManyToManyField(Person)
	agreed_payments = models.ManyToManyField(AgreedPayment)
	seller = models.ForeignKey(settings.AUTH_PROFILE_MODULE, blank=True, null=True, related_name='as_seller')
	another_seller = models.CharField(max_length=100, blank=True, null=True) #NOTE: Used for sellers that are not users in the platform
	booking_price = models.IntegerField()
	initial_fee = models.IntegerField()
	required_mortgage = models.IntegerField(blank=True, null=True)
	#unity = models.ForeignKey(Unity, related_name='payment_agreements')
	unity = models.OneToOneField(Unity)
	referral_info = models.TextField(blank=True, null=True)
	is_new_version = models.BooleanField(default=False)
	modification_date = models.DateField(blank=True, null=True)
	old_version_copy = models.FileField(upload_to="uploads/payment-agreements/", blank=True, null=True)
	timestamp = models.DateField(auto_now_add=True)
	created_by = models.ForeignKey(settings.AUTH_PROFILE_MODULE, blank=True, null=True)
	slug = models.CharField(max_length=500)

	def generate_pdf(self):
		data_dict = {
                "unity" : self.unity,
                "initial_fee" : self.initial_fee,
				"timestamp": self.timestamp
        }
		for agreed_payment in self.agreed_payments.all():
			parsed_name = agreed_payment.type_of_payment.replace("-", "_")
			data_dict[parsed_name] = agreed_payment.fee
		data_dict["monthly_fees"] = self.payment_fees.all().order_by('date_to_pay')
		try:
			pdf = render_to_pdf("formalizations/payment_agreement_pdf_copy.html", data_dict)
		except Exception as err:
			pdf = None
		filename = "Acuerdo de Pagos {}.pdf".format(self.unity.name)
		# content =  BytesIO(pdf.content)
		return (filename, pdf, 'application/pdf')

	def notify_team(self):
		project_team = ProjectTeam.objects.get(project=self.project)
		to_list = []
		for userprofile in project_team.users.filter(user_type="gerente"):
			if hasattr(userprofile.user, 'email'):
				to_list.append(userprofile.user.email)
		if self.created_by.user.email is not None:
			to_list.append(self.created_by.user.email)
		from_email = settings.DEFAULT_EMAIL_SENDER
		from_name = settings.DEFAULT_NAME_SENDER
		subject = "Nueva Unidad Formalizada"
		body = "La unidad:{} del proyecto {} fue formalizada por {} el {}".format(
			self.unity,
			self.project,
			self.created_by,
			self.timestamp
		)
		email = EmailMessage(subject, body, from_email, to_list)
		packed_params = self.generate_pdf()
		email.attach(*packed_params)
		email.send(fail_silently=False)


	def save(self, *args, **kwargs):
		if not self.pk:
			self.slug = defaultfilters.slugify("payment-agreement-{}".format(self.unity))
			while True:
				try:
					paymentagreement_with_slug_exists = PaymentAgreement.objects.get(slug__iexact=self.slug)
					if paymentagreement_with_slug_exists:
						self.slug = "{0}-{1}".format(self.slug, sha1(str(random.random()).encode('utf-8')).hexdigest()[:5])
				except PaymentAgreement.DoesNotExist: break
		super(PaymentAgreement, self).save(*args, **kwargs)
		self.unity.status = 'sold'
		self.unity.save()

	def __str__(self):
		return "Acuerdo de pago de la unidad: {}".format(self.unity.name)


class PaymentFee(models.Model):
	payment_agreement = models.ForeignKey(PaymentAgreement, related_name="payment_fees")
	date_to_pay = models.DateField()
	fee = models.IntegerField()
	extraordinary_fee = models.IntegerField()
	paid = models.BooleanField(default=False)
	partially_paid = models.BooleanField(default=False)
	debt = models.IntegerField(default=0)
	comment = models.TextField(max_length=500, blank=True, null=True)

	def __str__(self):
		return "Valor de cuota: {}. Fecha: {}".format(self.fee, self.date_to_pay)

class AdditionalPaymentAgreement(models.Model):
	unity = models.OneToOneField(Unity)
	new_initial_fee = models.IntegerField()
	current_debt = models.IntegerField()
	parent_payment_agreement = models.OneToOneField(PaymentAgreement)

	def __str__(self):
		return ("Additional Payment Agreement: {}".format(self.unity))

class AdditionalPaymentFee(models.Model):
	additional_payment_agreement = models.ForeignKey(AdditionalPaymentAgreement, related_name='additional_payment_fees')
	date_to_pay = models.DateField()
	fee = models.IntegerField()
	paid = models.BooleanField(default=False)
	partially_paid = models.BooleanField(default=False)
	debt = models.IntegerField(default=0)
	comment = models.TextField(max_length=500, blank=True, null=True)

	def __str__(self):
		return "Valor de cuota: {}. Fecha: {}".format(self.fee, self.date_to_pay)


class Campaign(models.Model):
	name = models.CharField(max_length=100)
	description = models.TextField(max_length=500, blank=True, null=True)
	campaign_type = models.CharField(max_length=20, choices=CampaignType.CHOICES, blank=True, null=True)
	active = models.BooleanField(default=True)
	start_date = models.DateField(blank=True, null=True)
	end_date = models.DateField(blank=True, null=True)
	project = models.ForeignKey(Project)
	booking_price = models.IntegerField()
	initial_fee_percentage = models.IntegerField()
	percentage_off = models.IntegerField(blank=True, null=True)
	fixed_price_off = models.IntegerField(blank=True, null=True)
	number_of_paymentfees = models.IntegerField(blank=True, null=True)
	slug = models.CharField(max_length=500)

	def save(self, *args, **kwargs):
		if not self.pk:
			self.slug = defaultfilters.slugify("campaign-{}-{}".format(self.project.name, self.name))
			while True:
				try:
					campaign_with_slug_exists = Campaign.objects.get(slug__iexact=self.slug)
					if campaign_with_slug_exists:
						self.slug = "{0}-{1}".format(self.slug, sha1(str(random.random()).encode('utf-8')).hexdigest()[:5])
				except Campaign.DoesNotExist: break
		super(Campaign, self).save(*args, **kwargs)

	def __str__(self):
		return "{}. Proyecto: {}".format(self.name, self.project.name)
