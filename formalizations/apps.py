from django.apps import AppConfig


class FormalizationsConfig(AppConfig):
    name = 'formalizations'
