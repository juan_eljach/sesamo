import django_filters
from .models import PaymentAgreement

class PaymentAgreementFilter(django_filters.FilterSet):
    persons = django_filters.CharFilter(
        name='persons__first_name',
        lookup_expr='contains',
    )

    class Meta:
        model = PaymentAgreement
        fields = ('persons',)


# class PaymentAgreementFilter(filters.FilterSet):
#     has_person = filters.Filter(name="persons", lookup_type='in')

#     class Meta:
#         model = PaymentAgreement
#         fields = ("initial_fee", "has_person",)