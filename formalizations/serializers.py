import datetime
from django import template
from django.core.files import File
from io import BytesIO
from django.core.files.uploadedfile import SimpleUploadedFile
from django.template.loader import get_template
from rest_framework import serializers
from weasyprint import HTML, CSS
from sesamo.utils import render_to_pdf, custom_capture_exception
from accounts.models import UserProfile
from persons.models import Person
from units.models import Unity
from payments.serializers import PaymentSerializer, SurplusSerializer
from persons.serializers import PersonSmallSerializer
from .models import PaymentAgreement, PaymentFee, AgreedPayment, Campaign, AdditionalPaymentFee, AdditionalPaymentAgreement

class AgreedPaymentSerializer(serializers.ModelSerializer):
    class Meta:
        model = AgreedPayment
        fields = '__all__'

class PaymentFeeSerializer(serializers.ModelSerializer):
    extraordinary_fee = serializers.IntegerField(required=False)
    class Meta:
        model = PaymentFee
        fields = (
            'date_to_pay',
            'fee',
            'extraordinary_fee',
        )

class PaymentFeeSerializerExtended(serializers.ModelSerializer):
    class Meta:
        model = PaymentFee
        fields = (
            "date_to_pay",
            "fee",
            "extraordinary_fee",
            "paid",
            "partially_paid",
            "debt",
            "comment"
        )


class PaymentAgreementSerializer(serializers.Serializer):
    agreed_payments = AgreedPaymentSerializer(many=True, required=False)
    payment_fees = PaymentFeeSerializer(many=True)
    persons = serializers.PrimaryKeyRelatedField(many=True, read_only=False, queryset=Person.objects.all(), required=False)
    booking_price = serializers.IntegerField(required=False)
    initial_fee = serializers.IntegerField(required=False)
    required_mortgage = serializers.IntegerField(required=True)
    seller = serializers.PrimaryKeyRelatedField(read_only=False, queryset=UserProfile.objects.all(), required=False)
    referral_info = serializers.CharField(required=False)
    another_seller = serializers.CharField(required=False)
    unity = serializers.PrimaryKeyRelatedField(read_only=False, queryset=Unity.objects.all(), required=False)

    def create(self, validated_data):
        try:
            existing_payment_agreement = validated_data.pop('existing_payment_agreement')
        except:
            existing_payment_agreement = None
        try:
            persons_m2m = validated_data.pop('persons')
        except Exception as err:
            custom_capture_exception(err)

        pdf_oldversion_file = None
        if existing_payment_agreement:
            data_dict = {
                "unity" : existing_payment_agreement.unity,
                "initial_fee" : existing_payment_agreement.initial_fee
            }
            for agreed_payment in existing_payment_agreement.agreed_payments.all():
                data_dict[agreed_payment.type_of_payment.replace("-", "_")] = agreed_payment.fee
            data_dict["monthly_fees"] = existing_payment_agreement.payment_fees.all()

            try:
                pdf_oldversion_file = render_to_pdf("formalizations/payment_agreement_pdf_copy.html", data_dict)
            except Exception as err:
                custom_capture_exception(err)
            document_name = "Copia Version Anterior-{}.pdf".format(existing_payment_agreement.unity.name)
            existing_payment_agreement.delete()

        payment_fees = validated_data.pop('payment_fees')
        agreed_payments = validated_data.pop('agreed_payments')

        new_payment_agreement = PaymentAgreement.objects.create(**validated_data)
        try:
            if pdf_oldversion_file:
                new_payment_agreement.is_new_version = True
                new_payment_agreement.modification_date = datetime.datetime.today()
                new_payment_agreement.old_version_copy.save(document_name, File(pdf_oldversion_file))
                #new_payment_agreement.old_version_copy = SimpleUploadedFile(document_name, pdf_oldversion_file, content_type='application/pdf')
                new_payment_agreement.save()
        except Exception as err:
            custom_capture_exception(err)
        for person in persons_m2m:
            new_payment_agreement.persons.add(person)
            person.sold = True
            person.save()

        for payment_fee in payment_fees:
            try:
                new_payment_fee = PaymentFee.objects.create(
                    payment_agreement=new_payment_agreement,
                    date_to_pay=payment_fee["date_to_pay"],
                    fee=payment_fee["fee"],
                    extraordinary_fee=payment_fee["extraordinary_fee"],
                )
            except Exception as err:
                custom_capture_exception(err)

        for agreed_payment in agreed_payments:
            try:
                new_agreed_payment = AgreedPayment.objects.create(
                    type_of_payment = agreed_payment["type_of_payment"],
                    fee = agreed_payment["fee"],
                    applied_to_initialfee = agreed_payment["applied_to_initialfee"],
                    )
                new_payment_agreement.agreed_payments.add(new_agreed_payment)
            except Exception as err:
                custom_capture_exception(err)

        #Creando modelo Agreed_Payment de tipo Separacion automaticamente
        #TODO: Change prints for Logging!
        try:
            separacion = AgreedPayment.objects.create(
                type_of_payment = "separacion",
                fee = new_payment_agreement.booking_price,
                applied_to_initialfee = True
            )
            new_payment_agreement.agreed_payments.add(separacion)
        except Exception as e:
            custom_msg = "Error agregando pago de separacion al acuerdo de pago. Crear manualmente"
            custom_capture_exception(e, custom_msg)

        try:
            mortgage = AgreedPayment.objects.create(
                type_of_payment = "credito-hipotecario",
                fee = new_payment_agreement.required_mortgage,
                applied_to_initialfee = False
            )
            new_payment_agreement.agreed_payments.add(mortgage)
        except Exception as e:
            custom_msg = "Error agregando credito hipotecario al acuerdo de pago. Crear manualmente"
            custom_capture_exception(e, custom_msg)

        try:
            new_payment_agreement.notify_team()
        except Exception as e:
            custom_msg = "Failed to notify the team of the project about the creation of a payment agreement"
            custom_capture_exception(e, custom_msg)

        return new_payment_agreement

class AdditionalPaymentFeeSerializer(serializers.ModelSerializer):
    class Meta:
        model = AdditionalPaymentFee
        fields = (
            "date_to_pay",
            "fee",
            "paid",
            "partially_paid",
            "debt",
            "comment"
        )

class AdditionalPaymentAgreementSerializer(serializers.ModelSerializer):
    additional_payment_fees = AdditionalPaymentFeeSerializer(many=True)
    class Meta:
        model = AdditionalPaymentAgreement
        fields = (
        	'id',
        	'new_initial_fee',
            'current_debt',
            'additional_payment_fees',
            'unity',
        )

    def create(self, validated_data):
        additional_payment_fees = validated_data.pop('additional_payment_fees')
        new = AdditionalPaymentAgreement.objects.create(**validated_data)
        for payment_fee in additional_payment_fees:
            try:
                new_payment_fee = AdditionalPaymentFee.objects.create(
                    additional_payment_agreement=new,
                    date_to_pay=payment_fee["date_to_pay"],
                    fee=payment_fee["fee"],
                )
            except Exception as err:
                print (err)
        return new_payment_agreement


class PaymentAgreementDetailSerializer(serializers.ModelSerializer):
    agreed_payments = AgreedPaymentSerializer(many=True, read_only=True)
    payment_fees = serializers.SerializerMethodField()
    persons = PersonSmallSerializer(many=True)
    payments = PaymentSerializer(many=True)
    surplus = SurplusSerializer(read_only=True)
    timestamp = serializers.DateField(format='iso-8601')
    paymentfees_enmora = serializers.SerializerMethodField()
    seller = serializers.StringRelatedField()
    unity = serializers.StringRelatedField()
    unity_price = serializers.SerializerMethodField()

    class Meta:
        model = PaymentAgreement
        fields = (
            'id',
            'persons',
            'payment_fees',
            'agreed_payments',
            'required_mortgage',
            'seller',
            'another_seller',
            'referral_info',
            'booking_price',
            'initial_fee',
            'payments',
            'surplus',
            'timestamp',
            'is_new_version',
            'old_version_copy',
            'modification_date',
            'paymentfees_enmora',
            'unity',
            'unity_price',
        )

    def get_paymentfees_enmora(self, obj):
        if obj.payment_fees.all():
            list_enmora = []
            for x in obj.payment_fees.all():
                if x.paid == False and x.date_to_pay < datetime.date.today():
                    list_enmora.append(x)
                else:
                    pass
            return len(list_enmora)
        else:
            return None

    def get_unity_price(self, obj):
        return obj.unity.price

    def get_payment_fees(self, obj):
        data=obj.payment_fees.all().order_by('date_to_pay')
        return PaymentFeeSerializerExtended(data, many=True, context=self.context).data

class CampaignSerializer(serializers.ModelSerializer):
    class Meta:
        model = Campaign
        fields = (
            'name',
            'booking_price',
            'campaign_type',
            'initial_fee_percentage',
            'number_of_paymentfees',
            'percentage_off',
            'fixed_price_off',
            'slug',
        )
