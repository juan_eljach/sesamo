import django_filters
import datetime
from datetime import date
from weasyprint import HTML, CSS
from tablib import Dataset
from django import template
from django.core.files.uploadedfile import SimpleUploadedFile
from django.http import HttpResponse
from django_filters import rest_framework as filters
from django.template.loader import get_template
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.pagination import PageNumberPagination
from rest_framework.filters import SearchFilter
from django.shortcuts import render, get_object_or_404
from django.views.generic import CreateView
from sesamo.mixins import SlugManagerMixin
from units.models import Unity
from .filters import PaymentAgreementFilter
from .models import PaymentAgreement, Campaign, AgreedPayment, PaymentFee, AdditionalPaymentAgreement
from .pagination import OfficePagination
from .serializers import PaymentAgreementSerializer, PaymentAgreementDetailSerializer, CampaignSerializer, AgreedPaymentSerializer, PaymentFeeSerializerExtended, AdditionalPaymentAgreementSerializer

class AgreedPaymentmentCreateView(generics.CreateAPIView):
    queryset = AgreedPayment.objects.all()
    serializer_class = AgreedPaymentSerializer

class PaymentAgreementCreateView(SlugManagerMixin, generics.CreateAPIView):
    queryset = PaymentAgreement.objects.all()
    serializer_class = PaymentAgreementSerializer

    def create(self, request, *args, **kwargs):
        self.project = self.get_project()
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        unity = serializer.validated_data.get('unity')
        try:
            payment_agreement_exists = PaymentAgreement.objects.get(unity=unity)
        except Exception as err:
            payment_agreement_exists = False

        if payment_agreement_exists:
            self.perform_create(serializer, existing_payment_agreement=payment_agreement_exists)
            return Response(serializer.data)
        else:
            self.perform_create(serializer)
            return Response(serializer.data)

    def perform_create(self, serializer, *args, **kwargs):
        serializer.save(
            created_by = self.request.user.user_profile,
            project=self.project,
            *args,
            **kwargs)

class AdditionalPaymentAgreementCreateView(SlugManagerMixin, generics.CreateAPIView):
    queryset = AdditionalPaymentAgreement.objects.all()
    serializer_class = AdditionalPaymentAgreementSerializer


class PaymentAgreementListAPIView(SlugManagerMixin, generics.ListAPIView):
    model_class = PaymentAgreement
    pagination_class = OfficePagination
    serializer_class = PaymentAgreementDetailSerializer
    filter_backends = (SearchFilter, DjangoFilterBackend)
    search_fields = ("^unity__name", 'persons__first_name')
    filter_fields = ['persons',]
    filter_class = PaymentAgreementFilter

    def get_queryset(self):
        project = self.get_project()
        queryset = self.model_class.objects.filter(project=project)
        unity_id = self.request.query_params.get('unity', None)
        if unity_id is not None:
            queryset = PaymentAgreement.objects.filter(unity=unity_id)
        return queryset

class PaymentAgreementDetailView(SlugManagerMixin, generics.RetrieveAPIView):
	model_class = PaymentAgreement
	serializer_class = PaymentAgreementDetailSerializer
	queryset = PaymentAgreement.objects.all()
	lookup_field = "id"
	lookup_url_kwarg = "payment_agreement_id"

class PaymentFeesListFilter(django_filters.FilterSet):
    class Meta:
        model = PaymentFee
        fields = ['paid']

class PaymentFeesListAPIView(SlugManagerMixin, generics.ListAPIView):
	model_class = PaymentFee
	serializer_class = PaymentFeeSerializerExtended

	def get_queryset(self):
		project = self.get_project()
		unity_slug = self.kwargs.get("unity_slug")
		unity = get_object_or_404(Unity, slug__iexact=unity_slug, project=project)
		return PaymentFee.objects.filter(payment_agreement__unity=unity)

	def list(self, request, *args, **kwargs):
		queryset = self.get_queryset()
		filtered = PaymentFeesListFilter(request.GET, queryset=queryset)
		serializer = self.serializer_class(filtered, many=True, context={"request": request})
		return Response(serializer.data)

class CampaignListAPIView(SlugManagerMixin, generics.ListAPIView):
	model_class = Campaign
	serializer_class = CampaignSerializer

	def get_queryset(self):
		project = self.get_project()
		q= self.model_class.objects.filter(
            project=project, 
            end_date__gte=date.today()
        )
		return q

class CampaignRetrieveAPIView(generics.RetrieveAPIView):
	model_class = Campaign
	queryset = Campaign.objects.all()
	serializer_class = CampaignSerializer
	lookup_field = "slug"
	lookup_url_kwarg = "campaign_slug"

from django.template.loader import render_to_string, get_template
from django.template import Context, Template
from django.http import HttpResponse
from weasyprint import HTML
import tempfile
"""
def compraventa(request):
    html_template = get_template("formalizations/compraventa.html")
    data = {"names":["Juan Eljach", "Jessica Hernandez"]}
    context = Context(data)
    return HttpResponse(html_template.render(context).encode(encoding="UTF-8"), request)
"""

#use decorators ---- for what Juan? Permissions?
class PurchaseAgreementDocumentsAPIView(SlugManagerMixin, APIView):

    def download_payment_agreement(self, request, **kwargs):
        project = self.get_project()
        payment_agreement_id = self.kwargs.get("payment_agreement_id")
        self.payment_agreement = get_object_or_404(PaymentAgreement, id=payment_agreement_id)
        persons = [person for person in self.payment_agreement.persons.all()]
        unity = self.payment_agreement.unity
        unity_name_splitted = unity.name.split("-")
        #preguntarle por la regex a david
        unity_tower = unity_name_splitted[0]
        unity_apartment = unity_name_splitted[1]
        if len(unity_apartment) == 3:
            unity_level = unity_apartment[0]
            unity_apartment_number = unity_apartment[1:]
        elif len(unity_apartment) == 4:
            unity_level = unity_apartment[0:2]
            unity_apartment_number = unity_apartment[2:]

        unity_built_area = unity.unity_type.built_area
        unity_private_area = unity.unity_type.private_area
        unity_price = unity.price

        payment_fees_sum = sum([payment_fee.fee + payment_fee.extraordinary_fee for payment_fee in self.payment_agreement.payment_fees.all()])
        agreed_payments_applied_to_initialfee_sum = sum([agreed_payment.fee for agreed_payment in self.payment_agreement.agreed_payments.all() if agreed_payment.applied_to_initialfee == True])
        total_initial_fee = int(payment_fees_sum + agreed_payments_applied_to_initialfee_sum)
        initial_fee_percentage = round((total_initial_fee * 100) / unity_price, 1)
        booking_price = self.payment_agreement.booking_price
        unity_price_remaining_balance = unity_price - total_initial_fee
        unity_price_remaining_balance_percentage = round((unity_price_remaining_balance / 100) / unity_price, 1)

        #write context data
        data = {
            "clients" : persons,
            "project_name" : project.name,
            "unity_tower" : unity_tower,
            "unity_level" : unity_level,
            "unity_apartment" : unity_apartment,
            "unity_apartment_number" : unity_apartment_number,
            "unity_built_area" : unity_built_area,
            "unity_private_area" : unity_private_area,
            "unity_price" : unity_price,
            "total_initial_fee" : total_initial_fee,
            "initial_fee_percentage" : initial_fee_percentage,
            "booking_price" : booking_price,
            "payment_fees_count" : self.payment_agreement.payment_fees.all().count(),
            "payment_fees" : self.payment_agreement.payment_fees.all(),
            "unity_price_remaining_balance" : unity_price_remaining_balance

        }

        html_string = render_to_string("formalizations/purchase-agreement-doc.html", data)
        pdf_file = HTML(string=html_string).write_pdf()
        response = HttpResponse(content_type='application/pdf;')
        response['Content-Disposition'] = 'inline; filename=list_people.pdf'
        response['Content-Transfer-Encoding'] = 'binary'
        with tempfile.NamedTemporaryFile(delete=True) as output:
            output.write(pdf_file)
            output.flush()
            output = open(output.name, 'rb')
            response.write(output.read())
        return response

    def download_payment_fees(self, request, **kwargs):
        fields_list = ('date_to_pay', 'fee', 'extraordinary_fee', 'comment')
        payment_fees = self.payment_agreement.payment_fees.all()

        dataset = Dataset()
        dataset.headers = fields_list
        for payment_fee in payment_fees:
            dataset.append([getattr(payment_fee, field) for field in fields_list])
        response = HttpResponse(dataset.csv, content_type="csv")
        response['Content-Disposition'] = 'attachment; filename={}-cuotas.csv'.format(self.payment_agreement.unity)
        return response


    def get(self, request, **kwargs):
        download_type = request.GET['type']
        payment_agreement_id = self.kwargs.get("payment_agreement_id")
        self.payment_agreement = get_object_or_404(PaymentAgreement, id=payment_agreement_id)
        if download_type == 'cuotas':
            return self.download_payment_fees(request, **kwargs)
        elif download_type == 'contrato':
            #NOTE: Descarga el contrato de compraventa
            return self.download_payment_agreement(request, **kwargs)
        elif download_type == 'acuerdo':
            filename, pdf_result, content_type = self.payment_agreement.generate_pdf()
            response = HttpResponse(pdf_result, content_type=content_type)
            response['Content-Disposition'] = 'attachment; filename={}-cuotas.pdf'.format(filename)
            return response
        else:
            return self.download_payment_agreement(request, **kwargs)

def compraventa(request):
    pa = PaymentAgreement.objects.all()[0]
    persons = pa.persons.all()
    ja = datetime.datetime.today()
    data = {"persons": persons, "value": 1250543, "date":pa.payment_fees.all()[0].date_to_pay, "another":ja}
    html_string = render_to_string("formalizations/compraventa.html", data)
    pdf_file = HTML(string=html_string).write_pdf()
    response = HttpResponse(content_type='application/pdf;')
    response['Content-Disposition'] = 'inline; filename=list_people.pdf'
    response['Content-Transfer-Encoding'] = 'binary'
    with tempfile.NamedTemporaryFile(delete=True) as output:
        output.write(pdf_file)
        output.flush()
        output = open(output.name, 'rb')
        response.write(output.read())
    return response
