types_of_agreed_payments = (
		("ahorro-programado", "Ahorro Programado"),
		("cesantias", "Cesantías"),
		("subsidio-vivienda", "Subsidio de vivienda"),
		("otro-pago-acordado", "Otro Pago Acordado"),
		("separacion", "Separacion"),
		("credito-hipotecario", "Credito Hipotecario")
)


class CampaignType:
	DYNAMIC = "DINAMICA"
	FIXED = "FIJA"
	CHOICES = ((DYNAMIC, DYNAMIC),(FIXED, FIXED))