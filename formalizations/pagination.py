from rest_framework.pagination import PageNumberPagination

class OfficePagination(PageNumberPagination):
    page_size = 15
