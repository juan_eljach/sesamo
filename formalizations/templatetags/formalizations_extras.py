from num2words import num2words
from django import template

register = template.Library()

@register.filter(name="to_text")
def to_text(value, keep_as_text):
    if keep_as_text == False:
        value = float(value)
    value_to_text = num2words(value, lang="es")
    return value_to_text
