# core/api.py
"""Called from the project root's urls.py URLConf thus:
url(r" ˆ api/", include("core.api", namespace="api")),
"""
from django.conf.urls import url
from rest_framework.routers import DefaultRouter
from accounts.views import UserDetailView, ProjectTeamListView, UserProfileDetailView
from alerts.views import AlertListAPIView
from projects.views import ProjectSettingsViewSet, ProjectRetrieveAPIView
from persons.views import PersonListAPIView, PersonMainCreateView, PersonDetailUpdateView, PersonSmallUpdateView, PersonSmallCreateView, ActivityListAPIView, ActivityCreateAPIView, ActivityUpdateAPIView, NoteCreateAPIView, OrderLogViewSet
from units.views import UnityListAPIView, UnityDetailUpdateAPIView, UnitySearchView, AvailableUnitiesListAPIView
from formalizations.views import PaymentAgreementCreateView, PaymentAgreementListAPIView, CampaignListAPIView, AgreedPaymentmentCreateView, CampaignRetrieveAPIView, PaymentAgreementDetailView, PaymentFeesListAPIView, PurchaseAgreementDocumentsAPIView, AdditionalPaymentAgreementCreateView
from presales.views import QuotationCreateAPIView, QuickQuotationCreateAPIView, VirtualBookingCreateView, VirtualBookingForUnityListAPIView, PersonSearchView
from payments.views import PaymentCreateAPIView
from reports.views import OperationalReportView, ManagementReportView, PaymentsReportView, PaymentAgreementReportView
from support.views import TicketViewSet, TicketDetailView, TickerAnswerCreateView, TicketNoteCreateView, ClientRequestAPIView, SpecialFilterTicketsAPIView, TemplatesApiView
from deeds.views import ReturnTemplate, SendNotificationView, TowerNotificationsTrackingView, UnityDeedsNotifications, DownloadNotificationDocuments, CreditCertificateViewSet, UnityDeedsCheckPayments, UnityDetailDeedNotification, BankConfirmationViewSet, DeedOrderViewSet, DatesTracking, GenerateOtroSi
from crm.views import NotesViewSet
from .views import GetTaskStatus

from documents.views import TemplateDetailView

router = DefaultRouter()
router.register(r'credits', CreditCertificateViewSet, base_name="credits")

from rest_framework.routers import Route, DefaultRouter, SimpleRouter


router = DefaultRouter()
router.register(r'projects/(?P<project_slug>[-\w]+)/orderlog', OrderLogViewSet, base_name='order_log')



urlpatterns = [
	#URL API configuration for Projects
	url(r'^projects/(?P<project_slug>[-\w]+)/settings/$', ProjectSettingsViewSet.as_view({"post":"create", "get":"retrieve", "put":"update"}), name='create_settings'),
	#url(r'^projects/(?P<project_slug>[-\w]+)/settings/update/$', ActivityCreateAPIView.as_view(), name='create_activity'),

	#URL API configuration for Persons and segmentation
	url(r'^projects/(?P<project_slug>[-\w]+)/segmentation/$', PersonListAPIView.as_view(), name='segmentation'),
	url(r'^projects/(?P<project_slug>[-\w]+)/segmentation/(?P<slug>[-\w]+)/$', PersonDetailUpdateView.as_view(), name='person_tracking'),
	#url(r'^projects/(?P<project_slug>[-\w]+)/orderlog/segmentation/$', OrderLogViewSet.as_view({'post': 'create', 'get':'retrieve', 'put':'update'}), name='order_log'),

	#URL API configuration for Activities and Notes
	url(r'^projects/(?P<project_slug>[-\w]+)/activities/today/$', ActivityListAPIView.as_view(), name='activities_list_for_salespeople'),
	url(r'^projects/(?P<project_slug>[-\w]+)/activities/create/$', ActivityCreateAPIView.as_view(), name='create_activity'),
	url(r'^projects/(?P<project_slug>[-\w]+)/activities/update/(?P<id>\d+)/$', ActivityUpdateAPIView.as_view(), name='update_activity'),
	url(r'^projects/(?P<project_slug>[-\w]+)/notes/create/$', NoteCreateAPIView.as_view(), name='create_activity'),

	#URL API configuration for Unities
	url(r'^projects/(?P<project_slug>[-\w]+)/unities/$', UnityListAPIView.as_view(), name='apiunities_list'),
	url(r'^projects/(?P<project_slug>[-\w]+)/info/$', ProjectRetrieveAPIView.as_view(), name='project_info'),
	url(r'^projects/(?P<project_slug>[-\w]+)/available-unities/$', AvailableUnitiesListAPIView.as_view(), name='apiunities_list'),
	url(r'^projects/(?P<project_slug>[-\w]+)/unities/(?P<id>[-\w]+)/$', UnityDetailUpdateAPIView.as_view(), name='apiunity_detail'),

	#URL API configuration for Unities Alerts
	url(r'^projects/(?P<project_slug>[-\w]+)/alerts/$', AlertListAPIView.as_view(), name='alerts'),


	#URL API configuration for Person models
	url(r'^projects/(?P<project_slug>[-\w]+)/persons/create/$', PersonMainCreateView.as_view(), name='person_main_create'),

	#URL API configuration for PRESALES
	url(r'^projects/(?P<project_slug>[-\w]+)/presales/quotation/create/$', QuotationCreateAPIView.as_view(), name='quotation_create'),
	url(r'^projects/(?P<project_slug>[-\w]+)/presales/quickquotation/create/$', QuickQuotationCreateAPIView.as_view(), name='quickquotation_create'),
	url(r'^projects/(?P<project_slug>[-\w]+)/presales/virtual-booking/create/$', VirtualBookingCreateView.as_view(), name='virtualbooking_create'),
	url(r'^projects/(?P<project_slug>[-\w]+)/presales/unity/(?P<id>[-\w]+)/virtual-booking-list/$', VirtualBookingForUnityListAPIView.as_view(), name='virtual_booking_list'),
	url(r'^projects/(?P<project_slug>[-\w]+)/presales/operational-report/download/$', OperationalReportView.as_view(), name='operational_report_generate'),
	url(r'^projects/(?P<project_slug>[-\w]+)/presales/management-report/download/$', ManagementReportView.as_view(), name='management_report_generate'),
	url(r'^projects/(?P<project_slug>[-\w]+)/presales/payments-report/download/$', PaymentsReportView.as_view(), name='payment_report_generate'),
	url(r'^projects/(?P<project_slug>[-\w]+)/presales/payment-agreements-report/download/$', PaymentAgreementReportView.as_view(), name='payment_agreements_report_generate'),


	#URL API configuration for Payment agreements in Formalizations
	url(r'^projects/(?P<project_slug>[-\w]+)/formalizations/campaigns/$', CampaignListAPIView.as_view(), name='campaign_list'),
	url(r'^projects/(?P<project_slug>[-\w]+)/formalizations/campaigns/(?P<campaign_slug>[-\w]+)/$', CampaignRetrieveAPIView.as_view(), name='campaign_retrieve_detail'),
	#url(r'^projects/(?P<project_slug>[-\w]+)/formalizations/agreed-payments/$', AgreedPaymentmentCreateView.as_view(), name='campaign_list'),
	url(r'^projects/(?P<project_slug>[-\w]+)/formalizations/payment-agreements/$', PaymentAgreementListAPIView.as_view(), name='paymentagreement_list'),
	url(r'^projects/(?P<project_slug>[-\w]+)/formalizations/payment-agreements/detail/(?P<payment_agreement_id>\d+)/$', PaymentAgreementDetailView.as_view(), name='paymentagreement_detail'),
	url(r'^projects/(?P<project_slug>[-\w]+)/formalizations/payment-agreement/create/$', PaymentAgreementCreateView.as_view(), name='paymentagreement_create'),
	url(r'^projects/(?P<project_slug>[-\w]+)/formalizations/person/create/$', PersonSmallCreateView.as_view(), name='person_create'),
	url(r'^projects/(?P<project_slug>[-\w]+)/formalizations/person/(?P<slug>[-\w]+)/small-update/$', PersonSmallUpdateView.as_view(), name='person_update'),
	url(r'^projects/(?P<project_slug>[-\w]+)/formalizations/unity/(?P<unity_slug>[-\w]+)/payment-fees/$', PaymentFeesListAPIView.as_view(), name='payment_fees_by_unity'),
	url(r'^projects/(?P<project_slug>[-\w]+)/formalizations/payment-agreements/(?P<payment_agreement_id>\d+)/download/$', PurchaseAgreementDocumentsAPIView.as_view(), name='paymentagreement_docs_download'),

	url(r'^projects/(?P<project_slug>[-\w]+)/formalizations/additional-payment-agreement/create/$', AdditionalPaymentAgreementCreateView.as_view(), name='addtional_paymentagreement_create'),

	#URL API CONFIGURATION FOR PAYMENTS
	url(r'^projects/(?P<project_slug>[-\w]+)/payment/create/$', PaymentCreateAPIView.as_view(), name='payment_create'),
	url(r'^projects/(?P<project_slug>[-\w]+)/payments/$', PaymentCreateAPIView.as_view(), name='payment_create'),

	#URL API for Persons
	url(r'^projects/(?P<project_slug>[-\w]+)/persons/$', PersonSearchView.as_view(), name='person_list_and_search'),
	url(r'^user/$', UserDetailView.as_view(), name='get_current_user'),
	url(r'^userprofile/$', UserProfileDetailView.as_view(), name='get_current_userprofile'),


	url(r'^projects/(?P<project_slug>[-\w]+)/search-unities/$', UnitySearchView.as_view(), name='unities_list_and_search'),


	#URL API for templates
	url(r'^templates/(?P<template_id>[-\w]+)/$', TemplateDetailView.as_view(), name='template_detail'),

	#URL API FOR SUPPORT
	url(r'^projects/(?P<project_slug>[-\w]+)/support/tickets/$', TicketViewSet.as_view({'put': 'create', 'get':'list'}), name='tickets'),
	url(r'^projects/(?P<project_slug>[-\w]+)/support/tickets/filter/(?P<filter_name>[-\w]+)/$', SpecialFilterTicketsAPIView.as_view() , name='filter_tickets'),
	url(r'^projects/(?P<project_slug>[-\w]+)/support/tickets/(?P<ticket_slug>[-\w]+)/$', TicketDetailView.as_view({'get':'retrieve', 'put':'update'}), name='ticket_detail'),
	url(r'^projects/(?P<project_slug>[-\w]+)/support/tickets/(?P<ticket_slug>[-\w]+)/answers/create/$', TickerAnswerCreateView.as_view(), name='ticket_answer_create'),
	url(r'^projects/(?P<project_slug>[-\w]+)/support/tickets/(?P<ticket_slug>[-\w]+)/ticket-notes/create/$', TicketNoteCreateView.as_view(), name='ticket_answer_create'),
	url(r'^projects/(?P<project_slug>[-\w]+)/team/$', ProjectTeamListView.as_view(), name='project_team_list'),
	url(r'^projects/(?P<project_slug>[-\w]+)/support/templates/$', TemplatesApiView.as_view(), name='project_team_list'),


	#URL API for deed
	#This URL receives a GET parameter that specifies the name of the template to look for
	url(r'^projects/(?P<project_slug>[-\w]+)/deeds/templates/$', ReturnTemplate.as_view(), name='return_template_deeds'),
	url(r'^projects/(?P<project_slug>[-\w]+)/deeds/send-notifications/$', SendNotificationView.as_view(), name='return_template_deeds'),

	#Returns wheter a Tower has been on the onboard process for notifications
	url(r'^projects/(?P<project_slug>[-\w]+)/deeds/notifications/tracking/tower/(?P<tower_number>\d+)/$', TowerNotificationsTrackingView.as_view(), name='return_template_deeds'),
	#Retorna las unidades para renderizar la tabla por torre
	url(r'^projects/(?P<project_slug>[-\w]+)/deeds/tower/(?P<tower_number>\d+)/$', UnityDeedsNotifications.as_view(), name='unities'),

	url(r'^projects/(?P<project_slug>[-\w]+)/deeds/tower/(?P<tower_number>\d+)/unity/(?P<unity_slug>[-\w]+)/$', UnityDetailDeedNotification.as_view(), name='unity_deeds_detail'),

	url(r'^projects/(?P<project_slug>[-\w]+)/deeds/tower/(?P<tower_number>\d+)/unity/(?P<unity_slug>[-\w]+)/credit/$', CreditCertificateViewSet.as_view({'post': 'create', 'get':'retrieve', 'put':'update'}), name='credit'),

	url(r'^projects/(?P<project_slug>[-\w]+)/deeds/tower/(?P<tower_number>\d+)/unity/(?P<unity_slug>[-\w]+)/check-payments/$', UnityDeedsCheckPayments.as_view(), name='unity_check_payments'),

	url(r'^projects/(?P<project_slug>[-\w]+)/deeds/tower/(?P<tower_number>\d+)/unity/(?P<unity_slug>[-\w]+)/generate-otrosi/$', GenerateOtroSi.as_view(), name='generate_otrosi'),

	url(r'^projects/(?P<project_slug>[-\w]+)/deeds/tower/(?P<tower_number>\d+)/unity/(?P<unity_slug>[-\w]+)/bank-confirmation/$', BankConfirmationViewSet.as_view({"post":"create", "get":"retrieve", "put":"update"}), name='bank_confirmation'),

	url(r'^projects/(?P<project_slug>[-\w]+)/deeds/tower/(?P<tower_number>\d+)/unity/(?P<unity_slug>[-\w]+)/deed-order/$', DeedOrderViewSet.as_view({"post":"create", "get":"retrieve", "put":"update"}), name='deed-order'),

	url(r'^projects/(?P<project_slug>[-\w]+)/deeds/tower/(?P<tower_number>\d+)/unity/(?P<unity_slug>[-\w]+)/dates-tracking/$', DatesTracking.as_view({"post":"create", "get":"retrieve", "put":"update"}), name='dates-tracking'),


	url(r'^projects/(?P<project_slug>[-\w]+)/deeds/notifications/tracking/tower/(?P<tower_number>\d+)/download/$', DownloadNotificationDocuments.as_view(), name='download_notifications'),

	#URL API for CRM
	url(r'^projects/(?P<project_slug>[-\w]+)/crm/unities/(?P<unity_slug>[-\w]+)/notes/$', NotesViewSet.as_view({"get":"list", "post":"create"}), name='retrieve_notes_per_unity'),

	##UTILS endpoints
	url(r'^projects/(?P<project_slug>[-\w]+)/support/client/$', ClientRequestAPIView.as_view(), name='client_request'),
	url(r'^tasks/status/$', GetTaskStatus.as_view(), name='get_task_status'),
]

urlpatterns += router.urls
