from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin
from django.conf.urls import url, include
from rest_framework import routers, serializers, viewsets
from units.models import Unity

from formalizations.views import compraventa
# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^', include('accounts.urls')),
    url(r'^', include('invitations.urls')),
    url(r'^', include('persons.urls')),
    url(r'^', include('documents.urls')),
    url(r'^', include('presales.urls')),
    url(r'^', include('projects.urls')),
    url(r'^', include('support.urls')),
    url(r'^', include('units.urls')),
    url(r'^compraventa/$', compraventa, name="compraventa"),
    url('^searchableselect/', include('searchableselect.urls')),
    url(r'^api/', include("sesamo.api", namespace="api")),
    url(r'^accounts/', include('userena.urls')),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)




def trigger_error(request):
    division_by_zero = 1 / 0

urlpatterns += [
    url(r'^sentry-debug/', trigger_error),
    # ...
]