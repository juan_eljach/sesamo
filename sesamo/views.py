from celery.result import AsyncResult
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

class GetTaskStatus(APIView):
	def post(self, request, *args, **kwargs):
		task_id = request.data.get("task_id")
		if task_id is not None:
			result = AsyncResult(task_id)
			if result.ready():
				print (result.get())
				data = {"message":result.state, "filename":result.get()}
			else:
				data = {"message":result.state}
			return Response(data=data, status= status.HTTP_200_OK)
		else:
			return Response(status= status.HTTP_404_NOT_FOUND)