from django.shortcuts import redirect, get_object_or_404
from functools import wraps
from django.http import HttpResponseForbidden
from django.core.urlresolvers import reverse
from projects.models import Project

def user_belongs_to_project(view_func):
	@wraps(view_func)
	def wrapper(request, *args, **kwargs):
		user = request.user
		project_slug = kwargs.get("project_slug")
		project = get_object_or_404(Project, slug__iexact=project_slug)
		if user.user_profile in project.projectteam.users.all():
			return view_func(request, *args, **kwargs)
		else:
			return HttpResponseForbidden()
	return wrapper