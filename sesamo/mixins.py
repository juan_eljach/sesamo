from django.core import serializers
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from projects.models import Project
from persons.models import Person
from units.models import Unity

class SlugManagerMixin(object):
	def get_project(self):
		project_slug = self.kwargs.get("project_slug")
		project = get_object_or_404(Project, slug__iexact=project_slug)
		return project

	def get_person(self):
		person_slug = self.kwargs.get("person_slug")
		person = get_object_or_404(Person, slug__iexact=person_slug)
		return person

	def get_unity(self):
		unity_slug = self.kwargs.get("unity_slug")
		project_slug = self.kwargs.get("project_slug")
		unity = get_object_or_404(Unity, slug__iexact=unity_slug, project__slug__iexact=project_slug)
		return unity

class JSONResponseMixin(object):
	def render_to_json_response(self, context, **response_kwargs):
		return JsonResponse(self.get_data(context), **response_kwargs)

	def get_data(self, context):
		print (data)
		return context