import datetime
import os
import sendgrid
import urllib.request as urllib
from io import BytesIO
from sentry_sdk import capture_exception
from sentry_sdk import configure_scope
from django.conf import settings
from django.http import HttpResponse
from django.template.loader import get_template
from xhtml2pdf import pisa
from celery.decorators import task
from sendgrid.helpers.mail import Email, Content, Substitution, Mail
from django.conf import settings

def SendEmail(to_email, to_name, from_email, from_name, subject, template_name,
    html_text=None, substitutions=None):
    print ("GOT INTO TASK")
    print ("API KEY:")
    print (os.environ.get('SENDGRID_API_KEY'))
    sg = sendgrid.SendGridAPIClient(apikey=os.environ.get('SENDGRID_API_KEY'))
    from_email = Email(email=from_email, name=from_name)
    subject = subject
    to_email = Email(to_email)
    content = Content("text/html", html_text)
    mail = Mail(from_email, subject, to_email, content)
    if substitutions:
        for tag, values in substitutions.items():
            for value in values:
                mail.personalizations[0].add_substitution(Substitution(tag, value))

    mail.set_template_id(template_name)
    print (mail.get())
    try:
        response = sg.client.mail.send.post(request_body=mail.get())
    except urllib.HTTPError as e:
        print (e.read())
        exit()


def render_to_pdf(template_src, context_dict={}):
    template = get_template(template_src)
    html  = template.render(context_dict)
    result = BytesIO()
    pdf = pisa.pisaDocument(BytesIO(html.encode("ISO-8859-1")), result)
    if not pdf.err:
        return result.getvalue()
        #return HttpResponse(result.getvalue(), content_type='application/pdf')
    return None


def custom_capture_exception(err, msg=None):
    if settings.PRODUCTION:
        if msg is not None:
            with configure_scope() as scope:
                scope.set_extra("custom_message", msg)
        capture_exception(err)
    else:
        #TODO: Add loggers
        print ("Local Error: ")
        print (err)
