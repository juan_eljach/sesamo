"""
Django settings for sesamo project.

Generated by 'django-admin startproject' using Django 1.9.6.

For more information on this file, see
https://docs.djangoproject.com/en/1.9/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.9/ref/settings/
"""

import os
from os.path import abspath, basename, dirname, join, normpath
from sys import path
from django.core.urlresolvers import reverse_lazy


#TODO: Move to PRODUCTION settings file

#--------------------------------------------------------

import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration
sentry_sdk.init(
    dsn="https://836254e08163435a8654fc3a1ed1671b@sentry.io/1878485",
    integrations=[DjangoIntegration()],

    # If you wish to associate users to errors (assuming you are using
    # django.contrib.auth) you may enable sending PII data.
    send_default_pii=True
)
PRODUCTION = True

#-------------------------------------------------------


# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

DJANGO_ROOT = dirname(dirname(abspath(__file__)))

SITE_ROOT = dirname(DJANGO_ROOT)

SITE_NAME = basename(DJANGO_ROOT)

path.append(DJANGO_ROOT)

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '_ee%xe$9k=$sz-wjjojnr8i&=+##6iet*)4jq*i7d-&x==zpn&'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['*']


DJANGO_APPS = (
    'django.contrib.sites',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.humanize',
    'django.contrib.postgres',
)

LOCAL_APPS = (
    'accounts',
    'deeds',
    'alerts',
    'formalizations',
    'invitations',
    'payments',
    'persons',
    'presales',
    'projects',
    'units',
    'documents',
    'support',
    'crm',
)

THIRD_PARTY_APPS = (
    'easy_pdf',
    'easy_thumbnails',
    'formtools',
    'guardian',
    'import_export',
    'userena',
    'rest_framework',
    'corsheaders',
    'django_extensions',
    'django_filters',
    'rest_framework_filters',
    'searchableselect',
    'mathfilters',
)

CORS_ORIGIN_WHITELIST = (
   'localhost:8080',
   '127.0.0.1:8080',
   'localhost:8000',
   '127.0.0.1:8000'
)

INSTALLED_APPS = DJANGO_APPS + LOCAL_APPS + THIRD_PARTY_APPS

MIDDLEWARE_CLASSES = [
    'corsheaders.middleware.CorsMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
]

CORS_ORIGIN_WHITELIST = (
    'localhost:8080',
    '127.0.0.1:8080'
    '127.0.0.1:8080',
    'localhost:8000',
    '127.0.0.1:8000'
)

INTERNAL_IPS = (
    '127.0.0.1',
)

ROOT_URLCONF = 'sesamo.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [normpath(join(SITE_ROOT, 'templates'))],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.core.context_processors.i18n',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'sesamo.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.9/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'sesamo_db',
	'USER': 'sesamo',
	'PASSWORD': 'jessyeljacht',
	'HOST': 'localhost',
	'PORT': '5432',
    }
}


# Password validation
# https://docs.djangoproject.com/en/1.9/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

#Userena/Guardian and User Profiles Configuration

ANONYMOUS_USER_ID = -1

AUTH_PROFILE_MODULE = "accounts.UserProfile"

SITE_ID = 1

USERENA_SIGNIN_REDIRECT_URL = reverse_lazy("projects")

LOGIN_URL = reverse_lazy("login")

LOGOUT_URL = reverse_lazy("logout")

USERENA_ACTIVATION_REQUIRED = False

USERENA_SIGNIN_AFTER_SIGNUP = True

# Internationalization
# https://docs.djangoproject.com/en/1.9/topics/i18n/


#THIS OPTIONS WERE DISABLED IN ORDER TO AVOID AN ISSUE WITH DJANGO WHILE SAVING DATETIMEFIELDS. DATETIMEFIELS GET STORED WITH UTC
#BUT ARE RENDERED BY TIMEZONE, THIS WAS GIVING US PROBLEMS. IF IN THE FUTURE TIMEZONES ARE REQUIRED, PLEASE ENABLE THIS OPTIONS AND
#FIND A WAY TO MAKE IT WORK WITH DJANGO REST FRAMEWORK.
#MORE INFO HERE: http://stackoverflow.com/questions/34275588/djangorestframework-modelserializer-datetimefield-only-converting-to-current-tim

#I UPDATED DJANGORESTFRAMEWORK AFTER A NEW RELEASE WAS PUBLISHED ON GITHUB THAT SUPPOSELY RESOLVES THE ISSUE

USE_I18N = True

USE_L10N = True

#USE_TZ = True

#USE_THOUSAND_SEPARATOR = True

LANGUAGE_CODE = 'es'

TIME_ZONE = 'America/Bogota'

#MEDIA

# See: https://docs.djangoproject.com/en/dev/ref/settings/#media-root
MEDIA_ROOT = normpath(join(SITE_ROOT, 'media'))

# See: https://docs.djangoproject.com/en/dev/ref/settings/#media-url
MEDIA_URL = '/media/'



# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.9/howto/static-files/


STATICFILES_DIRS = (
    normpath(join(SITE_ROOT, 'static')),
    )

STATIC_ROOT = normpath(join(SITE_ROOT, 'assets'))

STATIC_URL = '/static/'

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'dajaxice.finders.DajaxiceFinder',
)

#Authentication Backends

AUTHENTICATION_BACKENDS = (
    'userena.backends.UserenaAuthenticationBackend',
    'guardian.backends.ObjectPermissionBackend',
    'django.contrib.auth.backends.ModelBackend',
)


#TRANSACTIONAL EMAIL SETTINGS

SENDGRID_API_KEY = "gGxk6n5sSCicUN6kjXaodw"

DEFAULT_EMAIL_SENDER = "equipo@sesamo.com"

DEFAULT_NAME_SENDER = "Equipo de PCG Constructora"

INVITATION_TO_PROJECT_TEMPLATE_ID = "4189dde9-9e4d-423e-a258-8ed8c5ad5c2c"

QUOTATION_TEMPLATE_ID = "fea2b670-9acf-4d45-9ed9-ffeff223323e"

DEEDS_NOTIFICATION_TEMPLATE_ID = "3787213c-8cd7-4f96-8550-f816621f6fde"

# CELERY STUFF
BROKER_URL = 'redis://localhost:6379'
CELERY_ACCEPT_CONTENT = ['application/json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
CELERY_RESULT_BACKEND = "redis"
CELERY_TIMEZONE = 'America/Bogota'


#REST API CONFIGURATION
REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.BasicAuthentication',
    ),
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.IsAuthenticated'
    ],
    'DEFAULT_FILTER_BACKENDS': (
        'django_filters.rest_framework.DjangoFilterBackend',
        'rest_framework.filters.OrderingFilter'
    )
}


#PROJECT CONF

DAYS_INTERVAL_FOR_UNITY_VIRTUALBOOKING = 8

DAYS_INTERVAL_FOR_UNITY_ALERT = 5

#AWS
#EMAIL_USE_TLS = True
#EMAIL_HOST = 'email-smtp.us-west-2.amazonaws.com'
#EMAIL_PORT = 587
#EMAIL_HOST_USER = 'AKIA6BOS3UEF5EMOPHGP'
#EMAIL_HOST_PASSWORD = 'BEfRmpiej4an5fBmzXQwvtLxKdKp6t8f0n4FuvcnnQg4'

#SENDGRID
EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.sendgrid.net'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'apikey'
EMAIL_HOST_PASSWORD = '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'


#TINYMCE_JS_URL = '/static/tinymce/js/tinymce/tinymce.min.js'

#TINYMCE_JS_ROOT = normpath(join(SITE_ROOT, 'static/tinymce/js'))

#TINYMCE_COMPRESSOR = True
