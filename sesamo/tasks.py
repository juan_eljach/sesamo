import datetime
import os
import sendgrid
import urllib.request as urllib
from celery.decorators import task
from sendgrid.helpers.mail import Email, Content, Substitution, Mail
from django.conf import settings

@task(name="send_email")
def SendEmail(to_email, to_name, from_email, from_name, subject, template_name,
    html_text=None, substitutions=None, text=None):
    sg = sendgrid.SendGridAPIClient(apikey=os.environ.get('SENDGRID_API_KEY'))
    from_email = Email(email=from_email, name=from_name)
    subject = subject
    to_email = Email(to_email)
    content = Content("text/html", html_text)
    mail = Mail(from_email, subject, to_email, content)
    if substitutions:
        for tag, values in substitutions.items():
            for value in values:
                mail.personalizations[0].add_substitution(Substitution(tag, value))

    mail.set_template_id(template_name)
    print ("REQUEST BODY: ")
    print (mail.get())
    try:
        response = sg.client.mail.send.post(request_body=mail.get())
        if response:
            return True
    except urllib.HTTPError as e:
        print (e.read())
        exit()
