from django.conf import settings
from django.db import models
from persons.models import Person
from units.models import Unity

alert_types = (
	("unity_legalized", "Se ha legalizado esta unidad"),
	("virtualbooking_closed","Se ha liberado esta unidad de la separación tentativa"),
	("legalization_closed","Se ha liberado esta unidad de la legalización"),
)

class Alert(models.Model):
	alert_type = models.CharField(max_length=30, choices=alert_types)
	created_for = models.ForeignKey(settings.AUTH_PROFILE_MODULE, blank=True, null=True)
	person = models.ForeignKey(Person)
	unity = models.ForeignKey(Unity)
	timestamp = models.DateTimeField(auto_now_add=True)