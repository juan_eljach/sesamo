from rest_framework import serializers
from .models import Alert

class AlertSerializer(serializers.ModelSerializer):
    class Meta:
        model = Alert
        fields = ( 
            'alert_type', 
            'person', 
            'unity',
            'timestamp', 
        )