from django.shortcuts import render
from rest_framework import generics
from rest_framework import permissions
from .models import Alert
from .serializers import AlertSerializer

class AlertListAPIView(generics.ListAPIView):
    model_class = Alert
    serializer_class = AlertSerializer
    permission_classes = (permissions.DjangoModelPermissions,)

    def get_queryset(self):
        print (self.request)
        user_profile = self.request.user.user_profile
        #Agregar a la alerta el proyecto al que pertenece
        return self.model_class.objects.filter(created_for=user_profile)