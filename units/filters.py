from .models import Unity
import rest_framework_filters as filters


class UnityFilter(filters.FilterSet):
    paymentagreement__isnull = filters.BooleanFilter(name="paymentagreement", lookup_expr="isnull")

    class Meta:
        model = Unity
        fields = ("status", "paymentagreement__isnull")
        #fields = ("status",)
