from bs4 import BeautifulSoup
from django import template
from django.shortcuts import get_object_or_404
from units.models import UnityType
register = template.Library()

@register.simple_tag(takes_context=True)
def extract_unitytype_display(context, field):
	soup = BeautifulSoup(field)
	try:
		parsed_id = soup.find("span").text
	except:
		parsed_id = soup.find("ins").text
	unitytype = get_object_or_404(UnityType, id=parsed_id)
	unitytype_name = "{}-{}".format(unitytype.type_name, unitytype.unitytype_name)
	return unitytype_name
