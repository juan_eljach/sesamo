from import_export.admin import ImportExportModelAdmin
from .views import UnityResource
from django.contrib import admin
from .models import Unity, UnityType

class UnityResourceAdmin(ImportExportModelAdmin):
	resource_class = UnityResource

class UnityAdmin(admin.ModelAdmin):
	list_filter = ("project",)
	exclude = ("timestamp_for_status", "slug")

#admin.site.register(Unity, UnityAdmin)
admin.site.register(Unity, UnityResourceAdmin)
admin.site.register(UnityType)
