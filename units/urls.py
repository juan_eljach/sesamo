from django.conf.urls import patterns, url
from .views import UnityTypeCreateView, UnityImport, UnityExport, UnityProcessImport, UnityListView, UnityDetailUpdateAPIView

urlpatterns = patterns('',
	url(r'^projects/(?P<project_slug>[-\w]+)/unitytypes/create$', UnityTypeCreateView.as_view(), name='unity_types_create'),
#	url(r'^projects/(?P<project_slug>[-\w]+)/unities/$', UnityListView.as_view(), name='unities_list'),
#	url(r'^projects/(?P<project_slug>[-\w]+)/unities/(?P<slug>[-\w]+)/$', UnityDetailUpdateAPIView.as_view(), name='unity_detail'),
    url(r'^projects/(?P<project_slug>[-\w]+)/import/unities/$', UnityImport.as_view(), name='unities_import'),
    url(r'^projects/(?P<project_slug>[-\w]+)/export/unities/$', UnityExport.as_view(), name='unities_export'),
    url(r'^projects/(?P<project_slug>[-\w]+)/process_import/unities/$', UnityProcessImport.as_view(), name='unities_process_import'),
)