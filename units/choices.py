status_choices = (
	("available", "Disponible"),
	("virtually_booked", "Separado Tentativamente"),
	("booked_not_formalized", "Separado no formalizado"),
	("legalized", "Legalizado"),
	("sold", "Vendido"),
)