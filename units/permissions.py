
from rest_framework import permissions
from rest_framework import exceptions

class CanUpdateUnity(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        permitted_status = ['available', 'virtually_booked']
        if request.user.user_profile.user_type == 'vendedor':
            new_status = request.data.get('status')
            status_in_operation = [obj.status, new_status]
            if not all([status in permitted_status for status in status_in_operation]):
                raise exceptions.PermissionDenied
        return True