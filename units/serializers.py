from rest_framework import serializers
from persons.serializers import PersonSmallSerializer
from .models import Unity

class UnitySerializer(serializers.ModelSerializer):
    bathrooms = serializers.SerializerMethodField()
    bedrooms = serializers.SerializerMethodField()
    built_area = serializers.SerializerMethodField()
    private_area = serializers.SerializerMethodField()
    interested_persons = serializers.SerializerMethodField(read_only=True)
    paymentagreement = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = Unity
        fields = (
            'id',
            'name',
            'price',
            'status',
            'timestamp_for_status',
            'delivery_date',
            'bathrooms',
            'bedrooms',
            'built_area',
            'private_area',
            'interested_persons',
            'paymentagreement',
            'slug')

    def get_bathrooms(self, obj):
    	return obj.unity_type.bathrooms

    def get_bedrooms(self, obj):
    	return obj.unity_type.bedrooms

    def get_built_area(self, obj):
    	return obj.unity_type.built_area

    def get_private_area(self, obj):
    	return obj.unity_type.private_area

    def get_interested_persons(self, obj):
        persons_queryset = [vb.person for vb in obj.virtualbooking_set.filter(closed=False).order_by("id")]
        return PersonSmallSerializer(persons_queryset, many=True).data


#class UnityUpdateSerializer(serializers.ModelSerializer):
#    class Meta:
#        model = Unity
#        fields = ("id", "price", "status", "delivery_date")

#    def validate_status(self, value):
#        request_user_type = self.context["request"].user.user_profile.user_type
#        if request_user_type == 'administrador_preventa' or request_user_type == "gerente":
#            return value
#        else:
#            raise serializers.ValidationError("No tiene permisos para ejecutar esta accion")

class UnitySmallSerializer(serializers.ModelSerializer):
    class Meta:
        model = Unity
        fields = ("id", "name", "price", "status")
