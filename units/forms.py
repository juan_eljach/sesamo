from django import forms
from .models import UnityType

LABEL_CHOICE = [
		("label", "Selecciona el tipo de unidad")
	]

class UnityTypeForm(forms.ModelForm):
	class Meta:
		model = UnityType
		fields = ["unitytype_name", "type_name", "private_area", "built_area", "bathrooms", "bedrooms"]

	def __init__(self, *args, **kwargs):
		super(UnityTypeForm, self).__init__(*args, **kwargs)
		self.fields["unitytype_name"].label = "Nombre del tipo. Ej: Tipo 1"
		self.fields["type_name"].label = "Tipo de unidad"
		self.fields["private_area"].label = "Area privada"
		self.fields["built_area"].label = "Area Construida"
		self.fields["type_name"].widget.attrs.update({'class' : 'browser-default'})
		self.fields['type_name'].choices = self.fields["type_name"].choices[1:]
		self.fields["unitytype_name"].widget.attrs.update({'class' : 'browser-default'})
		self.fields['unitytype_name'].choices = self.fields["unitytype_name"].choices[1:]