import csv
import os
import tempfile
from datetime import datetime
import django_filters
from django.conf import settings
from django.db.models import Q
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.template.loader import render_to_string
from django.template.response import TemplateResponse
from django.utils.decorators import method_decorator
from django.utils.encoding import force_text
from django.views.generic import View, TemplateView, CreateView, ListView
from projects.models import Project
from import_export.forms import ImportForm
from import_export.forms import ConfirmImportForm
from import_export.formats import base_formats
from import_export.resources import modelresource_factory
from import_export import resources
from rest_framework import generics, filters, permissions, exceptions, viewsets
from rest_framework.response import Response
from sesamo.mixins import SlugManagerMixin
#from formalizations.models import UnityAlert
from .choices import status_choices
from .forms import UnityTypeForm
from .models import Unity, UnityType
from .serializers import UnitySerializer#, UnityUpdateSerializer
from .permissions import CanUpdateUnity
from .filters import UnityFilter
#START OF API VIEWS

@method_decorator(login_required, name='dispatch')
class UnityListAPIView(SlugManagerMixin, generics.ListAPIView):
    model_class = Unity
    serializer_class = UnitySerializer
    permission_classes = (permissions.IsAuthenticated,)

    def get_queryset(self):
        project = self.get_project()
        return self.model_class.objects.filter(project=project)

@method_decorator(login_required, name='dispatch')
class AvailableUnitiesListAPIView(SlugManagerMixin, generics.ListAPIView):
    model_class = Unity
    serializer_class = UnitySerializer
    permission_classes = (permissions.IsAuthenticated,)
    email_template = 'units/available_units_email.html'

    def send_email(self):
        user = self.request.user
        if user.email is not None:
            to_list = [user.email]
            from_email = settings.DEFAULT_EMAIL_SENDER
            from_name = settings.DEFAULT_NAME_SENDER
            subject = "Listado de unidades disponibles en el proyecto {}".format(self.project)
            data = {'available_units': self.q}
            body = render_to_string(self.email_template, data)
            email = EmailMessage(subject, body, from_email, to_list)
            email.send(fail_silently=False)

    def get_queryset(self):
        self.project = self.get_project()
        self.q = self.model_class.objects.filter(project=self.project).exclude(Q(status="sold") | Q(status="legalized"))
        if hasattr(self.kwargs, 'email') and self.kwargs.get('email') == True:
            try:
                self.send_email()
                return True
            except:
                print ("Failed")
        return self.q

@method_decorator(login_required, name='dispatch')
class UnityDetailUpdateAPIView(generics.RetrieveUpdateAPIView):
    model_class = Unity
    lookup_field = "id"
    queryset = Unity.objects.all()
    serializer_class = UnitySerializer
    permission_classes = (permissions.IsAuthenticated, CanUpdateUnity)


#class UnityDetailUpdateAPIView(SlugManagerMixin, viewsets.ModelViewSet):
#    model_class = Unity
#    queryset = Unity.objects.all()
#    serializer_class = UnitySerializer
#    create_update_serializer = UnityUpdateSerializer
#    permission_classes = (permissions.IsAuthenticated,)
#    lookup_field = "id"
#    lookup_url_kwarg = "id"

#    def get_serializer_class(self):
#        print ("GOT INTO SERIALIZER CLASS")
#        if self.action == 'retrieve':
#            print ("GOT INTO ACTION RETIREVE")
#            return self.detail_serializer
#        elif self.action == 'update':
#            print ("GOT INTO ACTION UPDATE")
#            return self.create_update_serializer

#    def update(self, request, *args, **kwargs):
#        request_usertype = request.user.user_profile.user_type
#        if request_usertype == "administrador_preventas" or request_usertype == "gerente":
#            serializer = self.serializer_class(data=request.data)
#            if serializer.is_valid():
#                serializer.save()
#                return Response(serializer.data)
#        else:
#            raise exceptions.PermissionDenied()

#class UnityAlertListAPIView(SlugManagerMixin, generics.ListAPIView):
#    model_class = UnityAlert
#    serializer_class = UnitySerializer
#    permission_classes = (permissions.DjangoModelPermissions,)

#    def get_queryset(self):
#        print (self.request.user)
#        user_profile = self.request.user.user_profile
#        return self.model_class.objects.filter(created_for=user_profile)
#END OF API VIEWS


#FUNCTIONS TO IMPORT-EXPORT UNITIES
class UnityResource(resources.ModelResource):

    def after_export(self, queryset, data, *args, **kwargs):
        """
        Override to add additional logic. Does nothing by default.
        """
        dataset = data
        i = 0
        last = dataset.height -1

        while i <= last:
            unitytype_id = dataset["unity_type"][0]
            try:
                unity_type = UnityType.objects.get(id=unitytype_id)
            except Exception as err:
                raise Exception("No se encuentra el tipo de unidad")

            unitytype_full_name = "{0}-{1}".format(unity_type.type_name, unity_type.unitytype_name)
            try:
                dataset.rpush(
                    (
                        dataset.get_col(0)[0],
                        dataset.get_col(1)[0],
                        dataset.get_col(2)[0],
                        dataset.get_col(3)[0],
                        unitytype_full_name,
                        dataset.get_col(5)[0],
                        dataset.get_col(6)[0],
                        dataset.get_col(7)[0],
                        dataset.get_col(8)[0],
                        dataset.get_col(9)[0],
                    )

                )
            except:
                raise Exception("La tabla no se pudo exportar")
            dataset.lpop()
            i = i + 1
        del dataset["project"]

    def before_import(self, dataset, dry_run, *args, **kwargs):
        project = kwargs['project']
        i = 0
        last = dataset.height - 1

        while i <= last:
            # Check if the unity type exists in DB
            unitytype = dataset["unity_type"][0].split("-")
            type_name = unitytype[0].lower()
            unitytype_name = unitytype[1].lower()
            try:
                unity_type = UnityType.objects.get(unitytype_name=unitytype_name, type_name=type_name, project=project)
            except UnityType.DoesNotExist:
                raise Exception("No se encuentra ese tipo de unidad")
            except UnityType.MultipleObjectsReturned:
                pass
            existing_unity_type = unity_type.id


            columns_indexes = ["dataset.get_col({0})".format(x) for x in range(0, len(dataset.headers))]

            #try:
                #lols = [(lambda x: dataset.get_col(x))(x) for x in range(len(dataset.headers))]
            #    print (lols)
            #except Exception as err:
            #    print (err)
            #    exit()

            #unitytype_index = dataset.headers.index("unity_type")
            #del(columns_indexes[unitytype_index])
            #columns_indexes.insert(unitytype_index, existing_unity_type)
            #print (columns_indexes)
            try:
                dataset.rpush(
                    tuple([existing_unity_type if dataset.headers.index("unity_type") == x else dataset.get_col(x)[0] for x in range(0, len(dataset.headers))])
                )
            except Exception as err:
                print (err)
                print ("MIERDERO")
            dataset.lpop()
            i = i + 1


        try:
            dataset["id"]
        except:
            try:
                dataset.append_col([None for row in range(len(dataset))], header='id')
            except:
                print ("Error agregando columna de ID al dataset")

        try:
            dataset.append_col([project.id for row in range(len(dataset))], header='project')
        except Exception as err:
            print (err)

    class Meta:
        model = Unity
        fields = (
            'id',
            'name',
            'nomenclature',
            'real_estate_registration',
            'unity_type',
            'coefficient',
            'price',
            'delivery_date',
            'project',
            'status'
        )
        export_order = (
            'id',
            'name',
            'nomenclature',
            'real_estate_registration',
            'unity_type',
            'coefficient',
            'price',
            'delivery_date',
        )

class UnityExport(View):
    model = Unity
    from_encoding = "utf-8"
    resource_class = UnityResource

    def get_resource_class(self):
        if not self.resource_class:
            return modelresource_factory(self.model)
        else:
            return self.resource_class

    def get_export_resource_class(self):
        """
        Returns ResourceClass to use for export.
        """
        return self.get_resource_class()

        resource = self.get_export_resource_class()()

    def get(self, *args, **kwargs ):
        project_slug = self.kwargs["project_slug"]
        project = get_object_or_404(Project, slug__iexact=project_slug)
        resource = self.get_export_resource_class()()
        result = resource.export(queryset=Unity.objects.filter(project=project).order_by("id"))
        response = HttpResponse(result.csv, content_type="csv")
        response['Content-Disposition'] = 'attachment; filename={0}-unidades.csv'.format(project.name)
        return response


@method_decorator(login_required, name='dispatch')
class UnityImport(View):
    model = Unity
    from_encoding = "utf-8"

    #: import / export formats
    DEFAULT_FORMATS = (
        base_formats.CSV,
        base_formats.XLS,
    )
    formats = DEFAULT_FORMATS
    #: template for import view
    import_template_name = 'units/import_units.html'
    resource_class = UnityResource

    def get_import_formats(self):
        """
        Returns available import formats.
        """
        return [f for f in self.formats if f().can_import()]

    def get_resource_class(self):
        if not self.resource_class:
            return modelresource_factory(self.model)
        else:
            return self.resource_class

    def get_import_resource_class(self):
        """
        Returns ResourceClass to use for import.
        """
        return self.get_resource_class()

    def get(self, *args, **kwargs ):
        '''
        Perform a dry_run of the import to make sure the import will not
        result in errors.  If there where no error, save the user
        uploaded file to a local temp file that will be used by
        'process_import' for the actual import.
        '''
        resource = self.get_import_resource_class()()
        project_slug = self.kwargs['project_slug']
        project = get_object_or_404(Project, slug__iexact=project_slug)

        context = {}
        context['project'] = project

        import_formats = self.get_import_formats()
        form = ImportForm(import_formats,
                          self.request.POST or None,
                          self.request.FILES or None)

        if self.request.POST and form.is_valid():
            input_format = import_formats[
                int(form.cleaned_data['input_format'])
            ]()
            import_file = form.cleaned_data['import_file']
            # first always write the uploaded file to disk as it may be a
            # memory file or else based on settings upload handlers
            with tempfile.NamedTemporaryFile(delete=False) as uploaded_file:
                for chunk in import_file.chunks():
                    uploaded_file.write(chunk)

            # then read the file, using the proper format-specific mode
            with open(uploaded_file.name,
                      input_format.get_read_mode()) as uploaded_import_file:
                # warning, big files may exceed memory
                data = uploaded_import_file.read()
                if not input_format.is_binary() and self.from_encoding:
                    data = force_text(data, self.from_encoding)
                dataset = input_format.create_dataset(data)
                result = resource.import_data(dataset, dry_run=True,
                                              raise_errors=True, project=project)

            context['result'] = result

            if not result.has_errors():
                context['confirm_form'] = ConfirmImportForm(initial={
                    'import_file_name': os.path.basename(uploaded_file.name),
                    'original_file_name': uploaded_file.name,
                    'input_format': form.cleaned_data['input_format'],
                })
            else:
                print ("RESULT: {}".format(result))

        context['form'] = form
        context['opts'] = self.model._meta
        context['fields'] = [f.column_name for f in resource.get_fields()]
        context.update(self.kwargs)

        return TemplateResponse(self.request, [self.import_template_name], context)


    def post(self, *args, **kwargs ):
        '''
        Perform a dry_run of the import to make sure the import will not
        result in errors.  If there where no error, save the user
        uploaded file to a local temp file that will be used by
        'process_import' for the actual import.
        '''
        resource = self.get_import_resource_class()()#
        project_slug = self.kwargs['project_slug']
        project = get_object_or_404(Project, slug__iexact=project_slug)
        print ("INTO POST REQUEST")
        print ("LEN OF UNITIES: {}".format(project.unity_set.all().count()))
        context = {}
        context['project'] = project

        import_formats = self.get_import_formats()
        form = ImportForm(import_formats,
                          self.request.POST or None,
                          self.request.FILES or None)


        if self.request.POST and form.is_valid():
            input_format = import_formats[
                int(form.cleaned_data['input_format'])
            ]()
            import_file = form.cleaned_data['import_file']
            # first always write the uploaded file to disk as it may be a
            # memory file or else based on settings upload handlers
            with tempfile.NamedTemporaryFile(delete=False) as uploaded_file:
                for chunk in import_file.chunks():
                    uploaded_file.write(chunk)

            # then read the file, using the proper format-specific mode
            with open(uploaded_file.name,
                      input_format.get_read_mode()) as uploaded_import_file:
                # warning, big files may exceed memory
                data = uploaded_import_file.read()
                if not input_format.is_binary() and self.from_encoding:
                    data = force_text(data, self.from_encoding)
                dataset = input_format.create_dataset(data)
                result = resource.import_data(dataset, dry_run=True,
                                              raise_errors=True, project=project)
                print ("LEN OF UNITIES 2: {}".format(project.unity_set.all().count()))

            context['result'] = result

            if not result.has_errors():
                context['confirm_form'] = ConfirmImportForm(initial={
                    'import_file_name': os.path.basename(uploaded_file.name),
                    'original_file_name': uploaded_file.name,
                    'input_format': form.cleaned_data['input_format'],
                })
            else:
                print ("RESULT: {}".format(result))

        context['form'] = form
        context['opts'] = self.model._meta
        context['fields'] = [f.column_name for f in resource.get_fields()]
        context.update(self.kwargs)
        return TemplateResponse(self.request, [self.import_template_name], context)

class UnityProcessImport(View):
    model = Unity
    from_encoding = "utf-8"

    #: import / export formats
    DEFAULT_FORMATS = (
        base_formats.CSV,
        base_formats.XLS,
        base_formats.TSV,
        base_formats.ODS,
        base_formats.JSON,
        base_formats.YAML,
        base_formats.HTML,
    )
    formats = DEFAULT_FORMATS
    #: template for import view
    import_template_name = 'units/import_units.html'
    resource_class = UnityResource

    def get_import_formats(self):
        """
        Returns available import formats.
        """
        return [f for f in self.formats if f().can_import()]

    def get_resource_class(self):
        if not self.resource_class:
            return modelresource_factory(self.model)
        else:
            return self.resource_class

    def get_import_resource_class(self):
        """
        Returns ResourceClass to use for import.
        """
        return self.get_resource_class()

    def post(self, *args, **kwargs):
        '''
        Perform the actual import action (after the user has confirmed he
    wishes to import)
        '''
        opts = self.model._meta
        resource = self.get_import_resource_class()()
        project_slug = self.kwargs['project_slug']
        project = get_object_or_404(Project, slug__iexact=project_slug)

        confirm_form = ConfirmImportForm(self.request.POST)
        if confirm_form.is_valid():
            import_formats = self.get_import_formats()
            input_format = import_formats[
                int(confirm_form.cleaned_data['input_format'])
            ]()
            import_file_name = os.path.join(
                tempfile.gettempdir(),
                confirm_form.cleaned_data['import_file_name']
            )
            import_file = open(import_file_name, input_format.get_read_mode())
            data = import_file.read()
            if not input_format.is_binary() and self.from_encoding:
                data = force_text(data, self.from_encoding)
            dataset = input_format.create_dataset(data)
            result = resource.import_data(dataset, dry_run=False,
                                 raise_errors=True, project=project)
            # Add imported objects to LogEntry
            #ADDITION = 1
            #CHANGE = 2
            #DELETION = 3
            #logentry_map = {
            #    RowResult.IMPORT_TYPE_NEW: ADDITION,
            #    RowResult.IMPORT_TYPE_UPDATE: CHANGE,
            #    RowResult.IMPORT_TYPE_DELETE: DELETION,
            #}
            #content_type_id=ContentType.objects.get_for_model(self.model).pk
            #'''
            #for row in result:
            #    LogEntry.objects.log_action(
            #        user_id=request.user.pk,
            #        content_type_id=content_type_id,
            #        object_id=row.object_id,
            #        object_repr=row.object_repr,
            #        action_flag=logentry_map[row.import_type],
            #        change_message="%s through import_export" % row.import_type,
            #    )
            #'''
            import_file.close()

            redirect_kwargs = {'project_slug': project_slug}

            return redirect(reverse('presale_splash', kwargs=redirect_kwargs))
        else:
            print (confirm_form.errors)


class AjaxableResponseMixin(object):
    """
    Mixin to add AJAX support to a form.
    Must be used with an object-based FormView (e.g. CreateView)
    """
    def form_invalid(self, form):
        try:
            response = super(AjaxableResponseMixin, self).form_invalid(form)
        except Exception as err:
            print (err)
        if self.request.is_ajax():
            print (form.errors)
            return JsonResponse(form.errors, status=400)
        else:
            return response

    def form_valid(self, form):
        # We make sure to call the parent's form_valid() method because
        # it might do some processing (in the case of CreateView, it will
        # call form.save() for example).
        project = get_object_or_404(Project, slug__iexact=self.kwargs.get("project_slug"))
        self.object = form.save(commit=False)
        self.object.project = project
        self.object.save()
        if self.request.is_ajax():
            data = {
                'pk': self.object.pk,
                'type_name': self.object.get_type_name_display(),
                'unitytype_name': self.object.get_unitytype_name_display(),
                'private_area': self.object.private_area,
                'built_area': self.object.built_area,
                'bathrooms': self.object.bathrooms,
                'bedrooms': self.object.bedrooms
            }
            return JsonResponse(data)
        else:
            response = super(AjaxableResponseMixin, self).form_valid(form)
            return response

@method_decorator(login_required, name='dispatch')
class UnityTypeCreateView(AjaxableResponseMixin, CreateView):
    form_class = UnityTypeForm
    model = UnityType
    template_name = "units/unitytype_form.html"

    def get_context_data(self, *args, **kwargs):
        context = super(UnityTypeCreateView, self).get_context_data(*args, **kwargs)
        project = get_object_or_404(Project, slug__iexact=self.kwargs.get("project_slug"))
        context["project"] = project
        try:
            unitytypes = self.model.objects.filter(project=project).order_by("id")
        except:
            unitytypes = None
        context["unitytypes"] = unitytypes
        context.update(self.kwargs)
        return context

@method_decorator(login_required, name='dispatch')
class UnityListView(SlugManagerMixin, ListView):
    model = Unity
    context_object_name = "unities"

    def get_queryset(self):
        project = self.get_project()
        return Unity.objects.filter(project=project)

@method_decorator(login_required, name='dispatch')
class UnitySearchView(SlugManagerMixin, generics.ListAPIView):
    queryset = Unity.objects.all()
    serializer_class = UnitySerializer
    filter_backends = (filters.SearchFilter, django_filters.rest_framework.DjangoFilterBackend)
    filter_class = UnityFilter
    search_fields = ("^name",)

    def get_queryset(self):
        project = self.get_project()
        queryset = Unity.objects.filter(project=project).order_by("id")
        valid_status = [x[0] for x in status_choices]
        if hasattr(self.kwargs, 'exclude') and self.kwargs.get("exclude") in valid_status:
            queryset = queryset.exclude(status=self.kwargs.get("exclude"))
        if hasattr(self.kwargs, 'status') and self.kwargs.get("status") in valid_status:
            queryset = queryset.filter(status=self.kwargs.get("status"))
        return queryset
