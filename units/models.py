from __future__ import unicode_literals
import datetime
from django.conf import settings
from django.core.validators import RegexValidator
from django.db import models
from django.template import defaultfilters
#from alerts.models import Alert
from projects.models import Project
from .choices import status_choices

type_name_choices = (
	("apartamento", "Apartamento"),
	("casa", "Casa"),
	("local", "Local"),
	("parqueadero", "Parqueadero"),
)

unitytype_choices = (
	("tipo1","Tipo 1"),
	("tipo2","Tipo 2"),
	("tipo3","Tipo 3"),
	("tipo4","Tipo 4"),
	("tipo5","Tipo 5"),
	("tipo6","Tipo 6"),
)


class UnityType(models.Model):
	built_area = models.FloatField()
	bathrooms = models.IntegerField()
	bedrooms = models.IntegerField()
	project = models.ForeignKey(Project)
	unitytype_name = models.CharField(max_length=30, choices=unitytype_choices)
	type_name = models.CharField(max_length=30, choices=type_name_choices, blank=False)
	private_area = models.FloatField()

	def save(self, *args, **kwargs):
		self.type_name = self.type_name.lower()
		super(UnityType, self).save(*args, **kwargs)

	def __str__(self, *args, **kwargs):
		return self.get_type_name_display() + self.get_unitytype_name_display()


class Unity(models.Model):
	name = models.CharField(max_length=30, blank=False, null=False)
	nomenclature = models.CharField(max_length=100)
	real_estate_registration = models.CharField(max_length=30)
	project = models.ForeignKey(Project)
	unity_type = models.ForeignKey(UnityType)
	coefficient = models.FloatField()
	price = models.IntegerField()
	delivery_date = models.DateField()
	status = models.CharField(max_length=50, choices=status_choices, default="available")
	timestamp_for_status = models.DateField(auto_now=False, auto_now_add=False, null=True, blank=True)
	slug = models.SlugField()

	def create_alert(self, alert_type, unity, person, created_for):
		from alerts.models import Alert
		Alert.objects.create(
			alert_type=alert_type,
			unity=unity,
			person=person,
			created_for=created_for,
		)


	def save(self, *args, **kwargs):
		self.slug = defaultfilters.slugify(self.name)
		if self.pk is not None:
			try:
				original_instance = Unity.objects.get(pk=self.pk)
				if original_instance.status != self.status:
					self.timestamp_for_status = datetime.date.today()
			except Exception as err:
				print ("Error: {}".format(err))
			super(Unity, self).save(*args, **kwargs)
		else:
			super(Unity, self).save(*args, **kwargs)
			#ESTA LÓGICA DEBE DESARROLLARSE CUANDO TENGAMOS LOS ACUERDOS DE PAGO FUNCIONANDO
			# :3
			#if original_instance.status == "legalized" and self.status == "available":
			#	self.create_alert(
			#		alert_type="legalization_closed",
			#		unity=original_instance,
			#		person=original_instance.person_set.all()[0],
			#		created_for=original_instance.person_set.all()[0].assigned_to,
			#	)
	#	else:
	#		super(Unity, self).save(*args, **kwargs)
	#		from crm.models import UnityProfile
	#		obj = UnityProfile.objects.create(unity=self)

	def __str__(self, *args, **kwargs):
		return self.name

#class VirtualBooking(models.Model):
#	person = models.ForeignKey(Person)
#	sales_person = models.ForeignKey(settings.AUTH_PROFILE_MODULE, blank=True, null=True)
#	unity = models.ForeignKey(Unity)
#	timestamp = models.DateField(auto_now_add=True)

#	def save(self):
#		self.unity.status = "virtually_booked"
#		self.unity.save()
#		super(VirtualBooking, self).save()
