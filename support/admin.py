from django.contrib import admin
from .models import Ticket, TicketNote, Answer, Template

class TicketAdmin(admin.ModelAdmin):
	exclude = ("slug",)

admin.site.register(Ticket, TicketAdmin)
admin.site.register(Answer)
admin.site.register(Template)
# Register your models here.
