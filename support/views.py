from django_filters import rest_framework as filters
from django.shortcuts import render, get_object_or_404
from django.views.generic import TemplateView
from formalizations.models import PaymentAgreement
from rest_framework import generics
from rest_framework.mixins import ListModelMixin
from rest_framework.parsers import FileUploadParser, JSONParser, FormParser, MultiPartParser
from rest_framework.response import Response
from rest_framework.views import APIView
from sesamo.mixins import SlugManagerMixin
from persons.models import Person
from units.models import Unity
from units.serializers import UnitySerializer
from .models import Answer, Ticket, TicketNote, Template
from .permissions import IsAdminOnTicket, IsOnTicketOrForbidden
from .serializers import TicketListSerializer, TicketCreateUpdateSerializer, TicketNoteSerializer, TicketAnswerCreateSerializer, TicketUpdateSerializer, TemplateSerializer
from rest_framework import viewsets

#class TicketCreateView(generics.CreateAPIView):
#	model_class = Ticket
#	serializer_class = TicketSerializer
#	queryset = Ticket.objects.all()


class TicketFilter(filters.FilterSet):
    class Meta:
        model = Ticket
        fields = ['status', 'priority', 'ticket_type']

class TicketViewSet(SlugManagerMixin, viewsets.ModelViewSet):
    model_class = Ticket
    list_serializer = TicketListSerializer
    create_serializer = TicketCreateUpdateSerializer
    parser_classes = (FormParser, MultiPartParser)

    def perform_create(self, obj):
        try:
            file_obj = self.validated_data['attached_file']
        except:
            print ("No file")
        project = self.get_project()
        print (self.request)
        #obj.attached_file = self.request.FILES.get('attached_file')
        obj.save(project=project)

    def get_queryset(self):
        project = self.get_project()
        return self.model_class.objects.filter(unity__project=project)

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        filtered = TicketFilter(request.GET, queryset=queryset)
        serializer = self.list_serializer(filtered.qs, many=True, context={"request": request})
        return Response(serializer.data)

    def get_serializer_class(self):
        if self.action == 'create':
            return self.create_serializer
        return serializers.Default


class TicketDetailView(SlugManagerMixin, viewsets.ModelViewSet):
	model_class = Ticket
	queryset = Ticket.objects.all()
	detail_serializer = TicketListSerializer
	create_update_serializer = TicketUpdateSerializer
	lookup_field = "slug"
	lookup_url_kwarg = "ticket_slug"

	def get_serializer_class(self):
		if self.action == 'retrieve':
			return self.detail_serializer
		elif self.action == 'create' or self.action == 'update':
			return self.create_update_serializer
		return serializers.Default

	def get_permissions(self):
		if self.request.method == 'GET':
			self.permission_classes = [IsOnTicketOrForbidden,]
		elif self.request.method == 'PUT':
			self.permission_classes = [IsOnTicketOrForbidden, IsAdminOnTicket]
		return super(TicketDetailView, self).get_permissions()

class TicketNoteCreateView(generics.CreateAPIView):
	model_class = TicketNote
	serializer_class = TicketNoteSerializer
	queryset = TicketNote.objects.all()

	def perform_create(self, serializer):
		ticket_slug = self.kwargs.get("ticket_slug")
		ticket_object = get_object_or_404(Ticket, slug__iexact=ticket_slug)
		serializer.save(written_by=self.request.user.user_profile, ticket=ticket_object)

class TickerAnswerCreateView(generics.CreateAPIView):
	model_class = Answer
	serializer_class = TicketAnswerCreateSerializer
	queryset = Answer.objects.all()

	def perform_create(self, serializer):
		ticket_slug = self.kwargs.get("ticket_slug")
		ticket_object = get_object_or_404(Ticket, slug__iexact=ticket_slug)
		serializer.save(written_by=self.request.user.user_profile, ticket=ticket_object)


class SpecialFilterTicketsAPIView(SlugManagerMixin, generics.ListAPIView):
	model_class = Ticket
	serializer_class = TicketListSerializer

	def get_queryset(self):
		project = self.get_project()
		user_profile = self.request.user.user_profile
		filter_name = self.kwargs.get("filter_name")
		print ("Special Filter: {}".format(filter_name))
		if filter_name == "unassigned":
			queryset = self.model_class.objects.filter(administrators=None, project=project)
		elif filter_name == "my_tickets_admin":
			queryset = self.model_class.objects.filter(administrators__in=[user_profile], project=project)
		elif filter_name == "my_tickets_collaborator":
			queryset = self.model_class.objects.filter(collaborators__in=[user_profile], project=project)
		return queryset

class ClientRequestAPIView(SlugManagerMixin, generics.ListAPIView):
	model_class = Unity
	serializer_class = UnitySerializer

	def get_queryset(self):
		project = self.get_project()
		identity = self.request.GET.get("identity")
		client = get_object_or_404(Person, identity=identity)
		unities = [pa.unity for pa in client.paymentagreement_set.all()]
		return unities


class SupportLandingTemplateView(TemplateView):
	template_name = "support/landing-support.html"


class TemplateFilter(filters.FilterSet):
    class Meta:
        model = Template
        fields = ['category']


class TemplatesApiView(SlugManagerMixin, generics.ListAPIView):
	model_class = Template
	serializer_class = TemplateSerializer

	def get_queryset(self):
		project = self.get_project()
		return self.model_class.objects.filter(project=project)

	def list(self, request, *args, **kwargs):
		queryset = self.get_queryset()
		filtered = TemplateFilter(request.GET, queryset=queryset)
		serializer = self.serializer_class(filtered, many=True, context={"request": request})
		return Response(serializer.data)
