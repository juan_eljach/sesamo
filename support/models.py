import random
from django.db import models
from django.conf import settings
from django.template import defaultfilters
from hashlib import sha1
from persons.models import Person
from projects.models import Project
from units.models import Unity

STATUS_CHOICES = (
    ('open', "Abierto"),
    ('re-opened', "Re abierto"),
    ('resolved', "Resuelto"),
    ('closed', "Cerrado"),
)

PRIORITY_CHOICES = (
    ("critical", "Critica"),
    ("high", "Alta"),
    ("normal", "Normal"),
    ("low", "Baja"),
    ("very-low", "Muy Baja"),
)

TICKET_TYPE_CHOICES = (
    ("modifications", "Modificaciones"),
    ("warranty", "Solicitudes de Garantia"),
    ("various", "Varios"),
    ("payment-agreements", "Preguntas sobre Planes de Pago"),
    ("complainsts-claims", "Quejas y Reclamos")
)

class Ticket(models.Model):
    administrators = models.ManyToManyField(settings.AUTH_PROFILE_MODULE, related_name="administrators", blank=True)
    attached_file = models.FileField(upload_to="uploads", blank=True, null=True)
    collaborators = models.ManyToManyField(settings.AUTH_PROFILE_MODULE, related_name="collaborators", blank=True)
    client = models.ForeignKey(Person, related_name="client")
    description = models.TextField(max_length=2000, blank=True, null=True)
    priority = models.CharField(max_length=20, choices=PRIORITY_CHOICES, blank=True, null=True)
    project = models.ForeignKey(Project)
    slug = models.SlugField(max_length=100)
    status = models.CharField(max_length=20, choices=STATUS_CHOICES, blank=True, null=True)
    ticket_type = models.CharField(max_length=20, choices=TICKET_TYPE_CHOICES, blank=True, null=True)
    timestamp = models.DateTimeField(auto_now_add=True)
    title = models.CharField(max_length=50)
    unity = models.ForeignKey(Unity, blank=True, null=True)

    def save(self, *args, **kwargs):
        if not self.pk:
            self.slug = defaultfilters.slugify(self.title)
            while True:
                try:
                    ticket_with_slug_exists = Ticket.objects.get(slug__iexact=self.slug)
                    if ticket_with_slug_exists:
                        self.slug = "{0}-{1}".format(self.slug, sha1(str(random.random()).encode('utf-8')).hexdigest()[:5])
                except Ticket.DoesNotExist: break

        super(Ticket, self).save(*args, **kwargs)

    def __str__(self, *args, **kwargs):
        return "{0} - Unidad: {1}".format(self.title, self.unity)

class Answer(models.Model):
    answer_attached_file = models.FileField(upload_to="uploads", blank=True, null=True)
    answer_text = models.TextField(max_length=2000)
    timestamp = models.DateTimeField(auto_now_add=True)
    ticket = models.ForeignKey(Ticket, related_name="answers")
    written_by = models.ForeignKey(settings.AUTH_PROFILE_MODULE)

    def __str__(self):
        return "Answer to ticket: {}".format(self.ticket)

class TicketNote(models.Model):
    note_text = models.TextField(max_length=2000)
    timestamp = models.DateTimeField(auto_now_add=True)
    ticket = models.ForeignKey(Ticket, related_name="notes")
    written_by = models.ForeignKey(settings.AUTH_PROFILE_MODULE, related_name="written_by")

class Template(models.Model):
    project = models.ForeignKey(Project)
    title = models.CharField(max_length=50)
    template_text = models.TextField(max_length=2000)
    category = models.CharField(max_length=20, choices=TICKET_TYPE_CHOICES, blank=True, null=True)

class Notification(models.Model):
    notification_text = models.TextField(max_length=2000)
    title = models.CharField(max_length=100)
