from rest_framework import serializers
from accounts.serializers import UserProfileSerializer
from accounts.models import UserProfile
from .models import Ticket, TicketNote, Answer, Template
from persons.serializers import PersonInformativeSerializer

class TicketAnswerCreateSerializer(serializers.ModelSerializer):
	answer_attached_file = serializers.FileField(allow_null=True, required=False)
	class Meta:
		model = Answer
		fields = (
			'answer_attached_file',
			'answer_text',
		)

class TicketAnswerDetailSerializer(serializers.ModelSerializer):
	answer_attached_file = serializers.FileField()
	written_by = UserProfileSerializer()
	class Meta:
		model = Answer
		fields = (
			'answer_attached_file',
			'answer_text',
			'timestamp',
			'written_by'
		)

class TicketNoteSerializer(serializers.ModelSerializer):
	class Meta:
		model = TicketNote
		fields = (
			'note_text',
		)

class TicketNoteDetailSerializer(serializers.ModelSerializer):
	written_by = UserProfileSerializer()
	class Meta:
		model = TicketNote
		fields = (
			'note_text',
			'written_by',
			'timestamp',
		)

class TicketListSerializer(serializers.ModelSerializer):
	answers = TicketAnswerDetailSerializer(many=True, read_only=True)
	administrators = UserProfileSerializer(many=True)
	attached_file = serializers.FileField(allow_empty_file=True)
	collaborators = UserProfileSerializer(many=True)
	client = PersonInformativeSerializer(read_only=True)
	unity = serializers.StringRelatedField()
	slug = serializers.SlugField(read_only=True)
	notes = TicketNoteDetailSerializer(many=True, read_only=True)
	class Meta:
		model = Ticket
		fields = (
			'answers',
			'administrators',
			'attached_file',
			'collaborators',
			'client',
			'description',
			'notes',
			'priority',
			'slug',
			'status',
			'ticket_type',
			'timestamp',
			'title',
			'unity',
		)

"""
class TicketCreateSerializer(serializers.ModelSerializer):
	administrators = serializers.PrimaryKeyRelatedField(many=True, read_only=False, queryset=UserProfile.objects.all())
	attached_file = serializers.FileField()
	collaborators = serializers.PrimaryKeyRelatedField(many=True, read_only=False, queryset=UserProfile.objects.all())
	client = PersonInformativeSerializer(read_only=True)
	unity = serializers.StringRelatedField()
	slug = serializers.SlugField(read_only=True)
	class Meta:
		model = Ticket
		fields = (
			'administrators',
			'attached_file',
			'collaborators',
			'client',
			'description',
			'priority',
			'slug',
			'status',
			'ticket_type',
			'timestamp',
			'title',
			'unity',
		)
"""

class TicketCreateUpdateSerializer(serializers.ModelSerializer):
	administrators = serializers.PrimaryKeyRelatedField(many=True, read_only=False, queryset=UserProfile.objects.all())
	attached_file = serializers.FileField(allow_empty_file=True, required=False)
	collaborators = serializers.PrimaryKeyRelatedField(many=True, read_only=False, queryset=UserProfile.objects.all())
	slug = serializers.SlugField(read_only=True)
	class Meta:
		model = Ticket
		fields = (
			'administrators',
			'attached_file',
			'client',
			'collaborators',
			'description',
			'priority',
			'slug',
			'ticket_type',
			'status',
			'title',
			'unity'
		)

class TicketUpdateSerializer(serializers.ModelSerializer):
	administrators = serializers.PrimaryKeyRelatedField(many=True, read_only=False, queryset=UserProfile.objects.all())
	collaborators = serializers.PrimaryKeyRelatedField(many=True, read_only=False, queryset=UserProfile.objects.all())
	class Meta:
		model = Ticket
		fields = (
			'administrators',
			'client',
			'collaborators',
			'priority',
			'ticket_type',
			'status',
		)

	def update(self, instance, validated_data):
		instance_current_administrators = instance.administrators.all()
		instance_current_collaborators = instance.collaborators.all()
		new_administrators = validated_data.pop('administrators')
		new_collaborators = validated_data.pop('collaborators')
		print ("VALIDATED_DATA: {}".format(validated_data))
		instance.client = validated_data.get('client', instance.client)
		instance.priority = validated_data.get('priority', instance.priority)
		instance.ticket_type = validated_data.get('ticket_type', instance.ticket_type)
		instance.status = validated_data.get('status', instance.status)
		instance.save()
		for adm in new_administrators:
			instance.administrators.add(adm)
		instance.save()
		for current_adm in instance_current_administrators:
			if current_adm not in new_administrators:
				print ("Current adm who isnt into new admisn array: {}".format(current_adm))
				instance.administrators.remove(current_adm)
		instance.save()
		for collaborator in new_collaborators:
			instance.collaborators.add(collaborator)
		instance.save()
		for current_collaborator in instance_current_collaborators:
			if current_collaborator not in new_collaborators:
				instance.collaborators.remove(current_collaborator)
		print ("ISNTACNE AFET SAVE : {}".format(instance.administrators.all()))
		print ("Collaborators new: {}".format('collaborators'))
		return instance


class TemplateSerializer(serializers.ModelSerializer):
	class Meta:
		model = Template
		fields = (
			'title',
			'template_text',
			'category',
		)
