from rest_framework import permissions
from rest_framework import exceptions


class IsOnTicketOrForbidden(permissions.BasePermission):
	def has_object_permission(self, request, view, obj):
		if not obj.administrators.all() or not obj.collaborators.all():
			return True
		return request.user.user_profile in obj.administrators.all() or request.user.user_profile in obj.collaborators.all()

class IsAdminOnTicket(permissions.BasePermission):
	def has_object_permission(self, request, view, obj):
		if not obj.administrators.all():
			return True
		return request.user.user_profile in obj.administrators.all()

class HasPersonAssigned(permissions.BasePermission):
	def has_object_permission(self, request, view, obj):
		if request.method in permissions.SAFE_METHODS:
			return True

		print (obj.assigned_to)
		print (request.user.user_profile)
		return obj.assigned_to == request.user

class CanCreateActivityOnPerson(permissions.BasePermission):
	def has_object_permission(self, request, view, obj):
		if request.method in permissions.SAFE_METHODS:
			return True

		print ("GOT INTO RIGHT PERMISSIONS CLASS")
		print (obj.person.assigned_to)
		print (request.user.user_profile)
		if obj.person.assigned_to == request.user.user_profile:
			print ("Son iguales")
			return True
		else:
			print ("No son iguales")
			raise exceptions.PermissionDenied