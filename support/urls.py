from django.conf.urls import patterns, url
from django.core.urlresolvers import reverse_lazy
from .views import SupportLandingTemplateView

urlpatterns = patterns('',
    url(r'^soporte/$', SupportLandingTemplateView.as_view(), name="support_landing"),
)
