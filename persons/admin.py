from django.contrib import admin
from .models import Person, Activity, Note, OrderLog

class PersonAdmin(admin.ModelAdmin):
	exclude = ("slug",)

admin.site.register(Person, PersonAdmin)
admin.site.register(Activity)
admin.site.register(Note)
admin.site.register(OrderLog)
# Register your models here.
