#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django import forms
from django.utils.translation import ugettext_lazy as _
from units.models import Unity
from .models import Person, Activity, Note

class PersonModelForm(forms.ModelForm):
	class Meta:
		model = Person
		fields = [
			"first_name", 
			"last_name", 
			"cellphone", 
			"phone", 
			"email", 
			"unity_of_interest", 
			"adquisition_channel", 
			"segment", 
		]

	def __init__(self, project, *args, **kwargs):
		super(PersonModelForm, self).__init__(*args, **kwargs)
		try:
			self.fields["unity_of_interest"].queryset = Unity.objects.filter(project=project)
		except:
			pass

class ActivityModelForm(forms.ModelForm):
	class Meta:
		model = Activity
		fields = [
			"activity_type",
			"description",
			"scheduled_date",
		]

class NoteModelForm(forms.ModelForm):
	class Meta:
		model = Note
		fields = [
			"note"
		]

#	def clean(self):
#		cleaned_data = super(AddProfileForm, self).clean()
#		unity_of_interest = cleaned_data.get("unity_of_interest")
#		send_automated_quotation = cleaned_data.get("send_automated_quotation")

#		if send_automated_quotation and not unity_of_interest:
#			raise forms.ValidationError(
#                    "Para enviar una cotización automática debes especificar una unidad de interes"
#                )
#		else:
#			return cleaned_data
