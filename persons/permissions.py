#PERMISSIONS FILE FOR API

from rest_framework import permissions
from rest_framework import exceptions


class IsOwnerOrForbidden(permissions.BasePermission):
	def has_object_permission(self, request, view, obj):
		print ("GOT INTO permissions")
		print ("obj: {}".format(obj))
		return obj.assigned_to == request.user.user_profile or obj.assigned_to == None

class HasPersonAssigned(permissions.BasePermission):
	def has_object_permission(self, request, view, obj):
		if request.method in permissions.SAFE_METHODS:
			return True

		print (obj.assigned_to)
		print (request.user.user_profile)
		return obj.assigned_to == request.user

class CanCreateActivityOnPerson(permissions.BasePermission):
	def has_object_permission(self, request, view, obj):
		if request.method in permissions.SAFE_METHODS:
			return True

		print ("GOT INTO RIGHT PERMISSIONS CLASS")
		print (obj.person.assigned_to)
		print (request.user.user_profile)
		if obj.person.assigned_to == request.user.user_profile:
			print ("Son iguales")
			return True
		else:
			print ("No son iguales")
			raise exceptions.PermissionDenied
