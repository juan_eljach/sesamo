#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import random
import datetime
from hashlib import sha1
from django.core.exceptions import ValidationError
from django.conf import settings
from django.contrib.postgres.fields import JSONField
from django.db import models
from django.template import defaultfilters
from django.template.loader import render_to_string
from sesamo.tasks import SendEmail
from units.models import Unity
from projects.models import Project

segment_choices= (
	("high","Altamente probable"),
	("medium","Medianamente probable"),
	("low","Poco probable"),
)

adquisition_channel_choices = (
	("room", "Sala de Ventas"),
	("fair", "Feria"),
	("phone", "Telefono"),
	("press", "Publicación en prensa"),
	("office", "Oficina"),
	("referal", "Referido"),
	("web", "Página Web"),
	("radio", "Radio"),
	("other", "Otros")
)

gender_choices = (
	("masculino", "Masculino"),
	("femenino", "Femenino")
)
#Cedula
#Direccion de notificacion
#Telefono de notificacion
#Fecha de Nacimiento
#Telefono fijo


CIVIL_STATUS_CHOICES = (
	('soltero', 'Soltero'),
	('casado', 'Casado'),
	('viudo', 'Viudo'),
	('union-libre', 'Union Libre'),
)


#TODO: Probably should make Pool a segment for easier filtering and SegmentLog tracking

class Person(models.Model):
	first_name = models.CharField(max_length=100, blank=False, null=False)
	last_name = models.CharField(max_length=100, blank=False, null=False)
	cellphone = models.CharField(max_length=20, blank=True, null=True)
	phone = models.CharField(max_length=15, blank=True, null=True)
	email = models.EmailField(blank=True, null=True)
	unity_of_interest = models.ForeignKey(Unity, blank=True, null=True)
	adquisition_channel = models.CharField(max_length=100, choices=adquisition_channel_choices)
	segment = models.CharField(max_length=10, choices=segment_choices, default="low", blank=False, null=False)
	assigned_to = models.ForeignKey(settings.AUTH_PROFILE_MODULE, blank=True, null=True)
	sold = models.BooleanField(default=False)
	lost = models.BooleanField(default=False)
	is_client = models.BooleanField(default=False)
	gender = models.CharField(max_length=30, choices=gender_choices, blank=True, null=True)
	identity = models.CharField(max_length=30, blank=True, null=True)
	expedition_place = models.CharField(max_length=30, blank=True, null=True)
	notification_address = models.CharField(max_length=100, blank=True, null=True)
	notification_phone = models.CharField(max_length=20, blank=True, null=True)
	city_of_residence = models.CharField(max_length=50)
	birthday = models.DateField(blank=True, null=True)
	civil_status = models.CharField(max_length=20, blank=True, null=True, choices=CIVIL_STATUS_CHOICES)
	average_income = models.DecimalField(max_digits=10,decimal_places=2,blank=True,null=True)
	timestamp = models.DateTimeField(auto_now_add=True)
	timestamp_for_sold = models.DateTimeField(auto_now=False, auto_now_add=False, null=True, blank=True)
	project = models.ForeignKey(Project)
	last_update = models.DateTimeField(blank=True, null=True)
	slug = models.CharField(max_length=500)

	def get_person_full_name(self):
		full_name = "{} {}".format(self.first_name, self.last_name)
		return full_name

	def get_last_update(self):
		last_activity = self.activities.order_by("timestamp").last()
		last_note = self.notes.order_by("timestamp").last()
		if last_activity is not None and last_note is not None:
			if last_activity.timestamp > last_note.timestamp:
				return last_activity.timestamp
			else:
				return last_note.timestamp
		elif last_activity is None and last_note is None:
			return None
		elif last_activity is not None and last_note is None:
			return last_activity.timestamp
		elif last_activity is None and last_note is not None:
			return last_note.timestamp

	def set_last_update(self):
		self.last_update = self.get_last_update()
		self.save()

	def _create_segment_record(self):
		payload = {
			"person_id":self.pk,
			"segment":self.segment,
			"assigned_to":self.assigned_to
		}
		PersonSegmentLogRecords.objects.create(**payload)

	def save(self, *args, **kwargs):
		if not self.pk:
			self.slug = defaultfilters.slugify(self.get_person_full_name())
			while True:
				try:
					person_with_slug_exists = Person.objects.get(slug__iexact=self.slug)
					if person_with_slug_exists:
						self.slug = "{0}-{1}".format(self.slug, sha1(str(random.random()).encode('utf-8')).hexdigest()[:5])
				except Person.DoesNotExist: break
			super(Person, self).save(*args, **kwargs)
			self._create_segment_record()
		else:
			try:
				original_instance = Person.objects.get(pk=self.pk)
				if original_instance.sold == False and self.sold == True:
					self.timestamp_for_sold = datetime.datetime.today()

				if original_instance.segment != self.segment or original_instance.assigned_to != self.assigned_to:
					self._create_segment_record()
			except Exception as err:
				print (err)
			super(Person, self).save(*args, **kwargs)


	def __str__(self, *args, **kwargs):
		return self.get_person_full_name()

class PersonSegmentLogRecords(models.Model):
	"""Tracks the segments a Person has belonged to over time"""
	person = models.ForeignKey(Person, related_name='segment_log_records')
	assigned_to = models.ForeignKey(settings.AUTH_PROFILE_MODULE, blank=True, null=True)
	timestamp = models.DateTimeField(auto_now_add=True)
	segment = models.CharField(max_length=10, choices=segment_choices, blank=False, null=False)

	def __str__(self):
		return "{} segment log. {}".format(
			self.person.get_person_full_name(),
			self.timestamp)


activity_types = (
	("llamada","Hacer llamada"),
	("email","Enviar correo"),
	("reunion","Reunion"),
	("visita", "Visita a Sala")
)

class OrderLog(models.Model):
	project = models.ForeignKey(Project)
	salesperson = models.ForeignKey(settings.AUTH_PROFILE_MODULE, blank=False, null=False, related_name="presale_orderlog")
	data = JSONField()

	def validate_unique(self, *args, **kwargs):
		qs = OrderLog.objects.filter(project=self.project, salesperson= self.salesperson)
		if qs.exists():
			raise ValidationError("OrderLog already exist. Try updating the existing log instead")

	def save(self, *args, **kwargs):
		if not self.pk:
			self.validate_unique()
		super(OrderLog, self).save(*args, **kwargs)

	def __str__(self, *args, **kwargs):
		return ("{} / {}".format(self.project, self.salesperson))


class Activity(models.Model):
	done = models.BooleanField(default=False)
	person = models.ForeignKey(Person, related_name="activities")
	project = models.ForeignKey(Project)
	activity_type = models.CharField(max_length=30, choices=activity_types)
	description = models.TextField(max_length=1000)
	scheduled_date = models.DateTimeField()
	assigned_to = models.ForeignKey(settings.AUTH_PROFILE_MODULE, blank=False, null=False, related_name="assigned_to")
	timestamp = models.DateTimeField(auto_now_add=True)
	checked_as_done_timestamp = models.DateTimeField(null=True, blank=True)

	def __str__(self, *args, **kwargs):
		return self.activity_type

	def save(self, *args, **kwargs):
		super(Activity, self).save(*args, **kwargs)
		self.person.last_update = datetime.datetime.now()
		self.person.save()

class Note(models.Model):
	person = models.ForeignKey(Person, related_name="notes")
	note = models.TextField(max_length=2000)
	assigned_to = models.ForeignKey(settings.AUTH_PROFILE_MODULE, related_name="assigned_notes", blank=False, null=False)
	timestamp = models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return ("Note on {}".format(self.person.get_person_full_name()))

	def save(self, *args, **kwargs):
		super(Note, self).save(*args, **kwargs)
		self.person.last_update = datetime.datetime.now()
		self.person.save()
