from django import template
from django.shortcuts import get_object_or_404
from projects.models import Project
register = template.Library()

@register.simple_tag(takes_context=True)
def get_quotations(context, person, project):
	try:
		quotations = person.quotation_set.filter(unity__project=project)
	except Exception as err:
		print (err)
		quotations = None
	print (quotations)
	return quotations

