from rest_framework import serializers
from .models import Person, Activity, Note, OrderLog
from presales.models import Quotation
from accounts.serializers import UserProfileSerializer

class ActivitySerializerForPerson(serializers.ModelSerializer):
    class Meta:
        model = Activity
        fields = (
            'activity_type',
            'checked_as_done_timestamp',
            'description',
            'id',
            'done',
            'scheduled_date',
            'timestamp'
        )


class NoteCreateSerializer(serializers.ModelSerializer):
    assigned_to = UserProfileSerializer(read_only=True)
    class Meta:
        model = Note
        fields = (
            'person',
            'note',
            'assigned_to',
            'timestamp'
        )

class NoteSerializer(serializers.ModelSerializer):
    assigned_to = UserProfileSerializer()
    class Meta:
        model = Note
        fields = (
            'person',
            'note',
            'assigned_to',
            'timestamp',
        )

class SmallQuotationSerializer(serializers.ModelSerializer):
    unity = serializers.SerializerMethodField()
    class Meta:
        model = Quotation
        fields = (
            "id",
            "unity"
        )

    def get_unity(self, obj):
        return obj.unity.name

class PersonSerializer(serializers.ModelSerializer):
    #adquisition_channel = serializers.CharField(source="get_adquisition_channel_display")
    quotations = serializers.SerializerMethodField()
    activities = ActivitySerializerForPerson(many=True, read_only=True)
    notes = NoteSerializer(many=True, read_only=True)
    subsidy = serializers.SerializerMethodField()
    savings = serializers.SerializerMethodField()
    severance = serializers.SerializerMethodField()
    others = serializers.SerializerMethodField()
    monthly_income = serializers.SerializerMethodField()

    class Meta:
        model = Person
        fields = (
            'id',
            'first_name',
            'last_name',
            'cellphone',
            'phone',
            'email',
            'unity_of_interest',
            'adquisition_channel',
            'segment',
            'assigned_to',
            'sold',
            'lost',
            'timestamp',
            'last_update',
            'civil_status',
            'average_income',
            'project',
            'subsidy',
            'savings',
            'severance',
            'others',
            'monthly_income',
            'activities',
            'notes',
            'quotations',
            'slug',
        )

    def get_quotations(self, obj):
        project = self.context.get("project", None)
        data = []
        for quotation in obj.quotations.filter(unity__project=project):
            data.append({"id": quotation.id, "name":quotation.unity.name})
        return data

    def get_subsidy(self, obj):
        if obj.quotations.all():
            return obj.quotations.all().last().subsidy
        else:
            return None

    def get_savings(self, obj):
        if obj.quotations.all():
            return obj.quotations.all().last().savings
        else:
            return None

    def get_severance(self, obj):
        if obj.quotations.all():
            return obj.quotations.all().last().severance
        else:
            return None

    def get_others(self, obj):
        if obj.quotations.all():
            return obj.quotations.all().last().others
        else:
            return None

    def get_monthly_income(self, obj):
        if obj.quotations.all():
            return obj.quotations.all().last().monthly_income

class PersonCreateSerializer(serializers.ModelSerializer):
    slug = serializers.SlugField(read_only=True)
    class Meta:
        model = Person
        fields = (
            "id",
            "first_name",
            "last_name",
            'cellphone',
            'phone',
            'email',
            'unity_of_interest',
            'adquisition_channel',
            'segment',
            'project',
            'assigned_to',
            'slug',
        )

class PersonInformativeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Person
        fields = (
            'id',
            'first_name',
            'last_name',
            'cellphone',
            'phone',
            'email',
            'slug',
        )

class PersonSmallSerializer(serializers.ModelSerializer):
    subsidy = serializers.SerializerMethodField()
    savings = serializers.SerializerMethodField()
    severance = serializers.SerializerMethodField()
    others = serializers.SerializerMethodField()
    monthly_income = serializers.SerializerMethodField()
    slug = serializers.SlugField(read_only=True)

    class Meta:
        model = Person
        fields = (
            'id',
            'first_name',
            'last_name',
            'identity',
            'segment',
            'expedition_place',
            'notification_address',
            'notification_phone',
            'birthday',
            'is_client',
            'cellphone',
            'phone',
            'email',
            'civil_status',
            'assigned_to',
            'subsidy',
            'savings',
            'severance',
            'others',
            'monthly_income',
            'project',
            'slug'
        )

    def get_subsidy(self, obj):
        if obj.quotations.all():
            return obj.quotations.all().last().subsidy
        else:
            return None

    def get_savings(self, obj):
        if obj.quotations.all():
            return obj.quotations.all().last().savings
        else:
            return None

    def get_severance(self, obj):
        if obj.quotations.all():
            return obj.quotations.all().last().severance
        else:
            return None

    def get_others(self, obj):
        if obj.quotations.all():
            return obj.quotations.all().last().others
        else:
            return None

    def get_monthly_income(self, obj):
        if obj.quotations.all():
            return obj.quotations.all().last().monthly_income

#THERE IS AN ISSUE IN THE WAY DJANGO STORE AND RENDER DATETIMEFIELDS. IT'S PROBABLY REQUIRED TO WRITE A CUSTOM DATETIMEFIELD
class ActivitySerializer(serializers.ModelSerializer):
    scheduled_date = serializers.DateTimeField(format=None)

    class Meta:
        model = Activity
        fields = (
            'id',
            'done',
            'person',
            'project',
            'activity_type',
            'description',
            'scheduled_date',
        )

class PersonObjectRelatedField(serializers.RelatedField):
    """
    A custom field to use for the `person_object` generic relationship.
    """

    def to_representation(self, value):
        print (value)
        """
        Serialize tagged objects to a simple textual representation.
        """
        if isinstance(value, Person):
            return "id: " + str(value.id) + ", name: " + value.get_person_full_name()
        else:
            raise Exception('Unexpected type of tagged object')

        return serializer.data

    def to_internal_value(self, value):
        return Person.objects.get(id=value)

class ActivitySerializerCustomRepresentations(serializers.ModelSerializer):
    scheduled_date = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S")
#    person = PersonObjectRelatedField(queryset=Person.objects.all())
    person = serializers.SerializerMethodField()
    person_id = serializers.IntegerField(source='person.id')
    person_slug = serializers.SlugField(source="person.slug", max_length=50, min_length=None, allow_blank=False, read_only=True)

    class Meta:
        model = Activity
        fields = (
            'id',
            'checked_as_done_timestamp',
            'done',
            'person',
            'person_slug',
            'person_id',
            'project',
            'activity_type',
            'description',
            'scheduled_date',
        )

    def get_person(self, obj):
        if obj.person:
            return obj.person.get_person_full_name()


class OrderLogSerializer(serializers.ModelSerializer):
    pk = serializers.IntegerField(read_only=True)
    class Meta:
        model = OrderLog
        fields = (
            'pk',
            'project',
            'salesperson',
            'data',
        )
