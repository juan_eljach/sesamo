import datetime
from celery.decorators import task, periodic_task
from celery.task.schedules import crontab

@periodic_task(run_every=(crontab(minute=0, hour=6)), name="daily_check_of_activities_in_persons", ignore_result=True)
#@periodic_task(run_every=(crontab(minute='*/2')), name="daily_check_of_activities_in_persons", ignore_result=True)
def CheckActivitiesInPerson():
    today = datetime.datetime.today().date()
    time_delta = datetime.timedelta(days=45)
    from projects.models import Project
    for project in Project.objects.all():
        for person in project.person_set.all():
            if person.activities.all().last():
                last_activity = person.activities.all().last().timestamp.date()
                if (today - last_activity) > time_delta and person.assigned_to is not None:
                    person.assigned_to = None
                    person.save()
            else:
                person_timestamp = person.timestamp.date()
                if (today - person_timestamp) > time_delta and person.assigned_to is not None:
                    person.assigned_to = None
                    person.save()