from django.conf.urls import patterns, url
from .views import PersonCreateView, PersonDetailView, PersonDetailUpdateView, PreSaleInitialView, PersonListAPIView, OfficeInitialView, PersonExport, PersonImport
#PersonListView

urlpatterns = patterns('',
	url(r'^projects/(?P<project_slug>[-\w]+)/person/create/$', PersonCreateView.as_view(), name='create_person'),
	url(r'^projects/(?P<project_slug>[-\w]+)/presale/#/segmentation/persona/(?P<person_slug>[-\w]+)/$', PersonDetailView.as_view(), name='person_tracking'),
	url(r'^projects/(?P<project_slug>[-\w]+)/presale/$', PreSaleInitialView.as_view(), name='presale_splash'),
	#Office Splash is the url for those users who are not salespeople
	url(r'^projects/(?P<project_slug>[-\w]+)/office/$', OfficeInitialView.as_view(), name='office_splash'),
	url(r'^projects/(?P<project_slug>[-\w]+)/import/persons/$', PersonImport.as_view(), name='unities_import'),
    url(r'^projects/(?P<project_slug>[-\w]+)/export/persons/$', PersonExport.as_view(), name='unities_export'),
	#url(r'^projects/(?P<project_slug>[-\w]+)/segmentation/$', PersonListView.as_view(), name='segmentation'),
)
