from datetime import datetime
from django.db.models import Q
from django.core import serializers
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse, Http404
from django.conf import settings
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from datetime import datetime
from django.shortcuts import render, redirect, get_object_or_404
from django.template import RequestContext
from django.template.loader import get_template
from django.utils.decorators import method_decorator
from django.views.generic import CreateView, DetailView, ListView, TemplateView, View
from formtools.wizard.views import SessionWizardView
from import_export.forms import ImportForm, ConfirmImportForm
from import_export.formats import base_formats
from import_export.resources import modelresource_factory
from import_export import resources
from weasyprint import HTML, CSS
from sesamo.mixins import SlugManagerMixin, JSONResponseMixin
from projects.models import Project
from units.models import Unity
from .forms import PersonModelForm, ActivityModelForm, NoteModelForm
from .models import Person, Activity, Note, OrderLog

#START OF API VIEWS
from rest_framework import generics, viewsets
from rest_framework.mixins import CreateModelMixin, ListModelMixin, RetrieveModelMixin, UpdateModelMixin
from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView
from .serializers import PersonSerializer, PersonCreateSerializer, PersonSmallSerializer, ActivitySerializer, ActivitySerializerCustomRepresentations, NoteSerializer, OrderLogSerializer, NoteCreateSerializer
from sesamo.mixins import SlugManagerMixin
from .permissions import HasPersonAssigned, CanCreateActivityOnPerson, IsOwnerOrForbidden



#APIS TO IMPORT-EXPORT PERSONS WITH CSV

class PersonResource(resources.ModelResource):
	def before_import(self, dataset, dry_run, *args, **kwargs):
		project = kwargs['project']
		i = 0
		last = dataset.height - 1

		try:
			dataset["id"]
		except:
			try:
				dataset.append_col([None for row in range(len(dataset))], header='id')
			except:
				print ("Error agregando columna de ID al dataset")

		try:
			dataset.append_col([project.id for row in range(len(dataset))], header='project')
		except Exception as err:
			print (err)


	class Meta:
		model = Person
		fields = (
			'id',
			'first_name',
			'last_name',
			'cellphone',
			'phone',
			'email',
			'is_client',
			'project'
		)

class PersonExport(View):
	model = Person
	from_encoding = "utf-8"
	resource_class = PersonResource

	def get_resource_class(self):
		if not self.resource_class:
			return modelresource_factory(self.model)
		else:
			return self.resource_class

	def get_export_resource_class(self):
		"""
		Returns ResourceClass to use for export.
		"""
		return self.get_resource_class()

		resource = self.get_export_resource_class()()

	def get(self, *args, **kwargs ):
		project_slug = self.kwargs["project_slug"]
		project = get_object_or_404(Project, slug__iexact=project_slug)
		resource = self.get_export_resource_class()()
		result = resource.export(queryset=Person.objects.filter(project=project).order_by("id"))
		response = HttpResponse(result.csv, content_type="csv")
		response['Content-Disposition'] = 'attachment; filename={0}-personas.csv'.format(project.name)
		return response


@method_decorator(login_required, name='dispatch')
class PersonImport(View):
	model = Person
	from_encoding = "utf-8"

	#: import / export formats
	DEFAULT_FORMATS = (
		base_formats.CSV,
		base_formats.XLS,
	)
	formats = DEFAULT_FORMATS
	#: template for import view
	import_template_name = 'persons/import_persons.html'
	resource_class = PersonResource

	def get_import_formats(self):
		"""
		Returns available import formats.
		"""
		return [f for f in self.formats if f().can_import()]

	def get_resource_class(self):
		if not self.resource_class:
			return modelresource_factory(self.model)
		else:
			return self.resource_class

	def get_import_resource_class(self):
		"""
		Returns ResourceClass to use for import.
		"""
		return self.get_resource_class()

	def get(self, *args, **kwargs ):
		'''
		Perform a dry_run of the import to make sure the import will not
		result in errors.  If there where no error, save the user
		uploaded file to a local temp file that will be used by
		'process_import' for the actual import.
		'''
		resource = self.get_import_resource_class()()
		project_slug = self.kwargs['project_slug']
		project = get_object_or_404(Project, slug__iexact=project_slug)

		context = {}
		context['project'] = project

		import_formats = self.get_import_formats()
		form = ImportForm(import_formats,
						  self.request.POST or None,
						  self.request.FILES or None)

		if self.request.POST and form.is_valid():
			input_format = import_formats[
				int(form.cleaned_data['input_format'])
			]()
			import_file = form.cleaned_data['import_file']
			# first always write the uploaded file to disk as it may be a
			# memory file or else based on settings upload handlers
			with tempfile.NamedTemporaryFile(delete=False) as uploaded_file:
				for chunk in import_file.chunks():
					uploaded_file.write(chunk)

			# then read the file, using the proper format-specific mode
			with open(uploaded_file.name,
					  input_format.get_read_mode()) as uploaded_import_file:
				# warning, big files may exceed memory
				data = uploaded_import_file.read()
				if not input_format.is_binary() and self.from_encoding:
					data = force_text(data, self.from_encoding)
				dataset = input_format.create_dataset(data)
				result = resource.import_data(dataset, dry_run=False,
											  raise_errors=True, project=project)

			context['result'] = result

			if not result.has_errors():
				context['confirm_form'] = ConfirmImportForm(initial={
					'import_file_name': os.path.basename(uploaded_file.name),
					'original_file_name': uploaded_file.name,
					'input_format': form.cleaned_data['input_format'],
				})
			else:
				print ("RESULT: {}".format(result))

		context['form'] = form
		context['opts'] = self.model._meta
		context['fields'] = [f.column_name for f in resource.get_fields()]
		context.update(self.kwargs)

		return TemplateResponse(self.request, [self.import_template_name], context)


	def post(self, *args, **kwargs ):
		'''
		Perform a dry_run of the import to make sure the import will not
		result in errors.  If there where no error, save the user
		uploaded file to a local temp file that will be used by
		'process_import' for the actual import.
		'''
		resource = self.get_import_resource_class()()#
		project_slug = self.kwargs['project_slug']
		project = get_object_or_404(Project, slug__iexact=project_slug)
		context = {}
		context['project'] = project

		import_formats = self.get_import_formats()
		form = ImportForm(import_formats,
						  self.request.POST or None,
						  self.request.FILES or None)


		if self.request.POST and form.is_valid():
			input_format = import_formats[
				int(form.cleaned_data['input_format'])
			]()
			import_file = form.cleaned_data['import_file']
			# first always write the uploaded file to disk as it may be a
			# memory file or else based on settings upload handlers
			with tempfile.NamedTemporaryFile(delete=False) as uploaded_file:
				for chunk in import_file.chunks():
					uploaded_file.write(chunk)

			# then read the file, using the proper format-specific mode
			with open(uploaded_file.name,
					  input_format.get_read_mode()) as uploaded_import_file:
				# warning, big files may exceed memory
				data = uploaded_import_file.read()
				if not input_format.is_binary() and self.from_encoding:
					data = force_text(data, self.from_encoding)
				dataset = input_format.create_dataset(data)
				result = resource.import_data(dataset, dry_run=False,
											  raise_errors=True, project=project)

			context['result'] = result

			if not result.has_errors():
				context['confirm_form'] = ConfirmImportForm(initial={
					'import_file_name': os.path.basename(uploaded_file.name),
					'original_file_name': uploaded_file.name,
					'input_format': form.cleaned_data['input_format'],
				})

		context['form'] = form
		context['opts'] = self.model._meta
		context['fields'] = [f.column_name for f in resource.get_fields()]
		context.update(self.kwargs)
		return TemplateResponse(self.request, [self.import_template_name], context)

class PersonProcessImport(View):
	model = Person
	from_encoding = "utf-8"

	#: import / export formats
	DEFAULT_FORMATS = (
		base_formats.CSV,
		base_formats.XLS,
		base_formats.TSV,
		base_formats.ODS,
		base_formats.JSON,
		base_formats.YAML,
		base_formats.HTML,
	)
	formats = DEFAULT_FORMATS
	#: template for import view
	import_template_name = 'persons/import_persons.html'
	resource_class = PersonResource

	def get_import_formats(self):
		"""
		Returns available import formats.
		"""
		return [f for f in self.formats if f().can_import()]

	def get_resource_class(self):
		if not self.resource_class:
			return modelresource_factory(self.model)
		else:
			return self.resource_class

	def get_import_resource_class(self):
		"""
		Returns ResourceClass to use for import.
		"""
		return self.get_resource_class()

	def post(self, *args, **kwargs):
		'''
		Perform the actual import action (after the user has confirmed he
	wishes to import)
		'''
		opts = self.model._meta
		resource = self.get_import_resource_class()()
		project_slug = self.kwargs['project_slug']
		project = get_object_or_404(Project, slug__iexact=project_slug)

		confirm_form = ConfirmImportForm(self.request.POST)
		if confirm_form.is_valid():
			import_formats = self.get_import_formats()
			input_format = import_formats[
				int(confirm_form.cleaned_data['input_format'])
			]()
			import_file_name = os.path.join(
				tempfile.gettempdir(),
				confirm_form.cleaned_data['import_file_name']
			)
			import_file = open(import_file_name, input_format.get_read_mode())
			data = import_file.read()
			if not input_format.is_binary() and self.from_encoding:
				data = force_text(data, self.from_encoding)
			dataset = input_format.create_dataset(data)
			result = resource.import_data(dataset, dry_run=False,
								 raise_errors=True, project=project)
			# Add imported objects to LogEntry
			#ADDITION = 1
			#CHANGE = 2
			#DELETION = 3
			#logentry_map = {
			#    RowResult.IMPORT_TYPE_NEW: ADDITION,
			#    RowResult.IMPORT_TYPE_UPDATE: CHANGE,
			#    RowResult.IMPORT_TYPE_DELETE: DELETION,
			#}
			#content_type_id=ContentType.objects.get_for_model(self.model).pk
			#'''
			#for row in result:
			#    LogEntry.objects.log_action(
			#        user_id=request.user.pk,
			#        content_type_id=content_type_id,
			#        object_id=row.object_id,
			#        object_repr=row.object_repr,
			#        action_flag=logentry_map[row.import_type],
			#        change_message="%s through import_export" % row.import_type,
			#    )
			#'''
			import_file.close()

			redirect_kwargs = {'project_slug': project_slug}

			return redirect(reverse('presale_splash', kwargs=redirect_kwargs))
		else:
			print (confirm_form.errors)


@method_decorator(login_required, name='dispatch')
class PersonListAPIView(SlugManagerMixin, generics.ListAPIView):
	model_class = Person
	serializer_class = PersonSerializer

	def get_queryset(self):
		project = self.get_project()
		user_profile = self.request.user.user_profile
		persons = Person.objects.filter(Q(assigned_to=user_profile) | Q(assigned_to=None), project=project, lost=False)
		return persons

	def get_serializer(self, queryset, **kwargs):
		"""
		Return the serializer instance that should be used for validating and
		eserializing input, and for serializing output.
		"""
		project = self.get_project()
		serializer_class = self.get_serializer_class()
		context = self.get_serializer_context()
		context.update({"project":project})
		kwargs['context'] = context
		serializer = serializer_class(queryset, **kwargs)
		return serializer

@method_decorator(login_required, name='dispatch')
class PersonDetailUpdateView(generics.RetrieveUpdateAPIView):
	lookup_field = "slug"
	queryset = Person.objects.all()
	serializer_class = PersonSerializer
	permission_classes = (IsOwnerOrForbidden,)

	def pre_save(self, obj):
		obj.assigned_to = self.request.user.user_profile

@method_decorator(login_required, name='dispatch')
class PersonSmallUpdateView(generics.RetrieveUpdateAPIView):
	lookup_field = "slug"
	queryset = Person.objects.all()
	serializer_class = PersonSmallSerializer
#    permission_classes = (IsOwnerOrForbidden,)

 #   def pre_save(self, obj):
 #   	obj.assigned_to = self.request.user.user_profile

@method_decorator(login_required, name='dispatch')
class PersonSmallCreateView(generics.CreateAPIView):
	model_class = Person
	serializer_class = PersonSmallSerializer
	queryset = Person.objects.all()

	def pre_save(self, obj):
		obj.assigned_to = self.request.user.user_profile


@method_decorator(login_required, name='dispatch')
class PersonMainCreateView(generics.CreateAPIView):
	model_class = Person
	serializer_class = PersonCreateSerializer

	def perform_create(self, serializer):
		if self.request.user.user_profile.user_type == "empleado":
			serializer.save(assigned_to=None)
		else:
			serializer.save(assigned_to=self.request.user.user_profile)

class ActivityAndNoteMixin(object):
	def create(self, request, *args, **kwargs):
		person_id = request.data.get("person")
		person = Person.objects.get(id=person_id)
		if person.assigned_to == self.request.user.user_profile:
			serializer = self.get_serializer(data=request.data)
			serializer.is_valid(raise_exception=True)
			self.perform_create(serializer)
			headers = self.get_success_headers(serializer.data)
			return Response(serializer.data, status=201, headers=headers)
		else:
			return Response(status=403)

	def perform_create(self, serializer):
		serializer.save(assigned_to=self.request.user.user_profile)

@method_decorator(login_required, name='dispatch')
class ActivityListAPIView(SlugManagerMixin, generics.ListAPIView):
	model_class = Activity
	serializer_class = ActivitySerializerCustomRepresentations

	def get_queryset(self):
		project = self.get_project()
		user_profile = self.request.user.user_profile
		today = datetime.today().date()
		activities = Activity.objects.filter(scheduled_date__date__lte=today, person__assigned_to=user_profile, project=project)
		return activities

@method_decorator(login_required, name='dispatch')
class ActivityCreateAPIView(ActivityAndNoteMixin, generics.CreateAPIView):
	serializer_class = ActivitySerializer
	queryset = Activity.objects.all()
	permission_classes = (IsOwnerOrForbidden,)

@method_decorator(login_required, name='dispatch')
class ActivityUpdateAPIView(ActivityAndNoteMixin, generics.RetrieveUpdateAPIView):
	lookup_field = "id"
	serializer_class = ActivitySerializer
	queryset = Activity.objects.all()
	permission_classes = (IsOwnerOrForbidden,)

	def perform_update(self, serializer):
		#serializer.save(assigned_to=self.request.user.user_profile)
		if serializer.validated_data.get("done"):
			try:
				serializer.save(checked_as_done_timestamp=datetime.now())
			except Exception as err:
				print (err)
		else:
			serializer.save()

@method_decorator(login_required, name='dispatch')
class NoteCreateAPIView(ActivityAndNoteMixin, generics.CreateAPIView):
	serializer_class = NoteCreateSerializer
	queryset = Note.objects.all()

#END OF API VIEWS

@method_decorator(login_required, name='dispatch')
class PersonCreateView(SlugManagerMixin, CreateView):
	model = Person
	form_class = PersonModelForm

	def form_valid(self, form):
		project = self.get_project()
		self.object = form.save(commit=False)
		if self.request.user.user_profile.user_type == "empleado":
			self.object.assigned_to = None
		else:
			self.object.assigned_to = self.request.user.user_profile
		self.object.project = project
		super(PersonCreateView, self).form_valid(form)
		return HttpResponseRedirect(
			reverse(
				"create_quotation",
				kwargs={
					"project_slug": project.slug,
					"person_slug": self.object.slug,
					"unity_slug": self.object.unity_of_interest
				}
			)
		)

	def get_form(self, form_class=None):
		project = self.get_project()
		if form_class is None:
			form_class = self.get_form_class()
		return form_class(project, **self.get_form_kwargs())

	def get_success_url(self):
		project = self.get_project()
		return reverse(
				"create_quotation",
				kwargs={
					"project_slug": project.slug,
					"person_slug": self.object.slug,
					"unity_slug": self.object.unity_of_interest
				}
			)

@method_decorator(login_required, name='dispatch')
class PersonDetailView(SlugManagerMixin, DetailView):
	model = Person
	slug_field = "slug"
	slug_url_kwarg = "person_slug"
	context_object_name = "person"

	def get_object(self):
		if self.slug_url_kwarg:
			slug = self.kwargs.get(self.slug_url_kwarg)
			project = self.get_project()
			queryset = get_object_or_404(Person, project=project, slug__iexact=slug)
			return queryset
		else:
			raise AttributeError(
				"Generic DetailView %s must be called with either an object pk or slug" % self.__class__.__name__
			)

	def get_context_data(self, *args, **kwargs):
		context = super(PersonDetailView, self).get_context_data(*args, **kwargs)
		activity_form = ActivityModelForm()
		note_form = NoteModelForm()
		project = self.get_project()
		context.update({'activity_form': activity_form, 'note_form':note_form, 'project':project})
		return context


class OrderLogViewSet(SlugManagerMixin,
					  CreateModelMixin,
					  ListModelMixin,
					  RetrieveModelMixin,
					  UpdateModelMixin,
                      viewsets.GenericViewSet,
					 ):
	model_class = OrderLog
	serializer_class = OrderLogSerializer
	lookup_field = "pk"
	queryset = OrderLog.objects.all()

	def list(self, *args, **kwargs):
		project = self.get_project()
		qs = self.get_queryset()
		qs.filter(project=project, salesperson=self.request.user.user_profile)
		serializer = self.serializer_class(qs, many=True)
		return Response(serializer.data)

	def perform_update(self, serializer, **kwargs):
		serializer.save()

@method_decorator(login_required, name='dispatch')
class PreSaleInitialView(SlugManagerMixin, ListView):
	model = Activity

	def get_queryset(self):
		project = self.get_project()
		user_profile = self.request.user.user_profile
		today = datetime.today()
		activities = Activity.objects.filter(
			project=project,
			assigned_to=user_profile,
			scheduled_date__year=today.year,
			scheduled_date__month=today.month,
			scheduled_date__day = today.day,
		)
		return activities

@method_decorator(login_required, name='dispatch')
class OfficeInitialView(SlugManagerMixin, TemplateView):
	template_name = "persons/office_initial.html"
