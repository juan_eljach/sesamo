const gulp = require('gulp')
const livereload = require('gulp-livereload')

const jsDir = 'static/js/*.js'
const cssDir = ['static/css/styles.css', 'static/css/base.css']
const htmlDir = ['templates/persons/*.html', 'static/templates/*.html', 'templates/userena/*.html', 'templates/invitations/*.html']

gulp.task('default', function () {
  livereload.listen()

  gulp.watch(cssDir, function () {
    gulp.src(cssDir).pipe(livereload())
  })

  gulp.watch(jsDir, function () {
    gulp.src(jsDir).pipe(livereload())
  })

  gulp.watch(htmlDir, function () {
    gulp.src(htmlDir).pipe(livereload())
  })
})