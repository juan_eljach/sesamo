from django.conf.urls import patterns, url
from .views import get_pdf, get_doc

urlpatterns = patterns('',
	url(r"^testpdf/$", get_pdf, name="pdf_test"),
	url(r"^lola/$", get_doc, name="sdtest"),
)