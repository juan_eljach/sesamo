from django.db import models
from units.models import Unity

from tinymce.models import HTMLField


category_choices = (
	("separacion", "Separación"),
	("formalizacion", "Formalización"),
	("cobro", "Cobro"),
	("cartera", "Cartera"),
)

class Document(models.Model):
	name = models.CharField(max_length=300)
	category = models.CharField(max_length=50, choices=category_choices)
	unity = models.ForeignKey(Unity)
	requires_return = models.BooleanField(default=False)
	expected_return_date = models.DateField(blank=True, null=True)
	departure_date = models.DateField(blank=True, null=True)
	timestamp = models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return "{}. Unidad: {}".format(self.name, self.unity)

class Template(models.Model):
	name = models.CharField(max_length=300)
	category = models.CharField(max_length=50, choices=category_choices)
	html_snippet = HTMLField()

	def __str__(self):
		return self.name
