from django.conf import settings
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext
from django.template.loader import get_template
from weasyprint import HTML, CSS

from formalizations.models import PaymentAgreement
from .models import Template
from .forms import TemplateForm
from .serializers import TemplateSerializer
from django.shortcuts import render, get_object_or_404

from rest_framework import generics

from units.models import Unity

import os
from os.path import abspath, basename, dirname, join, normpath
from sys import path
from django.core.urlresolvers import reverse_lazy

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

DJANGO_ROOT = dirname(dirname(abspath(__file__)))

SITE_ROOT = dirname(DJANGO_ROOT)

SITE_NAME = basename(DJANGO_ROOT)

path.append(DJANGO_ROOT)

class TemplateDetailView(generics.RetrieveUpdateAPIView):
    queryset = Template.objects.all()
    model_class = Template
    serializer_class = TemplateSerializer
    lookup_field = "id"
    lookup_url_kwarg = "template_id"

def get_doc(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = TemplateForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            form.save()
            # redirect to a new URL:
            return HttpResponseRedirect('/thanks/')

    # if a GET (or any other method) we'll create a blank form
    else:
        html_template = get_template("documents/compraventa.html")
        #unity_pk = request.GET.get("unity")
        #unity = get_object_or_404(Unity, pk=unity_pk)
        #payment_agreement = PaymentAgreement.objects.get(unity=unity)
        payment_agreement = PaymentAgreement.objects.all()[0]
        persons = payment_agreement.persons.all()
        city_of_residence = persons.first().city_of_residence
        project_name = payment_agreement.unity.project
        data_dict = {
            'persons': persons,
            'city_of_residence': city_of_residence,
            'project_name': project_name
        }
        rendered_html = html_template.render(RequestContext(request, data_dict)).encode(encoding="UTF-8")
        template = Template.objects.create(name="caucho", category="separacion", html_snippet=rendered_html)
        form = TemplateForm(initial={"html_snippet":rendered_html})
        print ("JUST CREATED THE FORM. GONNA PRINT THE WHOLE FORM: {}".format(form))
        print ("NOW GONNA PRINT JUST THE INITIAL HTML_SNIPPET: {}".format(form["html_snippet"].value()))
    return render(request, 'name.html', {'form': form})


def get_pdf(request):
    html_template = get_template("documents/tiny.html")
    #pa = PaymentAgreement.objects.all()[0]
    #persons = pa.persons.all()
    #city_of_residence = persons.first().city_of_residence
    #project_name = pa.unity.project
    #data_dict = {
    #    'persons': persons,
    #    'city_of_residence': city_of_residence,
    #    'project_name': project_name
    #}

    rendered_html = html_template.render(RequestContext(request)).encode(encoding="UTF-8")

    styles = normpath(join(DJANGO_ROOT, 'static/css/compraventastyles.css'))
    pdf_file = HTML(string=rendered_html).write_pdf(stylesheets=[CSS(filename=styles)])
    http_response = HttpResponse(pdf_file, content_type='application/pdf')
    http_response['Content-Disposition'] = 'filename="testpdf.pdf"'
    return http_response

