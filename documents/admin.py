from django.contrib import admin
from .models import Document, Template

admin.site.register(Document)
admin.site.register(Template)
