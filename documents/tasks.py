import datetime
from documents.models import Document
from celery.decorators import task, periodic_task
from celery.task.schedules import crontab
from sesamo.utils import SendEmail

#def create_document_and_send_email(name, category, unity, to_email, to_name, subject, template_name):
#	document_created = Document.objects.create(name=name, category=category, unity=unity)
#	if document_created:
#        SendEmail()
#		return True

"""
@periodic_task(run_every=(crontab(minute=0, hour=7)), name="daily_check_in_payments_for_notifications", ignore_result=True)
#@periodic_task(run_every=(crontab(minute='*/2')), name="daily_check_of_activities_in_persons", ignore_result=True)
def CheckPaymentsInDebt():
    today = datetime.datetime.today().date()
    time_delta_first_notification = datetime.timedelta(days=30)
    time_delta_second_notification = datetime.timedelta(days=45)
    time_delta_third_notification = datetime.timedelta(days=60)
    from formalizations.models import PaymentAgreement
    for payment_agreement in PaymentAgreement.objects.all():
        for payment_fee in payment_agreement.payment_fees.all():
            if (payment_fee.date_to_pay < today) and payment_fee.paid == False or payment_fee.partially_paid == True:
            	existing_documents = Document.objects.filter(category="cartera", unity=payment_agreement.unity)
            	if today - payment_fee.date_to_pay == time_delta_first_notification:
            		notification_name = "Notificacion de Mora No. 1"
            		in_queue_to_send = True
            	elif today - payment_fee.date_to_pay == time_delta_second_notification:
            		notification_name = "Notificacion de Mora No. 2"
            		in_queue_to_send = True
            	elif today - payment_fee.date_to_pay == time_delta_third_notification:
            		notification_name = "Notificacion de Mora No. 3"
            		in_queue_to_send = True
            	else:
            		in_queue_to_send = False
            		pass

            	if in_queue_to_send:
                    primary_person = payment_agreement.persons.first()
                    to_email = primary_person.email
                    to_name = primary_person.first_name
            		create_document_and_send_email(notification_name, "cartera", payment_agreement.unity, to_email, to_name)
            else:
            	pass
"""