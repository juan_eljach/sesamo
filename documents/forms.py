from django import forms
from .models import Template
from tinymce.widgets import TinyMCE

class TemplateForm(forms.ModelForm):
    html_snippet = forms.CharField(widget=TinyMCE(attrs={'cols': 200, 'rows': 60}))
    class Meta:
        model = Template
        fields = ["name", "html_snippet"]