from rest_framework import serializers
from accounts.serializers import UserProfileSerializer
from .models import Note

class NoteSerializerCreate(serializers.ModelSerializer):
	class Meta:
		model = Note
		fields = (
			'unity_profile',
			'note',
			'written_by'
		)

class NoteSerializerDetail(serializers.ModelSerializer):
	written_by = UserProfileSerializer(read_only=True)
	class Meta:
		model = Note
		fields = (
			'unity_profile',
			'note',
			'written_by',
			'timestamp',
		)