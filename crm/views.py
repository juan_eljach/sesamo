from django.shortcuts import render
from rest_framework import viewsets
from rest_framework.response import Response
from sesamo.mixins import SlugManagerMixin
from .models import Note
from .serializers import NoteSerializerCreate, NoteSerializerDetail

class NotesViewSet(SlugManagerMixin, viewsets.ModelViewSet):
	model_class = Note
	create_serializer =  NoteSerializerCreate
	serializer = NoteSerializerDetail

	def get_queryset(self):
		unity = self.get_unity()
		return self.model_class.objects.filter(unity_profile__unity=unity)

	def list(self, request, *args, **kwargs):
		queryset = self.get_queryset()
		serializer = self.serializer(queryset, many=True)
		return Response(serializer.data)

	def get_serializer_class(self):
		if self.action == "create":
			return self.create_serializer