from django.conf import settings
from django.db import models
from units.models import Unity

class UnityProfile(models.Model):
	unity = models.OneToOneField(Unity)

	def __str__(self):
		return self.unity.name

activity_types = (
	("llamada","Hacer llamada"),
	("email","Enviar correo"),
	("reunion","Reunion")
)

class Activity(models.Model):
	done = models.BooleanField(default=False)
	unity_profile = models.ForeignKey(UnityProfile, related_name="activities")
	activity_type = models.CharField(max_length=30, choices=activity_types)
	description = models.TextField(max_length=1000)
	scheduled_date = models.DateTimeField()
#	assigned_to = models.ForeignKey(settings.AUTH_PROFILE_MODULE, blank=False, null=False, related_name="assigned_to")
	timestamp = models.DateTimeField(auto_now_add=True)
	checked_as_done_timestamp = models.DateTimeField(null=True, blank=True)

class Note(models.Model):
	unity_profile = models.ForeignKey(UnityProfile, related_name="notes")
	note = models.TextField(max_length=2000)
	written_by = models.ForeignKey(settings.AUTH_PROFILE_MODULE)
	timestamp = models.DateTimeField(auto_now_add=True)