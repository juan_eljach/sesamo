(function () {

  var pslug = window.location.href.split('/')[4]

  function getCsrfToken(name){
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
  }

  var $landingSupportIdentity = $('#landingSupportIdentity'),
      $landingSupportOwner = $('#landingSupportOwner'),
      $landingSupportTower = $('#landingSupportTower'),
      $landingSupportApto = $('#landingSupportApto'),
      $landingSupportUnity = $('#landingSupportUnity'),
      $landingSupportType = $('#landingSupportType'),
      $landingSupportPriority = $('#landingSupportPriority'),
      $landingSupportTitle = $('#landingSupportTitle'),
      $landingSupportDescription = $('#landingSupportDescription'),
      $landingSupportFile = $('#landingSupportFile'),
      $landingSupportSend = $('#landingSupportSend'),
      $landingSupportSearchOwner = $('#landingSupportSearchOwner'),
      $landingSupportForm = $('#landingSupportForm'),
      $landingSupportOwnerNoResults = $('#landingSupportOwnerNoResults'),
      $landingSupportAptoNoResults = $('#landingSupportAptoNoResults')

  var clientUnits = []

  $landingSupportSearchOwner.on('click', function(e) {

    var landingSupportIdentityValue = $landingSupportIdentity.val()
    var URL_CLIENT = '/api/projects/callejuelas/support/client/?identity=' + landingSupportIdentityValue

    if(landingSupportIdentityValue){

      $landingSupportIdentity.val('')
      $landingSupportIdentity.css({border: '1px solid #e5e5e5'})
      $landingSupportOwner.focus()

      $.ajax({
        url: URL_CLIENT
      })
      .done((data) => {

        clientUnits = _.uniq(_.map(data, 'name'))

        if(data.length > 0) $landingSupportOwnerNoResults.css({display: 'none'})

        $landingSupportForm.css({display: 'block'})

      })

      .fail((error)=> {
        console.log(error)
        $landingSupportOwnerNoResults.css({display: 'block'})
      })
    }

    else $landingSupportIdentity.css({border: '1px solid #FF8A80'})

  })

  $landingSupportSend.on('click', function(e){
    e.preventDefault()

    var unityName = 'T' + $landingSupportTower.val() + '-' + $landingSupportApto.val()

    console.log(unityName, clientUnits);

    isUnitFound = _.some(clientUnits, (unity) => unity === unityName )

    if(!isUnitFound) return $landingSupportAptoNoResults.css({display: 'block'})

    console.log(isUnitFound);

    var formData = new FormData($('#landingSupportForm'))

    formData.append('administrators', [])
    formData.append('attached_file', $('#landingSupportFile')[0].files[0])
    formData.append('client', '?')
    formData.append('collaborators', [])
    formData.append('description', $landingSupportDescription.val())
    formData.append('priority', $landingSupportPriority.val())
    formData.append('ticket_type', $landingSupportType.val())
    formData.append('status', '?')
    formData.append('title', $landingSupportTitle.val())
    formData.append('unity', $landingSupportUnity.val())

    $.ajax({
      url: '/api/projects/callejuelas/support/tickets/',
      method: 'PUT',
      data: formData,
      headers: {
        'X-CSRFToken': getCsrfToken('csrftoken')
      },
      processData: false,
      contentType: false
    })
    .done((data) => {
      console.log(data)
    })
    .fail((error)=> console.log(error))

  })


})();
