
var app = angular.module('sesamoapp', ["ngRoute", "ui.bootstrap", "ngMaterial"]);

var Open = false;

app.config(['$httpProvider', '$interpolateProvider', function($httpProvider, $interpolateProvider) {
    $interpolateProvider.startSymbol('{$');
    $interpolateProvider.endSymbol('$}');

    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
}]);

app.config(['$routeProvider', function($routeProvider) {

    $routeProvider
    	.when('/', {
	      //templateUrl: '/templates/persons/activity_list.html',
	      controller: 'mainController'
	    })
      .when('/', {
          templateUrl : '/static/templates/segmentation_splash_screen.html',
          controller  : 'mainController'
      })
      .when('/segmentation/:slug?', {
          templateUrl : '/static/templates/segmentation_list.html',
          controller  : 'segmentController'
      })
      .when('/segmentation/:slug/:pslug?', {
          templateUrl : '/static/templates/segmentation_list.html',
          controller  : 'segmentController'
      })
      .when('/unitiestable/:slug?', {
          templateUrl : '/static/templates/unities_table.html',
          controller  : 'unitiesTableController'
      })
      .when('/formalization/:slug?', {
          templateUrl : '/static/templates/formalization.html',
          controller  : 'formalizationController'
      })
      .when('/formalization/:slug?/new_payment', {
          templateUrl : '/static/templates/formalization_newpayment.html',
          controller  : 'formalizationController'
      })
      .when('/person/create', {
          templateUrl : '/static/templates/add_person.html',
          controller  : 'createPersonController'
      })

        /*.otherwise({
	      redirectTo: '/'
	    });*/
}]);

app.controller('createPersonController', ['$scope', function($scope) {
  $('.bootstrap-select').selectpicker({
    style: '',
    width: '100%',
    size: 8
  });

  $scope.message = 'Soy data desde el controller'

  $('#id_segment').val('low').change()

}]);

app.controller('formalizationController', ['$scope', '$mdDialog', '$routeParams', 'httpService', function($scope, $mdDialog, $routeParams, httpService) {
  var slug = $routeParams.slug
  var new_payment = '/projects/'+ slug +'/presale/#/formalization/' + slug + '/new_payment';
  $('#new_payment').attr('href', new_payment)

  $scope.tab = 1

  $scope.selectTab = function(tabP){
    $scope.tab = tabP;
    if(tabP === 2) {
      $scope.addClientName = ''
      $scope.addClientLastName = ''
      $scope.addClientEmail = ''
      $scope.addClientCellphone = ''
      $scope.addClientIdentify = ''
      $scope.addClientPlaceExpedition = ''
      $scope.addClientBirthday = ''
      $scope.addClientPhone = ''
      $scope.addClientNotifPhone = ''
      $scope.addClientAdress = ''
    }
  }

  $scope.tabSelected = function(t){
    return t === $scope.tab
  }


  $scope.newClient = {}
  $scope.addedClients = []

  $scope.basicInfoClose = function(id){
    _.remove($scope.addedClients, function(el){
      return el.id == id
    })
  }

  $scope.addClientSave = function(){

    var $addClientInputs = $('.formalizationPaymentPlanModal-cont input')

    var isFillAllInputs = _.every($addClientInputs, function(el, i){
      return el.value !== ''
    })

    var clientCount = 0

    $scope.isNotFill = false;

    if(isFillAllInputs){

      var URL_PUT  = '/api/projects/' + slug + '/formalizations/person/' + $scope.infoPersonSelected.slug + '/small-update/';

      // endpoint para traer el apartamento /api/projects/slug/unities

      $scope.newClient.first_name = $scope.addClientName;
      $scope.newClient.last_name = $scope.addClientLastName;
      $scope.newClient.identity = $scope.addClientName;
      $scope.newClient.expedition_place = $scope.addClientName;
      $scope.newClient.email = $scope.addClientEmail;
      $scope.newClient.birthday = moment($scope.birthdayClient).format('YYYY-MM-DD');
      $scope.newClient.cellphone = $scope.addClientCellphone;
      $scope.newClient.notification_phone = $scope.addClientNotifPhone;
      $scope.newClient.notification_address = $scope.addClientAdress;
      $scope.newClient.is_client = true;
      $scope.newClient.slug = $scope.infoPersonSelected.slug;

      _.forEach($addClientInputs, function(el){
        el.value = ''
      })

      $scope.formalizationPerson = ''

      httpService.getData('PUT', URL_PUT, $scope.newClient)
        .then(function(data){

          $scope.addedClients.push(data)
        })
        .catch(function(err){console.log(err)})

      $('.formalizationPaymentBgPopup').fadeOut(200)

    }

    else{
      $scope.isNotFill = true;
    }

  }

  $scope.formalizationPaymentPersonSelected = false

  $scope.formalizationSearchPerson = function(inputPerson){

    if(inputPerson == ''){
      $scope.formalizationPaymentPersonSelected = true
    }else{
      $scope.formalizationPaymentPersonSelected = false
    }

    var URL = '/api/projects/'+ slug + '/persons?search=' + inputPerson

    httpService.getData('GET', URL)
      .then(function(d){
        $scope.formalizationPaymentPersons = d
      })
      .catch(function(err){console.log(err)})
  }

  $scope.selectPersonFormalizationPayment = function(person){

    $scope.infoPersonSelected = person

    $scope.formalizationPaymentPersonSelected = true
    $scope.addClientName = person.first_name
    $scope.addClientLastName = person.last_name
    $scope.addClientEmail = person.email
    $scope.addClientCellphone = person.cellphone
    $scope.addClientIdentify = person.identity
    $scope.addClientPlaceExpedition = person.expedition_place
    $scope.addClientBirthday = person.birthday
    $scope.addClientPhone = person.phone
    $scope.addClientNotifPhone = person.notification_phone
    $scope.addClientAdress = person.notification_address

    $scope.perSelected = person
  }

  $scope.formalizationAddClientClose = function(){
    $('.formalizationPaymentBgPopup').fadeOut(200)
    $('.formalizationPayment').css({display:'block'})
  }

  $scope.showAddClient = function(ev) {
    $('.formalizationPaymentBgPopup').fadeIn(200)
    $('.formalizationPaymentBgPopup').css({display: 'flex'})
    if($(window).width() < 768) $('.formalizationPayment').css({display:'none'})
  };

  $scope.showValidation = function(ev) {
    $mdDialog.show({
      controller: DialogController,
      templateUrl: '/static/templates/validation_formalization.tmpl.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true,
      fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
    })
    .then(function(answer) {
      $scope.status = 'You said the information was "' + answer + '".';
    }, function() {
      $scope.status = 'You cancelled the dialog.';
    });
  };

}]);

app.controller('modaalController', ['$scope', '$mdDialog', function($scope, $mdDialog) {

  function DialogController($scope, $mdDialog) {
    $scope.hide = function() {
      $mdDialog.hide();
    };

    $scope.cancel = function() {
      $mdDialog.cancel();
    };

    $scope.answer = function(answer) {
      $mdDialog.hide(answer);
    };
  }

  $scope.showAdvanced = function(ev) {
    $mdDialog.show({
      controller: DialogController,
      templateUrl: '/static/templates/configurations.tmpl.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true,
      fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
    })
    .then(function(answer) {
      $scope.status = 'You said the information was "' + answer + '".';
    }, function() {
      $scope.status = 'You cancelled the dialog.';
    });
  };

}]);

app.controller('mainController', ['$scope', '$http', 'httpService', function($scope, $http, httpService) {

  $scope.m = window.moment;

  httpService.getData('GET', '/api/user/')
    .then(function(user){
      $scope.userLogged = user
    })

  var slugP = window.location.pathname.replace('/projects/', '').replace('/presale/', ''),
      URI  = '/api/projects/'+slugP+'/activities/today';

  $('#crePer').attr('href', '/projects/' + slugP + '/presale/#/');

  $http({
	  method: 'GET',
	  url: URI
	})
  .then(function(response){
    _.forEach(response.data, function(actv, key){
      var extract = actv.person.replace(/(id:\s| name:\s)/g, '').split(',')
      actv.person = parseInt(extract[0])
      actv.name = extract[1]
    })

    $scope.data = response.data;

    $scope.activitiesDone = []

    $scope.data.map(function(activitie){
      if(activitie.done) $scope.activitiesDone.push(activitie)
    })

  })
  .catch(function(err){console.log(err)})

  $scope.activityCheck = function(person, project, activity_id, activity_type, description, scheduled_date) {
    if($scope.activitiesDone) $scope.activitiesDone.push({})

    var d = scheduled_date.substring(0,19).replace('T', ' '),
        URL = '/api/projects/'+slugP+'/activities/update/' + activity_id + '/',
        DATA = {done: true, person: person, project: project, activity_type: activity_type, description: description, scheduled_date: d}

    httpService.getData('PUT', URL, DATA)
      .then(function(data){
      })
      .catch(function(err){console.log(err)})
  }

  $scope.verifyDate = function (a) {
    var test = /hace/.test($scope.m(a.scheduled_date).locale('es').fromNow())
    return test;
  }

}]);

app.controller('unitiesTableController', ['$scope','$routeParams', 'httpService', '$window', '$modal', function($scope, $routeParams, httpService, $window, $modal) {
  $scope.userChangeStatus = false

  httpService.getData('GET', '/api/userprofile')
    .then(function (data) {
      var userType = data.user_type
      if (userType === 'gerente') {
        $scope.userChangeStatus = true
      }
    })

  var colors = ['#81c784', '#9575cd', '#ffd54f', '#29b6f6', '#ff8a80', '#f06292'];

  $scope.getRandomColor = function () {
     return colors[Math.floor(Math.random()*colors.length)];
  }

  var slug = $routeParams.slug;
  $scope.slug = slug
  $scope.hideBar = function(){
    var e = $('#unitiesTable-sideBar')
    e.css({'right':'-300px', transition:'.5s'})
    e.hide(500)
  }
  $scope.hideBar()
  $scope.showBar = function(){
    var e = $('#unitiesTable-sideBar')
    e.show(0)
    e.css({right:'0px', transition:'.5s'})
  }

  $scope.menu = 1;
  $scope.tab = 1;
  $scope.t = true

  $scope.selectMenu = function (menu) {
    if(menu === 2) {
      var URL = '/api/projects/' + slug + '/presales/unity/'+ $scope.unityId +'/virtual-booking-list'

      httpService.getData('GET', URL)
        .then(function(d){

          $scope.interestedPersons = d
        })
    }
    if(menu === 4) $('.newQuotation-header').css({marginTop:'-30px'})
    $scope.menu = menu;
    $scope.newSeparationSucess = false;
  };

  $scope.isActiveMenu = function (menu) {
    return menu === $scope.menu;
  };

  $scope.isActiveTab = function (tab) {
    return tab === $scope.tab;
  };

  $scope.selectTab = function (tab) {
    $scope.tab = tab;
    if(tab == 2 || tab == 1){
      $scope.monthsDeliver = $scope.monthDiff( new Date($scope.currentDate[0], $scope.currentDate[1], $scope.currentDate[2]),
        new Date($scope.futureDate[0], $scope.futureDate[1], $scope.futureDate[2])
      )
      $scope.monthsToDelivery = $scope.monthsDeliver
    }
  };

  // projects/slug/presales/virtual-booking/create
  var URL = '/api/projects/'+ slug +'/unities/';

  httpService.getData('GET', URL)
    .then(function(data){
      var d = {}
      var towers = []
      var floors = []
      var opt = []

      $scope.showAll = true;

      _.forEach(data, function(obj, key){

        $scope.data = data;

        var split = obj.slug.split('-')

        var set = split[1]

        obj.tower = split[0]

        if(set !== undefined && set.length === 3){
          obj.floor = set.substring(0,1)
          obj.apto = set.substring(1,3)
        }
        if(set !== undefined && set.length == 4){
          obj.floor = set.substring(0,2)
          obj.apto = set.substring(2,4)
        }
        towers.push(obj.tower)
        floors.push(obj.floor)
      })

      $scope.towers = _.uniq(towers)

      _.forEach($scope.towers, function(t, key){
        opt[key] = {name : 'Torre ' + t.slice(1,2), value: t}
      })
      opt.push({name:'Todas', value:'all'})
      $scope.towersList = opt

      $scope.twr = {twr: 'all'}

    })
    .catch(function(err){console.log(err)})

    $scope.getFloorsNumberByTower = function (tower) {
      var towers = _.filter($scope.data, {tower: tower})
      var floorsByTower = _.uniq(_.map(towers, t => Number(t.floor)))

      return floorsByTower.sort(function(a,b){ return a - b  })
    }

    $scope.filterTower =  function(tower) {

      if(tower === 'all'){$scope.showAll = true}
      else{$scope.showAll = false}

      $scope.hideBar()
      $scope.dataByTower = _.filter($scope.data, {tower: tower})

      $scope.floorsNumber = []

      _.forEach($scope.dataByTower, function(f, k){
        $scope.floorsNumber.push(f.floor)
      })

      $scope.floorsNumber = _.uniq($scope.floorsNumber.sort(function(a,b){ return a - b  }))

      $scope.filterUnities = function (floor) {
        return _.filter($scope.dataByTower, {floor: floor})
      }
    }

    $scope.floorsNumberAll = function(tower){

      var dataByTower = _.filter($scope.data, {tower: tower})
      dataByTower = _.orderBy(dataByTower, ['floor'])

      var floorsNumber = []
      _.forEach(dataByTower, function(f, k){
        floorsNumber.push(f.floor)
      })

      return _.uniq(floorsNumber.sort(function(a,b){ return a - b }))
    }

    $scope.filterUnitiesAll = function(floor, tower){
      var dataByTower = _.filter($scope.data, {tower: tower})
      var s =  _.filter(dataByTower, {floor: floor})
      return s
    }

  $scope.unity_selected = ''
  $scope.showUnity = function(unity, e){
    $scope.unity_element = e.target;

    $scope.unity_selected = unity.id;
    $scope.showBar()
    $scope.unityTitle = unity.floor + unity.apto
    $scope.uPrice = unity.price
    $scope.unityPriceNormal = unity.price
    $scope.unityPrice = unity.price.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")
    $scope.unityBedRooms = unity.bedrooms
    $scope.unityBathRooms = unity.bathrooms
    $scope.unityBuiltArea = unity.built_area
    $scope.unityPrivateArea = unity.private_area
    $scope.unityId = unity.id
    $scope.unityName = unity.name
    $scope.unityTower = unity.name.slice(1, 2)
    $scope.unitySlug = unity.slug
    $scope.unitytimeStampStatus = unity.timestamp_for_status
    $scope.unityStatusValue = unity.status
    $scope.unityInterestedPersons = unity.interested_persons
    $scope.deliveryDate = unity.delivery_date
    $scope.futureDate = $scope.deliveryDate.split('-')

    $scope.statusOptions = [
      {name: 'Separado tent', value: 'virtually_booked'},
      {name: 'Separado', value: 'booked_not_formalized'},
      {name: 'Disponible', value: 'available'},
      {name: 'Vendido', value: 'sold'},
      {name: 'Legalizado', value: 'legalized'}
    ]

    $scope.unityStatusName = _.filter($scope.statusOptions, {value: $scope.unityStatusValue})[0].name
    $scope.unityStatus = {status: _.filter($scope.statusOptions, {value: $scope.unityStatusValue})[0].value}

    var URL = '/api/projects/' + slug + '/presales/unity/'+ $scope.unityId +'/virtual-booking-list'

    httpService.getData('GET', URL)
      .then(function(d){

        var expDate = d.timestap
        $scope.interestedPersons = d
      })
  }

  $scope.unityChangeStatus = function(a){
    var URL = '/api/projects/' + slug + '/unities/' + $scope.unityId + '/'
    var $saleInformationModal = $('#saleInformationModal')


    var DATA = {name: $scope.unityName, price: $scope.unityPriceNormal, status:a.status,
                timestamp_for_status: $scope.unitytimeStampStatus, delivery_date: $scope.deliveryDate,
                slug: $scope.unitySlug}
    httpService.getData('PUT', URL, DATA)
      .then(function(results){

        if (results.status === 'virtually_booked') {
          $($scope.unity_element).css({background: '#29b6f6', color: '#fff'})
        }

        if (results.status === 'sold') {
          $($scope.unity_element).css({background: '#81C784', color: '#fff'})
        }
      })
  }

  $scope.newSeparationNoResultsMsg = false
  $scope.searchPersonVirtual = function(person){
    $scope.personSelected = false
    $scope.newSeparationSucess = false

    if(person == ''){
      $scope.personSelectedNormalQuotation = true
      $scope.personVirtualSelected = true
    }else{
      $scope.personVirtualSelected = false
    }
    var URL = '/api/projects/'+ slug + '/persons?search=' + person

    httpService.getData('GET', URL)
      .then(function(d){
        $scope.newSeparationPersonVirtual = d
        if( d.length === 0 ) $scope.newSeparationNoResultsMsg = true
        else{$scope.newSeparationNoResultsMsg = false}
      })
      .catch(function(err){console.log(err)})
  }

  $scope.searchPersonNormal = function(person){
    $scope.personSelected = false
    $scope.newSeparationSucess = false

    if(person == ''){
      $scope.personSelectedNormalQuotation = true
    }else{
      $scope.personSelectedNormalQuotation = false
    }
    var URL = '/api/projects/'+ slug + '/persons?search=' + person

    httpService.getData('GET', URL)
      .then(function(d){
        $scope.newSeparationPersonNormal = d.map(function (p) {
          return _.merge(p, {color: $scope.getRandomColor()})
        })
      })
      .catch(function(err){console.log(err)})
  }

  $scope.personSelected = false
  $scope.newSeparationSucess = false

  $scope.selectPerson = function(person){
    $scope.personVirtualSelected = true
    if(person == ''){
      $scope.personSelected = false
    }else{
      $scope.personSelected = true
    }
    $scope.newSeparationSucess = false
    $scope.selectPersonName = person.first_name
    $scope.selectPersonLastName = person.last_name
    $scope.selectPersonCellPhone = person.cellphone
    $scope.selectPersonEmail = person.email
    $scope.selectPersonId = person.id
    $scope.selectPersonAssignedTo = person.assigned_to

  }

  $scope.closeChip = function () {
    $scope.personSelected = false
    $scope.person = ''
  }

  $scope.personSelectedNormalQuotation = false

  $scope.selectPersonNormalQuotation = function(person){

    $scope.personSelectedNormalQuotation = true
    $scope.selectedPersonNormalQuotation = person

    $scope.normalQuotationSubsidy = person.subsidy.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")
    $scope.normalQuotationSavings = person.savings.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")
    $scope.normalQuotationMonthIncome = person.monthly_income.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")
    $scope.normalQuotationSeverance = person.severance.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")
    $scope.normalQuotationOther = person.others.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")
  }

  $scope.normalQuotationInput1 = function(){
    var temp = $scope.normalQuotationSubsidy.split('.').join('')
    $scope.normalQuotationSubsidy = temp.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")
  }

  $scope.normalQuotationInput2 = function(){
    var temp = $scope.normalQuotationSavings.split('.').join('')
    $scope.normalQuotationSavings = temp.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")
  }

  $scope.normalQuotationInput3 = function(){
    var temp = $scope.normalQuotationMonthIncome.split('.').join('')
    $scope.normalQuotationMonthIncome = temp.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")
  }

  $scope.normalQuotationInput4 = function(){
    var temp = $scope.normalQuotationSeverance.split('.').join('')
    $scope.normalQuotationSeverance = temp.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")
  }

  $scope.normalQuotationInput5 = function(){
    var temp = $scope.normalQuotationOther.split('.').join('')
    $scope.normalQuotationOther = temp.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")
  }

  $scope.createVBooking = function(){
    $scope.person = ''
    $scope.personSelected = !$scope.personSelected
    var URL = '/api/projects/' + slug + '/presales/virtual-booking/create/'
    var DATA = {person: $scope.selectPersonId, unity: $scope.unityId, assigned_to: $scope.selectPersonAssignedTo, notification_cellphone:$scope.selectPersonCellPhone, notification_email:$scope.selectPersonEmail}

    httpService.getData('POST', URL, DATA)
      .then(function(d){
        $scope.newSeparationSucess = true;
        $scope.personSelected = false

        $($scope.unity_element).css({background: '#29b6f6', color: '#fff'})
      })
      .catch(function(err){console.log(err)})
  }

  $scope.currentDate = moment(Date.now()).format('YYYY-MM-DD').split('-')

  $scope.monthDiff = function(d1, d2) {
    var months;
    months = (d2.getFullYear() - d1.getFullYear()) * 12;
    months -= d1.getMonth() + 1;
    months += d2.getMonth();
    return months <= 0 ? 0 : months;
  }

  $scope.quickQuotationSavingsInput = function(){
    var temp = $scope.quickQuotationSavings.split('.').join('')

    $scope.quickQuotationSavings = temp.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")
  }

  $scope.quickquotationDone = false
  $scope.createQuickQuotation = function(){

    var URL = '/api/projects/' + slug + '/presales/quickquotation/create/'
    var DATA = { email: $scope.quickQuotationEmail, first_name: $scope.quickQuotationName,
                  last_name: $scope.quickQuotationLastName, price: $scope.uPrice,
                  months_to_deliver: $scope.monthsDeliver, savings: $scope.quickQuotationSavings.split('.').join(''),
                  sent: false, unity: $scope.unityId
               }

    httpService.getData('POST', URL, DATA)
      .then(function(d){
        $scope.quickquotationDone = true
        $scope.quickquotationId = d.id

      })
      .catch(function(err){console.log(err)})
  }

  $scope.newQuotation = function(){
    $scope.quickquotationDone = false
    $scope.quickQuotationName = ''
    $scope.quickQuotationLastName = ''
    $scope.quickQuotationEmail = ''
    $scope.quickQuotationSavings = ''
    $scope.quickQuotationPrice = ''
  }

  $scope.normalQuotationSelectedPerson = function(p){
    $scope.normalQuotationPersonName = p.first_name
    $scope.normalQuotationPersonLastName = p.last_name
    $scope.normalQuotationPersonInitial = p.first_name[0].toUpperCase() + ' ' + p.last_name[0].toUpperCase()
    $scope.normalQuotationPersonId = p.id
    $scope.normalQuotationisSelected = true
    $scope.normalQuotationPersonInitialColor = p.color
  }

  $scope.selectedClose = function(){
    $scope.normalQuotationisSelected = false
    $scope.normalQuotationPerson = ''
  }

  $scope.normalizeValue = function (value) {
    if (typeof value === 'number') return value;

    if (value) return Number(value.split('.').join(''))

    return 0;
  }

  $scope.createNormalQuotation = function(){
    var URL = '/api/projects/' + slug + '/presales/quotation/create/'
    var DATA = {
      person: $scope.normalQuotationPersonId,
      unity: $scope.unityId,
      subsidy: $scope.normalizeValue($scope.normalQuotationSubsidy),
      savings:$scope.normalizeValue($scope.normalQuotationSavings),
      severance: $scope.normalizeValue($scope.normalQuotationSeverance),
      others: $scope.normalizeValue($scope.normalQuotationOther),
      monthly_income: $scope.normalizeValue($scope.normalQuotationMonthIncome),
      months_to_deliver: $scope.normalizeValue($scope.monthsToDelivery)
    }

    httpService.getData('POST', URL, DATA)
      .then(function(d){
        $scope.normalQuotationisSelected = false
        $scope.normalquotationDone = true
        $scope.normalquotationId = d.id
      }).catch(function(err){console.log(err)})
  }

  $scope.newNormalQuotation = function(){
    $scope.normalquotationDone = false
    $scope.normalQuotationOther = ''
    $scope.normalQuotationSubsidy = ''
    $scope.normalQuotationSavings = ''
    $scope.normalQuotationMonthIncome = ''
    $scope.normalQuotationSeverance = ''
    $scope.normalQuotationPerson = ''
  }


  /*
    saleInformation
   */

  $scope.openSaleInfoModal = function () {
  }

}]);

app.controller('segmentController', ['$rootScope', '$scope','$http','$routeParams','$modal', function($rootScope, $scope, $http, $routeParams, $modal)  {
  $('.actividades').css('display','none');
  $('.page-content').addClass('table');

	var slug = $routeParams.slug;

  $('#crePer').attr('href', '/projects/' + slug + '/presale/#/');

  var personslug = $routeParams.pslug;

  $scope.slug = slug;

  if(slug == 'persona')
  {
    var actual_url = window.location.href;
    var proj_slug = actual_url.split('/');
    slug = proj_slug[4];

  }

  if(personslug != undefined)
  {
    if(Open == false)
    {
      var idPer = personslug;
      $modal.open({
          templateUrl: 'modal.html',
          controller: 'modalController',
          resolve: {
            item: function () {
               return idPer;
            }
          }
      })
      .result.then(function() {

      }, function() {
      });

      Open = true;
    }

  }
  else
  {

  }

  var URL = '/api/projects/'+slug+'/segmentation/';

	var crslug = '/projects/' + slug + '/person/create/';
	$('.creNewPer').attr('href', crslug);

    $rootScope.cardsH = [];
    $rootScope.cardsM = [];
    $rootScope.cardsP = [];
    $scope.cardsV = [];
    $rootScope.cardsPo = [];

    $scope.cardAll = [];

    var colors = Array('green','violet','yellow','blue','red');

	$http({
	  method: 'GET',
	  url: URL
	}).then(function successCallback(response) {
	    // this callback will be called asynchronously
	    // when the response is available
	    $scope.data = response.data;

	    angular.forEach(response.data, function(value, key){
	    	//console.log('key: '+ key + ': ' + value.email + ' -- ' + value.segment);
	    	var color = colors[Math.floor(Math.random()*colors.length)];
	    	var initials = value.first_name.charAt(0) + '' + value.last_name.charAt(0);

  	    var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
  			var firstDate = new Date(value.timestamp);
  			var secondDate = new Date();

  			var date = 'Ult Interacción hace '+Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)))+ ' dia';

        if(value.assigned_to !== null){
          $scope.assignedTo = value.assigned_to
        }

			  if(value.segment == 'low')
	    	{
          if(value.sold == false && value.assigned_to != null)
          {
            $scope.cardsP.push({project: value.project, id: value.id, slug: value.slug, date: date, initials: initials, color: color, first_name: value.first_name, last_name: value.last_name, phone: value.phone, cellphone: value.cellphone, unity_of_interest: value.unity_of_interest, adquisition_channel: value.adquisition_channel, email: value.email, segment: value.segment});
            $scope.cardAll.push({project: value.project, id: value.id, slug: value.slug, date: date, initials: initials, color: color, first_name: value.first_name, last_name: value.last_name, phone: value.phone, cellphone: value.cellphone, unity_of_interest: value.unity_of_interest, adquisition_channel: value.adquisition_channel, email: value.email, segment: value.segment});
          }

	    	}
	    	else if(value.segment == 'medium')
	    	{
          if(value.sold == false && value.assigned_to != null)
          {
            $scope.cardsM.push({project: value.project, id: value.id, slug: value.slug, date: date, initials: initials, color: color, first_name: value.first_name, last_name: value.last_name, phone: value.phone, cellphone: value.cellphone, unity_of_interest: value.unity_of_interest, adquisition_channel: value.adquisition_channel, email: value.email, segment: value.segment});
            $scope.cardAll.push({project: value.project, id: value.id, slug: value.slug, date: date, initials: initials, color: color, first_name: value.first_name, last_name: value.last_name, phone: value.phone, cellphone: value.cellphone, unity_of_interest: value.unity_of_interest, adquisition_channel: value.adquisition_channel, email: value.email, segment: value.segment});
          }
	    	}
	    	else if(value.segment == 'high')
	    	{
          if(value.sold == false && value.assigned_to != null)
          {
	    		 $scope.cardsH.push({project: value.project, id: value.id, slug: value.slug, date: date, initials: initials, color: color, first_name: value.first_name, last_name: value.last_name, phone: value.phone, cellphone: value.cellphone, unity_of_interest: value.unity_of_interest, adquisition_channel: value.adquisition_channel, email: value.email, segment: value.segment});
           $scope.cardAll.push({project: value.project, id: value.id, slug: value.slug, date: date, initials: initials, color: color, first_name: value.first_name, last_name: value.last_name, phone: value.phone, cellphone: value.cellphone, unity_of_interest: value.unity_of_interest, adquisition_channel: value.adquisition_channel, email: value.email, segment: value.segment});
          }
	    	}

        if(value.sold == true)
        {
           $scope.cardsV.push({project: value.project, id: value.id, slug: value.slug, date: date, initials: initials, color: color, first_name: value.first_name, last_name: value.last_name, phone: value.phone, cellphone: value.cellphone, unity_of_interest: value.unity_of_interest, adquisition_channel: value.adquisition_channel, email: value.email, segment: value.segment});
           $scope.cardAll.push({project: value.project, id: value.id, slug: value.slug, date: date, initials: initials, color: color, first_name: value.first_name, last_name: value.last_name, phone: value.phone, cellphone: value.cellphone, unity_of_interest: value.unity_of_interest, adquisition_channel: value.adquisition_channel, email: value.email, segment: value.segment});
        }

        if(value.assigned_to == null)
        {
           $scope.cardsPo.push({project: value.project, id: value.id, slug: value.slug, date: date, initials: initials, color: color, first_name: value.first_name, last_name: value.last_name, phone: value.phone, cellphone: value.cellphone, unity_of_interest: value.unity_of_interest, adquisition_channel: value.adquisition_channel, email: value.email, segment: value.segment});
           $scope.cardAll.push({project: value.project, id: value.id, slug: value.slug, date: date, initials: initials, color: color, first_name: value.first_name, last_name: value.last_name, phone: value.phone, cellphone: value.cellphone, unity_of_interest: value.unity_of_interest, adquisition_channel: value.adquisition_channel, email: value.email, segment: value.segment});
        }

	    });

	  }, function errorCallback(response) {
	    // called asynchronously if an error occurs
	    // or server returns response with an error status.
	    //console.log(response);
	  });

	//console.log($scope.cards);

	// $scope.handleDrop = function() {
  //       alert('Item has been dropped');
  //   }

  $scope.loadEditForm = function (id) {

      $('.card-info--recently').removeClass('card-info--recently')

      var idPer = id.slug;
      //console.log(idPer);
      $modal.open({
          templateUrl: 'modal.html',
          controller: 'modalController',
          resolve: {
            item: function () {
               return idPer;
            }
          }
      })
      .result.then(function() {

      }, function() {

      });
  };

  $scope.openAddPersonModal = function () {
      $modal.open({
          templateUrl: 'addPersonModal.html',
          controller: 'addPersonModalController',
          resolve: {
		        item: function () {
		           return '';
		        }
	        }
      })
      .result.then(function() {

      }, function() {

      });
  };

}]);

app.controller('addPersonModalController', ['$rootScope', '$scope','$rootScope','$http', '$q', '$routeParams', '$modalInstance','item', '$location', 'httpService', '$modal', '$compile', function($rootScope, $scope, $rootScope, $http, $q, $routeParams, $modalInstance, item, $location, httpService, $modal, $compile) {

  $scope.closeModal = function () {
    $modalInstance.close();
  }

  $scope.loadEditForm = function (id) {
      var idPer = id.slug;

      $modal.open({
          templateUrl: 'modal.html',
          controller: 'modalController',
          resolve: {
            item: function () {
               return idPer;
            }
          }
      })
      .result.then(function() {

      }, function() {

      });
  };

  $rootScope.itemName = item;

  var pslug = $routeParams.slug;

  httpService.getData('GET', '/api/projects/' + pslug + '/info')
    .then(function (results) {
      $scope.projectInfo = results;
    });


  // Submit
  $scope.onSubmit = function (event) {
    var $inputs = $('input.form-element');
    var $selects = $('select.form-element');
    var invalidClassName = 'form-element--invalid';

    function onChangeHandler () {
      var $el = verifyElement($(this));

      var hasInvalid = $el.hasClass(invalidClassName);

      if (hasInvalid) {
        $el.removeClass(invalidClassName);
        $el.parent().find('div.form-element__required').hide();
      }
    }

    // Event handlers
    $inputs.keyup(onChangeHandler)
    $selects.change(onChangeHandler)

    event.preventDefault();

    $inputs.each(function (index) {
      var $el = $(this);

      if (!$el.val()) {
        $el.parent().find('div.form-element__required').show();
        $el.addClass(invalidClassName)
      }
    });

    $selects.each(function (index) {
      var $el = $(this);

      if (!$el.val()) {
        $el.parent().parent().find('div.form-element__required').show();
        $el.parent().addClass(invalidClassName);
      }
    });

    var isValid = $('.form-group .form-element--invalid').length === 0;

    var payload = {
      first_name: $('#id_first_name').val(),
      last_name: $('#id_last_name').val(),
      cellphone: $('#id_cellphone').val(),
      phone: $('#id_phone').val(),
      email: $('#id_email').val(),
      unity_of_interest: Number($('#id_unity_of_interest').val()),
      adquisition_channel: $('#id_adquisition_channel').val(),
      segment: $('#id_segment').val(),
      project: $scope.projectInfo.id,
      assigned_to: ""
    }

    if (isValid) {
      var url = '/api/projects/' + pslug + '/persons/create/';

      httpService.getData('POST', url, payload)
        .then(function(results){
          var initials = results.first_name.charAt(0) + '' + results.last_name.charAt(0);

          results.initials = initials
          results.color = 'blue'
          results.date = 'Ult. interacción hace un momento'
          results.recently = 'true'

          if (results.segment === 'low') {
            $rootScope.cardsP.unshift(results)
          }

          else if (results.segment === 'medium') {
            $rootScope.cardsM.unshift(results)
          }

          else if (results.segment === 'high') {
            $rootScope.cardsH.unshift(results)
          }

          $modalInstance.close();
        })
        .catch(function(err){console.log(err)})
    }
  }

  // Helpers
  function verifyElement ($el) {
    var isSelect = $el.is("select");

    if (isSelect) {
      return $el.parent();
    }

    return $el;
  }
}]);

app.controller('modalController', ['$scope','$rootScope','$http', '$q', '$routeParams', '$modalInstance','item', '$location', 'httpService', '$modal', function($scope, $rootScope, $http, $q, $routeParams, $modalInstance, item, $location, httpService, $modal) {

  $rootScope.itemName = item;

  $scope.m = window.moment
  $scope.segments = ['Poco Probable','Medianamente Probable','Altamente Probable']
  $scope.segment = {segment:''}

  httpService.getData('GET', '/api/user/')
    .then(function(data){
      $scope.seller = data;
    })
    .catch(function(err){console.log(err)})

  $scope.verifyDate = function (a) {
    var test = /hace/.test($scope.m(a.scheduled_date).locale('es').fromNow())
    return test;
  }

  $scope.calculatePoints = function(val){
    if(val) {
      var value =  val.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")
      return value;
    }
  }

	var slugp = $routeParams.slug;
	var URI  = '/api/projects/'+slugp+'/segmentation/'+item+'/';
    $http({
	  method: 'GET',
	  url: URI
	}).then(function successCallback(response) {

      var segm = response.data.segment

      if(segm === 'low') $scope.segment.segment = $scope.segments[0]
      if(segm === 'medium') $scope.segment.segment = $scope.segments[1]
      if(segm === 'high') $scope.segment.segment = $scope.segments[2]

	    $scope.data = response.data;

	  }, function errorCallback(response) {
	    // called asynchronously if an error occurs
	    // or server returns response with an error status.
	  });


	$scope.notes = {};
  $scope.activities = {};

  $scope.hours = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12']
  $scope.hourActivityInput = $scope.hours[1]

  $scope.activityCheck = function(person, project, activity_id, activity_type, description, scheduled_date) {

    var d = scheduled_date.substring(0,19).replace('T', ' '),
        URL = '/api/projects/'+slugp+'/activities/update/' + activity_id + '/',
        DATA = {done: true, person: person, project: project, activity_type: activity_type, description: description, scheduled_date: d}

    httpService.getData('PUT', URL, DATA )
      .then(function(data){

      })
      .catch(function(err){console.log(err)})
  }

  $scope.create = function(id, project) {

    if($('#addNote').hasClass('isActive')) {
      var NOTES_URL  = '/api/projects/'+slugp+'/notes/create/',
          noteDescription = $('#modalDescription').val(),
          firstName = $scope.data.first_name,
          noteDate = moment(Date.now()).fromNow();

      httpService.getData('POST', NOTES_URL, {person: id, note: noteDescription })
        .then(function(data){
          $scope.data.notes.push(data)
          $('#modalDescription').val('')
          // $('#addNote').removeClass('isActive')
          // $('.icon-note').css({color:'#7c7c7c'})
        })
        .catch(function(err){console.log(err)})
    }

    else if ($('#addActivity').hasClass('isActive')) {
      var $typeActivity = $('#typeActivity'),
          $periodActivity = $('#periodActivity'),
          $hourActivity = $('#hourActivity'),
          $minutesActivity = $('#minutesActivity'),
          ACTIVITY_URL = '/api/projects/'+slugp+'/activities/create/',
          activityDescription = $('#modalDescription').val(),
          activityType = $('#typeActivity').val(),
          activityDone = false,
          activityDate = $scope.myDate,
          activityRealized = null,
          hourActivity = $scope.hourActivityInput || '02',
          minutesActivity = $scope.minutesActivityInput || '00'


      if($periodActivity.val() === 'pm') {
        var h = hourActivity
        h = parseInt(h)

        if(h !== 0) {
          h += 12
          hourActivity = h
        }
      }

      if(activityType !== "" && activityDescription !== ""){

        var d = moment(activityDate).format().substring(0,10),
            scheduled_date = d + ' ' + hourActivity + ':' + minutesActivity

        httpService.getData('POST', ACTIVITY_URL, {done: false, person: id, project: project, activity_type: activityType, description: activityDescription, scheduled_date: scheduled_date})
          .then(function(data){
            $scope.data.activities.push(data)
            $scope.addActivityActive = false
            $('#modalDescription').val('')
            $('#addActivity').removeClass('isActive')
            $('#addNote').addClass('isActive')
            $('.modal-panel').fadeOut(100)
            $('.modal-description button').css({top:'80px'})
            $('.modal-notesList').css({marginTop: '55px'})
            $(".modalNotesBlock .modal-actions ul span:nth-child(3)").css({color:'#b1b1b2'})
          })
          .catch(function(err){console.log(err)})
      }
      else {
        alert('debes seleccionar el tipo y descripción de actividad')
      }
    }

    else {
      alert('Tienes que seleccionar agregar nota o actividad')
    }

  }
  $scope.addActivityActive = false;
  $scope.addActivity = function() {
    if(!$scope.addActivityActive) $scope.addActivityActive = !$scope.addActivityActive
    var $addActivity = $('#addActivity'),
        $modalPanel = $('.modal-panel'),
        $notesList = $('.modal-notesList'),
        $iconActivity = $(".modalNotesBlock .modal-actions ul span:nth-child(3)")


    if($addActivity.hasClass('isActive')){
      $addActivity.removeClass('isActive');
      $modalPanel.fadeOut(100);
      $('.modal-description button').css({top:'85px'})
      $notesList.css({marginTop: '55px'})
      $iconActivity.css({color:'#b1b1b2'})

    } else {
      $('#addNote').removeClass('isActive')
      $modalPanel.css({display:'flex'}).fadeIn(200)
      $('#addActivity').addClass('isActive')
      $('.modal-description button').css({top:'150px'})
      $notesList.css({marginTop: '115px'})
      $iconActivity.css({color:'#039be5'})
      $(".modalNotesBlock .modal-actions ul span:nth-child(1)").css({color:'#7c7c7c'})
    }

  }

  $scope.addNote = function() {
    if($scope.addActivityActive) $scope.addActivityActive = !$scope.addActivityActive
    var $addNote = $('#addNote'),
        $notesList = $('.modal-notesList'),
        $iconNote = $(".modalNotesBlock .modal-actions ul span:nth-child(1)")

    if($('#addActivity').hasClass('isActive')){
      $(".modalNotesBlock .modal-actions ul span:nth-child(3)").css({color:'#7c7c7c'})
    }

    if($addNote.hasClass('isActive')){
      $addNote.removeClass('isActive')
      $iconNote.css({color:'#7c7c7c'})
    } else {
      $addNote.addClass('isActive')
      $('.modal-panel').fadeOut(100)
      $('#addActivity').removeClass('isActive')
      $notesList.css({marginTop:'55px', transition:'.5s'})
      $iconNote.css({color:'#039be5'})
    }
    $('.modal-description button').css({top:'83px'})


  }

  $scope.modalClose = function(){
    $modalInstance.close();
  }

  $scope.changeSegment = function(id){
      $scope.info = id;

      delete $scope.info.quotations;
      delete $scope.info.activities;
      delete $scope.info.notes;
      delete $scope.info.assigned_to;
      delete $scope.info.lost;

      var seg = $scope.segment.segment;

      if(seg === "Medianamente Probable"){
        $scope.info.segment = 'medium'
      } else if(seg === "Altamente Probable") {
        $scope.info.segment = 'high'
      } else {
        $scope.info.segment = 'low'
      }

      var URIL  = '/api/projects/'+slugp+'/segmentation/' + $scope.info.slug + '/';

      $http({
          method  : 'PUT',
          url     : URIL,
          data    : $scope.info, //forms user object
          headers : {'Content-Type': 'application/json'}
         })
        .success(function(data) {
          if (data.errors) {
            // Showing errors.
          } else {
            $modalInstance.close();
            window.location.reload();
          }
      });
  }

  $scope.myDate = new Date();

//Datepicker
  $scope.clear = function () {
    $scope.nactiv.date = null;
  };

  $scope.open = function($event) {
    $event.preventDefault();
    $event.stopPropagation();

    $scope.opened = true;
  };

  $scope.formats = ['dd-MM-yyyy'];
  $scope.format = $scope.formats[0];


  $scope.submitFormA = function(id, project) {
    // Posting data to php file
    //console.log(id);

      var dateObj = new Date($scope.nactiv.date);
      var month = ("0" + (dateObj.getUTCMonth() + 1)).slice(-2); //months from 1-12
      var day = dateObj.getUTCDate();
      var year = dateObj.getUTCFullYear();

      var dateString = day+'/'+month+'/'+year+' '+$scope.nactiv.time1+':'+$scope.nactiv.time2;

      dateTimeParts = dateString.split(' '),
      timeParts = dateTimeParts[1].split(':'),
      dateParts = dateTimeParts[0].split('/');

      Ndate = new Date(dateParts[2], parseInt(dateParts[1], 10) - 1, dateParts[0], timeParts[0], timeParts[1]);

      var URIA  = '/api/projects/'+slugp+'/activities/create/';

      $http({
        method  : 'POST',
        url     : URIA,
        data    : {person: id, project: project, activity_type: $scope.nactiv.type, description: $scope.nactiv.description, scheduled_date: Ndate}, //forms user object
        headers : {'Content-Type': 'application/json'}
       })
        .success(function(data) {
          if (data.errors) {
            // Showing errors.

          } else {
            $('.activid').append('<div class="activ"><p>'+ $scope.nactiv.type +'</p><small>'+ $scope.nactiv.date +'</small></div>');
            $('#tipoAct').val('');
            $('#descAct').val('');
            $('#dateAct').val('');
            //$modalInstance.close();
          }
      });

    };

}]);


// Services
app.factory('httpService', function ($http, $q) {

  function getCookie(name){
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
  }

  return {
    getData: getData
  }

  function getData(method, url, data) {

    var defered = $q.defer();
    var promise = defered.promise;

    $http({
      method  : method,
      url     : url,
      data    : data || {},
      headers : {'Content-Type': 'application/json', 'X-CSRFToken': getCookie('csrftoken')}
     })
      .success(function(data) {
        if (data.errors) {
          defered.reject(data.errors);
        } else {
          defered.resolve(data);
        }
    });

    return promise;
  }

});

/*---- Dragabble ----*/
app.directive('draggable', function() {
  return function(scope, element) {
    // this gives us the native JS object
    var el = element[0];

    el.draggable = true;

    el.addEventListener(
      'dragstart',
      function(e) {
        //console.log("dragstart");
        e.dataTransfer.effectAllowed = 'move';
        e.dataTransfer.setData('Text', this.id);
        this.classList.add('drag');
        return false;
      },
      false
    );

    el.addEventListener(
      'dragend',
      function(e) {
      	this.classList.remove('drag');
        //console.log("dragend");
        /*console.log(el);
        var item = el;*/
        return false;
      },
      false
    );
  }
});

app.directive('droppable', function() {
  return {
    scope: {
      drop: '&',
      bin: '='
    },
    link: function(scope, element) {
      // again we need the native object
      var elm = element[0];

      elm.addEventListener(
        'dragover',
        function(e) {
          // allows us to drop
          e.dataTransfer.dropEffect = 'move';
          if (e.preventDefault) e.preventDefault();
          this.classList.add('over');
          //console.log("dragover");
          return false;
        },
        false
      );

      elm.addEventListener(
        'dragenter',
        function(e) {
          //console.log("dragenter");
          this.classList.add('over');
          return false;
        },
        false
      );

      elm.addEventListener(
        'dragleave',
        function(e) {
          //console.log("dragleave");
          this.classList.remove('over');
          return false;
        },
        false
      );

      elm.addEventListener(
        'drop',
        function(e) {
          // Stops some browsers from redirecting.

          if (e.stopPropagation) e.stopPropagation();

          this.classList.remove('over');

          var binId = this.id;
          var item = document.getElementById(e.dataTransfer.getData('Text'));
          this.appendChild(item);
          // call the passed drop function
          scope.$apply(function(scope) {
            var fn = scope.drop();
            if ('undefined' !== typeof fn) {
              fn(item.id, binId);
            }
          });

          return false;
        },
        false
      );
    }
  }
});

app.controller('DragDropCtrl', function($scope, $http, $routeParams) {

  $scope.handleDrop = function(item, bin) {
    var slugp = $routeParams.slug;

    $scope.info = JSON.parse(item);
    $scope.info.segment = bin;
    $scope.info.assigned_to = $scope.assignedTo
    delete $scope.info.initials;
    delete $scope.info.date;
    delete $scope.info.color;

    if(bin == "sell")
    {
      $scope.info.sold = "true"
    }

    var URIL  = '/api/projects/'+slugp+'/segmentation/' + $scope.info.slug + '/';

	$http({
      method  : 'PUT',
      url     : URIL,
      data    : $scope.info, //forms user object
      headers : {'Content-Type': 'application/json'}
     })
      .success(function(data) {
        if (data.errors) {
          // Showing errors.
        }
    });
  }
});
