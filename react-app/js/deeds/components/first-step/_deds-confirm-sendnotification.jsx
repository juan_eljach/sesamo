import React, { Component } from 'react'
import {IconCheck} from '../../svg/deeds-icons'
export default class DeedsFirstStepSendNotificationsConfirm extends Component {

  constructor (props) {
    super(props)

  }

  componentDidMount () {
  }

  render () {
    return (
      <div className={`DeedsFirstStepSendNotifications__confirm ${this.props.step === 3 ? '' : 'displayNone'}`}>
        <div className="DeedsFirstStepSendNotifications__confirmTower">
          <div>
            <IconCheck isCheck/>
          </div>
          <div>
            <h4>Torre</h4>
            <span>Torre {this.props.tower}</span>
          </div>
        </div>
        <div className="DeedsFirstStepSendNotifications__confirmAddressee">
          <div>
            <IconCheck isCheck/>
          </div>
          <div>
            <h4>Destinatarios</h4>
            <span>{this.props.selectedClientsLenght}</span>
          </div>
          <span onClick={()=> this.props.stepHandler(1)}>Editar</span>
        </div>
        <div className="DeedsFirstStepSendNotifications__confirmNotificationMsg">
            <div>
              <IconCheck isCheck/>
            </div>
            <div>
              <h4>Mensaje de la notificación</h4>
            </div>
            <span onClick={()=> this.props.stepHandler(2)}>Editar</span>
        </div>
      </div>
    )
  }
}
