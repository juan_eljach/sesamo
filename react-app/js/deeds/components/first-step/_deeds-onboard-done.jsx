import React, { Component } from 'react'
import {IconCheck} from '../../svg/deeds-icons'

export default class DeedsSendNotificationsDone extends Component {

  constructor (props) {
    super(props)

  }

  componentDidMount () {

  }

  render () {
    return (
      <div className={`DeedsFirstStepSendNotifications__done ${this.props.sendNotificationsDone ? '' : 'displayNone'}`}>
        <span onClick={this.props.closeNotificationSucess}>x</span>
        <div>
          <img src="/static/img/deeds-onboard-done.png" alt=""/>
          <p>Enviaste {this.props.selectedUnities.length} notificaciones!</p>
          <p>Pendientes: {this.props.unitiesLength - this.props.selectedUnities.length} clientes de la torre {this.props.tower} sin notificar</p>
        </div>
      </div>
    )
  }
}
