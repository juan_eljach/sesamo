import React, { Component } from 'react'

import {IconCheck} from '../../svg/deeds-icons'

import getCsrfToken from '../../../lib/csrftoken'

export default class WritingTable extends Component {
  constructor(props){
    super(props)

    this.selectedUnitiesTemp = []
    this.floorsSelectedTemp = []

    this.state = {
      selectedUnities: [],
      isMessageConfirmation: false,
      floorsSelected: []
    }

  }

  componentDidMount(){
    this.$writingEditTemplate = $('#writingEditTemplate')
  }

  checkHandler(e, unity){

    let unityFound = _.find(this.state.selectedUnities, (unityId) => unityId === unity.id)

    if(unityFound){
      _.remove(this.selectedUnitiesTemp, (unityId) => unityId === unity.id )

      return this.setState({selectedUnities: this.selectedUnitiesTemp}, ()=> {
        this.props.selectedUnitiesHandler(this.state.selectedUnities)
        console.log(this.state.selectedUnities);
      })
    }

    this.selectedUnitiesTemp.push(unity.id)

    this.setState({selectedUnities: _.uniq(this.state.selectedUnities.concat(this.selectedUnitiesTemp))}, ()=> this.props.selectedUnitiesHandler(this.state.selectedUnities))

  }

  selectAllByFloor(floor) {

    if(_.find(this.floorsSelectedTemp, (f) => f === floor )){
      _.remove(this.floorsSelectedTemp, (flr) => flr === floor)
      this.setState({floorsSelected: this.floorsSelectedTemp})
    }
    else{
      this.floorsSelectedTemp.push(floor)
      this.setState({floorsSelected: this.floorsSelectedTemp})
    }


    let unitiesByFloor = _.filter(this.props.unitiesByTower, (unity) => unity.floor == floor)

    let selectedByFloor = _.map(
      ( _.filter(unitiesByFloor, (unity, key) => {

        return _.find(this.state.selectedUnities, (selectUnity) => selectUnity == unity.id)

      }) ), 'id'
    )

    if(selectedByFloor.length === unitiesByFloor.length){

      _.remove(this.selectedUnitiesTemp, (selectedTempId) => {
        return _.find(selectedByFloor, (selectFloorId) => selectFloorId === selectedTempId )
      })

      return this.setState({ selectedUnities:  this.selectedUnitiesTemp}, ()=> this.props.selectedUnitiesHandler(this.state.selectedUnities))

    }

    this.selectedUnitiesTemp = this.state.selectedUnities.concat(_.map(unitiesByFloor, 'id'))

    this.setState({ selectedUnities: _.uniq(this.state.selectedUnities.concat(_.map(unitiesByFloor, 'id')) )}, ()=> this.props.selectedUnitiesHandler(this.state.selectedUnities))

  }

  render(){
    return (
      <div className='DeedsFirstStepSendNotifications__tableBody'>
        {
          this.props.floors ? _.sortBy(this.props.floors, (floor)=> parseInt(floor)).map((floor, key)=>{

            return <div className="DeedsFirstStepSendNotifications__tableRow" key={key}>
              <div className="DeedsFirstStepSendNotifications__floor" onClick={ () => this.selectAllByFloor( key + 1 ) }> {key + 1}
                <span></span>
                <IconCheck isCheck={(_.find(this.state.floorsSelected, (flr) => flr == floor )) ? true : false }/>
              </div>

              {
                _.sortBy(_.filter(this.props.unitiesByTower, {floor: floor}), (u)=> parseInt(u.apto) ).map((unity, key) => {
                  return <div key={key}>
                    <div className={`DeedsFirstStepSendNotifications__apto DeedsFirstStepSendNotifications__aptoHover ${_.find(this.state.selectedUnities, (u) => u === unity.id )  ? 'backgroundGray' : ''}`} onClick={ (e)=> this.checkHandler(e, unity) }>

                      <IconCheck isCheck={ (_.find(this.state.selectedUnities, (unityId) => unityId === unity.id )) ? true : false } />
                      { unity.slug.toUpperCase() }
                    </div>
                  </div>

                })
              }

            </div>
          }) : null
        }

      </div>
    )
  }
}
