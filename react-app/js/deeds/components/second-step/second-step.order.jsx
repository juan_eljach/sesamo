import React, { Component } from 'react'
import DeedsSecondStepRequiredInput from './second-step-requiredInput'
import {IconCheckFlex, IconDeedsCalendar, IconArrowDown} from '../../svg/deeds-icons'
import DeedsSecondStepNoRequired from './second-step-norequired'
import DeedsSecondStepfooterNav from './second-step-footerNav'
import DeedsSecondStepChangesPopup from './second-step-changesPopup'
import getCsrfToken from '../../../lib/csrftoken'

export default class DeedsSecondStepOrder extends Component {

  constructor (props) {
    super(props)

    this.notary = [
      'Notaria de Piedecuesta',
      'Notaria Tercera'
    ]

    this.notary_choices = {
      'Notaria de Piedecuesta': 'notaria_piedecuesta',
      'Notaria Tercera': 'notaria_tercera'
    }

    this.state = {
      isCheckSent: false,
      isCheckSafe: false,
      isCheckSign: false,
      orderInfo: null,
      thereAreChanges: false
    }

    this.checkSendHandler = this.checkSendHandler.bind(this)
    this.checkSafeHandler = this.checkSafeHandler.bind(this)
    this.checkSignHandler = this.checkSignHandler.bind(this)
    this.saveChanges = this.saveChanges.bind(this)
    this.saveChangesPopup = this.saveChangesPopup.bind(this)
    this.notaryChangeHandler = this.notaryChangeHandler.bind(this)
    this.nextStepHandler = this.nextStepHandler.bind(this)
  }

  componentDidMount () {
    $('[data-toggle="datepicker"]').datepicker({format: 'yyyy-mm-dd', autoHide: true})

    this.$sentNotaryDate = $('#sentNotaryDate')
    this.orderNotary = $('#orderNotary')
    this.$notaryCheckingDate = $('#notaryCheckingDate')
    this.$orderDeedSignedDate = $('#orderDeedSigned')

    $.ajax({
      url: `/api/projects/${this.props.pslug}/deeds/tower/${this.props.unity.split('-')[0].slice(1)}/unity/${this.props.unity}/deed-order/`
    })
    .done((data) => {
      console.log(data);
      this.setState({orderInfo: data})

      this.props.orderRequiredHandler(data.required)

      this.setState({isCheckSent: data.deed_sent_to_notary})
      this.setState({isCheckSafe: data.notary_date_and_time_checking})
      this.setState({isCheckSign: data.deed_signed})

      this.$sentNotaryDate.datepicker('setDate', data.deed_sent_to_notary_date)
      this.$notaryCheckingDate.datepicker('setDate', data.notary_date_and_time_checking_date)
      this.$orderDeedSignedDate.datepicker('setDate', data.deed_signed_date)

      $('#orderTasksInputs [data-toggle="datepicker"]').change(()=> this.props.changesHandler(1))
    })
    .fail((error)=> {
      console.log(error)
    })

  }

  notaryChangeHandler(){
    this.props.changesHandler(1)
  }

  checkSendHandler(){
    this.props.changesHandler(1)
    if(this.state.isCheckSent){
      return this.setState({isCheckSent: false})
    }
    this.setState({isCheckSent: true})
  }

  checkSafeHandler(){
    this.props.changesHandler(1)
    if(this.state.isCheckSafe){
      return this.setState({isCheckSafe: false})
    }

    this.setState({isCheckSafe: true})
  }

  checkSignHandler(){
    this.props.changesHandler(1)
    if(this.state.isCheckSign){
      return this.setState({isCheckSign: false})
    }

    this.setState({isCheckSign: true})
  }

  previousStepHandler(){

  }

  nextStepHandler(){
    if((this.props.tasksDone + 4) === this.props.step){
      this.props.taskDoneHandler(this.props.tasksDone + 4)
    }

    if(this.props.step === 5){
      if(this.props.changes === 1){
        return this.setState({thereAreChanges: true})
      }
      this.props.stepHandler(6)
    }
  }

  saveChangesPopup(action){
    if(action === 'save'){
      this.saveChanges()
    }
    this.setState({thereAreChanges: false})
    this.props.changesHandler(0)
    this.props.stepHandler(6)
  }

  saveChanges(){
    let data = {
      required: this.props.orderRequired,
      unity: this.props.unityInfo.id,
      deed_sent_to_notary: this.state.isCheckSent,
      deed_sent_to_notary_date: this.$sentNotaryDate.val(),
      notary: this.orderNotary.val(),
      notary_date_and_time_checking: this.state.isCheckSafe,
      notary_date_and_time_checking_date: this.$notaryCheckingDate.val(),
      deed_signed: this.state.isCheckSign,
      deed_signed_date: this.$orderDeedSignedDate.val()
    }

    console.log(data)

    $.ajax({
      url: `/api/projects/${this.props.pslug}/deeds/tower/${this.props.unity.split('-')[0].slice(1)}/unity/${this.props.unity}/deed-order/`,
      method: this.state.orderInfo ? 'PUT' : 'POST',
      headers: {
        'X-CSRFToken': getCsrfToken('csrftoken')
      },
      data
    })
    .done((data) => {
      console.log(data, 'he')
      this.props.changesHandler(0)
      this.props.changesAlertHandler()
    })
    .fail((error)=> console.log(error))

  }

  render () {
    return (
      <div className={`DeedsSecondStep__bank ${this.props.step === 5 ? '' : 'displayNone' }`}>

        <DeedsSecondStepRequiredInput
          text={'Orden de escrituración'}
          handler={this.props.orderRequiredHandler}
          required={this.props.orderRequired}
          step={this.props.step}
        />

        <div className={`DeedsSecondStep__bankContent ${this.props.orderRequired ? '' : 'displayNone'}`}>
          <figure>
            <img src="/static/img/deeds-secondstep-order.png" width="180" alt=""/>
            <p>En este paso tendrás que realizar las siguientes tareas y marcarlas a medida que las realices  para poder avanzar</p>
          </figure>

          <div id="orderTasksInputs" className="DeedsSecondStep__bankTasks">
            <h3>Tareas:</h3>

            <div>
              <IconCheckFlex
                isCheck={this.state.isCheckSent}
                checkHandler={this.checkSendHandler}
              />
              <span onClick={this.checkSendHandler} className={`${this.state.isCheckSent ? 'displayNone' : ''}`}></span>
              <p>Otro sí Envío de Orden de escrituración a la Notaria</p>
              <div className={`displayFlex ${this.state.isCheckSent ? '' : 'displayNone'}`}>
                <div>
                  <IconDeedsCalendar />
                  <IconArrowDown />
                  <input type="text" id="sentNotaryDate" placeholder="Elige fecha de realización" data-toggle="datepicker"/>
                </div>
                <div className="DeedsSecondStep__orderNotary">

                  <select name="" id="orderNotary" onChange={this.notaryChangeHandler}>
                    {
                      !this.state.orderInfo && <option>Elige una Notaria</option>
                    }
                    {
                      this.notary.map((notary, k) => {
                        if(this.state.orderInfo && this.notary_choices[notary] == this.state.orderInfo.notary){
                          return <option value={this.notary_choices[notary]} key={k} selected>{notary}</option>
                        }
                        return <option value={this.notary_choices[notary]} key={k}>{notary}</option>
                      })
                    }

                  </select>
                  <IconArrowDown />
                </div>
              </div>
            </div>

            <div>
              <IconCheckFlex
                isCheck={this.state.isCheckSafe}
                checkHandler={this.checkSafeHandler}
              />
              <span onClick={this.checkSafeHandler} className={`${this.state.isCheckSafe ? 'displayNone' : '' }`}></span>
              <p>Revisión de fecha y hora estipulada por la Notaría para que el cliente firme</p>
              <div className={`displayFlex ${this.state.isCheckSafe ? '' : 'displayNone'}`}>
                <div>
                  <IconDeedsCalendar />
                  <IconArrowDown />
                  <input id="notaryCheckingDate" type="text" placeholder="Elige fecha de realización" data-toggle="datepicker"/>
                </div>
              </div>
            </div>

            <div>
              <IconCheckFlex
                isCheck={this.state.isCheckSign}
                checkHandler={this.checkSignHandler}
              />
              <span onClick={this.checkSignHandler} className={`${this.state.isCheckSign ? 'displayNone' : '' }`}></span>
              <p>Firma de la escritura por parte del cliente</p>
              <div className={`displayFlex ${this.state.isCheckSign ? '' : 'displayNone'}`}>
                <div>
                  <IconDeedsCalendar />
                  <IconArrowDown />
                  <input id="orderDeedSigned" type="text" placeholder="Elige fecha de realización" data-toggle="datepicker"/>
                </div>
              </div>
            </div>

          </div>


        </div>

        <DeedsSecondStepNoRequired
          required={this.props.orderRequired}
        />

        <DeedsSecondStepfooterNav
          step={this.props.step}
          currentStep={this.props.currentStep}
          nextStepHandler={this.nextStepHandler}
          previousStepHandler={this.previousStepHandler}
          saveChanges={this.saveChanges}
        />

        <DeedsSecondStepChangesPopup
          thereAreChanges={this.state.thereAreChanges}
          saveChangesPopup={this.saveChangesPopup}
        />

      </div>
    )
  }
}