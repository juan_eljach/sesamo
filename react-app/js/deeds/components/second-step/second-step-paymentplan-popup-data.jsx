import React, { Component } from 'react'
import {IconArrowGreen} from '../../svg/deeds-icons'

export default class DeedsSecondStepPaymentPlanPopupData extends Component {

  constructor (props) {
    super(props)
  }

  componentDidMount () {
    console.log(this.props.lateralPadding);
  }

  render () {
    let {paymentPlanData} = this.props
    return (
      <div className={`DeedsSecondStep__paymentPlanPopupData ${this.props.lateralPadding ? '' : 'lateralPadding'}`}>
        <h5>Datos generales</h5>
        <div className="DeedsSecondStep__paymentPlanPopupGeneralData">

          <div>
            <label>Valor del apto</label>
            <div>${paymentPlanData.unity_price.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")}</div>
          </div>

          <div>
            <label>Valor de la cuota inicial</label>
            <div>${paymentPlanData.initial_fee.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")}</div>
          </div>

          <div>
            <IconArrowGreen />
            <label>  Pagos realizados hasta la fecha</label>
            <div>${paymentPlanData.done_payments_fees_sum.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")}</div>
          </div>

          <div>
            <label>Valor del crédito</label>
            <div>${paymentPlanData.credit.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")}</div>
          </div>

        </div>

        <h5>Otros pagos</h5>

        <div className="DeedsSecondStep__paymentPlanPopupOthers">

          <div>
            <div className="DeedsSecondStep__paymentPlanPopupApply">
              Cesantías: <span>{_.find(this.props.paymentPlanData.agreed_payments, {type_of_payment: 'cesantias'}).applied_to_initialfee ? '' : 'No' } Aplicado a CI</span>
            </div>
            <div className="DeedsSecondStep__paymentPlanPopupCancelled">
              <IconArrowGreen />
              <label>Valor cancelado:</label>
              <div>${_.find(this.props.paymentPlanData.agreed_payments, {type_of_payment: 'cesantias'}).fee }</div>
            </div>
            <div className="DeedsSecondStep__paymentPlanPopupWithoutCanceling">
              <div>
                <span>Sin cancelar: </span>
                $20.000.0000
              </div>
            </div>
          </div>

          <div>
            <div className="DeedsSecondStep__paymentPlanPopupApply">
              Subsidios: <span>{_.find(this.props.paymentPlanData.agreed_payments, {type_of_payment: 'subsidio-vivienda'}).applied_to_initialfee ? '' : 'No' } Aplicado a CI</span>
            </div>
            <div className="DeedsSecondStep__paymentPlanPopupCancelled">
              <IconArrowGreen />
              <label>Valor cancelado:</label>
              <div>${_.find(this.props.paymentPlanData.agreed_payments, {type_of_payment: 'subsidio-vivienda'}).fee }</div>
            </div>
            <div className="DeedsSecondStep__paymentPlanPopupWithoutCanceling">
              <div>
                <span>Sin cancelar: </span>
                $20.000.0000
              </div>
            </div>
          </div>

          <div>
            <div className="DeedsSecondStep__paymentPlanPopupApply">
              Ahorros programados: <span>{_.find(this.props.paymentPlanData.agreed_payments, {type_of_payment: 'ahorro-programado'}).applied_to_initialfee ? '' : 'No' } Aplicado a CI</span>
            </div>
            <div className="DeedsSecondStep__paymentPlanPopupCancelled">
              <IconArrowGreen />
              <label>Valor cancelado:</label>
              <div>${_.find(this.props.paymentPlanData.agreed_payments, {type_of_payment: 'ahorro-programado'}).fee }</div>
            </div>
            <div className="DeedsSecondStep__paymentPlanPopupWithoutCanceling">
              <div>
                <span>Sin cancelar: </span>
                $20.000.0000
              </div>
            </div>
          </div>

          <div>
            <div className="DeedsSecondStep__paymentPlanPopupApply">
              Otros: <span>{_.find(this.props.paymentPlanData.agreed_payments, {type_of_payment: 'otro-pago-acordado'}).applied_to_initialfee ? '' : 'No' } Aplicado a CI</span>
            </div>
            <div className="DeedsSecondStep__paymentPlanPopupCancelled">
              <IconArrowGreen />
              <label>Valor cancelado:</label>
              <div>${_.find(this.props.paymentPlanData.agreed_payments, {type_of_payment: 'otro-pago-acordado'}).fee }</div>
            </div>
            <div className="DeedsSecondStep__paymentPlanPopupWithoutCanceling">
              <div>
                <span>Sin cancelar: </span>
                $20.000.0000
              </div>
            </div>
          </div>

        </div>

      </div>
    )
  }
}