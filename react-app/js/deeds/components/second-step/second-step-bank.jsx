import React, { Component } from 'react'
import DeedsSecondStepRequiredInput from './second-step-requiredInput'
import {IconCheckFlex, IconDeedsCalendar, IconArrowDown} from '../../svg/deeds-icons'
import DeedsSecondStepNoRequired from './second-step-norequired'
import DeedsSecondStepfooterNav from './second-step-footerNav'
import DeedsSecondStepChangesPopup from './second-step-changesPopup'
import getCsrfToken from '../../../lib/csrftoken'

export default class DeedsSecondStepBank extends Component {

  constructor (props) {
    super(props)

    this.state = {
      isCheckSent: false,
      goodStanding: false,
      bankConfirmationData: null
    }

    this.checkSentHandler = this.checkSentHandler.bind(this)
    this.checkSafeHandler = this.checkSafeHandler.bind(this)
    this.saveChanges = this.saveChanges.bind(this)
    this.saveChangesPopup = this.saveChangesPopup.bind(this)
    this.nextStepHandler = this.nextStepHandler.bind(this)
  }

  componentDidMount () {
    $('[data-toggle="datepicker"]').datepicker({format: 'yyyy-mm-dd', autoHide: true})

    $.ajax({
      url: `/api/projects/${this.props.pslug}/deeds/tower/${this.props.unity.split('-')[0].slice(1)}/unity/${this.props.unity}/bank-confirmation/`
    })
    .done((data) => {
      this.setState({bankConfirmationData: data})
      console.log(data, 'reqqqq')
      this.props.bankRequiredHandler(data.required)
      this.setState({isCheckSent: data.documents_sent_to_bank})
      this.setState({goodStanding: data.client_is_ok_with_bank})

      $('#sentBankDocuments').datepicker('setDate', data.documents_sent_to_bank_date)
      $('#goodStanding').datepicker('setDate', data.client_is_ok_with_bank_date)
    })
    .fail((error)=> {
      console.log(error)
    })
  }

  checkSentHandler(){
    if(this.state.isCheckSent){
      return this.setState({isCheckSent: false})
    }
    this.setState({isCheckSent: true})

    console.log('Hola');
  }

  checkSafeHandler(){
    if(this.state.goodStanding){
      return this.setState({goodStanding: false})
    }

    this.setState({goodStanding: true})
  }


  previousStepHandler(){
  }

  nextStepHandler(){
    if((this.props.tasksDone + 3) === this.props.step){
      this.props.taskDoneHandler(this.props.tasksDone + 3)
    }

    if(this.props.step === 4){
      if(this.props.changes === 1){
        return this.setState({thereAreChanges: true})
      }
      this.props.stepHandler(5)
    }
  }

  saveChangesPopup(action){
    if(action === 'save'){
      this.saveChanges()
    }
    this.setState({thereAreChanges: false})
    this.props.changesHandler(0)
    this.props.stepHandler(5)
  }

  saveChanges(e){

    let payload = {
      required: this.props.bankRequired,
      unity: this.props.unityInfo.id,
      documents_sent_to_bank: this.state.isCheckSent,
      documents_sent_to_bank_date: $('#sentBankDocuments').val(),
      client_is_ok_with_bank: this.state.goodStanding,
      client_is_ok_with_bank_date: $('#goodStanding').val()
    }

    $.ajax({
      url: `/api/projects/${this.props.pslug}/deeds/tower/${this.props.unity.split('-')[0].slice(1)}/unity/${this.props.unity}/bank-confirmation/`,
      method: `${this.state.bankConfirmationData ? 'PUT' : 'POST'}`,
      data: payload,
      headers: {
        'X-CSRFToken': getCsrfToken('csrftoken'),
        // 'Content-Type' : 'application/json'
      }
    })
    .done((data) => {
      console.log(data, 'asasas')
      this.props.changesHandler(0)
      this.props.changesAlertHandler()
    })
    .fail((error)=> {
      console.log(error)
    })
  }

  render () {
    return (
      <div className={`DeedsSecondStep__bank ${this.props.step === 4 ? '' : 'displayNone' }`}>

        <DeedsSecondStepRequiredInput
          text={'Confirmación con banco'}
          handler={this.props.bankRequiredHandler}
          required={this.props.bankRequired}
          step={this.props.step}
        />

        <div className={`DeedsSecondStep__bankContent ${this.props.bankRequired ? '' : 'displayNone'}`}>
          <figure>
            <img src="/static/img/deeds-secondstep-bank.png" width="180" alt=""/>
            <p>En este paso tendrás que realizar las siguientes tareas y marcarlas a medida que las realices  para poder avanzar</p>
          </figure>

          <div className="DeedsSecondStep__bankTasks">
            <h3>Tareas:</h3>

            <div>
              <IconCheckFlex
                isCheck={this.state.isCheckSent}
                checkHandler={this.checkSentHandler}
              />
              <span onClick={this.checkSentHandler} className={`${this.state.isCheckSent ? 'displayNone' : ''}`}></span>
              <p>Envío de promesa de compraventa, certificado del crédito y fotocopia de la cédula al Banco BBVA</p>
              <div className={`${this.state.isCheckSent ? '' : 'displayNone'}`}>
                <IconDeedsCalendar />
                <IconArrowDown />
                <input type="text" id="sentBankDocuments" placeholder="Elige fecha de realización" data-toggle="datepicker" />
              </div>
            </div>

            <div>
              <IconCheckFlex
                isCheck={this.state.goodStanding}
                checkHandler={this.checkSafeHandler}
              />
              <span  onClick={this.checkSafeHandler} className={`${this.state.goodStanding ? 'displayNone' : '' }`}></span>
              <p>El cliente se encuenta a paz y salvo con el Banco BBVA</p>
              <div className={`${this.state.goodStanding ? '' : 'displayNone'}`}>
                <IconDeedsCalendar />
                <IconArrowDown />
                <input type="text" id="goodStanding" placeholder="Elige fecha de realización" data-toggle="datepicker"/>
              </div>
            </div>

          </div>

        </div>

        <DeedsSecondStepNoRequired
          required={this.props.bankRequired}
        />

        <DeedsSecondStepfooterNav
          step={this.props.step}
          currentStep={this.props.currentStep}
          nextStepHandler={this.nextStepHandler}
          previousStepHandler={this.previousStepHandler}
          saveChanges={this.saveChanges}
        />

        <DeedsSecondStepChangesPopup
          thereAreChanges={this.state.thereAreChanges}
          saveChangesPopup={this.saveChangesPopup}
        />

      </div>
    )
  }
}