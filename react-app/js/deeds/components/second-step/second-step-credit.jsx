import React, { Component } from 'react'
import {IconArrowDown} from '../../../support/svg/support-icons'
import {IconDeedsUpload,
        IconDeedsCalendar,
        IconDeedsValue,
        IconDeedsBank,
        IconDeedsArrowRight,
        IconPdfDocument} from '../../svg/deeds-icons'
import DeedsSecondStepRequiredInput from './second-step-requiredInput'
import DeedsSecondStepNoRequired from './second-step-norequired'
import DeedsSecondStepfooterNav from './second-step-footerNav'
import DeedsSecondStepChangesPopup from './second-step-changesPopup'
import getCsrfToken from '../../../lib/csrftoken'

export default class DeedsSecondStepCredit extends Component {

  constructor (props) {
    super(props)

    this.banks = [
      'Caja Social',
      'Davivienda',
      'Colpatria',
      'AV Villas',
      'Bancolombia',
      'Banco de Bogota',
      'Fondo Nacional del ahorro',
      'Credifamilia'
    ]

    this.bank_choices = {
      'Caja Social' : 'caja_social',
      'Davivienda' : 'davivienda',
      'Colpatria' : 'colpatria',
      'AV Villas' : 'av_villas',
      'Bancolombia' : 'bancolombia',
      'Banco de Bogota' : 'banco_bogota',
      'Fondo Nacional del ahorro' : 'fna',
      'Credifamilia' : 'credifamilia'
    }

    this.state = {
      fileName: '',
      creditInfo: null,
      certifiedRequired: false,
      isCreditSent: false,
      thereAreChanges: false
    }

    this.fileHandler = this.fileHandler.bind(this)
    this.certifiedRequiredHandler = this.certifiedRequiredHandler.bind(this)
    this.updateCreditAmount = this.updateCreditAmount.bind(this)
    this.nextStepHandler = this.nextStepHandler.bind(this)
    this.bankHandler = this.bankHandler.bind(this)
    this.previousStepHandler = this.previousStepHandler.bind(this)
    this.creditCheck = this.creditCheck.bind(this)
    this.sendCreditPOST = this.sendCreditPOST.bind(this)
    this.sendCreditPUT = this.sendCreditPUT.bind(this)
    this.saveChanges = this.saveChanges.bind(this)
    this.saveChangesPopup = this.saveChangesPopup.bind(this)
  }

  componentDidMount () {
    this.$creditFile = $('#creditFile')
    this.$creditDate = $('#creditDate')
    this.$creditBank = $('#creditBank')
    this.$creditAmount = $('#creditAmount')
    this.$creditDocument = $('#creditFile')[0]
    this.$creditDocumentChange = $('#creditFileChange')[0]

    $('[data-toggle="datepicker"]').datepicker({format: 'yyyy-mm-dd', autoHide: true})

    this.$creditDate.change(()=> this.props.changesHandler(1))

    $.ajax({
      url: `/api/projects/${this.props.pslug}/deeds/tower/${this.props.unity.split('-')[0].slice(1)}/unity/${this.props.unity}/credit/`
    })
    .done((data) => {
      this.setState({creditInfo: data})
      // this.props.updateRequired('certifiedRequired', data.required)
      this.setState({certifiedRequired: true})
      $('#creditAmount').val(data.amount.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."))
      this.props.taskDoneHandler(1)
    })
    .fail((error)=> {
      console.log(error)
    })
  }

  fileHandler(e){
    this.setState({fileName: e.target.files[0].name})
    this.props.changesHandler(1)
  }

  certifiedRequiredHandler(value){
    if(!this.state.creditInfo){
      this.setState({certifiedRequired: value})
    }
    this.props.changesHandler(1)
  }

  updateCreditAmount(e){
    $(`#${e.target.id}`).val(e.target.value.split('.').join('').toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."))
    this.props.changesHandler(1)
  }

  bankHandler(){
    this.props.changesHandler(1)
  }

  nextStepHandler(){
    if((this.props.tasksDone + 1) === this.props.step){
      this.props.taskDoneHandler(this.props.tasksDone + 1)
    }

    if(this.props.step === 1){
      if(this.props.changes === 1){
        return this.setState({thereAreChanges: true})
      }
      this.props.stepHandler(2)
    }

  }

  previousStepHandler(){
    if(this.props.step > 1){
      this.props.stepHandler(this.props.step - 1)
    }
  }

  saveChangesPopup(action){
    if(action === 'save'){
      this.saveChanges()
    }
    this.setState({thereAreChanges: false})
    this.props.changesHandler(0)
    this.props.stepHandler(2)
  }

  saveChanges(){
    if(this.props.step === 1) {
      this.creditCheck()
      this.setState({thereAreChanges: false})
      this.props.changesHandler(0)
      this.props.changesAlertHandler()
    }
  }

  creditCheck(){
    $.ajax({
      url: `/api/projects/${this.props.pslug}/deeds/tower/${this.props.unity.split('-')[0][1]}/unity/${this.props.unity}/credit/`
    })
    .done((data) => {
      this.setState({isCreditSent: true}, () => this.sendCreditPUT(data))
    })
    .fail((error) => {
      this.sendCreditPOST()
    })
  }

  sendCreditPOST(){
    let formData = new FormData($('#creditForm'))

    formData.append('unity', this.props.unityInfo.id)
    formData.append('amount', $('#creditAmount').val().split('.').join(''))
    formData.append('bank', this.bank_choices[$('#creditBank').val()])
    formData.append('expedition_timestamp', $('#creditDate').val())
    formData.append('document', $('#creditFile')[0].files[0])
    formData.append('required', this.state.certifiedRequired)

    $.ajax({
      url: `/api/projects/${this.props.pslug}/deeds/tower/${this.props.unity.split('-')[0][1]}/unity/${this.props.unity}/credit/`,
      method: 'POST',
      data: formData,
      headers: {
        'X-CSRFToken': getCsrfToken('csrftoken')
      },
      processData: false,
      contentType: false
    })
    .done((data) => {

    })
    .fail((error)=> console.log(error))
  }

  sendCreditPUT(data){

    if(this.$creditDocumentChange.files[0]){
      let formData = new FormData($('#creditForm'))
      formData.append('pk', data.pk)
      formData.append('unity', data.unity)
      formData.append('amount', this.$creditAmount.val() ? $('#creditAmount').val().split('.').join('') : data.amount)
      formData.append('bank', this.$creditBank.val() ? this.bank_choices[this.$creditBank.val()] : data.bank)
      formData.append('expedition_timestamp', this.$creditDate.val() ? this.$creditDate.val() : data.expedition_timestamp)
      formData.append('document', this.$creditDocumentChange.files[0] ? this.$creditDocumentChange.files[0] : null)
      formData.append('required', data.required)

      $.ajax({
        url: `/api/projects/${this.props.pslug}/deeds/tower/${this.props.unity.split('-')[0][1]}/unity/${this.props.unity}/credit/`,
        method: 'PUT' ,
        data: formData,
        headers: {
          'X-CSRFToken': getCsrfToken('csrftoken')
        },
        processData: false,
        contentType: false
      })
      .done((data) => {
      })
      .fail((error)=> console.log(error))

    }
    else{
      let payload = {
        pk: data.pk,
        unity: data.unity,
        amount: this.$creditAmount.val() ? this.$creditAmount.val().split('.').join('') : data.amount,
        bank: this.$creditBank.val() ? this.bank_choices[this.$creditBank.val()] : data.bank,
        expedition_timestamp: this.$creditDate.val() ? this.$creditDate.val() : data.expedition_timestamp,
        document: this.$creditDocument.files[0] ? this.$creditDocument.files[0] : null,
        required: data.required
      }

      $.ajax({
        url: `/api/projects/${this.props.pslug}/deeds/tower/${this.props.unity.split('-')[0][1]}/unity/${this.props.unity}/credit/`,
        method: 'PUT',
        data: payload,
        headers: {
          'X-CSRFToken': getCsrfToken('csrftoken')
        },
        'Content-Type' : 'application/json'
      })
      .done((data) => {

      })
      .fail((error)=> console.log(error))
    }
  }

  render () {
    return (
      <div className={`DeedsSecondStep__credit ${this.props.step === 1 ? '' : 'displayNone'}`}>

        <DeedsSecondStepRequiredInput
          text={'Certificado de aprobación del crédito:'}
          handler={this.certifiedRequiredHandler}
          required={this.state.certifiedRequired}
          isChecked={this.state.creditInfo ? this.state.creditInfo.required : false }
          step={this.props.step}
        />

        <div className={`DeedsSecondStep__uploadCertified ${this.state.certifiedRequired || this.state.creditInfo ? '' : 'displayNone'}`}>

          <label htmlFor="" className={`DeedsSecondStep__addCertified ${this.state.fileName || this.state.creditInfo ? 'displayNone' : ''}`}>
            <IconDeedsUpload />
            <input type="file" id="creditFile" onChange={this.fileHandler}/>
            <div>Agregar certificado del crédito</div>
          </label>


          <div className={`DeedsSecondStep__certifiedAdded ${this.state.fileName === '' ? 'displayNone': ''}`}>
            <IconPdfDocument />
            Certificado adjunto: {this.state.fileName}
          </div>

          <div className={`DeedsSecondStep__certifiedRequired ${!this.state.creditInfo || this.state.fileName ? 'displayNone' : ''}`}>
            <IconPdfDocument />
            <a href={`${this.state.creditInfo ? this.state.creditInfo.document : ''}`} target="_blank">Ver certificado del crédito adjunto</a>
          </div>


          <label htmlFor="" className={`DeedsSecondStep__changeCertified ${this.state.fileName || this.state.creditInfo ? '' : 'displayNone'}`}>
            Cambiar certificado
            <input type="file" id="creditFileChange" onChange={this.fileHandler}/>
          </label>

        </div>

        <div className={`DeedsSecondStep__creditData ${this.state.certifiedRequired || this.state.creditInfo ? '' : 'displayNone'}`}>

          <form id="creditForm">
            <div className="DeedsSecondStep__creditDataInputs">
              <div>
                <label htmlFor="">Fecha de expedición</label>
                <IconDeedsCalendar />
                <input id="creditDate" type="text" placeholder={`${this.state.creditInfo ? this.state.creditInfo.expedition_timestamp : 'Fecha de expedición'}`} data-toggle="datepicker"/>
              </div>
              <div>
                <IconDeedsBank />
                <label htmlFor="">Banco del crédito</label>
                <select name="" id="creditBank" onChange={this.bankHandler}>
                  {
                    this.banks.map((bank, key) => {
                      if(this.state.creditInfo && this.bank_choices[bank] == this.state.creditInfo.bank){
                        return <option value={bank} key={key} selected>{bank}</option>
                      }
                      return <option value={bank} key={key}>{bank}</option>
                    })
                  }
                </select>
                <span><IconArrowDown /></span>
              </div>
              <div>
                <IconDeedsValue />
                <label htmlFor="">Valor</label>
                <input id="creditAmount" type="text" placeholder='Monto del crédito' onChange={(e) => this.updateCreditAmount(e)} />
              </div>
            </div>
          </form>

        </div>

        <DeedsSecondStepNoRequired
          required={this.state.certifiedRequired}
        />

        <DeedsSecondStepfooterNav
          step={this.props.step}
          currentStep={this.props.currentStep}
          nextStepHandler={this.nextStepHandler}
          previousStepHandler={this.previousStepHandler}
          saveChanges={this.saveChanges}
        />

        <DeedsSecondStepChangesPopup
          thereAreChanges={this.state.thereAreChanges}
          saveChangesPopup={this.saveChangesPopup}
        />

      </div>
    )
  }
}
