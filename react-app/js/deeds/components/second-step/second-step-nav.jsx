import React, { Component } from 'react'
import {browserHistory} from 'react-router';

export default class DeedsSecondStepNav extends Component {

  constructor (props) {
    super(props)
  }

  componentDidMount () {
  }

  render () {
    return (
      <nav className="DeedsSecondStep__headerNav">
        <div onClick={() => this.props.router.push('/deeds') }>Todos los clientes </div>
        <div>> Proceso de la {this.props.unity}</div>
      </nav>
    )
  }
}
