import React, { Component } from 'react'
import Switch from 'rc-switch'

export default class DeedsSecondStepfooterNav extends Component {

  constructor (props) {
    super(props)
  }

  componentDidMount () {

  }

  render () {
    return (
      <div className="DeedsSecondStep__nav">
        <div onClick={this.props.previousStepHandler} >Ir al paso anterior</div>
        <div onClick={this.props.saveChanges}>
          GUARDAR
        </div>
        <div onClick={this.props.nextStepHandler}> Ir al siguiente paso </div>
      </div>
    )
  }
}