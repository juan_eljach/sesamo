import React, { Component } from 'react'
import DeedsSecondStepRequiredInput from './second-step-requiredInput'
import DeedsSecondStepNoRequired from './second-step-norequired'
import DeedsSecondStepfooterNav from './second-step-footerNav'

export default class DeedsSecondStepAnotherif extends Component {

  constructor (props) {
    super(props)
  }

  componentDidMount () {

  }

  previousStepHandler(){

  }

  nextStepHandler(){

  }

  render () {
    return (
      <div className={`DeedsSecondStep__anotherif ${this.props.step === 3 ? '' : 'displayNone' }`}>

        <DeedsSecondStepRequiredInput
          text={'Realización de otro sí'}
          required={this.props.anotherifRequired}
          handler={this.props.anotherifRequiredHandler}
          step={this.props.step}
        />

        <div className={`DeedsSecondStep__anotherifContent ${this.props.anotherifRequired ? '' : 'displayNone' }`}>

          <figure>
            <img src="/static/img/deeds-secondstep-another.jpg" alt=""/>
          </figure>

          <p>
            Te generamos un documento plantilla con varios datos ya puestos.
            Sin embargo, es necesario que tú te encargues de poner otros.
          </p>

          <p>Cada vez que encuentres el texto <span>{`"{{USER_INPUT}}"`}</span> es un dato que tú tendrás que poner manualmente</p>

          <div>
            <button>DESCARGAR DOCUMENTO</button>
          </div>

        </div>

        <DeedsSecondStepNoRequired
          required={this.props.anotherifRequired}
        />

        <DeedsSecondStepfooterNav
          step={this.props.step}
          currentStep={this.props.currentStep}
          nextStepHandler={this.nextStepHandler}
          previousStepHandler={this.previousStepHandler}
        />

      </div>
    )
  }
}
