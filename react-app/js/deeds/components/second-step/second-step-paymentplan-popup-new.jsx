import React, { Component } from 'react'
import {IconArrowDown, IconDeedsCalendar, IconPaymentPlan} from '../../svg/deeds-icons'
import moment from 'moment'

export default class DeedsSecondStepPaymentPlanPopupNew extends Component {

  constructor (props) {
    super(props)

    this.state = {
      currentDebt: 0,
      newInitialFee: 0,
      feeValue: 0,
      numberFees: 1,
      remainingDates: []
    }

    this.numberFeesHandler = this.numberFeesHandler.bind(this)

  }

  componentDidMount () {
    $('[data-toggle="datepicker"]').datepicker({format: 'yyyy-mm-dd', autoHide: true})

    $('#paymentPlanFirstDate').change((e) => {
      let remainingDates = []

      _.forEach(_.range(1, this.state.numberFees + 1), (n, k)=>{
        remainingDates.push(moment(e.target.value).add(n, 'M').format('YYYY-MM-DD'))
      })

      this.setState({remainingDates: remainingDates})
    })
  }

  updateValues (e, type) {
    let value = e.target.value.split('.').join('')
    this.setState({[`${type}`]: value})
  }

  numberFeesHandler(e){
    let numberFees = parseInt(e.target.value)
    this.setState({numberFees: numberFees}, () => {
      let feeValue = this.state.currentDebt / numberFees

      this.setState({feeValue})
    })
  }

  render () {
    return (
      <div className="DeedsSecondStep__paymentPlanPopupNew">
        <h5> <IconPaymentPlan /> Nuevo Plan de pago</h5>
        <div className="DeedsSecondStep__paymentPlanPopupTable">

          <div className="DeedsSecondStep__paymentPlanPopupTableHeader">
            <div>
              <label htmlFor="">Número de cuotas</label>
              <div>
                <IconArrowDown />
                <select name="" id="" onChange={this.numberFeesHandler}>
                  { _.times(10, (n)=> {
                      return <option value={n} key={n}>{n+1}</option>
                    })
                  }
                </select>
              </div>
            </div>
            <div>
              <span>$</span>
              <label htmlFor="">Nueva cuota inicial</label>
              <input type="text" value={this.state.newInitialFee.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")} onChange={(e) => this.updateValues(e, 'newInitialFee') } />
            </div>
            <div>
              <span>$</span>
              <label htmlFor="">Deuda actual</label>
              <input type="text" value={this.state.currentDebt.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")} onChange={(e) => this.updateValues(e, 'currentDebt') } />
            </div>
          </div>

          <div className="DeedsSecondStep__paymentPlanPopupTableBody">
            <div className="DeedsSecondStep__paymentPlanPopupTableHead">
              <div># cuota</div>
              <div>Fecha</div>
              <div>Valor de la cuota</div>
            </div>

            <div className="DeedsSecondStep__paymentPlanPopupTableFirstRow">
              <div>1</div>
              <div> <IconDeedsCalendar /> <input id="paymentPlanFirstDate" type="text" data-toggle="datepicker"/> </div>
              <div>$ {this.state.feeValue ? _.round(this.state.feeValue) : ''}</div>
              <div className="DeedsSecondStep__paymentPlanPopupRemainingDates">
                {
                  this.state.remainingDates.map((date, k) => {
                    return <div key={k}>{date}</div>
                  })
                }
              </div>
            </div>

            <div className="DeedsSecondStep__paymentPlanPopupTableCont">
              {
                _.range(1, this.state.numberFees + 1).map((n, k) => {
                  return (
                    <div className="DeedsSecondStep__paymentPlanPopupTableRow" key={k}>
                      <div>{n + 1}</div>
                      {/* <IconDeedsCalendar /> 15/08/2016 */}
                      <div> </div>
                      <div>$ {_.round(this.state.feeValue) }</div>
                    </div>
                  )
                })
              }
            </div>

          </div>
        </div>

        <button>GUARDAR</button>

      </div>
    )
  }
}
