import React, { Component } from 'react'
import DeedsSecondStepRequiredInput from './second-step-requiredInput'
import DeedsSecondStepPaymentPlanPopup from './second-step-paymentplan-popup'
import {IconArrowDownAction, IconWithout, IconCheckGreen, IconCheckRed, IconPaymentPlan, IconPencil} from '../../svg/deeds-icons'
import DeedsSecondStepNoRequired from './second-step-norequired'
import DeedsSecondStepfooterNav from './second-step-footerNav'
import DeedsSecondStepPaymentPlanPopupData from './second-step-paymentplan-popup-data'

export default class DeedsSecondStepPaymentPlan extends Component {

  constructor (props) {
    super(props)

    this.tower = this.props.unity.split('-')[0].slice(1)

    this.state = {
      accordionTabActive: 2,
      isOpenPopup: 0,
      isChecked: false,
      paymentPlanData: {}
    }

    this.openPopup = this.openPopup.bind(this)
    this.closePopup = this.closePopup.bind(this)
    this.nextStepHandler = this.nextStepHandler.bind(this)
    this.previousStepHandler = this.previousStepHandler.bind(this)
  }

  componentDidMount () {
    $.ajax({
      url: `/api/projects/${this.props.pslug}/deeds/tower/${this.tower}/unity/${this.props.unity}/check-payments/`
    })
    .done((data) => {
      console.log(data, 'ji')
      this.setState({paymentPlanData: data})
    })
    .fail((error)=> console.log(error))

  }

  accordionHandler(accordionTabActive){
    if(accordionTabActive === this.state.accordionTabActive){
      return this.setState({accordionTabActive: 0})
    }
    this.setState({accordionTabActive})
  }

  openPopup(){
    this.setState({isOpenPopup: 1})
  }

  closePopup(){
    this.setState({isOpenPopup: 2})
    setTimeout(()=> this.setState({isOpenPopup: 0}), 200)
  }

  previousStepHandler(){

  }

  nextStepHandler(){

  }

  render () {
    let {paymentPlanData} = this.state

    return paymentPlanData.unity_price ? (
      <div className={`DeedsSecondStep__paymentPlan ${this.props.step === 2 ? '' : 'displayNone'}`}>

        <DeedsSecondStepPaymentPlanPopup
          isOpenPopup={this.state.isOpenPopup}
          closePopup={this.closePopup}
          paymentPlanData={paymentPlanData}

        />

        <DeedsSecondStepRequiredInput
          text={'Verificación del Plan de Pago'}
          handler={this.props.paymentPlanRequiredHandler}
          required={this.props.paymentPlanRequired}
          step={this.props.step}
        />

        <div className={`DeedsSecondStep__paymentPlanData ${this.props.paymentPlanRequired ? '' : 'displayNone'}`}>

          <DeedsSecondStepPaymentPlanPopupData
            paymentPlanData={paymentPlanData}
            lateralPadding={'yes'}
          />

          <div className="DeedsSecondStep__paymentPlanFees">
            <div className="DeedsSecondStep__paymentPlanFeesHeader">
              <h4> <IconPaymentPlan /> Plan de pago</h4>
              <span onClick={this.openPopup}> <IconPencil /> <div>Modificar plan de pago</div></span>
            </div>
            <div className={`DeedsSecondStep__paymentPlanFeesCancel ${this.state.accordionTabActive === 1 ? 'heightAuto' : ''}`}>
              <div onClick={()=> this.accordionHandler(1)}> <IconCheckGreen /> Cuotas canceladas: 9   <IconArrowDownAction color={'#81C784'} state={this.state.accordionTabActive} tab={1}/>  </div>
              <div className="DeedsSecondStep__paymentPlanFeesTable">
                <div className="DeedsSecondStep__paymentPlanFeesHead">
                  <div># cuota</div>
                  <div>Fecha</div>
                  <div>Valor de la cuota</div>
                  <div>Cuota extraordinaria</div>
                  <div>Pago realizado</div>
                </div>

                <div className="DeedsSecondStep__paymentPlanFeesBody">

                  <div className="DeedsSecondStep__paymentPlanFeesRow">
                    <div>1</div>
                    <div>15/08/2016</div>
                    <div>20.000.000</div>
                    <div>20.000.000</div>
                    <div>20.000.000</div>
                  </div>

                  <div className="DeedsSecondStep__paymentPlanFeesRow">
                    <div>1</div>
                    <div>15/08/2016</div>
                    <div>20.000.000</div>
                    <div>20.000.000</div>
                    <div>20.000.000</div>
                  </div>

                </div>

              </div>
            </div>

            <div className={`DeedsSecondStep__paymentPlanFeesNoCancel ${this.state.accordionTabActive === 2 ? 'heightAuto' : ''}`}>
              <div onClick={()=> this.accordionHandler(2)}> <IconCheckRed /> Cuotas sin canceladar: 5  <IconArrowDownAction color={'#FF8A80'} state={this.state.accordionTabActive} tab={2}/> </div>
              <div className="DeedsSecondStep__paymentPlanFeesTable">
                <div className="DeedsSecondStep__paymentPlanFeesHead">
                  <div># cuota</div>
                  <div>Fecha</div>
                  <div>Valor de la cuota</div>
                  <div>Cuota extraordinaria</div>
                  <div>Saldo pendiente</div>
                </div>

                <div className="DeedsSecondStep__paymentPlanFeesBody">

                  <div className="DeedsSecondStep__paymentPlanFeesRow">
                    <div>1</div>
                    <div>15/08/2016</div>
                    <div>20.000.000</div>
                    <div>20.000.000</div>
                    <div>20.000.000</div>
                  </div>

                </div>

              </div>
            </div>

          </div>

        </div>

        <DeedsSecondStepNoRequired
          required={this.props.paymentPlanRequired}
        />

        <DeedsSecondStepfooterNav
          step={this.props.step}
          currentStep={this.props.currentStep}
          nextStepHandler={this.nextStepHandler}
          previousStepHandler={this.previousStepHandler}
        />

      </div>
    ) : null
  }
}
