import React, { Component } from 'react'

export default class DeedsSecondStepChangesPopup extends Component {

  constructor (props) {
    super(props)

    this.state = {

    }

    this.saveHandler = this.saveHandler.bind(this)
    this.noSaveHandler = this.noSaveHandler.bind(this)
  }

  componentDidMount () {

  }

  saveHandler(){
    this.props.saveChangesPopup('save')
  }

  noSaveHandler(){
    this.props.saveChangesPopup()
  }

  render () {
    return (
      <div className={`DeedsSecondStep__paymentPlanPopup ${this.props.thereAreChanges ? '' : 'displayNone'}`}>

        <div className={`DeedsSecondStep__changesPopupContent ${this.props.isOpenPopup === 1 ? 'animated zoomIn' : 'animated zoomOut'}`}>
          <p>Has realizado cambios y no lo guardaste</p>
          <span>Deseas:</span>
          <div className="DeedsSecondStep__changesPopupButtons">
            <button onClick={this.saveHandler}>Guardar y continuar</button>
            <button onClick={this.noSaveHandler}>No guardar y continuar</button>
          </div>
        </div>

      </div>
    )
  }
}