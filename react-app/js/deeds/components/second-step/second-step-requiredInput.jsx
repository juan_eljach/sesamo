import React, { Component } from 'react'
import Switch from 'rc-switch'

export default class DeedsSecondStepRequiredInput extends Component {

  constructor (props) {
    super(props)
  }

  componentDidMount () {

  }

  render () {
    return (
      <div className="DeedsSecondStep__requiredInput">
        <p><span>{this.props.step}</span>{this.props.text}</p>
        <div className="DeedsSecondStep__requiredInputSwitch">
          <Switch onChange={this.props.handler} checked={this.props.required}  />
          {/* <Switch onChange={this.props.handler} className={`${this.props.required ? '' : 'displayNone'}`} /> */}
          <label className={`${this.props.required ? 'blueColor' : ''}`} htmlFor="">{`${this.props.required || this.props.isChecked ? 'Requerido' : 'No Requerido'}`}</label>
        </div>
      </div>
    )
  }
}
