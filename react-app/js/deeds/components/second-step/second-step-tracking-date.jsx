import React, { Component } from 'react'
import DeedsSecondStepRequiredInput from './second-step-requiredInput'
import {IconCheckFlex, IconDeedsCalendar, IconArrowDown} from '../../svg/deeds-icons'
import DeedsSecondStepNoRequired from './second-step-norequired'
import DeedsSecondStepfooterNav from './second-step-footerNav'
import DeedsSecondStepChangesPopup from './second-step-changesPopup'
import getCsrfToken from '../../../lib/csrftoken'

export default class DeedsSecondStepTrackingDate extends Component {

  constructor (props) {
    super(props)

    this.state = {
      isCheckLegalSign: false,
      isCheckBankSign: false,
      isCheckDeedsOfice: false,
      isCheckMoneyBank: false,
      trackingInfo: null,
      thereAreChanges: false
    }

    this.checkLegalSignHandler = this.checkLegalSignHandler.bind(this)
    this.checkBankSignHandler = this.checkBankSignHandler.bind(this)
    this.checkDeedsOfficeHandler = this.checkDeedsOfficeHandler.bind(this)
    this.checkBankMoneyBankHandler = this.checkBankMoneyBankHandler.bind(this)
    this.nextStepHandler = this.nextStepHandler.bind(this)
    this.saveChanges = this.saveChanges.bind(this)
    this.saveChangesPopup = this.saveChangesPopup.bind(this)
  }

  componentDidMount () {
    $('[data-toggle="datepicker"]').datepicker({format: 'yyyy-mm-dd', autoHide: true})

    this.$deedSignedLegal = $('#deedSignedLegal')
    this.deedSignedBank = $('#deedSignedBank')
    this.deedArrivesOffice = $('#deedArrivesOffice')
    this.bankCashOutlay = $('#bankCashOutlay')

    $.ajax({
      url: `/api/projects/${this.props.pslug}/deeds/tower/${this.props.unity.split('-')[0].slice(1)}/unity/${this.props.unity}/dates-tracking/`
    })
    .done((data) => {
      console.log(data, 'aaaa');
      this.setState({trackingInfo: data})

      this.props.trackingDateRequiredHandler(data.required)

      this.setState({isCheckLegalSign: data.deed_signed_by_legal_representative})
      this.setState({isCheckBankSign: data.deed_signed_by_bank})
      this.setState({isCheckDeedsOfice: data.deed_arrives_to_office})
      this.setState({isCheckMoneyBank: data.bank_cash_outlay})

      this.$deedSignedLegal.datepicker('setDate', data.deed_signed_by_legal_representative)
      this.deedSignedBank.datepicker('setDate', data.deed_signed_by_bank)
      this.deedArrivesOffice.datepicker('setDate', data.deed_arrives_to_office)
      this.bankCashOutlay.datepicker('setDate', data.bank_cash_outlay)

      $('#orderTasksInputs [data-toggle="datepicker"]').change(()=> this.props.changesHandler(1))
    })
    .fail((error)=> {
      console.log(error, 'ooo')
    })

  }

  checkLegalSignHandler(){
    this.props.changesHandler(1)
    if(this.state.isCheckLegalSign){
      return this.setState({isCheckLegalSign: false})
    }
    this.setState({isCheckLegalSign: true})
  }

  checkBankSignHandler(){
    this.props.changesHandler(1)
    if(this.state.isCheckBankSign){
      return this.setState({isCheckBankSign: false})
    }

    this.setState({isCheckBankSign: true})
  }

  checkDeedsOfficeHandler(){
    this.props.changesHandler(1)
    if(this.state.isCheckDeedsOfice){
      return this.setState({isCheckDeedsOfice: false})
    }

    this.setState({isCheckDeedsOfice: true})
  }

  checkBankMoneyBankHandler(){
    this.props.changesHandler(1)
    if(this.state.isCheckMoneyBank){
      return this.setState({isCheckMoneyBank: false})
    }

    this.setState({isCheckMoneyBank: true})
  }

  previousStepHandler(){

  }

  nextStepHandler(){
    if((this.props.tasksDone + 6) === this.props.step){
      this.props.taskDoneHandler(this.props.tasksDone + 6)
    }

    if(this.props.step === 6){
      if(this.props.changes === 1){
        return this.setState({thereAreChanges: true})
      }
      this.props.stepHandler(6)
    }
  }

  saveChangesPopup(action){
    if(action === 'save'){
      this.saveChanges()
    }
    this.setState({thereAreChanges: false})
    this.props.changesHandler(0)
    this.props.stepHandler(6)
  }

  saveChanges(){
    let data = {
      required: this.props.trackingDateRequired,
      unity: this.props.unityInfo.id,
      deed_signed_by_legal_representative: this.$deedSignedLegal.val(),
      deed_signed_by_bank: this.deedSignedBank.val(),
      deed_arrives_to_office: this.deedArrivesOffice.val(),
      bank_cash_outlay: this.bankCashOutlay.val()
    }

    $.ajax({
      url: `/api/projects/${this.props.pslug}/deeds/tower/${this.props.unity.split('-')[0].slice(1)}/unity/${this.props.unity}/dates-tracking/`,
      method: this.state.trackingInfo ? 'PUT' : 'POST',
      headers: {
        'X-CSRFToken': getCsrfToken('csrftoken')
      },
      data
    })
    .done((data) => {
      console.log(data);
      this.setState({trackingInfo: data})
      this.props.changesAlertHandler()

    })
    .fail((error)=> {
      console.log(error)
    })

  }

  render () {
    return (
      <div className={`DeedsSecondStep__bank ${this.props.step === 6 ? '' : 'displayNone' }`}>

        <DeedsSecondStepRequiredInput
          text={'Seguimiento de fechas interno'}
          handler={this.props.trackingDateRequiredHandler}
          required={this.props.trackingDateRequired}
          step={this.props.step}
        />

        <div className={`DeedsSecondStep__bankContent ${this.props.trackingDateRequired ? '' : 'displayNone'}`}>
          <figure>
            <img src="/static/img/deeds-secondstep-datetracking.png" width="190" alt=""/>
            <p>En este paso tendrás que realizar las siguientes tareas y marcarlas a medida que las realices  para poder avanzar</p>
          </figure>

          <div className="DeedsSecondStep__bankTasks">
            <h3>Tareas:</h3>

            <div>
              <IconCheckFlex
                isCheck={this.state.isCheckLegalSign}
                checkHandler={this.checkLegalSignHandler}
              />
              <span onClick={this.checkLegalSignHandler} className={`${this.state.isCheckLegalSign ? 'displayNone' : ''}`}></span>
              <p>Firma de escritura por parte del representante legal</p>

              <div className={`${this.state.isCheckLegalSign ? '' : 'displayNone'}`}>
                <IconDeedsCalendar />
                <IconArrowDown />
                <input id="deedSignedLegal" type="text" placeholder="Elige fecha de realización" data-toggle="datepicker"/>
              </div>

            </div>

            <div>
              <IconCheckFlex
                isCheck={this.state.isCheckBankSign}
                checkHandler={this.checkBankSignHandler}
              />
              <span onClick={this.checkBankSignHandler} className={`${this.state.isCheckBankSign ? 'displayNone' : '' }`}></span>
              <p>Firma de escritura por parte del Banco</p>

              <div className={`${this.state.isCheckBankSign ? '' : 'displayNone'}`}>
                <IconDeedsCalendar />
                <IconArrowDown />
                <input id="deedSignedBank" type="text" placeholder="Elige fecha de realización" data-toggle="datepicker"/>
              </div>

            </div>

            <div>
              <IconCheckFlex
                isCheck={this.state.isCheckDeedsOfice}
                checkHandler={this.checkDeedsOfficeHandler}
              />
              <span onClick={this.checkDeedsOfficeHandler} className={`${this.state.isCheckDeedsOfice ? 'displayNone' : '' }`}></span>
              <p>Escrituras llegaron a la oficina</p>

              <div className={`${this.state.isCheckDeedsOfice ? '' : 'displayNone'}`}>
                <IconDeedsCalendar />
                <IconArrowDown />
                <input id="deedArrivesOffice" type="text" placeholder="Elige fecha de realización" data-toggle="datepicker"/>
              </div>

            </div>

            <div>
              <IconCheckFlex
                isCheck={this.state.isCheckMoneyBank}
                checkHandler={this.checkBankMoneyBankHandler}
              />
              <span onClick={this.checkBankMoneyBankHandler} className={`${this.state.isCheckMoneyBank ? 'displayNone' : '' }`}></span>
              <p>Desembolso del dinero al banco</p>

              <div className={`${this.state.isCheckMoneyBank ? '' : 'displayNone'}`}>
                <IconDeedsCalendar />
                <IconArrowDown />
                <input id="bankCashOutlay" type="text" placeholder="Elige fecha de realización" data-toggle="datepicker"/>
              </div>

            </div>

          </div>

        </div>

        <DeedsSecondStepNoRequired
          required={this.props.trackingDateRequired}
        />

        <DeedsSecondStepfooterNav
          step={this.props.step}
          currentStep={this.props.currentStep}
          nextStepHandler={this.nextStepHandler}
          previousStepHandler={this.previousStepHandler}
          saveChanges={this.saveChanges}
        />

        <DeedsSecondStepChangesPopup
          thereAreChanges={this.state.thereAreChanges}
          saveChangesPopup={this.saveChangesPopup}
        />

      </div>
    )
  }
}