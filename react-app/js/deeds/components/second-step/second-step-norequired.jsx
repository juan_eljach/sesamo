import React, { Component } from 'react'

export default class DeedsSecondStepNoRequired extends Component {

  constructor (props) {
    super(props)
  }

  componentDidMount () {

  }

  render () {
    return (
      <div className={`DeedsSecondStep__noRequired ${this.props.required ? 'displayNone' : ''}`}>
        <figure>
          <img src="/static/img/deeds-secondstep-norequired.png" alt=""/>
        </figure>
        <p>Este paso está marcado como no requerido, si lo requieres sólo cambia al modo activado</p>
      </div>
    )
  }
}