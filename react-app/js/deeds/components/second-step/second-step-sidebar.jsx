import React, { Component } from 'react'
import {IconCheck} from '../../svg/deeds-icons'
import getCsrfToken from '../../../lib/csrftoken'

export default class DeedsSecondStepSidebar extends Component {
  constructor (props) {
    super(props)

    this.commentText = ''

    this.state = {
      commentsTab: 1,
      userLogged: {},
      commentInputIsValid: false
    }

    this.tabHandler = this.tabHandler.bind(this)
    this.sendComment = this.sendComment.bind(this)
    this.updateCommentText = this.updateCommentText.bind(this)
    this.menuStepHandler = this.menuStepHandler.bind(this)
  }

  componentDidMount () {
    this.$commentInput = $('#commentInput')

    $.ajax({
      method: 'GET',
      url: `/api/userprofile/`
    })
    .done((data)=> {
      console.log(data)
      this.setState({userLogged: data})
    })
    .fail((error)=> console.log(error))
  }

  tabHandler(commentsTab){
    this.setState({commentsTab})
  }

  updateCommentText(e){
    if(this.commentText){
      this.setState({commentInputIsValid: false})
    }
    this.commentText =  e.target.value
  }

  sendComment(){

    if(!this.commentText){
      return this.setState({commentInputIsValid: true})
    }

    let payload = {
      unity_profile: 't2-101',
      note: this.commentText,
      written_by: this.state.userLogged.id
    }

    console.log(payload)

    $.ajax({
      // /api/projects/villa-paraiso/crm/unities/t2-103/notes/
      url: `/api/projects/${this.props.pslug}/crm/unities/t2-101/notes/`,
      method: 'POST',
      data: JSON.stringify(payload),
      headers: {
        'X-CSRFToken': getCsrfToken('csrftoken'),
        'Content-Type' : 'application/json'
      }
    })
    .done((data) => {
      console.log(data)
      this.$commentInput.val('')
    })
    .fail((error)=> console.log(error))
  }

  menuStepHandler(step){
    this.props.stepHandler(step)
  }

  render () {
    return (
      <div className="DeedsSecondStep__sidebar">

        <div className="DeedsSecondStep__sidebarClientInfo">
          <div className="DeedsSecondStep__sidebarClientInfoHeader">
            <div>
              <span>VA</span>
            </div>
            <div>
              <h4>Victor Aguirre</h4>
            </div>
            <div>Apto {this.props.unity}</div>
          </div>
          <div className="DeedsSecondStep__sidebarClientInfoBody">
            <div>Información de contacto</div>
            <div className="DeedsSecondStep__sidebarClientInfoData">
              <div>
                <div>Celular:</div>
                <div>3168654324</div>
              </div>
              <div>
                <div>Teléfono:</div>
                <div>3168654324</div>
              </div>
              <div>
                <div>Correo:</div>
                <div>victor@gmail.com</div>
              </div>
            </div>
          </div>
        </div>

        <div className="DeedsSecondStep__sidebarTab">
          <div className={`${this.state.commentsTab === 1 ? 'activeTab' : ''}`} onClick={ ()=> this.tabHandler(1) }>Tareas</div>
          <div className={`${this.state.commentsTab === 2 ? 'activeTab' : ''}`} onClick={ ()=> this.tabHandler(2)}>Comentarios</div>
        </div>

        <div className="DeedsSecondStep__sidebarWrap">
          <div className={`DeedsSecondStep__comments ${this.state.commentsTab === 2 ? '' : 'displayNone'}`}>
            <div className='DeedsSecondStep__commentInput'>
              <span>AG</span>
              <textarea name="" id="commentInput" cols="30" rows="3" placeholder='Escribe tu comentario' onChange={this.updateCommentText}></textarea>
              <button onClick={this.sendComment}>Publicar</button>
              <div className={`${this.state.commentInputIsValid ? '' : 'displayNone'}`}>Campo inválido</div>
            </div>

            <div className="DeedsSecondStep__commentList">

              <div className="DeedsSecondStep__comment">
                <div>AG</div>
                <div>
                  <span>Hace 5 min</span>
                  <h3>Angie Gómez</h3>
                  <p>El cliente va a esperar un mes para validar el primer paso</p>
                </div>
              </div>

              <div className="DeedsSecondStep__comment">
                <div>AG</div>
                <div>
                  <span>Hace 5 min</span>
                  <h3>Angie Gómez</h3>
                  <p>El cliente va a esperar un mes para validar el primer paso</p>
                </div>
              </div>
            </div>
          </div>

          <div className={`DeedsSecondStep__tasks ${this.state.commentsTab === 1 ? '' : 'displayNone'}`}>
            <div className="DeedsSecondStep__progressTask">
              <label>Progreso</label>
              <div>
                <span style={{width:`${this.props.tasksDone > 0 ? ((this.props.tasksDone + 1) * 14) : 0}%`}}></span>
              </div>
              <span>{`${this.props.tasksDone > 0 ? ((this.props.tasksDone + 1) * 14) : 0}%`}</span>
            </div>

            <ul>

              <li className={`${this.props.step === 1 ? 'DeedsSecondStep__currentTask' : ''}`} onClick={()=> this.menuStepHandler(1)} style={{textDecoration: `${this.props.tasksDone > 0 ? 'line-through' : ''}`}}>
                <span className={`${this.props.tasksDone > 0 ? 'displayNone' : ''}`}>1</span>
                <IconCheck isCheck={this.props.tasksDone > 0} />
                Crédito
              </li>
              <li className={`${this.props.step === 2 ? 'DeedsSecondStep__currentTask' : ''}`} onClick={()=> this.menuStepHandler(2)} style={{textDecoration: `${this.props.tasksDone > 1 ? 'line-through' : ''}`}}>
                <span className={`${this.props.tasksDone > 1 ? 'displayNone' : ''}`}>2</span>
                <IconCheck isCheck={this.props.tasksDone > 1} />
                Verificación de pagos
              </li>
              <li className={`${this.props.step === 3 ? 'DeedsSecondStep__currentTask' : ''}`} onClick={()=> this.menuStepHandler(3)} style={{textDecoration: `${this.props.tasksDone > 2 ? 'line-through' : ''}`}}>
                <span className={`${this.props.tasksDone > 2 ? 'displayNone' : ''}`}>3</span>
                <IconCheck isCheck={this.props.tasksDone > 2} />
                Otro sí
              </li>
              <li className={`${this.props.step === 4 ? 'DeedsSecondStep__currentTask' : ''}`} onClick={()=> this.menuStepHandler(4)} style={{textDecoration: `${this.props.tasksDone > 3 ? 'line-through' : ''}`}}>
                <span className={`${this.props.tasksDone > 3 ? 'displayNone' : ''}`}>4</span>
                <IconCheck isCheck={this.props.tasksDone > 3} />
                Confirmación con Banco
              </li>
              <li className={`${this.props.step === 5 ? 'DeedsSecondStep__currentTask' : ''}`} onClick={()=> this.menuStepHandler(5)} style={{textDecoration: `${this.props.tasksDone > 4 ? 'line-through' : ''}`}}>
                <span className={`${this.props.tasksDone > 4 ? 'displayNone' : ''}`}>5</span>
                <IconCheck isCheck={this.props.tasksDone > 4} />
                Orden de escrituración
              </li>
              <li className={`${this.props.step === 6 ? 'DeedsSecondStep__currentTask' : ''}`} onClick={()=> this.menuStepHandler(6)} style={{textDecoration: `${this.props.tasksDone > 5 ? 'line-through' : ''}`}}>
                <span className={`${this.props.tasksDone > 5 ? 'displayNone' : ''}`}>6</span>
                <IconCheck isCheck={this.props.tasksDone > 5} />
                Seguimiento de fechas
              </li>
              <li>
                <span><span></span></span>Finalización del proceso
              </li>
            </ul>
          </div>

        </div>
      </div>
    )
  }
}
