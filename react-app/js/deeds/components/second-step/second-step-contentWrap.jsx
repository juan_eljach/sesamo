import React, { Component } from 'react'
import DeedsSecondStepCredit from './second-step-credit'
import DeedsSecondStepPaymentPlan from './second-step-paymentPlan'
import DeedsSecondStepAnotherif from './second-step-anotherif'
import DeedsSecondStepBank from './second-step-bank'
import DeedsSecondStepOrder from './second-step.order'
import DeedsSecondStepTrackingDate from './second-step-tracking-date'
import DeedsSecondStepfooterNav from './second-step-footerNav'

export default class DeedsSecondStepContentWrap extends Component {

  constructor (props) {
    super(props)

    this.currentStep = [
      'Crédito',
      'Verificación de pagos',
      'Otro sí',
      'Confirmación con banco',
      'Orden de escrituración',
      'Seguimiento de fechas'
    ]

    this.state = {
      paymentPlanRequired: true,
      anotherifRequired: false,
      bankRequired: false,
      orderRequired: false,
      trackingDateRequired: false,
      unityInfo: {},
      changes: 0,
      // changeAlert -> 0: displayNone 1: fadeIn  2: fadeOut
      changesAlert: 0
    }

    this.paymentPlanRequiredHandler = this.paymentPlanRequiredHandler.bind(this)
    this.anotherifRequiredHandler = this.anotherifRequiredHandler.bind(this)
    this.bankRequiredHandler = this.bankRequiredHandler.bind(this)
    this.trackingDateRequiredHandler = this.trackingDateRequiredHandler.bind(this)
    this.updateRequired = this.updateRequired.bind(this)
    this.changesHandler = this.changesHandler.bind(this)
    this.changesAlertHandler = this.changesAlertHandler.bind(this)
  }

  componentDidMount () {
    $('[data-toggle="datepicker"]').datepicker({format: 'yyyy-mm-dd', autoHide: true})

    $.ajax({
      url: `/api/projects/${this.props.pslug}/deeds/tower/${this.props.unity.split('-')[0][1]}/unity/${this.props.unity}/`,
    })
    .done((data) => {
      this.setState({unityInfo: data})
    })
    .fail((error)=> console.log(error))
  }

  updateRequired(type, value){
    this.setState({[`${type}`]: value})
  }

  paymentPlanRequiredHandler(value){
    this.setState({paymentPlanRequired: value})
  }

  anotherifRequiredHandler(value){
    this.setState({anotherifRequired: value})
  }

  bankRequiredHandler(value){
    this.setState({bankRequired: value})
  }

  orderRequiredHandler(value){
    this.setState({orderRequired: value})
  }

  trackingDateRequiredHandler(value){
    this.setState({trackingDateRequired: value})
  }

  changesHandler(changes){
    this.setState({changes})
  }

  changesAlertHandler(){
    this.setState({changesAlert: 1})

    setTimeout(()=>{
      this.setState({changesAlert: 2})
    }, 2500)
  }

  render () {
    let changesAlert = this.state.changesAlert

    return (
      <div className="DeedsSecondStep__contentWrap">

        <div className="DeedsSecondStep__paymentsVerification">

          <DeedsSecondStepCredit
            certifiedRequired={this.state.certifiedRequired}
            certifiedRequiredHandler={this.certifiedRequiredHandler}
            menuActive={this.props.menuActive}
            itemActive={this.props.itemActive}
            step={this.props.step}
            stepHandler={this.props.stepHandler}
            unity={this.props.unity}
            unityInfo={this.state.unityInfo}
            updateRequired={this.updateRequired}
            currentStep={this.currentStep}
            tasksDone={this.props.tasksDone}
            taskDoneHandler={this.props.taskDoneHandler}
            pslug={this.props.pslug}
            changes={this.state.changes}
            changesHandler={this.changesHandler}
            changesAlertHandler={this.changesAlertHandler}
          />

          <DeedsSecondStepPaymentPlan
            paymentPlanRequired={this.state.paymentPlanRequired}
            paymentPlanRequiredHandler={this.paymentPlanRequiredHandler}
            menuActive={this.props.menuActive}
            itemActive={this.props.itemActive}
            step={this.props.step}
            stepHandler={this.props.stepHandler}
            currentStep={this.currentStep}
            unity={this.props.unity}
            pslug={this.props.pslug}
          />

          <DeedsSecondStepAnotherif
            anotherifRequired={this.state.anotherifRequired}
            anotherifRequiredHandler={this.anotherifRequiredHandler}
            step={this.props.step}
            currentStep={this.currentStep}
          />

          <DeedsSecondStepBank
            bankRequired={this.state.bankRequired}
            bankRequiredHandler={this.bankRequiredHandler}
            step={this.props.step}
            stepHandler={this.props.stepHandler}
            currentStep={this.currentStep}
            unity={this.props.unity}
            unityInfo={this.state.unityInfo}
            tasksDone={this.props.tasksDone}
            taskDoneHandler={this.props.taskDoneHandler}
            pslug={this.props.pslug}
            changes={this.state.changes}
            changesHandler={this.changesHandler}
            changesAlertHandler={this.changesAlertHandler}
          />

          <DeedsSecondStepOrder
            orderRequired={this.state.bankRequired}
            orderRequiredHandler={this.bankRequiredHandler}
            step={this.props.step}
            stepHandler={this.props.stepHandler}
            unity={this.props.unity}
            unityInfo={this.state.unityInfo}
            tasksDone={this.props.tasksDone}
            taskDoneHandler={this.props.taskDoneHandler}
            currentStep={this.currentStep}
            pslug={this.props.pslug}
            changes={this.state.changes}
            changesHandler={this.changesHandler}
            changesAlertHandler={this.changesAlertHandler}
          />

          <DeedsSecondStepTrackingDate
            trackingDateRequired={this.state.trackingDateRequired}
            trackingDateRequiredHandler={this.trackingDateRequiredHandler}
            step={this.props.step}
            stepHandler={this.props.stepHandler}
            currentStep={this.currentStep}
            unity={this.props.unity}
            unityInfo={this.state.unityInfo}
            tasksDone={this.props.tasksDone}
            taskDoneHandler={this.props.taskDoneHandler}
            pslug={this.props.pslug}
            changes={this.state.changes}
            changesHandler={this.changesHandler}
            changesAlertHandler={this.changesAlertHandler}
          />

        </div>

        <div
          className={
            `DeedsSecondStep__changesAlert animated
            ${changesAlert === 0 ? 'displayNone' : changesAlert === 1 ? 'fadeInRight' : 'fadeOutRight'}`}>

          <p>Los cambios se han guardado éxitosamente</p>

        </div>

      </div>
    )
  }
}
