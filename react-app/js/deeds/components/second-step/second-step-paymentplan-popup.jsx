import React, { Component } from 'react'
import DeedsSecondStepPaymentPlanPopupData from './second-step-paymentplan-popup-data'
import DeedsSecondStepPaymentPlanPopupNew from './second-step-paymentplan-popup-new'
import {IconArrowDown, IconDeedsCalendar} from '../../svg/deeds-icons'

export default class DeedsSecondStepPaymentPlanPopup extends Component {

  constructor (props) {
    super(props)

    this.state = {

    }
  }

  componentDidMount () {
    $('[data-toggle="datepicker"]').datepicker({format: 'yyyy-mm-dd', autoHide: true})
  }

  render () {
    return (
      <div className={`DeedsSecondStep__paymentPlanPopup ${this.props.isOpenPopup === 0 ? 'displayNone' : ''}`}>

        <div className={`DeedsSecondStep__paymentPlanPopupCont ${this.props.isOpenPopup === 1 ? 'animated zoomIn' : 'animated zoomOut'}`}>
          <span className="DeedsSecondStep__paymentPlanPopupClose" onClick={this.props.closePopup}>x</span>
          <h2>Modificar plan de Pago</h2>

          <DeedsSecondStepPaymentPlanPopupData
            paymentPlanData={this.props.paymentPlanData}
          />

          <DeedsSecondStepPaymentPlanPopupNew
            paymentPlanData={this.props.paymentPlanData}
          />

        </div>

      </div>
    )
  }
}
