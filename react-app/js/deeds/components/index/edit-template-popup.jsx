import React, { Component } from 'react'

export default class DeedsEditTemplatePopup extends Component {
  constructor (props) {
    super(props)

  }

  render () {
    return (
      <div className={`Deeds__editTemplatePopup ${this.props.isOpenEditTemplatePopup ? '' : 'displayNone' }`}>
        <div>
          <nav className="Deeds__editTemplateNav">
            <div onClick={this.props.closeEditTemplatePopup}>CANCELAR</div>
            <div>Confirma el mensaje de la notificación</div>
            <div onClick={this.props.sendNotification}>GUARDAR Y ENVIAR</div>
          </nav>
          <div className="Deeds__editTemplateContainer">
            <div className="Deeds__editTemplateHead">
              <div>Edita el mensaje del documento</div>
              <div onClick={this.props.editTemplateNotification}>{ `${this.props.editingTemplateNotification ? 'Editando' : 'Editar'}` }</div>
            </div>
            <div id="contentNotificationTemplate" className="Deeds__editTemplateBody">

            </div>
          </div>
        </div>
      </div>
    )
  }
}
