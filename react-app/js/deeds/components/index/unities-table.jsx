import React, { Component } from 'react'
import {IconCheck} from '../../svg/deeds-icons'
import { Link } from 'react-router'

export default class DeedsUnitiesTable extends Component {

  constructor (props) {
    super(props)

    this.randomColors = [
      '#81c784',
      '#9575cd',
      '#ffd54f',
      '#29b6f6',
      '#ff8a80',
      '#f06292'
    ]
  }

  render () {
    return (
      <div className="Deeds__table">
        <div className="Deeds__tableHead">
          <div>
            <span className={`Deeds__tableHeadCheck ${this.props.isAllCheck ? 'displayNone' : ''}`} onClick={this.props.allIsCheckHandler}></span>
            <IconCheck
              isCheck={this.props.isAllCheck}
              checkHandler={this.props.allCheckSvgHandler}
              unity={'nothing'}
            />
          </div>
          <div>Apartamento</div>
          <div>Cliente</div>
          <div>Estado</div>
          <div>Fecha de notificación</div>
          <div>Documento</div>
        </div>

        <div className={`Deeds__withoutNotifications ${this.props.unities.length > 0 || this.props.loading || this.props.noResultsFilter ? 'displayNone' : ''}`}>
          <p>La torre { this.props.towerSelected } que seleccionaste está sin notificar</p>
          <button onClick={this.props.redirectHandler}>Notificar esta torre</button>
        </div>

        <div className={`Deeds__noResults ${!this.props.noResultsFilter || this.props.unities.length || this.props.loading ? 'displayNone' : ''}`}>No se encontraron resultados</div>

        <div className="Deeds__tableBody">

          {
            this.props.unities.map((unity, key) => {

              return (
                <div className="Deeds__tableRow" key={key}>
                  <div>
                    <span className={`Deeds__tableRowCheck ${(_.find(this.props.selectedUnities, (id) => id === unity.id )) || this.props.isAllCheck ? 'displayNone' : ''}`} onClick={() => this.props.checkHandler(unity)}></span>
                    <IconCheck
                      isCheck={_.some(this.props.selectedUnities, (u) => u === unity.id) }
                      checkHandler={this.props.checkSvgHandler}
                      unity={unity}
                    />
                  </div>
                  <div><Link to={`/deeds/second-step/${unity.slug}`}>{unity.name}</Link></div>
                  <div>

                    {
                      unity.clients ? (
                        <div>

                          {
                            unity.clients.map((client, key)=>{
                              if(unity.clients.length > 1) {


                                return (
                                  <div key={key}>
                                    <span className="Deeds__clientNameRound" style={{ background: `${ this.randomColors[Math.floor(Math.random() * 6)] }` }}>{`${client.split(' ')[0][0]} ${client.split(' ')[1][0]}`}</span>
                                    <div key={key}> {client}, </div>
                                  </div>
                                )

                              }

                              return <div key={key}><span className="Deeds__clientNameRound" style={{ background: `${ this.randomColors[Math.floor(Math.random() * 6)] }` }}>{`${client.split(' ')[0][0]} ${client.split(' ')[1][0]}`}</span> {client}</div>
                            })
                          }

                        </div>
                      ) : null
                    }
                  </div>
                  <div>{`${unity.unitynotificationlog === null ? 'Sin notificar' : 'Notificado'}`} </div>
                  <div>
                    {`${unity.unitynotificationlog === null ? 'Sin notificar' : unity.unitynotificationlog.email_notification_sent_timestamp === null ? 'Sin notificar' : unity.unitynotificationlog.email_notification_sent_timestamp }`}
                  </div>
                  <div>
                    <span className={`${unity.unitynotificationlog ? 'displayNone' : ''}`}>Sin documento</span>
                    <a href={`${unity.unitynotificationlog.document}`} target="_blank" className={`${unity.unitynotificationlog ? '' : 'displayNone'}`}>Documento {`${unity.name}.pdf`}</a>
                  </div>
                </div>
              )
            })
          }
        </div>
      </div>
    )
  }
}
