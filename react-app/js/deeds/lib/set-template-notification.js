function setTemplate(template) {
  return _.map(template.html().split(''), (c)=>{
    switch (c) {
      case 'á':
        c = '&aacute;'
        break;
      case 'é':
        c = '&eacute;'
        break;
      case 'í':
        c = '&iacute;'
        break;
      case 'ó':
        c = '&oacute;'
        break;
      case 'ú':
        c = '&uacute;'
        break;
      case 'Á':
        c = '&Aacute;'
        break;
      case 'É':
        c = '&Eacute;'
        break;
      case 'Í':
        c = '&Iacute;'
        break;
      case 'Ó':
        c = '&Oacute;'
        break;
      case 'Ú':
        c = '&Uacute;'
        break;
      case 'ñ':
        c = '&ntilde;'
        break;
      default:

    }
   return c

  }).join('')

}

export {setTemplate}
