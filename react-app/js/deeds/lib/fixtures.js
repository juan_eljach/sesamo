let template = {text: `<div id="focusNotificationEditing" style="font-family:Arial; font-style:normal; font-weight:200; font-size:14px; text-align:justify">
  <p>Bucaramanga, <span>{{date}}</span></p>
  <p>
    Señor (es): <br>
    <span>{{names}}</span> <br>
    Propietario(s) Apartamento <span>{{unity}}</span> Torre <span>{{tower}}</span> <br>
    Proyecto <span>{{project}}</span>
  </p>

  <p>Ref: Proyecto <span>{{project}}</span></p>

  <p>
    Por medio de la presente le informamos que a partir del día 15 de Octubre de 2016, el apartamento adquirido por usted(es) en el proyecto <span>{{project}}</span>, se encontrará listo para iniciar trámite de escrituración. <br>
    <br>
    Les recordamos que para esta fecha también deben tener una suma aproximada de DOS MILLONES QUINIENTOS MIL PESOS ($2.500.000) M/CTE, para gastos de escrituración y registro, según estipulado en la promesa de compraventa. <br>
    <br>
    Le solicitamos ponerse en contacto con nuestra oficina ubicada en la calle 30A No. 30 -18 ubicada en el Barrio la Aurora en Bucaramanga; con el fin de resolver cualquier inquietud que se pueda presentar.
    <br>
    <br>
    <br>
    Agradezco su atención,
  </p>
  <br>
  <br>
  <br>
  <br>
  <p>
    <div style="font-weight:bold">PEDRO CLAVER GÓMEZ VEGA</div>
    Representante Legal
  </p>
</div>`}

export {template}
