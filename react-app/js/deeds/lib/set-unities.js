function setUnities(unities){
  let towers = []
  let floors = []

  _.forEach(unities, (unity) => {

    let splitedUnity = unity.name.split('-')

    let floorApto = splitedUnity[1]
    unity.tower = splitedUnity[0]

    if(floorApto !== undefined && floorApto.length === 3){
      unity.floor = floorApto.substring(0,1)
      unity.apto = floorApto.substring(1,3)
    }
    if(floorApto !== undefined && floorApto.length == 4){
      unity.floor = floorApto.substring(0,2)
      unity.apto = floorApto.substring(2,4)
    }

    towers.push(unity.tower)
    floors.push(unity.floor)
  })

  towers = _.uniq(towers.sort())
  floors = _.uniq(_.orderBy(floors.map(Number)))

  return {
    towers,
    floors
  }
}

export {setUnities}
