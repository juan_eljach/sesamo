import React, { Component } from 'react'
import DeedsSecondStepNav from '../components/second-step/second-step-nav'
import DeedsSecondStepSidebar from '../components/second-step/second-step-sidebar'
import DeedsSecondStepContentWrap from '../components/second-step/second-step-contentWrap'

export default class DeedsSecondStep extends Component {

  constructor (props) {
    super(props)

    this.pslug = window.location.href.split('/')[4]
    this.unity = this.props.params.unity

    this.state = {
      menuActive: 2,
      itemActive: 1,
      step: 1,
      tasksDone: 0
    }

    this.menuActiveHandler = this.menuActiveHandler.bind(this)
    this.stepHandler = this.stepHandler.bind(this)
    this.taskDoneHandler = this.taskDoneHandler.bind(this)
  }

  // Endpoint para sabe si la unidad ya está notificada: http://localhost:8000/api/projects/villa-paraiso/deeds/tower/2/unity/t2-101/

  componentDidMount () {
    // replace(/\./g, '') replace points price inputs

    // $.ajax({
    //   url: `/api/projects/${this.pslug}/unities/`
    // })
    // .done((data) => {
    //   this.selectTowers(data)
    // })
    // .fail((error)=> console.log(error))
  }

  menuActiveHandler(menuActive){
    this.setState({menuActive})
  }

  stepHandler(step){
    this.setState({step})
  }

  taskDoneHandler(tasksDone){
    console.log('change');
    this.setState({tasksDone})
  }

  render () {
    return (
      <div className='DeedsSecondStep'>

        <div className="DeedsSecondStep__body">

          <div className="DeedsSecondStep__content">

            <DeedsSecondStepNav
              unity={this.unity}
              router={this.props.router}
            />

            <DeedsSecondStepContentWrap
              menuActive={this.state.menuActive}
              itemActive={this.state.itemActive}
              step={this.state.step}
              pslug={this.pslug}
              stepHandler={this.stepHandler}
              unity={this.unity}
              taskDoneHandler={this.taskDoneHandler}
              tasksDone={this.state.tasksDone}
            />

          </div>

          <DeedsSecondStepSidebar
            menuActiveHandler={this.menuActiveHandler}
            menuActive={this.state.menuActive}
            step={this.state.step}
            pslug={this.pslug}
            unity={this.unity}
            stepHandler={this.stepHandler}
            tasksDone={this.state.tasksDone}
          />

        </div>

      </div>

    )
  }
}
