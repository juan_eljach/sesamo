import React, { Component } from 'react'
import {IconArrowDown} from '../../support/svg/support-icons'

export default class DeedsFirstStepOnBoard extends Component {

  constructor (props) {
    super(props)

    this.pslug = window.location.href.split('/')[4]

    this.state = {
      step: 1,
      towers: [],
      selectedTower: 0
    }

    this.stepHandler = this.stepHandler.bind(this)
    this.selectTowers = this.selectTowers.bind(this)
    this.selectTowersHandler = this.selectTowersHandler.bind(this)
  }

  componentDidMount () {

    $.ajax({
      url: `/api/projects/${this.pslug}/unities/`
    })
    .done((data) => {
      this.selectTowers(data)
    })
    .fail((error)=> console.log(error))
  }

  selectTowers(unities){
    let towers = []

    _.forEach(unities, (unity) => {

      let splitedUnity = unity.slug.split('-')

      let floorApto = splitedUnity[1]
      unity.tower = splitedUnity[0]

      if(floorApto !== undefined && floorApto.length === 3){
        unity.floor = floorApto.substring(0,1)
        unity.apto = floorApto.substring(1,3)
      }
      if(floorApto !== undefined && floorApto.length == 4){
        unity.floor = floorApto.substring(0,2)
        unity.apto = floorApto.substring(2,4)
      }

      towers.push(unity.tower)
    })

    towers = _.uniq(towers.sort())
    this.setState({selectedTower: towers[0].slice(1)})
    this.setState({towers})
  }

  stepHandler (step) {
    this.setState({step})
  }

  selectTowersHandler(e){
    this.setState({selectedTower: e.target.value})
  }

  render () {
    return (
      <div className='DeedsFirstStepOnBoard'>

        <div className={`DeedsFirstStepOnBoard__selectTower ${this.state.step === 2 ? 'displayNone' : ''}`}>
          <p>Hola...</p>
          <h3>¿Qué torre vas a escriturar de Villa Paraiso?</h3>
          <div className='DeedsFirstStepOnBoard__select'>
            <IconArrowDown />
            <div>
              <select name='' id='' onChange={this.selectTowersHandler}>
                {
                  this.state.towers.map((tower, key)=>{
                    return <option value={tower.slice(1)} key={key}>Torre {tower.slice(1)}</option>
                  })
                }
              </select>
            </div>
          </div>
          <button onClick={() => this.stepHandler(2)}>Siguiente</button>
          <div className='DeedsFirstStepOnBoard__step'>
            <span className={`${this.state.step === 1 ? 'backgroundBlue' : 'backgroundGray'}`} />
            <span className={`${this.state.step === 2 ? 'backgroundBlue' : 'backgroundGray'}`} />
          </div>
        </div>

        <div className={`DeedsFirstStepOnBoard__startProcess ${this.state.step === 1 ? 'displayNone' : ''}`}>
          <span onClick={() => this.stepHandler(1)}>Atrás</span>
          <img src='/static/img/deeds-onboard.png' alt='' />
          <p>
            { `Estás a punto de iniciar el proceso de escrituracion para la Torre ${this.state.selectedTower}.
              El primer paso es generar y enviar notificaciones masivas a los clientes alertando del inicio de dicho proceso. Da click a continuació para iniciar`}
          </p>
          <button onClick={()=> this.props.router.push(`/deeds/send-notification/${this.state.selectedTower}`)}>Iniciar proceso</button>
          <div className='DeedsFirstStepOnBoard__step'>
            <span className={`${this.state.step === 1 ? 'backgroundBlue' : ''}`} />
            <span className={`${this.state.step === 2 ? 'backgroundBlue' : ''}`} />
          </div>
        </div>

      </div>

    )
  }
}
