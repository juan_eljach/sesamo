import React, { Component } from 'react'
import TableSendNotifications from '../components/first-step/_deeds-table-sendnotifications'
import DeedsFirstStepSendNotificationsEdit from '../components/first-step/_deeds-edit-sendnotifications'
import DeedsFirstStepSendNotificationsConfirm from '../components/first-step/_deds-confirm-sendnotification'
import DeedsSendNotificationsDone from '../components/first-step/_deeds-onboard-done'
import {IconCheck, IconEmail} from '../svg/deeds-icons'
import getCsrfToken from '../../lib/csrftoken'
import {setTemplate} from '../lib/set-template-notification'

export default class DeedsFirstStepSendNotifications extends Component {
  constructor (props) {
    super(props)

    this.pslug = window.location.href.split('/')[4]

    this.selectedUnities = []

    this.state = {
      step: 1,
      unities: [],
      unitiesByTower: [],
      towers: [],
      floors: [],
      towerSelected: '',
      templateToSend: '',
      sendingNotificationsPopup: false,
      sendNotificationsDone: false
    }

    this.stepHandler = this.stepHandler.bind(this)
    this.prepareUnities = this.prepareUnities.bind(this)
    this.selectedUnitiesHandler = this.selectedUnitiesHandler.bind(this)
    this.sendNotification = this.sendNotification.bind(this)
    this.sendingNotifications = this.sendingNotifications.bind(this)
    this.sendNotificationsSucess = this.sendNotificationsSucess.bind(this)
    this.closeNotificationSucess = this.closeNotificationSucess.bind(this)
  }

  componentDidMount () {
    $.ajax({
      url: `/api/projects/${this.pslug}/unities/`
    })
    .done((data) => {
      this.prepareUnities(data)

    })
    .fail((error)=> console.log(error))
  }

  prepareUnities(unities){

    let towers = []
    let floors = []

    _.forEach(unities, (unity) => {

      let splitedUnity = unity.slug.split('-')

      let floorApto = splitedUnity[1]
      unity.tower = splitedUnity[0]

      if(floorApto !== undefined && floorApto.length === 3){
        unity.floor = floorApto.substring(0,1)
        unity.apto = floorApto.substring(1,3)
      }
      if(floorApto !== undefined && floorApto.length == 4){
        unity.floor = floorApto.substring(0,2)
        unity.apto = floorApto.substring(2,4)
      }

      towers.push(unity.tower)
      floors.push(unity.floor)
    })

    towers = _.uniq(towers.sort())
    floors = _.uniq(floors.sort())

    this.setState({unities})
    this.setState({towers})
    this.setState({floors: _.sortBy(floors.map((floor) => parseInt(floor))) }, () => {
      this.updateSelectTower()
    })

  }

  updateSelectTower(e){
    //e ? e.target.value : this.state.towers[0]
    let unitiesByTowerTemp = _.filter(this.state.unities, { tower: `t${this.props.params.tower}` })

    this.setState({unitiesByTower: unitiesByTowerTemp.reverse()}, ()=>{

      this.setState({ floors: _.uniq(_.map(this.state.unitiesByTower, 'floor')) })

    })
  }

  selectedUnitiesHandler(unities){
    this.selectedUnities = unities
  }

  stepHandler (step) {
    this.setState({step})
  }

  nextHandler(){
    if(this.state.step === 3) {
      return this.sendNotification(this.selectedUnities, $('#deedsSendNotificationTemplate').html())
    }

    this.setState({step: this.state.step + 1}, ()=> console.log(this.state.step))

  }

  previousHandler(){
    if(this.state.step == 1) {
      return this.props.router.push(`/deeds/onboard`)
    }
    this.setState({step: this.state.step - 1}, ()=> console.log(this.state.step))
  }

  sendNotification(selectedUnities, template){

    let templateToSend = setTemplate($('#deedsSendNotificationTemplate'))

    let payload = {
      unities: selectedUnities,
      html_snippet: templateToSend,
      tower_number: parseInt(this.props.params.tower)
    }
    console.log(payload)
    $.ajax({
      method: 'POST',
      url: `/api/projects/${this.pslug}/deeds/send-notifications/`,
      data: JSON.stringify(payload),
      headers: {
        'X-CSRFToken': getCsrfToken('csrftoken'),
        'Content-Type' : 'application/json'
      }
    })
    .done((data) => {
      console.log(data, 'asas');
      this.setState({sendingNotificationsPopup: true})
      this.sendingNotifications(data.task_id)
    })
    .fail((error)=> console.log(error))

  }

  sendingNotifications(task_id){
    let payload = {
      task_id
    }

    this.sendNotificationInterval = setInterval(()=>{
      $.ajax({
        method: 'POST',
        url: `/api/tasks/status/`,
        data: JSON.stringify(payload),
        headers: {
          'X-CSRFToken': getCsrfToken('csrftoken'),
          'Content-Type' : 'application/json'
        }
      })
      .done((data) => {
        if(data.message === 'SUCCESS'){
          console.log('sucess', data)
          window.open(data.filename.url_path)
          this.sendNotificationsSucess()
          this.setState({sendNotificationsDone: true})
        }
      })
      .fail((error)=> console.log(error))

    }, 2000)

  }

  sendNotificationsSucess(){
    clearInterval(this.sendNotificationInterval)

    // setTimeout(()=> this.props.router.push(`/deeds/`), 4000)
  }

  closeNotificationSucess(){
    this.props.router.push(`/deeds/`)
  }

  render () {
    return (
      <div className={`DeedsFirstStepSendNotifications ${this.state.step === 3 ? `backgroundWhite borderLeftGray` : ''}`}>

        <header className='DeedsFirstStepSendNotifications__header'>
          <span>Torre {this.props.params.tower}</span>
          <div>
            <IconCheck isCheck={this.state.step === 1 ? true : false}/>
            <IconEmail step={this.state.step}/>
            <h3>
              {
                `${this.state.step === 1 ? 'Haz click sobre una unidad para seleccionarla' : this.state.step === 2 ? 'Edita el mensaje de la notificación' : this.state.step === 3 ? 'Confirma el envío' : '' }`
              }
            </h3>
          </div>
        </header>

        <div className="DeedsFirstStepSendNotifications__content">

          <div className={`DeedsFirstStepSendNotifications__table ${this.state.step === 1 ? '' : 'displayNone'}`}>
            <TableSendNotifications
              unitiesByTower={this.state.unitiesByTower}
              floors={this.state.floors}
              pslug={this.pslug}
              step={this.state.step}
              selectedUnitiesHandler={this.selectedUnitiesHandler}
            />
          </div>

          <DeedsFirstStepSendNotificationsEdit step={this.state.step}/>

          <DeedsFirstStepSendNotificationsConfirm
            step={this.state.step}
            selectedClientsLenght={this.selectedUnities.length}
            stepHandler={this.stepHandler}
            tower={this.props.params.tower}
          />

        </div>

        <footer className='DeedsFirstStepSendNotifications__footer'>
          <div onClick={()=> this.previousHandler()}>{`${this.state.step === 1 ? 'CANCELAR' : 'REGRESAR' }`}</div>
          <nav>
            <ul>
              <li>
                <a onClick={()=> this.stepHandler(1)} className={`${this.state.step === 1 ? 'blueColor' : ''}`}>1. Selecciona los apartamentos ></a>
              </li>
              <li>
                <a onClick={()=> this.stepHandler(2)} className={`${this.state.step === 2 ? 'blueColor' : ''}`}>2. Valida el template y envía ></a>
              </li>
              <li>
                <a onClick={()=> this.stepHandler(3)} className={`${this.state.step === 3 ? 'blueColor' : ''}`}>3. Confirmar</a>
              </li>
            </ul>
          </nav>
          <div onClick={()=> this.nextHandler()}>{`${this.state.step === 3 ? 'ENVIAR' : 'SIGUIENTE'}`}</div>
        </footer>
        <div className={`DeedsFirstStepSendNotifications__popup ${this.state.sendingNotificationsPopup ? '' : 'displayNone'}`}>
          <div className={`DeedsFirstStepSendNotifications__waiting ${this.state.sendNotificationsDone ? 'displayNone' : ''}`}>
            <div className="DeedsFirstStepSendNotifications__loader-1">
              <div className="DeedsFirstStepSendNotifications__loader-2">
                <div className="DeedsFirstStepSendNotifications__loader-3"></div>
              </div>
            </div>
            <span className="DeedsFirstStepSendNotifications__waitingText">
              <p>Enviando tus notificaciones</p>
              <p>...Este proceso puede tardar unos minutos</p>
            </span>
          </div>

          <DeedsSendNotificationsDone
            selectedUnities={this.selectedUnities}
            unities={this.state.unities}
            unitiesLength={this.state.unitiesByTower.length}
            sendNotificationsDone={this.state.sendNotificationsDone}
            closeNotificationSucess={this.closeNotificationSucess}
            tower={this.props.params.tower}
          />

        </div>
      </div>

    )
  }
}
