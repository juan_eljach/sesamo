import React, { Component } from 'react'
import getCsrfToken from '../lib/csrftoken'
import {IconArrowDown} from '../support/svg/support-icons'
import {IconCheck} from './svg/deeds-icons'
import {setTemplate} from './lib/set-template-notification.js'
import {setUnities} from './lib/set-unities'
import {template} from './lib/fixtures'
import DeedsEditTemplatePopup from './components/index/edit-template-popup'
import DeedsSendNotificationsDone from './components/first-step/_deeds-onboard-done'
import DeedsUnitiesTable from './components/index/unities-table'
import Loader from '../shared/components/_loader'

export default class Deeds extends Component {
  constructor (props) {
    super(props)

    this.pslug = window.location.href.split('/')[4]
    this.unitiesTemp = []
    this.selectedUnitiesTemp = []

    this.state = {
      unities: [],
      selectedUnities: [],
      isCheck: false,
      isAllCheck: false,
      isOpenEditTemplatePopup: false,
      editingTemplateNotification: false,
      floors: [],
      towers: [],
      sendNotificationsDone: false,
      towerSelected: null,
      floorSelected: null,
      towersWithNotifications: [],
      towersWithoutNotifications: [],
      loading: true,
      noResultsFilter: false
    }

    this.checkHandler = this.checkHandler.bind(this)
    this.allIsCheckHandler = this.allIsCheckHandler.bind(this)
    this.openEditTemplatePopup = this.openEditTemplatePopup.bind(this)
    this.editTemplateNotification = this.editTemplateNotification.bind(this)
    this.closeEditTemplatePopup = this.closeEditTemplatePopup.bind(this)
    this.sendNotification = this.sendNotification.bind(this)
    this.closeNotificationSucess = this.closeNotificationSucess.bind(this)
    this.towerHandler = this.towerHandler.bind(this)
    this.checkSvgHandler = this.checkSvgHandler.bind(this)
    this.allCheckSvgHandler = this.allCheckSvgHandler.bind(this)
    this.floorHandler = this.floorHandler.bind(this)
    this.checkTowers = this.checkTowers.bind(this)
    this.towersWithHandler = this.towersWithHandler.bind(this)
    this.redirectHandler = this.redirectHandler.bind(this)
  }

  componentDidMount () {

    this.$contentNotificationTemplate = $('#contentNotificationTemplate')
    this.$contentNotificationTemplateSpans = $('#contentNotificationTemplate span')

    let payload = {}

    $.ajax({
      // url: `/api/projects/${this.pslug}/deeds/unities/tracking/`,
      url: `/api/projects/${this.pslug}/unities/`,
      data: JSON.stringify(payload),
      headers: {
        'X-CSRFToken': getCsrfToken('csrftoken'),
        'Content-Type' : 'application/json'
      }
    })
    .done((data) => {
      let setedUnities = setUnities(data)
      let {towers} = setedUnities
      let {floors} = setedUnities
      this.setState({towerSelected: towers[0].slice(1)})
      this.setState({floorSelected: floors[0]})
      this.setState({towers})
      this.setState({floors})
      // this.setState({unities: _.filter(this.state.unitiesTemp, (u) => u.tower === `T${this.state.towerSelected}`)})
      // this.setState({unitiesTemp: data})

      this.checkTowers()
    })
    .fail((error)=> console.log(error))

  }

  checkTowers(){
    _.each(this.state.towers, (tower) => {
      $.ajax({
        url: `/api/projects/${this.pslug}/deeds/notifications/tracking/tower/${tower.slice(1)}`
      })
      .done((data) => {
        if(data.massive_notifications_sent){
          this.setState({towersWithNotifications: this.state.towersWithNotifications.concat([tower])})
          this.towersWithHandler()
        }
      })
      .fail((error)=> {
        if (error.status === 404) {
          this.setState({towersWithoutNotifications: this.state.towersWithoutNotifications.concat([tower])})
        }
      })
    })
  }

  towersWithHandler(){
    let firstTower = this.state.towersWithNotifications[0]

    if(firstTower){
      $.ajax({
        url: `/api/projects/${this.pslug}/deeds/tower/${firstTower.slice(1)}`
      })
      .done((data) => {
        this.unitiesTemp = data

        let floorsSelected = _.filter(this.unitiesTemp, (unity) => {
          let unitySet = unity.name.split('-')[1]
          let floorSelected
          if(unitySet.length === 3){
            floorSelected = unitySet.slice(0, 1)
          }

          else if(unitySet.length === 4) {
            floorSelected = unitySet.slice(0, 2)
          }

          if(floorSelected == this.state.floorSelected) {
            return unity
          }

        })

        this.setState({unities: floorsSelected})
        this.setState({loading: false})
      })
      .fail((error)=> {
        console.log(error)
      })
      return
    }

    this.props.router.push(`/deeds/onboard`)
  }

  checkHandler(unity){
    this.selectedUnitiesTemp.push(unity.id)
    this.setState({selectedUnities: this.selectedUnitiesTemp})
  }

  checkSvgHandler(unity){
    let unityFound = _.find(this.state.selectedUnities, (unityId) => unityId === unity.id)

    if(unityFound){

      _.remove(this.selectedUnitiesTemp, (unityId) => unityId === unity.id )

      this.setState({isAllCheck: false})
      return this.setState({selectedUnities: this.selectedUnitiesTemp})
    }
  }

  allIsCheckHandler(){
    this.selectedUnitiesTemp = _.map(this.state.unities, 'id')
    this.setState({selectedUnities: this.selectedUnitiesTemp})
    this.setState({isAllCheck: true})
  }

  allCheckSvgHandler(){
    this.setState({selectedUnities: []})
    return this.setState({isAllCheck: false})
  }

  openEditTemplatePopup(){
    this.setState({isOpenEditTemplatePopup: true})

    this.$contentNotificationTemplate.html(template.text)
  }

  editTemplateNotification(){
    this.setState({editingTemplateNotification: true})

    _.each($('#contentNotificationTemplate span'), (spanTag) => spanTag.setAttribute('contenteditable', 'false'))

    $('#focusNotificationEditing')[0].setAttribute('contenteditable', 'true')

    $('#focusNotificationEditing').focus()
  }

  closeEditTemplatePopup(){
    this.setState({isOpenEditTemplatePopup: false})
  }

  towerHandler(e){
    let tower = e.target.value

    $.ajax({
      url: `/api/projects/${this.pslug}/deeds/tower/${tower.slice(1)}`
    })
    .done((data) => {
      this.unitiesTemp = data
    })
    .fail((error)=> {
      console.log(error)
    })

    setTimeout(()=> {
      let towersFilter = _.filter(this.unitiesTemp, (unity) => {
        let floorSelected
        let unitySet = unity.name.split('-')[1]

        if(unitySet.length === 3){
          floorSelected = unitySet.slice(0, 1)
        }

        else if(unitySet.length === 4) {
          floorSelected = unitySet.slice(0, 2)
        }

        return unity.name.split('-')[0] === tower && floorSelected == this.state.floorSelected
      })

      this.setState({towerSelected: tower.slice(1)})
      this.setState({unities: towersFilter})

      console.log(towersFilter);
      this.setState({noResultsFilter: false})
    }, 100)
  }

  floorHandler(e){

    let unitiesByFloor = _.filter(this.unitiesTemp, (unity) => {

      let unitySet = unity.name.split('-')[1]
      let towerSelected = unity.name.split('-')[0][1]
      let floorSelected

      if(unitySet.length === 3){
        floorSelected = unitySet.slice(0, 1)
      }

      else if(unitySet.length === 4) {
        floorSelected = unitySet.slice(0, 2)
      }

      return floorSelected == e.target.value && towerSelected == this.state.towerSelected
    })

    if(!unitiesByFloor.length) this.setState({noResultsFilter: true})

    this.setState({floorSelected: e.target.value})
    this.setState({unities: unitiesByFloor}, () => console.log())
  }

  sendNotification(){
    let templateToSend = setTemplate(this.$contentNotificationTemplate)

    let payload = {
      unities: this.state.selectedUnities,
      html_snippet: templateToSend,
      tower_number: parseInt(this.state.towerSelected)
    }

    $.ajax({
      method: 'POST',
      url: `/api/projects/${this.pslug}/deeds/send-notifications/`,
      data: JSON.stringify(payload),
      headers: {
        'X-CSRFToken': getCsrfToken('csrftoken'),
        'Content-Type' : 'application/json'
      }
    })
    .done((data) => {
      this.setState({isOpenEditTemplatePopup: false})
      this.setState({sendNotificationsDone: true})
    })
    .fail((error)=> console.log(error))
  }

  closeNotificationSucess(){
    this.setState({sendNotificationsDone: false})
  }

  redirectHandler(){
    this.props.router.push(`/deeds/send-notification/${this.state.towerSelected}`)
  }

  render () {
    return (
      <section className='Deeds'>

        <div className={`Deeds__sendNotificationsDoneContainer ${this.state.sendNotificationsDone ? '' : 'displayNone'}`}>
          <DeedsSendNotificationsDone
            unities={this.state.unities}
            selectedUnities={this.state.selectedUnities}
            sendNotificationsDone={this.state.sendNotificationsDone}
            closeNotificationSucess={this.closeNotificationSucess}
            unitiesLength={this.state.unities.length}
            tower={this.state.towerSelected}
          />
        </div>

        <DeedsEditTemplatePopup
          isOpenEditTemplatePopup={this.state.isOpenEditTemplatePopup}
          closeEditTemplatePopup={this.closeEditTemplatePopup}
          sendNotification={this.sendNotification}
          editTemplateNotification={this.editTemplateNotification}
          editingTemplateNotification={this.state.editingTemplateNotification}
        />

        <div className="Deeds__container">
          <div className="Deeds__filterTower">
            <label htmlFor="deedsfilterTower">Filtrando Por:</label>
            <IconArrowDown />
            <div>
              <select name="" id="deedsfilterTower" onChange={this.towerHandler}>
                {
                  _.map(this.state.towers, (tower, key)=>{

                    return (
                      <option value={tower} key={key}>{`Torre ${tower.slice(1)}`}</option>
                    )
                  })
                }
              </select>
            </div>
          </div>

          <div className='Deeds__header'>
            <div className="Deeds__searchInputs">
              <div className='Deeds__filterFloor'>
                <IconArrowDown />
                <div>
                  <select name="" id="" onChange={this.floorHandler}>
                    {
                      _.map(this.state.floors, (floor, key)=>{
                        return <option value={floor} key={key}>Piso {floor}</option>
                      })
                    }
                  </select>
                </div>
              </div>
              {/* <input type="text" placeholder="Busca por persona o apto"/> */}
            </div>
            <div className="Deeds__sendNotificationsButton">
              <button onClick={this.openEditTemplatePopup}>Enviar notificaciones</button>
            </div>
          </div>


          <DeedsUnitiesTable
            unities={this.state.unities}
            selectedUnities={this.state.selectedUnities}
            checkHandler={this.checkHandler}
            allIsCheckHandler={this.allIsCheckHandler}
            allCheckSvgHandler={this.allCheckSvgHandler}
            checkSvgHandler={this.checkSvgHandler}
            isCheck={this.state.isCheck}
            isAllCheck={this.state.isAllCheck}
            redirectHandler={this.redirectHandler}
            towerSelected={this.state.towerSelected}
            loading={this.state.loading}
            noResultsFilter={this.state.noResultsFilter}
          />          

        </div>

      </section>
    )
  }
}
