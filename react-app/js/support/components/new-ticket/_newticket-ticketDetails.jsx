import React, { Component } from 'react'

import {
  IconTicketSupport,
  IconArrowDown } from '../../svg/support-icons'

export default class NewTicketTicketDetails extends Component {

  constructor(props){
    super(props)
  }

  componentDidMount(){
  }

  render(){
    return (
      <div className="NewTicket__ticketDetail">
        <h4><IconTicketSupport />Detalles del ticket</h4>

        <div className="NewTicket__ticketDetailWrap">
          <div>
            <label htmlFor="">Tipo:</label>
            <IconArrowDown />
            <select name="" id="" onChange={ (e)=> this.props.updateTicketDetails(e, 'ticketDetailType') }>
              <option value="modifications">Modificaciones</option>
              <option value="warranty">Solicitudes de garantía</option>
              <option value="various">Varios</option>
              <option value="payment-agreements">Preguntas sobre planes de pago</option>
              <option value="complaints-claims">Quejas y reclamos</option>
            </select>
          </div>

          <div>
            <label htmlFor="">Estado:</label>
            <IconArrowDown />
            <select name="" id="" onChange={ (e)=> this.props.updateTicketDetails(e, 'ticketDetailStatus') }>
              <option value="open">Abierto</option>
              <option value="re-opened">Re abierto</option>
              <option value="resolved">Resuelto</option>
              <option value="closed">Cerrado</option>
            </select>
          </div>

          <div>
            <label htmlFor="">Prioridad:</label>
            <IconArrowDown />
            <select name="" id="" onChange={ (e)=> this.props.updateTicketDetails(e, 'ticketDetailPriority') }>
              <option value="low">Baja</option>
              <option value="very-low">Muy baja</option>
              <option value="high">Alta</option>
              <option value="critical">Crítica</option>
            </select>
          </div>
        </div>

      </div>
    )
  }
}
