import React, { Component } from 'react'

import { IconSearch } from '../../../formalization/svg/_svg-icons'
import {
  IconPen } from '../../svg/support-icons'


export default class NewTicketTicketActors extends Component {

  constructor(props){
    super(props)
  }

  componentDidMount(){
  }

  render(){
    return (
      <div className="NewTicket__ticketActors">
        <h4><IconPen/>Administradores o Colaboradores</h4>

        <div className="NewTicket__ticketActorsWrap">

          <div className="NewTicket__ticketCollaborators">
            <label htmlFor="">Colaboradores</label>
            <IconSearch />
            <input id="newTicketSearchCollaborator" type="text" placeholder="Busca el Colaborador" onChange={(e)=> this.props.searchAdministrators(e)}/>

            <div className="NewTicket__actorsResults" id="newTicketCollaboratorsResults">

              {
                this.props.actorsFind.map((actor, key)=>{
                  return <div className="TicketDetail__actorsResultsItem" key={key}>
                    <div onClick={ ()=> this.props.addActor(actor, 'collaborators') }> { `${actor.user.first_name} ${actor.user.last_name}` } </div>
                  </div>
                })
              }

            </div>

            <div className="NewTicket__actorsList">

              {
                this.props.collaboratorsList.map((collaborator, key)=>{
                  return <div className="TicketDetail__actorsItem" key={key}>
                    <span className="TicketDetail__actorsRound">{ `${collaborator.user.first_name[0]} ${collaborator.user.last_name[0]}` }</span>
                    <div>{ `${collaborator.user.first_name} ${collaborator.user.last_name}` }</div>
                    <span className="TicketDetail__actorsItemDelete" onClick={ ()=> this.props.removeActor(collaborator, 'collaborators') }>x</span>
                  </div>
                })
              }

            </div>
          </div>

          <div className="NewTicket__ticketAdministrators">
            <label htmlFor="">Administradores</label>
            <IconSearch />
            <input  id="newTicketSearchAdministrator" type="text" placeholder="Busca el Administrador" onChange={(e)=> this.props.searchAdministrators(e)}/>

            <div className="NewTicket__actorsResults" id="newTicketAdministratorsResults">
              {
                this.props.actorsFind.map((actor, key)=>{
                  return <div className="TicketDetail__actorsResultsItem" key={key}>

                    <div onClick={ ()=> this.props.addActor(actor, 'administrators') }> { `${actor.user.first_name} ${actor.user.last_name}` } </div>

                  </div>
                })
              }
            </div>

            <div className="NewTicket__actorsList">

              {
                this.props.administratorsList.map((administrator, key)=>{
                  return <div className="TicketDetail__actorsItem" key={key}>
                    <span className="TicketDetail__actorsRound">{ `${administrator.user.first_name[0]} ${administrator.user.last_name[0]}` }</span>
                    <div>{ `${administrator.user.first_name} ${administrator.user.last_name}` }</div>
                    <span className="TicketDetail__actorsItemDelete" onClick={ ()=> this.props.removeActor(administrator, 'administrators') }>x</span>
                  </div>
                })
              }

            </div>
          </div>
        </div>

      </div>
    )
  }
}
