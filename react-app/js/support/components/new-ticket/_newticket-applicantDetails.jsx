import React, { Component } from 'react'

import {
  IconPersonSupport,
  IconAptoSupport,
  IconArrowDown,
  IconApto } from '../../svg/support-icons'

export default class NewTicketApplicantDetails extends Component {

  constructor(props){
    super(props)
  }

  componentDidMount(){
  }

  render(){
    return (
      <div className="NewTicket__applicantDetails">
        <h4><IconPersonSupport />Detalles del solicitante</h4>

        <div className="NewTicket__applicantDetailsWrap">

          <div className="NewTicket__applicantSearch">
            <label htmlFor="">Cliente</label>
            <div className="NewTicket__selectedClient" style={{display: `${ this.props.showChip ? 'inline-block' : 'none' }`}}>
              <div>{`${this.props.selectedClientState.first_name} ${this.props.selectedClientState.last_name}`}</div>
              <span onClick={this.props.hideChip}>x</span>
            </div>
            <IconAptoSupport />
            <input type="text" placeholder="Busca nombre del cliente" id="newTicketSearchClient" onChange={(e)=> this.props.searchClient(e)} />
            <div className="NewTicket__searchResults" id="newTicketSearchResults">
              {
                this.props.foundClients.map((client, key)=> {
                  return <div className="NewTicket__client" key={key} onClick={()=> this.props.selectedClient(client)}> { `${client.first_name} ${client.last_name}` } </div>
                })
              }
            </div>
          </div>

          <div className="NewTicket__applicantApto">
            <label htmlFor="">Apto:</label>
            <IconArrowDown />
            <IconApto />
            <select name="" id="newTicketApplicantApto">
              {
                this.props.clientUnits.map((unity, key)=>{
                  return <option value={`${unity.id}`} key={key}> { unity.name } </option>
                })
              }
            </select>
          </div>

          <div className="NewTicket__applicantEmail">
            <label>Correo:</label>
            <div className="" id="newTicketApplicantEmail"> {this.props.selectedClientState.email} </div>
          </div>

          <div className="NewTicket__applicantPhone">
            <label>Celular:</label>
            <div className="" id="newTicketApplicantPhone"> { this.props.selectedClientState.cellphone } </div>
          </div>

        </div>

      </div>
    )
  }
}
