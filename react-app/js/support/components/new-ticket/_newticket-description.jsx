import React, { Component } from 'react'

import {
  IconPen,
  IconAttachmentFile } from '../../svg/support-icons'

export default class NewTicketDescription extends Component {

  constructor(props){
    super(props)
  }

  componentDidMount(){
  }

  render(){
    return (
      <div className="NewTicket__description">
        <h4><IconPen />Descripción del ticket</h4>
        <div className="NewTicket__descriptionWrap">
          <div className="NewTicket__formGroup">
            <label htmlFor="">Título</label>
            <IconPen />
            <input id="newTicketTitle" type="text" className={this.props.ticketTitleIsValid ? '' : 'u-invalidForm'} placeholder="Dale un título al problema" onChange={ this.props.checkValidForm }/>
          </div>
          <div className="NewTicket__formGroup">
            <label htmlFor="">Descripción</label>
            <textarea name="" placeholder="Agrega una descripción" id="newTicketDescription" className={this.props.ticketDescriptionIsValid ? '' : 'u-invalidForm'} cols="30" rows="8" onChange={this.props.checkValidForm}></textarea>
          </div>

          <div className="NewTicket__formFile">
            {/* <input type="file" id="newTicketAttachedFile" /> */}
            <div className="NewTicket__inputFile">
              <label className="TicketDetail__fileContainer">
                <IconAttachmentFile />
                Subir archivo
                <input type="file" id="newTicketAttachedFile" onChange={(e)=> this.props.updateNameFile(e) }/>
              </label>
              <span className="NewTicket__attachedFileName"> {this.props.selectedFileName} </span>
            </div>
          </div>

        </div>
      </div>
    )
  }
}
