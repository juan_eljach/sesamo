import React, { Component } from 'react'
import moment from 'moment'

import { IconResponse, IconNote, IconAssign, IconAdd, IconArrowLeft } from '../../svg/support-icons'
import {IconSearch} from '../../../formalization/svg/_svg-icons'
import getCsrfToken from '../../../lib/csrftoken'

export default class TicketDetailActors extends Component {

  constructor(props){
    super(props)
  }

  componentDidMount(){

    this.$ticketDetailAdministratorsResults = $('#ticketDetailAdministratorsResults')
    this.$ticketDetailColaboratorsResults = $('#ticketDetailColaboratorsResults')

  }

  render(){
    return (<div className="TicketDetail__actors">

      <div className="TicketDetail__representatives">

        <div className="TicketDetail__actorsLabel"> <IconArrowLeft handler={this.props.openActorsPopup} actors='administrators' actorsActive={this.props.actorsActive}/>
          <IconAssign/> Administradores
          <span className="TicketDetail__actorsCounter">{ this.props.administratorsIds.length } </span>


          <div className={`TicketDetail__actorsSearch ${this.props.actorsActive === 'administrators' ? '' : 'displayNone' }`}>
            <IconSearch />
            <input id="ticketDetailSearchAdministrator" type="text" placeholder="Busca el nombre del administrador" onChange={(e)=> this.props.searchAdministrators(e)}/>
            <div className="TicketDetail__actorsResults" id="ticketDetailAdministratorsResults">

              {
                this.props.actorsFind.map((actor, key)=>{
                  return <div className="TicketDetail__actorsResultsItem" key={key}>

                    <div onClick={ ()=> this.props.addActor(actor, 'administrators') }> { `${actor.user.first_name} ${actor.user.last_name}` } </div>

                  </div>
                })
              }

            </div>
          </div>

          <div className={`TicketDetail__actorsList ${this.props.actorsActive === 'administrators' ? '' : 'displayNone' }`}>

            {
              this.props.administratorsList.map((admin, key)=>{
                return <div className="TicketDetail__actorsItem" key={key}>
                  <span className="TicketDetail__actorsRound">{ `${admin.user.first_name[0]} ${admin.user.last_name[0]}` }</span>
                  <div>{ `${admin.user.first_name} ${admin.user.last_name}` }</div>
                  <span className="TicketDetail__actorsItemDelete" onClick={ ()=> this.props.removeActor(admin, 'administrators') }>x</span>
                </div>
              })
            }
          </div>


        </div>

      </div>


      <div className="TicketDetail__colaborators">

        <div className="TicketDetail__actorsLabel">
          <IconArrowLeft handler={this.props.openActorsPopup} actors='colaborators' actorsActive={this.props.actorsActive}/>
          <IconAdd /> Colaboradores
          <span className="TicketDetail__actorsCounter">{ this.props.collaboratorsIds.length }</span>

          <div className={`TicketDetail__actorsSearch ${this.props.actorsActive === 'colaborators' ? '' : 'displayNone' }`}>
            <IconSearch />

            <input id="ticketDetailSearchCollaborator" type="text" placeholder="Busca el nombre del colaborador" onChange={(e)=> this.props.searchAdministrators(e)}/>

            <div className="TicketDetail__actorsResults" id="ticketDetailColaboratorsResults">

              {
                this.props.actorsFind.map((actor, key)=>{
                  return <div className="TicketDetail__actorsResultsItem" key={key}>

                    <div onClick={ ()=> this.props.addActor(actor, 'collaborators') }> { `${actor.user.first_name} ${actor.user.last_name}` } </div>

                  </div>
                })
              }

            </div>

          </div>

          <div className={`TicketDetail__actorsList ${this.props.actorsActive === 'colaborators' ? '' : 'displayNone' }`}>

            {
              this.props.collaboratorsList.map((collaborator, key)=>{
                return <div className="TicketDetail__actorsItem" key={key}>
                  <span className="TicketDetail__actorsRound">{ `${collaborator.user.first_name[0]} ${collaborator.user.last_name[0]}` }</span>
                  <div>{ `${collaborator.user.first_name} ${collaborator.user.last_name}` }</div>
                  <span className="TicketDetail__actorsItemDelete" onClick={ ()=> this.props.removeActor(collaborator, 'collaborators') }>x</span>
                </div>
              })
            }

          </div>

        </div>

      </div>

    </div>)
  }
}
