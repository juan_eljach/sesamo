import React, { Component } from 'react'

import { IconResponse, IconNote, IconCheckMark, IconAttachmentFile } from '../../svg/support-icons'

export default class TicketDetailAddResponses extends Component {
  constructor(props){
    super(props)

    this.state = {
      templates: [],
      templateSelected: '',
      templateHover: 0,
      templateAdded: -1,
      selectedFileName: ''
    }

    this.selectTemplate = this.selectTemplate.bind(this)
    this.hoverTemplate = this.hoverTemplate.bind(this)
    this.updateTextArea = this.updateTextArea.bind(this)
    this.updateNameFile = this.updateNameFile.bind(this)

  }

  componentDidMount(){

    this.$answerAttachedFile = $('#answerAttachedFile')


    $.ajax({
      url: `/api/projects/${this.props.pslug}/support/templates/`
    })

    .done((data) => {
      console.log(data);
      this.setState({templates: data})
    })
    .fail((error)=> console.log(error))

  }

  selectTemplate(template, templateNumber){
    this.setState({templateSelected: template.template_text})
    this.setState({templateAdded: templateNumber})
  }

  hoverTemplate(key){
    this.setState({templateHover: key})
  }

  updateTextArea(e){
    this.setState({ templateSelected: e.target.value })
  }

  updateNameFile(e){
    this.setState({selectedFileName: this.$answerAttachedFile[0].files[0].name})
  }

  render(){
    return <div className="TicketDetail__addResponse">

      <h3>Agrega una respuesta o una nota interna</h3>
      <div className="TicketDetail__fastResponsesPossible">Posibles respuestas:</div>

      <div className="TicketDetail__fastResponses">

        {
          this.state.templates.map((template, key)=>{
            return (
              <div className="TicketDetail__fastReponse" key={key} onMouseEnter={()=> this.hoverTemplate(key)} onMouseLeave={()=> this.setState({templateHover: -1})}> <div className={ `${this.state.templateHover === key || this.state.templateAdded === key ? 'displayBlock' : 'displayNone' }` }> {template.title} </div>
                <div> { template.template_text } </div>
                <div className={`TicketDetail__fastReponseAdd ${this.state.templateHover === key || this.state.templateAdded === key ? 'displayBlock' : 'displayNone' }`} onClick={()=> this.selectTemplate(template, key)}> <IconCheckMark color={'#fff'} templateAdded={this.state.templateAdded} number={key} />  {this.state.templateAdded === key ? 'AGREGADO' : 'AGREGAR'}</div>
              </div>
            )
          })
        }

        <div className="TicketDetail__morefastResponses">
          <span>Ver más templates</span>
        </div>

      </div>

      <div className="TicketDetail__responseInput" id="ticketForm">
        <div className="TicketDetail__responseInputTab">
          <div className="TicketDetail__responseInputTabWrap">
            <div onClick={()=>this.props.selectTabSendResponse(1)} className={ this.props.tabSendResponse === 1 ? 'u-tabSendResponseActive' : '' }><IconResponse />Respuesta</div>
            <div onClick={()=>this.props.selectTabSendResponse(2)} className={ this.props.tabSendResponse === 2 ? 'u-tabSendResponseActive' : '' }><IconNote />Nota interna</div>
          </div>
        </div>
        <textarea name="answer_text" id="answerText" cols="30" rows="5" value={ this.state.templateSelected ? this.state.templateSelected : '' } placeholder={ this.props.tabSendResponse === 1 ? 'Describe tu respuesta' : 'Describe la nota interna' } onChange={ (e)=> this.updateTextArea(e) }></textarea>
        <div className="TicketDetail__responseInputActions">

          <div className="TicketDetail__inputFile">
            <label className="TicketDetail__fileContainer">
              <IconAttachmentFile />
              Subir archivo
              <input type="file" id="answerAttachedFile" onChange={(e)=> this.updateNameFile(e) }/>
            </label>
            <span className="TicketDetail__attachedFileName"> {this.state.selectedFileName} </span>
          </div>

          <div>
            <button onClick={this.props.sendResponse}>Enviar</button>
          </div>
        </div>
      </div>

    </div>
  }
}
