import React, { Component } from 'react'
import moment from 'moment'

import { IconResponse, IconNote, IconAssign, IconAdd, IconArrowLeft } from '../../svg/support-icons'
import {IconSearch} from '../../../formalization/svg/_svg-icons'
import { IconArrowDown } from '../../svg/support-icons'
import getCsrfToken from '../../../lib/csrftoken'

import TicketDetailActors from './_ticket-detail-actors'

export default class TicketDetailSideBar extends Component {

  constructor(props){
    super(props)

    this.labelSelects = {
      'ticket_type': 'Tipo',
      status: 'Estado',
      priority: 'Prioridad'
    }

    this.statusList = {
      open: 'Abierto',
      're-opened': 'Re abierto',
      resolved: 'Resuelto',
      closed: 'Cerrado'
    }

    this.ticket_typeList = {
      modifications: 'Modificaciones',
      warranty: 'Solicitudes de Garantía',
      various: 'Varios',
      'payment-agreements': 'Preguntas sobre planes de pago',
      'complainsts-claims': 'Quejas y reclamos'
    }

    this.priorityList = {
      critical: 'Crítica',
      high: 'Alta',
      low: 'Baja',
      'very-low': 'Muy Baja'
    }

    this.state = {
      actorsFind:[],
      actorsActive: '',
      administratorsList: [],
      collaboratorsList: [],
      administratorsIds: [],
      collaboratorsIds: [],
      alertMessage: ''
    }

    this.filterActors = this.filterActors.bind(this)
    this.closeActorsPopup = this.closeActorsPopup.bind(this)
    this.updateStatus = this.updateStatus.bind(this)

    this.showAlertMessage = this.showAlertMessage.bind(this)
    this.alertUpdatePriority = this.alertUpdatePriority.bind(this)
  }

  componentWillMount(){

    this.setState({administratorsList: this.props.ticketDetail.administrators})
    this.setState({collaboratorsList: this.props.ticketDetail.collaborators})

    this.setState({administratorsIds: _.map(this.props.ticketDetail.administrators, 'id')})
    this.setState({collaboratorsIds: _.map(this.props.ticketDetail.collaborators, 'id')})
  }

  componentDidMount(){
    this.$ticketDetailAlertMessage = $('#ticketDetailAlertMessage')
    this.$ticketDetailAdministratorsResults = $('#ticketDetailAdministratorsResults')
    this.$ticketDetailColaboratorsResults = $('#ticketDetailColaboratorsResults')
    this.ticketDetailSearchAdministrator = $('#ticketDetailSearchAdministrator')
    this.ticketDetailSearchCollaborator = $('#ticketDetailSearchCollaborator')

  }

  openActorsPopup(actor){

    this.setState({actorsActive: actor})

    console.log(this.state.actorsActive);
  }

  closeActorsPopup(){
    this.setState({actorsActive: ''})
  }

  searchAdministrators(e){

    let inputValue = e.target.value

    console.log(e.target.id);

    if(e.target.value === '') {

      if(e.target.id === 'ticketDetailSearchAdministrator'){
        this.$ticketDetailAdministratorsResults.fadeOut(100)
      }
      if(e.target.id === 'ticketDetailSearchCollaborator'){
        this.$ticketDetailColaboratorsResults.fadeOut(100)
      }

    }

    else {

      if(e.target.id === 'ticketDetailSearchAdministrator'){
        this.$ticketDetailAdministratorsResults.css({ display: 'block' })
        this.$ticketDetailAdministratorsResults.fadeIn(100)
      }

      if(e.target.id === 'ticketDetailSearchCollaborator'){
        this.$ticketDetailColaboratorsResults.css({ display: 'block' })
        this.$ticketDetailColaboratorsResults.fadeIn(100)
      }

      $.ajax({
        url: `/api/projects/${this.props.pslug}/team`,
        method: 'GET'
      })
      .done((data) => {

        console.log(data);

        let actorsFind = data.map(( actor ) => {

          let actorName = `${actor.user.first_name} ${actor.user.last_name}`
          actorName = actorName.toLowerCase()

          actor.user.fullName = actorName

          if( (actor.user.fullName.search(inputValue)) > -1){
            return actor
          }

        })

        actorsFind = _.remove(actorsFind, (actor)=>{
          return actor !== undefined
        })

        if(this.state.actorsActive === 'administrators'){

          if(this.state.administratorsList.length > 0){
            _.each(this.state.administratorsList, (ticketActor)=>{
              let actorsFindResults = this.filterActors(ticketActor, actorsFind)
              return this.setState({actorsFind: actorsFindResults})
            })
          }
          return this.setState({actorsFind: actorsFind})
        }

        if(this.state.collaboratorsList.length > 0){
          _.each(this.state.collaboratorsList, (ticketActor)=>{
            let actorsFindResults = this.filterActors(ticketActor, actorsFind)
            return this.setState({actorsFind: actorsFindResults})
          })
        }

        return this.setState({actorsFind: actorsFind})

      })

      .fail((error)=> console.log(error))

    }

  }

  filterActors(ticketActor, actorsFind){
    _.remove(actorsFind, (actor)=>{
      return actor.id ===  ticketActor.id
    })

    return actorsFind
  }

  addActor(actor, actorType){

    this.setState({[`${actorType}List`]: this.state[`${actorType}List`].concat([actor])})

    this.$ticketDetailAdministratorsResults.fadeOut(100)
    this.$ticketDetailColaboratorsResults.fadeOut(100)

    $('.TicketDetail__actorsSearch input').val('')

    this.updateActorList()

  }

  removeActor(actor, actorType){

    let actorListTemp = this.state[`${actorType}List`]

    _.remove(actorListTemp, (actorTemp) => actorTemp.id === actor.id )

    this.setState({[`${actorType}List`] : actorListTemp})

    this.updateActorList()

  }

  updateActorList(){

    setTimeout(()=>{
      let administratorsIds = []
      let collaboratorsIds = []

      _.each(this.state.administratorsList, (admin)=>{
        administratorsIds.push(admin.id)
      })

      _.each(this.state.collaboratorsList, (admin)=>{
        collaboratorsIds.push(admin.id)
      })

      let ticketDetailTemp = this.props.ticketDetail

      ticketDetailTemp.administrators = administratorsIds
      ticketDetailTemp.collaborators = collaboratorsIds

      this.setState({ administratorsIds })
      this.setState({ collaboratorsIds })

      this.saveChanges(ticketDetailTemp)

    },300)

  }

  saveChanges(modifiedTicketDetail){

    let payload = {
      administrators: modifiedTicketDetail.administrators,
      client: this.props.ticketClientId,
      collaborators: modifiedTicketDetail.collaborators,
      priority: modifiedTicketDetail.priority,
      ticket_type: modifiedTicketDetail.ticket_type,
      status: modifiedTicketDetail.status
    }

    console.log(payload);

    $.ajax({
      url: `/api/projects/${this.props.pslug}/support/tickets/${this.props.ticketSlug}/`,
      method: 'PUT',
      data: JSON.stringify(payload),
      headers: {
        'X-CSRFToken': getCsrfToken('csrftoken'),
        'Content-Type' : 'application/json'
      }
    })

    .done((data) => {
      console.log(data);
    })
    .fail((error)=> console.log(error))

  }

  updateStatus(e, label){

    let ticketDetailTemp = this.props.ticketDetail

    let payload = {
      administrators: _.map(ticketDetailTemp.administrators, 'id'),
      client: this.props.ticketClientId,
      collaborators: _.map(ticketDetailTemp.collaborators, 'id'),
      priority: label === 'priority' ? e.target.value : ticketDetailTemp.priority,
      ticket_type: label === 'ticket_type' ? e.target.value : ticketDetailTemp.ticket_type,
      status: label === 'status' ? e.target.value : ticketDetailTemp.status
    }

    console.log(payload);

    $.ajax({
      url: `/api/projects/${this.props.pslug}/support/tickets/${this.props.ticketSlug}/`,
      method: 'PUT',
      data: JSON.stringify(payload),
      headers: {
        'X-CSRFToken': getCsrfToken('csrftoken'),
        'Content-Type' : 'application/json'
      }
    })

    .done((data) => {
      console.log(data, label);

      this.alertUpdatePriority(label, data[label])
    })
    .fail((error)=> console.log(error))

  }

  showAlertMessage(){

    this.$ticketDetailAlertMessage.css({display: 'block'})

    setTimeout(()=> this.$ticketDetailAlertMessage.css({right: '100%', transition: '.5s'}), 300)

    setTimeout(()=> this.$ticketDetailAlertMessage.css({right: '.9%', transition: '.5s'}), 2600)

    setTimeout(()=> this.$ticketDetailAlertMessage.css({display: 'none'}), 3000)
  }

  alertUpdatePriority(label, value){
    console.log(label, value)
    this.setState({alertMessage: `El ticket ha actualizado su ${this.labelSelects[label]} a ${this[`${label}List`][value]}`})
    this.showAlertMessage()
  }

  render(){
    return <div className="TicketDetail__sideBar">
      <div id="ticketDetailAlertMessage" className="TicketDetail__ticketDetailAlertMessage">
        { this.state.alertMessage }
      </div>
      <div className="TicketDetail__sideBarWrap">

        <div className="TicketDetail__details">
          <h3>Detalles del solicitante</h3>

          <div className="TicketDetail__applicantInfo">
            <div className="TicketDetail__applicantHead">
              <div className="TicketDetail__applicantHeadRound">{ `${this.props.ticketDetail.client.first_name[0]}${this.props.ticketDetail.client.last_name[0]}` }</div>
              <div>
                <div> { `${this.props.ticketDetail.client.first_name} ${this.props.ticketDetail.client.last_name}` } </div>
                <div> { this.props.ticketDetail.unity } </div>
              </div>
            </div>

            <div className="TicketDetail__applicantInfo">
              <span>Correo: </span>
              <span> { this.props.ticketDetail.client.email } </span>
            </div>

            <div className="TicketDetail__applicantInfo">
              <span>Celular: </span>
              <span> { this.props.ticketDetail.client.cellphone } </span>
            </div>


          </div>

          <h3>Detalles del ticket</h3>
          <div className="TicketDetail__ticketDetail">
            <div className="TicketDetail__creationDate">
              <span>Fecha de creación: </span>
              <div>  <span>{ moment(this.props.ticketDetail.timestamp).locale('es').format('DD MMMM YYYY') }</span> <span>( { moment(this.props.ticketDetail.timestamp).locale('es').fromNow() } )</span></div>
            </div>
            <div className="TicketDetail__ticketDetailInputs">
              <span>Tipo: </span>
              <span>
                <IconArrowDown />
                <div className="selectContainer">
                  <select className="selectStyle" onChange={ (e)=> this.updateStatus(e, 'ticket_type') }>
                    <option value="">{ this.props.typeList[this.props.ticketDetail.ticket_type] }</option>
                    {
                      this.props.typeListArray.map((v, key)=>{

                        if(this.props.typeList[this.props.ticketDetail.ticket_type] === v ) return

                        return <option value={ this.props.typeListInvert[v] } key={key}>{ v }</option>
                      })
                    }
                  </select>
                </div>
              </span>
            </div>

            <div className="TicketDetail__ticketDetailInputs">
              <span>Estado: </span>
              <span>
                <IconArrowDown />
                <div className="selectContainer">
                  <select className="selectStyle" onChange={ (e)=> this.updateStatus(e, 'status') }>
                    <option value="">{ this.props.statusList[this.props.ticketDetail.status] }</option>
                    {
                      this.props.statusListArray.map((v, key)=>{

                        if(this.props.statusList[this.props.ticketDetail.status] === v ) return

                        return <option value={ this.props.statusListInvert[v] } key={key}>{ v }</option>
                      })
                    }
                  </select>
                </div>
              </span>
            </div>

            <div className="TicketDetail__ticketDetailInputs">
              <span>Prioridad: </span>
              <span>
                <IconArrowDown />
                <div className="selectContainer">
                  <select className="selectStyle" onChange={ (e)=> this.updateStatus(e, 'priority') }>
                    <option value="">{ this.props.priorityList[this.props.ticketDetail.priority] }</option>
                    {
                      this.props.priorityListArray.map((v, key)=>{

                        if(this.props.priorityList[this.props.ticketDetail.priority] === v ) return

                        return <option value={ this.props.priorityListInvert[v] } key={key}>{ v }</option>
                      })
                    }
                  </select>
                </div>

              </span>
            </div>

          </div>

        </div>

        <TicketDetailActors
          actorsActive={this.state.actorsActive}
          actorsFind={this.state.actorsFind}
          administratorsList={this.state.administratorsList}
          collaboratorsList={this.state.collaboratorsList}
          openActorsPopup={this.openActorsPopup.bind(this)}
          closeActorsPopup={this.closeActorsPopup}
          searchAdministrators={this.searchAdministrators.bind(this)}
          addActor={this.addActor.bind(this)}
          removeActor={this.removeActor.bind(this)}
          administratorsIds={this.state.administratorsIds}
          collaboratorsIds={this.state.collaboratorsIds}
        />

      </div>

    </div>
  }
}
