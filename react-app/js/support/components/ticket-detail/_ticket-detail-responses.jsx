import React, { Component } from 'react'
import moment from 'moment'
import { IconResponse, IconNote} from '../../svg/support-icons'

export default class TicketDetailResponses extends Component {
  constructor(props){
    super(props)

    this.checkAutor = this.checkAutor.bind(this)
  }

  checkAutor(answer){

    let administratorsIds = _.map(this.props.ticketDetail.administrators, 'id')
    let collaboratorsIds = _.map(this.props.ticketDetail.collaborators, 'id')

    let writtenBy = answer.written_by.id

    let isAdministrator = _.some(administratorsIds, (adminId) => writtenBy === adminId )
    let isCollaborator = _.some(collaboratorsIds, (collaboratorId) => writtenBy === collaboratorId )

    if(isAdministrator && isCollaborator) return 'Administrador y Colaborador'

    if(isAdministrator) return 'Administrador'

    if(isCollaborator) return 'Colaborador'

    return 'Colaborador'

  }

  render(){
    return <div className="TicketDetail__responses">

      {
        this.props.ticketDetail.answers ? this.props.ticketDetail.answers.map(( answer, key )=>{

          return (
            <div className="TicketDetail__response" key={key} style={{ borderLeft: '2px solid #039be5' }}>
                <div className="TicketDetail__responseAvatar">
                  <IconResponse color={'#039be5'}/>
                </div>
                <div className="TicketDetail__responseDescription">
                  <span>{ `${answer.written_by.user.first_name} ${answer.written_by.user.last_name}` } { `( ${ this.checkAutor(answer)} del ticket )` }</span>
                  <p> { answer.answer_text } </p>
                  <a href={`${answer.answer_attached_file}`} className="TicketDetail__responseDocument">{ answer.answer_attached_file ? answer.answer_attached_file.split('/')[5] : '' }</a>
                  <div className="TicketDetail__responseDate">
                    <span> { moment(answer.timestamp).locale('es').format('DD MMMM YYYY') } </span>
                    <span>( { moment(answer.timestamp).locale('es').fromNow() } )</span>
                  </div>
                </div>
            </div>
          )
        }) : []


      }

    </div>

  }
}
