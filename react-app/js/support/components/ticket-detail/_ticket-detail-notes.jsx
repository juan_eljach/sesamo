import React, { Component } from 'react'
import moment from 'moment'
import { IconResponse, IconNote} from '../../svg/support-icons'

export default class TicketDetailNotes extends Component {
  constructor(props){
    super(props)

    this.checkAutor = this.checkAutor.bind(this)
  }

  checkAutor(note){

    let administratorsIds = _.map(this.props.ticketDetail.administrators, 'id')
    let collaboratorsIds = _.map(this.props.ticketDetail.collaborators, 'id')

    let writtenBy = note.written_by.id

    let isAdministrator = _.some(administratorsIds, (adminId) => writtenBy === adminId )
    let isCollaborator = _.some(collaboratorsIds, (collaboratorId) => writtenBy === collaboratorId )

    if(isAdministrator && isCollaborator) return 'Administrador y Colaborador'

    if(isAdministrator) return 'Administrador'

    if(isCollaborator) return 'Colaborador'

    return 'Colaborador'
  }

  render(){
    return <div className="TicketDetail__responses">

      {
        this.props.notes ? this.props.notes.map(( note, key )=>{
          return (
            <div className="TicketDetail__response" key={key} style={{ borderLeft: '2px solid #ea8953' }}>
                <div className="TicketDetail__responseAvatar">
                  <IconNote color={'#ea8953'}/>
                </div>
                <div className="TicketDetail__responseDescription">
                  <span>{ `${note.written_by.user.first_name} ${note.written_by.user.last_name}` } { `(${ this.checkAutor(note) } del ticket)  ` }</span>
                  <p> {note.note_text } </p>
                  <div className="TicketDetail__responseDate">
                    <span> { moment(note.timestamp).locale('es').format('DD MMMM YYYY') } </span>
                    <span>( { moment(note.timestamp).locale('es').fromNow() } )</span>
                  </div>
                </div>
            </div>
          )
        }) : []
      }

    </div>

  }
}
