'use strict'

import getCsrfToken from '../../lib/csrftoken'

const pslug = window.location.href.split('/')[4]
const baseURL = `/api/projects/${pslug}/support`


function getTickets(){

  return $.ajax({
    url: `${baseURL}/tickets/`,
    method: 'GET',
    headers: {
      'X-CSRFToken': getCsrfToken('csrftoken'),
      'X-Requested-With' : 'XMLHttpRequest'
    }
  })

}

function getTicketDetail(ticketSlug){

  return $.ajax({
    url: `${baseURL}/tickets/${ticketSlug}`,
    method: 'GET',
    headers: {
      'X-CSRFToken': getCsrfToken('csrftoken'),
      'X-Requested-With' : 'XMLHttpRequest'
    }
  })

}


export { getTickets, getTicketDetail }
