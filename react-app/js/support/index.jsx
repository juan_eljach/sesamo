import React, {Component} from 'react'
import { getTickets } from './api/api-tickets'
import {IconSearch} from '../formalization/svg/_svg-icons'
import { Link } from 'react-router'
import { IconEye, IconHand, IconAssign, IconTicket, IconNotification, IconArrowDown } from './svg/support-icons'
import getCsrfToken from '../lib/csrftoken'

export default class Support extends Component {

  constructor(props){
    super(props)

    this.pslug = window.location.href.split('/')[4]

    this.URL_ALL_TICKETS = `/api/projects/${this.pslug}/support/tickets/`
    this.URL_SIDEBAR_FILTERS = `/api/projects/${this.pslug}/support/tickets/filter/`

    this.statusList = {
      open: 'Abierto',
      're-opened': 'Re abierto',
      resolved: 'Resuelto',
      closed: 'Cerrado'
    }

    this.ticketTypeList = {
      modifications: 'Modificaciones',
      warranty: 'Solicitudes de Garantía',
      various: 'Varios',
      'payment-agreements': 'Preguntas sobre planes de pago',
      'complaints-claims': 'Quejas y Reclamos'
    }

    this.priorityList = {
      critical: 'Crítica',
      high: 'Alta',
      low: 'Baja',
      'very-low': 'Muy baja'
    }

    this.priorityListArray = [
      'Crítica',
      'Alta',
      'Baja',
      'Muy baja'
    ]

    this.statusListInvert = {
      'Abierto' : 'open',
      'Re abierto' : 're-opened',
      'Resuelto' : 'resolved',
      'Cerrado' : 'closed'
    }

    this.typeListInvert = {
      'Modificaciones' : 'modifications',
      'Solicitudes de Garantía' : 'warranty',
      'Varios' : 'various',
      'Preguntas sobre planes de pago' : 'payment-agreements',
      'Quejas y Reclamos' : 'complainsts-claims'
    }

    this.priorityListInvert = {
      'Crítica' : 'critical',
      'Alta': 'high',
      'Baja' : 'low',
      'Muy baja' : 'very-low'
    }

    this.randomColors = [
      '#81c784',
      '#9575cd',
      '#ffd54f',
      '#29b6f6',
      '#ff8a80',
      '#f06292'
    ]

    this.state = {
      tickets: [],
      selectedTickets: [],
      statusFilter: '',
      typeFilter: '',
      priorityFilter: '',
      priorityColors: {
        'Crítica': '#dd7373',
        'Alta': '#ea8953',
        'Baja': '#82cb79',
        'Muy baja': '#ebbb51'
      },
      tabSidebarSelected: 1,
      allTickets: [],
      ticketsAsAdmin: [],
      ticketsAsCollaborator: [],
      unassignedTickets: [],
      alertMessage: '',
      isOpenPopup: false,
      popupTicketTitle: ''
    }

    this.getAllTickets = this.getAllTickets.bind(this)
    this.getAdminTickets = this.getAdminTickets.bind(this)
    this.getCollaboratorTickets = this.getCollaboratorTickets.bind(this)
    this.getUnassignedTickets = this.getUnassignedTickets.bind(this)
    this.showAlertMessage = this.showAlertMessage.bind(this)
    this.alertUpdatePriority = this.alertUpdatePriority.bind(this)
    this.alertTakeTicket = this.alertTakeTicket.bind(this)
    this.updateColor = this.updateColor.bind(this)
    this.closePopup = this.closePopup.bind(this)
  }

  componentDidMount(){
    this.getAllTickets()
    this.getAdminTickets()
    this.getCollaboratorTickets()
    this.getUnassignedTickets()

    this.$supportAlertMessage = $('#supportAlertMessage')
  }

  getAllTickets(){
    $.ajax({
      url: this.URL_ALL_TICKETS
    })
    .done((data) => {
      this.setState({tickets: data})
      this.setState({allTickets: data})
      this.ticketsTemp = data
    })
    .fail((error)=> console.log(error))
  }

  getAdminTickets(){
    $.ajax({
      url: `${this.URL_SIDEBAR_FILTERS}my_tickets_admin/`
    })
    .done((data) => {
      this.setState({ ticketsAsAdmin: data })
      this.setState({ tickets : data })
    })
    .fail((error)=> console.log(error))
  }

  getCollaboratorTickets(){
    $.ajax({
      url: `${this.URL_SIDEBAR_FILTERS}my_tickets_collaborator/`
    })
    .done((data) => {
      this.setState({ ticketsAsCollaborator: data })
      this.setState({ tickets : data })
    })
    .fail((error)=> console.log(error))
  }

  getUnassignedTickets(){
    $.ajax({
      url: `${this.URL_SIDEBAR_FILTERS}unassigned/`
    })
    .done((data) => {
      this.setState({ unassignedTickets : data })
      this.setState({ tickets : data })
    })
    .fail((error)=> console.log(error))
  }

  checkHandler(e){
    $(`#${e.target.id}`).parent().parent().toggleClass('backgroundGray')
  }

  headerFilters(value, selectFilter){
    this.setState({ [`${selectFilter}`] : value }, ()=>{

      let URL = `/api/projects/${this.pslug}/support/tickets/?${this.state.statusFilter ? `status=${this.state.statusFilter}` : '' }${this.state.typeFilter ? `&ticket_type=${this.state.typeFilter}` : ''}${this.state.priorityFilter ? `&priority=${this.state.priorityFilter}` : ''}`

      $.ajax({
        url: URL,
        method: 'GET'
      })
      .done((data) => {

        if(selectFilter === 'priorityFilter'){
          this.setState({tickets:[]})
          this.setState({tickets: data})
          return
        }
        this.setState({tickets: data})
      })
      .fail((error)=> console.log(error))
    })

  }

  updateTablePriority(e, ticketSlug){
    let {target} = e

    _.map(this.ticketsTemp, (ticket) => {

      if(ticket.slug === ticketSlug){

        ticket.priority = this.priorityListInvert[e.target.value]

        let administratorsIds = _.map(ticket.administrators, 'id')
        let collaboratorsIds = _.map(ticket.collaborators, 'id')

        let payload = {
          administrators: administratorsIds,
          client: ticket.client.id,
          collaborators: collaboratorsIds,
          priority: ticket.priority,
          ticket_type: ticket.ticket_type,
          status: ticket.status
        }

        $.ajax({
          url: `/api/projects/${this.pslug}/support/tickets/${ticketSlug}/`,
          method: 'PUT',
          data: JSON.stringify(payload),
          headers: {
            'X-CSRFToken': getCsrfToken('csrftoken'),
            'Content-Type' : 'application/json'
          }
        })
        .done((data) => {
          this.updateColor(target, target.value)

          this.alertUpdatePriority(ticket.priority, ticket.title)
        })
        .fail((error)=> {
          if(error.status === 403) {
            this.updateColor(this.priorityList[payload.priority])
            this.setState({isOpenPopup: true})
            this.setState({popupTicketTitle: ticket.title})
          }
        })
      }
    })

  }

  updateColor(target, ticketPriority) {
    let targetElement = $(target)
    let priorityColor = this.state.priorityColors[ticketPriority]

    $(target).css({color: priorityColor})
    $(target).parent().prev().css({background: priorityColor})
  }

  closePopup() {
    this.setState({isOpenPopup: false})
  }

  takeTicket(ticketTaken){
    $.ajax({
      url: '/api/userprofile',
      method: 'GET',
    })
    .done((data)=> {

      let ticketTemp = {
        administrators: [data],
        answers: ticketTaken.answers,
        attached_file: ticketTaken.attached_file,
        client: ticketTaken.client,
        collaborators: ticketTaken.collaborators,
        description: ticketTaken.description,
        notes: ticketTaken.notes,
        priority: ticketTaken.priority,
        slug: ticketTaken.slug,
        status: ticketTaken.status,
        ticket_type: ticketTaken.ticket_type,
        timestamp: ticketTaken.timestamp,
        title: ticketTaken.title,
        unity: ticketTaken.unity
      }

      let temp = []

      _.each(this.state.tickets, (ticket) => {
        if(ticket.slug === ticketTaken.slug) return temp.push(ticketTemp)
        return temp.push(ticket)
      })

      this.setState({tickets: temp})

      let payload = {
        administrators: _.map(ticketTemp.administrators, 'id'),
        client: ticketTemp.client.id,
        collaborators: _.map(ticketTemp.collaborators, 'id'),
        priority: ticketTemp.priority,
        ticket_type: ticketTemp.ticket_type,
        status: ticketTemp.status
      }

      $.ajax({
        url: `/api/projects/${this.pslug}/support/tickets/${ticketTaken.slug}/`,
        method: 'PUT',
        data: JSON.stringify(payload),
        headers: {
          'X-CSRFToken': getCsrfToken('csrftoken'),
          'Content-Type' : 'application/json'
        }
      })
      .done((data)=> {
        this.alertTakeTicket(ticketTaken.title)
      })
      .fail((error)=> console.log(error))

    })
    .fail((error)=> console.log(error))

  }

  sidebarFilters(filter, tab, sidebarFilterHandler){

    this.setState({tabSidebarSelected: tab})

    sidebarFilterHandler()

  }

  showAlertMessage(){

    this.$supportAlertMessage.css({display: 'block'})

    setTimeout(()=> this.$supportAlertMessage.css({right: '20.6%', transition: '.5s'}), 300)

    setTimeout(()=> this.$supportAlertMessage.css({right: '.9%', transition: '.5s'}), 3200)

    setTimeout(()=> this.$supportAlertMessage.css({display: 'none'}), 3500)
  }

  alertUpdatePriority(priority, title){
    this.setState({alertMessage: `El ticket: ${title} se ha actualizado a prioridad ${this.priorityList[priority]}`})
    this.showAlertMessage()
  }

  alertTakeTicket(title){
    this.setState({alertMessage: `Has tomado el ticket: ${title}`})
    this.showAlertMessage()
  }

  render(){
    return (
      <section className="Support">

        <div className="Support__header">
          <div className="Support__filters">
            <div className="Support__stateFilter">
              <label htmlFor="">Estado:</label>
              <div className="selectContainer">
                <select className="selectStyle" onChange={(e)=> this.headerFilters(e.target.value, 'statusFilter') }>
                  <option value="">Filtrar</option>
                  <option value="open">Abierto</option>
                  <option value="re-open">Re Abierto</option>
                  <option value="resolved">Resuelto</option>
                  <option value="closed">Cerrado</option>
                </select>
              </div>

              <IconArrowDown />
            </div>
            <div className="Support__typeFilter">
              <label htmlFor="">Tipo:</label>
              <div className="selectContainer">
                <select className="selectStyle" id="" onChange={(e)=> this.headerFilters(e.target.value, 'typeFilter') }>
                  <option value="">Filtrar</option>
                  <option value="modifications">Modificaciones</option>
                  <option value="warranty">Solicitudes de garantía</option>
                  <option value="various">Varios</option>
                  <option value="payment-agreements">Preguntas planes de pago</option>
                  <option value="complaints-claims">Quejas y reclamos</option>
                </select>
              </div>
              <IconArrowDown />
            </div>
            <div className="Support__priorityFilter">
              <label htmlFor="">Prioridad:</label>
              <span style={{background:`${this.state.priorityColors[this.state.priorityFilter]}`}}></span>
              <div className="selectContainer">
                <select className="selectStyle" id="supportPriorityFilter" onChange={(e)=> this.headerFilters(e.target.value, 'priorityFilter') }>
                  <option value="">Filtrar</option>
                  <option value="critical">Crítica</option>
                  <option value="high">Alta</option>
                  <option value="low">Baja</option>
                  <option value="very-low">Muy baja</option>
                </select>
              </div>
              <IconArrowDown />
            </div>

          </div>

          <div className="Support__actions">

            <div className="Support__newTicket"><Link to="/support/new-ticket">NUEVO TICKET</Link></div>
          </div>

        </div>

        <div className="Support__filterInfo">{ `Filtrando por tickets de tipo ${ this.state.typeFilter ? this.ticketTypeList[this.state.typeFilter] : '' } de prioridad ${this.state.priorityFilter ? this.priorityList[this.state.priorityFilter] : ''}` }</div>

        {/* <div className="Support__searchTickets">
          <IconSearch />
          <input type="text"/>
        </div> */}

        <div className="Support__table">
          <div id="supportAlertMessage" className="Support__alertMessage">
            { this.state.alertMessage }
          </div>
          <div className="Support__tableHead">
            <div>Solicitante</div>
            <div>Tipo</div>
            <div>Título</div>
            <div>Administrado por</div>
            <div>Prioridad</div>
            <div></div>
          </div>
          <div className={`Support__noResults ${this.state.tickets.length > 0 ? 'displayNone' : 'displayBlock'}`}>No se encontraron resultados</div>
          {
            this.state.tickets.map((ticket, key) => {
              return <div className="Support__tableRow" key={key}>
                <div className="Support__applicant">
                  {/* <div className="Support__check" id={key} onClick={(e)=>this.checkHandler(e, ticket)}></div> */}
                  <div className="Support__tableRound" style={{ background: `${ this.randomColors[Math.floor(Math.random() * 6)] }` }}>{ `${ticket.client.first_name[0]} ${ticket.client.last_name[0]}` }</div>
                  <div>
                    <div className="Support__name">{ `${ticket.client.first_name} ${ticket.client.last_name}` }</div>
                    <span>{ ticket.unity }</span>
                  </div>
                </div>
                <div>{ this.ticketTypeList[ticket.ticket_type] }</div>
                <div>{ ticket.title }</div>

                {
                  ticket.administrators.length > 0  ? (
                    <div>

                      {
                        ticket.administrators.map((admin, key)=>{

                          if(ticket.administrators.length > 1) return <span key={key}> {admin.user.first_name}, </span>

                          return <span key={key}> {admin.user.first_name}</span>
                        })
                      }

                    </div>
                  ) : <div className="Support__takeTicket" onClick={ ()=> this.takeTicket(ticket) }><span>Tomar ticket</span></div>
                }


                <div>
                  <span style={{background:`${this.state.priorityColors[this.priorityList[ticket.priority]]}`}} className={ticket.status ? '' : 'displayNone'}></span>
                  <div>
                    <select onChange={(e)=> this.updateTablePriority(e, ticket.slug) } style={{color: `${this.state.priorityColors[this.priorityList[ticket.priority]]}`}}>

                      <option value={`${ticket.priority}`} >{ this.priorityList[ticket.priority] }</option>
                      {
                        this.priorityListArray.map((v, key)=>{

                          if(this.priorityList[ticket.priority] === v ) return
                          return <option value={`${v}`} key={key}>{ v }</option>

                        })
                      }
                    </select>
                  </div>
                  <IconArrowDown />
                </div>
                <div className="Support__tableRowDetailView">
                  <Link to={`/support/ticket/${ticket.slug}`}>
                    <div>Ver</div>
                    <IconEye />
                  </Link>
                </div>
              </div>
            })
          }

        </div>

        <div className="Support__ticketState">

          <div className="Support__ticketStateTab">
            <div> <IconTicket />  Tickets</div>
            {/* <div> <IconNotification /> Notificaciones</div> */}
          </div>

          <div className="Support__ticketStateList">
            <div onClick={()=> this.sidebarFilters('allTickets', 1, this.getAllTickets)} className={`${this.state.tabSidebarSelected === 1 ? 'hoverTab' : '' }`}>
              <div>Todos los Tickets</div>
              <span> { this.state.allTickets.length } </span>
            </div>
            <div onClick={()=> this.sidebarFilters('ticketsAsAdmin', 2, this.getAdminTickets)} className={`${this.state.tabSidebarSelected === 2 ? 'hoverTab' : '' }`}>
              <div>Tickets que administras</div>
              <span>{ this.state.ticketsAsAdmin.length }</span>
            </div>
            <div onClick={()=> this.sidebarFilters('ticketsAsCollaborator', 3, this.getCollaboratorTickets)} className={`${this.state.tabSidebarSelected === 3 ? 'hoverTab' : '' }`}>
              <div>Tickets en que eres colaborador</div>
              <span>{ this.state.ticketsAsCollaborator.length }</span>
            </div>
            <div onClick={()=> this.sidebarFilters('unassignedTickets', 4, this.getUnassignedTickets)} className={`${this.state.tabSidebarSelected === 4 ? 'hoverTab' : '' }`}>
              <div>Tickets sin asignar</div>
              <span>{this.state.unassignedTickets.length}</span>
            </div>

          </div>
        </div>
        <div className={`Support__popup ${this.state.isOpenPopup ? '' : 'displayNone' }`}>
          <div className={`${this.props.isOpenPopup ? 'animated zoomOut' : 'animated zoomIn'}`}>
            <p>No tienes permisos para cambiar la prioridad del ticket: {this.state.popupTicketTitle}</p>
            <button onClick={this.closePopup}>Entiendo</button>
          </div>
        </div>
      </section>
    )
  }
}
