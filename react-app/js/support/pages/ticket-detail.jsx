import React, { Component } from 'react'
import { getTicketDetail } from '../api/api-tickets'
import moment from 'moment'

import { IconResponse, IconNote, IconAdd, IconAssign } from '../svg/support-icons'

import TicketDetailResponses from '../components/ticket-detail/_ticket-detail-responses'
import TicketDetailNotes from '../components/ticket-detail/_ticket-detail-notes'
import TicketDetailAllResponses from '../components/ticket-detail/_ticket-detail-allResponses'
import TicketDetailAddResponses from '../components/ticket-detail/_ticket-detail-addResponse'
import TicketDetailSideBar from '../components/ticket-detail/_ticket-detail-sidebar'


export default class TicketDetail extends Component{

  constructor(props){
    super(props)

    this.pslug = window.location.href.split('/')[4]

    this.statusList = {
      open: 'Abierto',
      're-opened': 'Re abierto',
      resolved: 'Resuelto',
      closed: 'Cerrado'
    }

    this.typeList = {
      modifications: 'Modificaciones',
      warranty: 'Solicitudes de Garantía',
      various: 'Varios',
      'payment-agreements': 'Preguntas sobre planes de pago',
      'complainsts-claims': 'Quejas y reclamos'
    }

    this.priorityList = {
      critical: 'Crítica',
      high: 'Alta',
      low: 'Baja',
      'very-low': 'Muy Baja'
    }

    this.statusListInvert = {
      'Abierto' : 'open',
      'Re abierto' : 're-opened',
      'Resuelto' : 'resolved',
      'Cerrado' : 'closed'
    }

    this.typeListInvert = {
      'Modificaciones' : 'modifications',
      'Solicitudes de Garantía' : 'warranty',
      'Varios' : 'various',
      'Preguntas sobre planes de pago' : 'payment-agreements',
      'Quejas y Reclamos' : 'complainsts-claims'
    }

    this.priorityListInvert = {
      'Crítica' : 'critical',
      'Alta': 'high',
      'Baja' : 'low',
      'Muy Baja' : 'very-low'
    }

    this.priorityListArray = [
      'Crítica',
      'Alta',
      'Baja',
      'Muy Baja'
    ]

    this.typeListArray = [
      'Modificaciones',
      'Solicitudes de Garantía',
      'Varios',
      'Preguntas sobre planes de pago',
      'Quejas y Reclamos'
    ]

    this.statusListArray = [
      'Abierto',
      'Re abierto',
      'Resuelto',
      'Cerrado'
    ]

    this.state = {
      ticketDetail: {},
      ticketAdministrators: [],
      ticketColaborators: [],
      tabSendResponse: 1,
      tabResponseOptions: 1,
      allResponse: [],
      ticketClientId: 0
    }

  }

  componentDidMount(){

    $.ajax({
      url: `/api/projects/${this.pslug}/support/tickets/${this.props.params.ticketSlug}`,
      method: 'GET',
      headers: {
        'X-CSRFToken': this.getCookie('csrftoken'),
        'X-Requested-With' : 'XMLHttpRequest'
      }
    })
      .done((data) => {
        console.log(data);
        this.setState({ticketDetail: data})
        this.setState({ticketClientId: data.client.id})

        this.setState({ticketAdministrators: data.administrators})
        this.setState({ticketColaborators: data.collaborators})

        let allResponseTemp = []

        allResponseTemp = allResponseTemp.concat(data.answers).concat(data.notes)

        allResponseTemp = _.sortBy(allResponseTemp, (res) => {
          return res.timestamp
        })

        this.setState({ allResponse: allResponseTemp.reverse()})

      })
      .fail((error)=> console.log(error))

  }

  selectTabSendResponse(tab){
    this.setState({tabSendResponse: tab})
  }

  selectTabResponseOptions(tab){
    this.setState({tabResponseOptions: tab})
  }

  sendResponse(){

    if(this.state.tabSendResponse === 1){

      var formData = new FormData($('#ticketForm'))

      formData.append('answer_attached_file', $('#answerAttachedFile')[0].files[0])
      formData.append('answer_text', $('#answerText').val())

      $.ajax({
        url: `/api/projects/${this.pslug}/support/tickets/${this.props.params.ticketSlug}/answers/create/`,
        method: 'POST',
        data: formData,
        headers: {
          'X-CSRFToken': this.getCookie('csrftoken')
        },
        processData: false,
        contentType: false
      })
      .done((data) => {
        console.log(data);
        $('#answerAttachedFile')[0].value = ''
        $('#answerText').val('')
        console.log(this.state.allResponse);
        // this.setState({ allResponse: this.state.allResponse.concat(data.answers)})
      })
      .fail((error)=> console.log(error))

      return;
    }

    $.ajax({
      url: `/api/projects/${this.pslug}/support/tickets/${this.props.params.ticketSlug}/ticket-notes/create/`,
      method: 'POST',
      data: { note_text:  $('#answerText').val()},
      headers: {
        'X-CSRFToken': this.getCookie('csrftoken')
      }
    })
    .done((data) => {
      console.log(data);
      $('#answerText').val('')
    })
    .fail((error)=> console.log(error))

  }

  getCookie(name){
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
  }

  render(){
    return (
      <section className="TicketDetail">
        <div className="TicketDetail__header">
          <div>Ticket {this.state.ticketDetail.unity} <span onClick={()=> this.props.router.push('/support')}>x</span></div>
          {/* <button>GUARDAR CAMBIOS</button> */}
        </div>
        <div className="TicketDetail__body">
          <div className="TicketDetail__reason">
            <div className="TicketDetail__reasonTitle">
              <span>Título:</span>
              <div> { this.state.ticketDetail.title } </div>
            </div>
            <div className="TicketDetail__description">
              <span>Descripción:</span>
              <p>
                {
                  this.state.ticketDetail.description
                }
              </p>
            </div>
            <div className="TicketDetail__document">
              <span>Documentos:</span>
              <div><a href={`${this.state.ticketDetail.attached_file}`} target="_blank">{ this.state.ticketDetail.attached_file ? this.state.ticketDetail.attached_file.split('/')[5] : '' }</a></div>
            </div>
          </div>

          <div className="TicketDetail__responseOptions">
            <div onClick={ ()=> this.selectTabResponseOptions(1) } className={ this.state.tabResponseOptions === 1 ? 'u-tabResponseOptions' : '' }>
              <span>Todas</span>
            </div>
            <div onClick={ ()=> this.selectTabResponseOptions(2) } className={ this.state.tabResponseOptions === 2 ? 'u-tabResponseOptions' : '' }>
              <IconResponse />
              <span>Respuestas</span>
            </div>
            <div onClick={ ()=> this.selectTabResponseOptions(3) } className={ this.state.tabResponseOptions === 3 ? 'u-tabResponseOptions' : '' }>
              <IconNote />
              <span>Notas internas</span>
            </div>
          </div>

          {
            this.state.tabResponseOptions === 1 ? <TicketDetailAllResponses allResponse={this.state.allResponse} ticketDetail={this.state.ticketDetail}/> : null
          }

          {
            this.state.tabResponseOptions === 2 ? <TicketDetailResponses ticketDetail={this.state.ticketDetail}/> : null
          }

          {
            this.state.tabResponseOptions === 3 ? <TicketDetailNotes notes={this.state.ticketDetail.notes.reverse()} ticketDetail={this.state.ticketDetail} /> : null
          }

          <TicketDetailAddResponses
            ticketDetail={this.state.ticketDetail}
            sendResponse={this.sendResponse.bind(this)}
            selectTabSendResponse={this.selectTabSendResponse.bind(this)}
            tabSendResponse={this.state.tabSendResponse}
            pslug={this.pslug}
          />

        </div>

        {
          this.state.ticketDetail.client ? (
            <TicketDetailSideBar
              ticketDetail={this.state.ticketDetail}
              statusList={this.statusList}
              typeList={this.typeList}
              priorityList={this.priorityList}
              statusListArray={this.statusListArray}
              statusListInvert={ this.statusListInvert}
              priorityListInvert={ this.priorityListInvert}
              typeListInvert={ this.typeListInvert}
              typeListArray={this.typeListArray}
              priorityListArray={this.priorityListArray}
              pslug={this.pslug}
              ticketSlug={this.props.params.ticketSlug}
              ticketClientId={this.state.ticketClientId}
            />
          ) : null
        }


      </section>
    )
  }
}
