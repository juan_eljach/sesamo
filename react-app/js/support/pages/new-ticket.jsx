import React, { Component } from 'react'
import TicketDetailActors from '../components/ticket-detail/_ticket-detail-actors'
import getCsrfToken from '../../lib/csrftoken'
import { IconSearch } from '../../formalization/svg/_svg-icons'
import {
  IconPen,
  IconArrowDown,
  IconTicketSupport,
  IconArrowBack } from '../svg/support-icons'

import NewTicketApplicantDetails from '../components/new-ticket/_newticket-applicantDetails'
import NewTicketDescription from '../components/new-ticket/_newticket-description'
import NewTicketTicketDetails from '../components/new-ticket/_newticket-ticketDetails'
import NewTicketTicketActors from '../components/new-ticket/_newticket-ticketActors'

export default class NewTicket extends Component{
  constructor(props){
    super(props)

    this.pslug = window.location.href.split('/')[4]

    this.hideChip = this.hideChip.bind(this)
    this.showChip = this.showChip.bind(this)
    this.openActorsPopup = this.openActorsPopup.bind(this)
    this.closeActorsPopup = this.closeActorsPopup.bind(this)
    this.searchAdministrators = this.searchAdministrators.bind(this)
    this.addActor = this.addActor.bind(this)
    this.removeActor = this.removeActor.bind(this)
    this.saveTicket = this.saveTicket.bind(this)
    this.checkValidForm = this.checkValidForm.bind(this)
    this.updateNameFile = this.updateNameFile.bind(this)
    this.searchClient = this.searchClient.bind(this)
    this.selectedClient = this.selectedClient.bind(this)
    this.updateTicketDetails = this.updateTicketDetails.bind(this)
    this.goBack = this.goBack.bind(this)

    this.state = {
      foundClients: [],
      selectedClient: {},
      showChip: false,
      ticketDetailType: 'modifications',
      ticketDetailStatus: 'open',
      ticketDetailPriority: 'low',
      clientUnits: [],
      actorsFind:[],
      actorsActive: '',
      administratorsIds: [],
      collaboratorsIds: [],
      administratorsList: [],
      collaboratorsList: [],
      isValidTicketForm: true,
      ticketTitleIsValid: true,
      ticketDescriptionIsValid: true
    }

  }

  componentDidMount(){
    this.$newTicketSearchClient = $('#newTicketSearchClient')
    this.$newTicketSearchResults = $('#newTicketSearchResults')
    this.$newTicketAdministratorsResults = $('#newTicketAdministratorsResults')
    this.$newTicketCollaboratorsResults = $('#newTicketCollaboratorsResults')
    this.$ticketTitle = $('#newTicketTitle')
    this.$ticketDescription = $('#newTicketDescription')
    this.$newTicketApplicantApto = $('#newTicketApplicantApto')
    this.$newTicketApplicantEmail = $('#newTicketApplicantEmail')
    this.$newTicketApplicantPhone = $('#newTicketApplicantPhone')
    this.$newTicketAttachedFile = $('#newTicketAttachedFile')
  }

  searchClient(e){

    if(e.target.value === '') this.$newTicketSearchResults.fadeOut(100)

    else{
      this.$newTicketSearchResults.css({display: 'block'})
      this.$newTicketSearchResults.fadeIn(100)
      $.ajax({
        method: 'GET',
        url: `/api/projects/${this.pslug}/persons?search=${e.target.value}`
      })
      .done((data) => {
        console.log(data)
        this.setState({ foundClients: data })
      })
      .fail((error)=> console.log(error))
    }

  }

  selectedClient(client){
    this.setState({selectedClient: client})
    this.setState({showChip: true})
    this.$newTicketSearchResults.css({display: 'none'})

    $.ajax({
      url: `/api/projects/${this.pslug}/support/client/?identity=${client.identity}`
    })
    .done((data) => {
      this.setState({clientUnits: _.uniqBy(data, 'id')})
    })
    .fail((error)=> console.log(error))

  }

  showChip(){
    this.setState({showChip : true })
  }

  hideChip(){
    this.setState({showChip : false })
    this.$newTicketSearchClient.val('')
    // this.$newTicketApplicantApto.val('')
    this.setState({clientUnits:[]})
    this.setState({selectedClient: {}  })
  }

  updateTicketDetails(e, label){
    console.log(e.target.value, label);
    this.setState({ [label] : e.target.value })
  }

  openActorsPopup(actor){
    this.setState({actorsActive: actor})
  }

  closeActorsPopup(){
    this.setState({actorsActive: ''})
  }

  searchAdministrators(e){

    let inputValue = e.target.value

    if(e.target.value === '') {

      if(e.target.id === 'newTicketSearchCollaborator'){
        return this.$newTicketCollaboratorsResults.fadeOut(100)
      }
      return this.$newTicketAdministratorsResults.fadeOut(100)
    }

    else {

      if(e.target.id === 'newTicketSearchCollaborator'){
        this.$newTicketCollaboratorsResults.css({ display: 'block' })
        this.$newTicketCollaboratorsResults.fadeIn(100)
      }
      else{
        this.$newTicketAdministratorsResults.css({ display: 'block' })
        this.$newTicketAdministratorsResults.fadeIn(100)
      }

      $.ajax({
        url: `/api/projects/${this.pslug}/team`,
        method: 'GET'
      })
      .done((data) => {

        let actorsFind = data.map(( actor ) => {

          let actorName = `${actor.user.first_name} ${actor.user.last_name}`
          actorName = actorName.toLowerCase()

          actor.user.fullName = actorName

          if( (actor.user.fullName.search(inputValue)) > -1){
            return actor
          }

        })

        actorsFind = _.remove(actorsFind, (actor)=>{
          return actor !== undefined
        })

        if(this.state.actorsActive === 'administrators'){

          if(this.state.administratorsList.length > 0){
            _.each(this.state.administratorsList, (ticketActor)=>{
              let actorsFindResults = this.filterActors(ticketActor, actorsFind)
              return this.setState({actorsFind: actorsFindResults})
            })
          }
          return this.setState({actorsFind: actorsFind})
        }

        if(this.state.collaboratorsList.length > 0){
          _.each(this.state.collaboratorsList, (ticketActor)=>{
            let actorsFindResults = this.filterActors(ticketActor, actorsFind)
            return this.setState({actorsFind: actorsFindResults})
          })
        }

        return this.setState({actorsFind: actorsFind})

      })

      .fail((error)=> console.log(error))

    }

  }

  filterActors(ticketActor, actorsFind){
    _.remove(actorsFind, (actor)=>{
      return actor.id ===  ticketActor.id
    })

    return actorsFind
  }

  addActor(actor, actorType){

    this.setState({[`${actorType}List`]: this.state[`${actorType}List`].concat([actor])})

    if(actorType === 'administrators') {
      this.$newTicketAdministratorsResults.fadeOut(100)
      $('#newTicketSearchAdministrator').val('')
    }

    else {
      this.$newTicketCollaboratorsResults.fadeOut(100)
      $('#newTicketSearchCollaborator').val('')
    }

    this.updateActorList()

  }

  removeActor(actor, actorType){

    let actorListTemp = this.state[`${actorType}List`]

    _.remove(actorListTemp, (actorTemp) => actorTemp.id === actor.id )

    this.setState({[`${actorType}List`] : actorListTemp})

    this.updateActorList()

  }

  updateActorList(){

    setTimeout(() => {

      let administratorsIds = _.map(this.state.administratorsList, 'id')
      let collaboratorsIds = _.map(this.state.collaboratorsList, 'id')

      console.log(administratorsIds, collaboratorsIds);

      this.setState({ administratorsIds })
      this.setState({ collaboratorsIds })

    }, 300)

  }

  saveChanges(modifiedTicketDetail){

    let payload = {
      administrators: modifiedTicketDetail.administrators,
      client: this.props.ticketClientId,
      collaborators: modifiedTicketDetail.collaborators,
      priority: modifiedTicketDetail.priority,
      ticket_type: modifiedTicketDetail.ticket_type,
      status: modifiedTicketDetail.status
    }

    console.log(payload);

    $.ajax({
      url: `/api/projects/${this.props.pslug}/support/tickets/${this.props.ticketSlug}/`,
      method: 'PUT',
      data: JSON.stringify(payload),
      headers: {
        'X-CSRFToken': getCsrfToken('csrftoken'),
        'Content-Type' : 'application/json'
      }
    })

    .done((data) => {
      console.log(data);
    })
    .fail((error)=> console.log(error))

  }

  checkValidForm(e){
    console.log(e.target.id);
    if(e.target.value === '') return this.setState({ [`${e.target.id}IsValid`] : false})

    return this.setState({ [`${e.target.id}IsValid`] : true})
  }

  updateNameFile(e){
    this.setState({selectedFileName: this.$newTicketAttachedFile[0].files[0].name})
  }

  saveTicket(e){

    e.preventDefault()

    let isValid = this.state.ticketTitleIsValid && this.state.ticketDescriptionIsValid

    if(isValid){
      console.log('Es válido');
      this.setState({ticketTitleIsValid: true})
      this.setState({ticketDescriptionIsValid: true})

      var formData = new FormData($('#newTicketForm'))

      this.state.administratorsIds.length > 0 ? formData.append('administrators', this.state.administratorsIds) : ''
      formData.append('attached_file', this.$newTicketAttachedFile[0].files[0])
      formData.append('client', this.state.selectedClient.id)
      this.state.collaboratorsIds.length > 0 ? formData.append('collaborators', this.state.collaboratorsIds) : ''
      formData.append('description', $('#newTicketDescription').val())
      formData.append('priority', this.state.ticketDetailPriority)
      formData.append('ticket_type', this.state.ticketDetailType)
      formData.append('status', this.state.ticketDetailStatus)
      formData.append('title', $('#newTicketTitle').val())
      formData.append('unity', this.state.clientUnits[0].id)

      $.ajax({
        url: `/api/projects/${this.pslug}/support/tickets/`,
        method: 'PUT',
        data: formData,
        headers: {
          'X-CSRFToken': getCsrfToken('csrftoken')
        },
        processData: false,
        contentType: false
      })
      .done((data) => {
        console.log(data)

        this.props.router.push(`/support/ticket/${data.slug}`)

      })
      .fail((error)=> console.log(error))

    }

  }

  goBack(){

  }

  render(){
    return (
      <section className="NewTicket">
        <div className="NewTicket__header">

        </div>

        <div className="NewTicket__body">
          <IconArrowBack router={this.props.router} />
          <h3>Crea un nuevo ticket</h3>

          <div className="NewTicket__form">
            <form id="newTicketForm">


              <NewTicketApplicantDetails
                showChip={this.state.showChip}
                selectedClientState={this.state.selectedClient}
                foundClients={this.state.foundClients}
                clientUnits={this.state.clientUnits}
                hideChip={this.hideChip}
                selectedClient={this.selectedClient}
                searchClient={this.searchClient}
              />

              <NewTicketDescription
                ticketTitleIsValid={this.state.ticketTitleIsValid}
                ticketDescriptionIsValid={this.state.ticketDescriptionIsValid}
                selectedFileName={this.state.selectedFileName}
                checkValidForm={this.checkValidForm}
                updateNameFile={this.updateNameFile}
              />

              <NewTicketTicketDetails updateTicketDetails={this.updateTicketDetails}/>

              <NewTicketTicketActors
                actorsFind={this.state.actorsFind}
                administratorsList={this.state.administratorsList}
                collaboratorsList={this.state.collaboratorsList}
                searchAdministrators={this.state.administratorsList}
                addActor={this.addActor}
                removeActor={this.removeActor}
                searchAdministrators={this.searchAdministrators}
              />


              <div className="NewTicket__saveTicket">
                <button onClick={this.saveTicket}>GUARDAR TICKET</button>
              </div>

            </form>
          </div>
        </div>

      </section>
    )
  }
}
