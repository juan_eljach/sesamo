import React, {Component} from 'react'


export default class LandingSupport extends Component{

  constructor(props){
    super(props)
  }

  componentDidMount(){
  }

  render(){
    return (
      <section className="LandingSupport">

        <header className="LandingSupport__header">
          <div className="LandingSupport__headerWrap">
            <div className="LandingSupport__headerLogo">Logo</div>
            <div className="LandingSupport__headerNav">
              <nav>
                <ul>
                  <li><a href="">Inicio</a></li>
                  <li><a href="">Preguntas frecuentes</a></li>
                </ul>
              </nav>
            </div>
          </div>
        </header>

        <div className="LandingSupport__above">
          <div className="LandingSupport__aboveWrap">
            <h1>Soporte Sesamo</h1>
            <p>Empieza por escribir el tipo de tu necesidad</p>
            <div className="LandingSupport__aboveSearch">
              <input type="text" placeholder="Garantía"/>
              <button>BUSCAR</button>
            </div>
          </div>
        </div>

        <div className="LandingSupport__faq">
          <h2>Preguntas frecuentes</h2>
          <div className="LandingSupport__faqFilter">
            <label htmlFor="">Filtrar por: </label>
            <select name="" id="">
              <option value="">Todas</option>
            </select>
          </div>
          <div className="LandingSupport__faqContent">
            <div className="LandingSupport__faqContentItem">
              <h4>¿La garantía cubre cuanto tiempo?</h4>
              <p><span>Respuesta:</span> Con pcg encontrarás una aliada amiga que durante un año va a recibir tus problemas e intentamos solucionarlos en un tiempo muy corto</p>
            </div>
            <div className="LandingSupport__faqContentItem">
              <h4>¿La garantía cubre cuanto tiempo?</h4>
              <p><span>Respuesta:</span> Con pcg encontrarás una aliada amiga que durante un año va a recibir tus problemas e intentamos solucionarlos en un tiempo muy corto</p>
            </div>
            <div className="LandingSupport__faqContentItem">
              <h4>¿La garantía cubre cuanto tiempo?</h4>
              <p><span>Respuesta:</span> Con pcg encontrarás una aliada amiga que durante un año va a recibir tus problemas e intentamos solucionarlos en un tiempo muy corto</p>
            </div>
          </div>

          <h4> Ver más preguntas frecuentes </h4>

          <div className="LandingSupport__otherRequest">
            <h4> Tienes otra solicitud ¿Cuál es el problema?</h4>
            <p>Llena este formulario para que tu petición quede en nuestras manos y en un corto tiempo te responderemos por correo</p>

            <div className="LandingSupport__otherRequestInput">
              <input type="text" placeholder="C.C. Digita la cédula del propietario"/>
              <button>BUSCAR</button>
            </div>

            <form className="LandingSupport__otherRequestForm">
              <h4>Completa el formulario</h4>
              <fieldset>
                <div className="LandingSupport__otherRequestFormGroup">
                  <label htmlFor="">Propietaria:</label>
                  <input type="text" placeholder="Ligia Maria Trinidad"/>
                </div>
                <div className="LandingSupport__otherRequestFormGroup">
                  <label htmlFor="">Apartamento:</label>
                  <select name="ticket_unity" id="">
                    <option value="t1-201">T1 204</option>
                  </select>
                </div>

                <div className="LandingSupport__otherRequestFormGroup">
                  <label htmlFor="">Tipo de solicitud:</label>
                  <select name="ticket_type" id="">
                    <option value="">Seleccione el tipo de solicitud</option>
                  </select>
                </div>

                <div className="LandingSupport__otherRequestFormGroup">
                  <label htmlFor="">Prioridad:</label>
                  <select name="ticket_type" id="">
                    <option value="">Normal</option>
                  </select>
                </div>

                <div className="LandingSupport__otherRequestFormGroup">
                  <label htmlFor="">Título:</label>
                  <input type="text" placeholder="Agrega un título a tu problema o solicitud"/>
                </div>

                <div className="LandingSupport__otherRequestFormGroup LandingSupport__description">
                  <label htmlFor="">Descripción:</label>
                  <textarea name="" id="" cols="30" rows="10" placeholder="agregar una descripción de tu problema"></textarea>
                  <div className="LandingSupport__otherRequestFormFile">
                    <input type="file"/>
                  </div>
                </div>
                <div className="LandingSupport__otherRequestFormButton">
                  <button>ENVIAR</button>
                </div>
              </fieldset>
            </form>
          </div>

        </div>

        <footer className="LandingSupport__footer">
          <div className="LandingSupport__footerWrap">
            <span>Logo</span>
            <div>© Sesamo.com 2017. Estamos  para organizar tu constructora.</div>
          </div>
        </footer>

      </section>
    )
  }
}
