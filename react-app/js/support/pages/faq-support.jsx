import React, { Component } from 'react'


export default class FaqSupport extends Component{

  constructor(props){
    super(props)


  }

  componentDidMount(){


  }

  render(){
    return (
      <section className="FaqSupport">

        <header className="FaqSupport__header">
          <div className="FaqSupport__headerWrap">
            <div className="FaqSupport__headerLogo">Logo</div>
            <div className="FaqSupport__headerNav">
              <nav>
                <ul>
                  <li><a href="">Inicio</a></li>
                  <li><a href="">Preguntas frecuentes</a></li>
                </ul>
              </nav>
            </div>
          </div>
        </header>


        <div className="FaqSupport__body">
          <h2><span>?</span> Preguntas frecuentes</h2>

          <div className="FaqSupport__contentFaqs">

            <div className="FaqSupport__filter">
              <label htmlFor="">Filtrar por:</label>
              <select name="" id="">
                <option value="">Todas</option>
              </select>
            </div>

            <div className="FaqSupport__contentItem">
              <h4>1. ¿La garantía cubre cuanto tiempo?</h4>
              <p><span>Respuesta:</span> Con pcg encontrarás una aliada amiga que durante un año va a recibir tus problemas e intentamos solucionarlos en un tiempo muy corto</p>
            </div>

            <div className="FaqSupport__contentItem">
              <h4>2. ¿La garantía cubre cuanto tiempo?</h4>
              <p><span>Respuesta:</span> Con pcg encontrarás una aliada amiga que durante un año va a recibir tus problemas e intentamos solucionarlos en un tiempo muy corto</p>
            </div>

            <div className="FaqSupport__contentItem">
              <h4>3. ¿La garantía cubre cuanto tiempo?</h4>
              <p><span>Respuesta:</span> Con pcg encontrarás una aliada amiga que durante un año va a recibir tus problemas e intentamos solucionarlos en un tiempo muy corto</p>
            </div>

            <div className="FaqSupport__contentItem">
              <h4>4. ¿La garantía cubre cuanto tiempo?</h4>
              <p><span>Respuesta:</span> Con pcg encontrarás una aliada amiga que durante un año va a recibir tus problemas e intentamos solucionarlos en un tiempo muy corto</p>
            </div>

          </div>

        </div>


        <footer className="FaqSupport__footer">
          <div className="FaqSupport__footerWrap">
            <span>Logo</span>
            <div>© Sesamo.com 2017. Estamos  para organizar tu constructora.</div>
          </div>
        </footer>

      </section>
    )
  }
}
