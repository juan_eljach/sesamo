import React, { Component } from 'react'
import Checkbox from 'material-ui/Checkbox'

import { IconArrowDown } from './svg/_svg-icons'

export default class NewPaymentSummary extends Component {

  constructor (props) {
    super(props)
  }

  render () {
    return (
      <div className="NewPayment__summary">
        <div className="NewPayment__summaryIcon"><div>$</div></div>
        <div className="NewPayment__paymentSummary">
          <h3>RESUMEN DE PAGO</h3>
          <div className="NewPayment__campaigns">
            <select name="" id="" onChange={ (e)=> this.props.selectCampaign(e) }>
              <option value="">Selecciona una campaña</option>
              {
                this.props.campaigns.map((campaign, k)=>{
                  return <option value={campaign.name} key={k}>{ campaign.name }</option>
                })
              }              
            </select>            
          </div>
          <div className="NewPayment__paymentSummaryCont">
            <div>
              <span>$</span>
              <label htmlFor="">Valor del apto</label>
              <input type="text" placeholder={ this.props.aptoSelected.price ? this.props.aptoSelected.price.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.") : '' } disabled="true"/>
            </div>
            <div>
              <span>$</span>
              <label htmlFor="">Cuota inicial</label>
              <input type="text" placeholder={ this.props.initialFee.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.") } disabled="true"/>
            </div>
            <div>
              <span>$</span>
              <label htmlFor="">% de cuota inicial</label>
              <input type="text" placeholder={ this.props.initialFeePercentage ? this.props.initialFeePercentage : ''} disabled="true"/>
            </div>
            <div>
              <span>$</span>
              <label htmlFor="">Separación</label>
              <input type="text" placeholder={this.props.booking_price ? this.props.booking_price.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.") : '' } disabled="true"/>
            </div>
            <div>
              <span>$</span>
              <label htmlFor="">Crédito a solicitar</label>
              <input type="text" placeholder={this.props.creditToApply.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")} disabled="true"/>
            </div>
          </div>
        </div>

        <div className="NewPayment__othersPayment">
          <h3>OTROS PAGOS ACORDADOS</h3>
          <div className="NewPayment__applyCI">
            <Checkbox label='Marca el cuadro para aplicar a Cuota Inicial' defaultChecked={true} disabled={true}/>
          </div>

          <div className="NewPayment__othersPaymentCont">
            <div>
              <label htmlFor="">Ahorro programado</label>
              <div className="NewPayment__othersPaymentLabel">
                <Checkbox onCheck={ (e)=>  this.props.othersPaymentHandler('scheduledSavingsApply',  !this.props.scheduledSavingsApply)  }/>
                <input id="scheduledSavings" className="othersPaymentsInputs" name="ahorro-programado" type="text" onChange={ (e)=> this.props.updateOtherPaymentsValue(e, 'scheduledSavings') }/>
              </div>

            </div>
            <div>
              <label htmlFor="">Cesantías</label>
              <div className="NewPayment__othersPaymentLabel">
                <Checkbox onCheck={ (e)=> this.props.othersPaymentHandler('severanceApply', !this.props.severanceApply) }/>
                <input id="severance" className="othersPaymentsInputs" name="cesantias" type="text" onChange={ (e)=> this.props.updateOtherPaymentsValue(e, 'severance') }/>
              </div>


            </div>
            <div>
              <label htmlFor="">Subsidio de vivienda</label>
              <div className="NewPayment__othersPaymentLabel">
                <Checkbox onCheck={ (e)=> this.props.othersPaymentHandler('housingAllowanceApply', !this.props.housingAllowanceApply)  }/>
                <input id="housingAllowance" className="othersPaymentsInputs" name="subsidio-vivienda" type="text" onChange={ (e)=> this.props.updateOtherPaymentsValue(e, 'housingAllowance')}/>

              </div>


            </div>
            <div>
              <label htmlFor="">Otros</label>
              <div className="NewPayment__othersPaymentLabel">
                <Checkbox onCheck={ (e)=> this.props.othersPaymentHandler('othersPaymentsApply', !this.props.othersPaymentsApply) }/>
                <input id="othersPayments" className="othersPaymentsInputs" name="otro-pago-acordado" type="text" onChange={ (e)=> this.props.updateOtherPaymentsValue(e, 'othersPayments') }/>

              </div>
            </div>
          </div>

        </div>
      </div>
    )
  }
}


