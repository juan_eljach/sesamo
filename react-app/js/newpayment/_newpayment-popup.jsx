'use strict'

import React, {Component} from 'react'
import moment from 'moment'
import getCookie from '../lib/csrftoken'
import { IconSearch } from '../payments/svg/_svg-icons'

export default class NewPaymentPopup extends Component {
  constructor(props){
    super(props)

    this.URL_GET = `/api/projects/${this.props.pslug}/persons?search=`
    this.URL_POST = `/api/projects/${this.props.pslug}/formalizations/person/`
    this.dataForm = {}
    this.clientIdSelected = 0
    this.state = {
      first_name: true,
      last_name: true,
      identity: true,
      expedition_place: true,
      email: true,
      birthday: true,
      cellphone: true,
      phone: true,
      notification_phone: true,
      notification_address: true,
      persons: [],
      slugPerson: '',
      assignedTo: 0,
      project: 0,
      tabSelected : 1
    }
  }

  componentDidMount(){
    this.$allInputs = $('.NewPaymentPopup__formBody div input')
    this.$first_name = $('#first_name')
    this.$last_name = $('#last_name')
    this.$identity = $('#identity')
    this.$expedition_place = $('#expedition_place')
    this.$email = $('#email')
    this.$birthday = $('#birthday')
    this.$cellphone = $('#cellphone')
    this.$phone = $('#phone')
    this.$notification_phone = $('#notification_phone')
    this.$notification_address = $('#notification_address')
    this.$formSearch = $('#formSearch')
    this.$formResults = $('.NewPaymentPopup__formResults')
    this.$NewPaymentPopupSearch = $('#NewPaymentPopupSearch')
    this.$searchPersonTab = $('#searchPersonTab')
    this.$addPersonTab = $('#addPersonTab')

    this.$searchPersonTab.addClass('tabSelected')
  }

  inputUpdate(e, inp){
    if(e.target.value == '') this.setState({[inp]: false})
    else{
      this.setState({[inp]: true})
      this.dataForm[inp] = e.target.value
    }
  }

  searchPerson(e){

    if(e.target.value === '') this.$formResults.fadeOut(100)

    else{
      this.$formResults.css({display: 'block'})
      this.$formResults.fadeIn(100)
      $.ajax({
        method: 'GET',
        url: this.URL_GET + e.target.value
      })
      .done((data) => {
        this.setState({persons: data})
        console.log(data)
      })
      .fail((error)=> console.log(error))
    }

  }

  selectPerson(e, p){
    this.$first_name.val(p.first_name)
    this.$last_name.val(p.last_name)
    this.$identity.val(p.identity)
    this.$expedition_place.val(p.expedition_place)
    this.$email.val(p.email)
    this.$birthday.val(p.birthday)
    this.$cellphone.val(p.cellphone)
    this.$phone.val(p.phone)
    this.$notification_phone.val(p.notification_phone)
    this.$notification_address.val(p.notification_address)

    this.setState({slugPerson: p.slug})
    this.setState({assignedTo: p.assigned_to})
    this.setState({project: p.project})

    this.$formResults.fadeOut(100)
    this.$NewPaymentPopupSearch.val('')
  }

  selectTab(tab){

    _.forEach(this.$allInputs, (el)=> el.value = '')

    if(tab === 1){
      this.$formSearch.fadeIn(200)
      this.setState({tabSelected : 1})
      this.$addPersonTab.removeClass('tabSelected')
      this.$searchPersonTab.addClass('tabSelected')
    }
    else if(tab === 2){
      this.$formSearch.fadeOut(200)
      this.setState({tabSelected : 2})
      this.$searchPersonTab.removeClass('tabSelected')
      this.$addPersonTab.addClass('tabSelected')
    }
  }

  inputDate(e, a){
    $('#birthday').val(moment(a).format('YYYY-MM-DD'))
  }

  savedClient(){

    _.forEach(this.$allInputs, (el) => {
      if(el.value === ''){
        this.setState({[el.name]: false})
      }
    })

    let isFillAll = _.every(this.$allInputs, (el) => {
      return el.value !== ''
    })

    if(isFillAll){

      let inputValues = {
        first_name: this.$first_name.val(),
        last_name: this.$last_name.val(),
        identity: this.$identity.val(),
        expedition_place: this.$expedition_place.val(),
        email: this.$email.val(),
        birthday: this.$birthday.val(),
        cellphone: this.$cellphone.val(),
        phone: this.$phone.val(),
        notification_phone: this.$notification_phone.val(),
        notification_address: this.$notification_address.val(),
        slug: this.state.slugPerson,
        assigned_to: this.state.assignedTo,
        is_client: true,
        project: this.state.project
      }

      if(this.state.tabSelected === 1){
        $.ajax({
          method: 'PUT',
          url: this.URL_POST + this.state.slugPerson + '/small-update/',
          headers: {
            'X-CSRFToken': getCookie('csrftoken')
          },
          data: inputValues
        })
        .done((data) => {
          console.log(data)
          this.props.addClient(data)
          _.forEach(this.$allInputs, (el)=> el.value = '')
        })
        .fail((error)=> console.log(error))
      }

      else if(this.state.tabSelected === 2){

        $.ajax({
          method: 'GET',
          url: `/api/projects/${this.props.pslug}/info/`
        })
        .done((data)=>{
          console.log(data)
          if(data.id){
            inputValues.project = data.id
            inputValues.assigned_to = ''
            $.ajax({
              method: 'POST',
              url: this.URL_POST + 'create/',
              headers: {
                'X-CSRFToken': getCookie('csrftoken')
              },
              data: inputValues
            })
            .done((data) => {
              this.props.addClient(data)
              _.forEach(this.$allInputs, (el)=> el.value = '')
            })
            .fail((error)=> console.log(error))
          }

        })
        .fail((error)=> console.log(error))

      }

      $('#NewPaymentPopup').fadeOut(200)
    }

  }

  closePopup(){
    $('#NewPaymentPopup').fadeOut(200)
  }

  render () {
    return (
      <div className="NewPaymentPopup" id="NewPaymentPopup">
        <div className="NewPaymentPopup__form">
          <div className="NewPaymentPopup__formHead">
            <h2>AGREGAR CLIENTE</h2>
            <span id="closePopup" onClick={this.closePopup}>x</span>
          </div>
          <div className="NewPaymentPopup__formTabs">
            <div id="searchPersonTab" onClick={ ()=> this.selectTab(1) } >Busca una persona existente</div>
            <div id="addPersonTab" onClick={ ()=> this.selectTab(2) } >Agrega una persona nueva</div>
          </div>
          <div className="NewPaymentPopup__formSearch" id="formSearch">
            <IconSearch />
            <input id="NewPaymentPopupSearch" type="text" placeholder="Busca una persona existente" onChange={(e)=> this.searchPerson(e)} />
            <div className="NewPaymentPopup__formResults">
              { this.state.persons.map((p, i)=>{
                  return <div key={i} onClick={ (e)=> this.selectPerson(e, p) }>{ `${p.first_name} ${p.last_name}` }</div>
                })
              }
            </div>
          </div>
          <div className="NewPaymentPopup__formBody">
            <div>
              <label htmlFor="">Nombres*</label>
              <input id="first_name" type="text" name="first_name" onChange={(e)=> this.inputUpdate(e, 'first_name') } className={ this.state.first_name ? "isValid" : "isNotValid" }/>
            </div>
            <div>
              <label htmlFor="">Apellidos*</label>
              <input id="last_name" type="text" name="last_name" onChange={(e)=> this.inputUpdate(e, 'last_name') } className={ this.state.last_name ? "isValid" : "isNotValid" }/>
            </div>
            <div>
              <label htmlFor="">Cédula*</label>
              <input id="identity" type="text" name="identity" onChange={(e)=> this.inputUpdate(e, 'identity') } className={ this.state.identity ? "isValid" : "isNotValid" } />
            </div>
            <div>
              <label htmlFor="">Lugar de expedición*</label>
              <input id="expedition_place" type="text" name="expedition_place" onChange={(e)=> this.inputUpdate(e, 'expedition_place') } className={ this.state.expedition_place ? "isValid" : "isNotValid" }/>
            </div>
            <div>
              <label htmlFor="">Correo electrónico*</label>
              <input id="email" type="text" name="email" onChange={(e)=> this.inputUpdate(e, 'email') } className={ this.state.email ? "isValid" : "isNotValid" }/>
            </div>
            <div>
              <label htmlFor="">Fecha de Nacimiento*</label>
              {/* <DatePicker onChange={ this.inputDate } /> */}
              <input id="birthday" type="text" name="birthday" data-toggle="datepicker" onChange={(e)=> this.inputUpdate(e, 'birthday') } className={ this.state.birthday ? "isValid" : "isNotValid" }/>
            </div>
            <div>
              <label htmlFor="">Celular*</label>
              <input id="cellphone" type="text" name="cellphone" onChange={(e)=> this.inputUpdate(e, 'cellphone') } className={ this.state.cellphone ? "isValid" : "isNotValid" }/>
            </div>
            <div>
              <label htmlFor="">Teléfono*</label>
              <input id="phone" type="text" name="phone" onChange={(e)=> this.inputUpdate(e, 'phone') } className={ this.state.phone ? "isValid" : "isNotValid" }/>
            </div>
            <div>
              <label htmlFor="">Teléfono de notificación*</label>
              <input id="notification_phone" type="text" name="notification_phone" onChange={(e)=> this.inputUpdate(e, 'notification_phone') } className={ this.state.notification_phone ? "isValid" : "isNotValid" }/>
            </div>
            <div>
              <label htmlFor="">Dirección de notificación*</label>
              <input id="notification_address" type="text" name="notification_address" onChange={(e)=> this.inputUpdate(e, 'notification_address') } className={ this.state.notification_address ? "isValid" : "isNotValid" }/>
            </div>
            <div className="NewPaymentPopup__button">
              <button onClick={this.savedClient.bind(this)}>GUARDAR</button>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
