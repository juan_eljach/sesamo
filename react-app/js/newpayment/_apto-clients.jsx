import React, {Component} from 'react'
import {IconEdit} from './svg/_svg-icons.jsx'

export default class AptoClients extends Component{
  constructor(props){
    super(props)
  }

  openPopup(){
    $('#NewPaymentPopup').fadeIn(200)
    $('#NewPaymentPopup').css({display:'flex'})
  }

  openPopupEdit(client){
    console.log('open popup editedaaaaaa')
    $('#NewPaymentPopupEdit').fadeIn(200)
    $('#NewPaymentPopupEdit').css({display:'flex'})

    $('#first_name_edit').val(client.first_name)
    $('#last_name_edit').val(client.last_name)
    $('#identity_edit').val(client.identity)
    $('#expedition_place_edit').val(client.expedition_place)
    $('#email_edit').val(client.email)
    $('#birthday_edit').val(client.birthday)
    $('#cellphone_edit').val(client.cellphone)
    $('#phone_edit').val(client.phone)
    $('#notification_phone_edit').val(client.notification_phone)
    $('#notification_address_edit').val(client.notification_address)

    this.props.clientSelectedEditInfo({assigned_to: client.assigned_to,
      slug: client.slug, project: client.project, is_client: client.is_client })

  }

  render(){
    return <div className="NewPayment__aptoClients">
            <label htmlFor="" className="NewPayment__clientsLabel">Clientes de la Unidad</label>
      { this.props.clients.map((el, i)=>{
          return <div className="NewPayment__client" key={i}><span>{i + 1}</span> {el.first_name} {el.last_name}  <IconEdit openPopupEdit={this.openPopupEdit.bind(this, el)}/> <span onClick={()=>this.props.removeClient(el.id)}>x</span></div>
        })
      }

      <div className="NewPayment__addClient" onClick={this.openPopup}>Agregar nuevo cliente +</div>
    </div>
  }
}
