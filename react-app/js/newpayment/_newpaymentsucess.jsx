import React, {Component} from 'react'
import {Link} from 'react-router'
import {IconDownload} from './svg/_svg-icons'

export default class NewPaymentSucess extends Component{
  constructor(props){
    super(props)
  }

  render(){
    return <section className="NewPaymentSucess">
      <div className="NewPaymentSucess__container">
        <div className="NewPaymentSucess__head">
          <svg width="65px" height="65px" viewBox="212 257 52 52" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
              <desc>Created with Sketch.</desc>
              <defs></defs>
              <g id="success-2" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd" transform="translate(212.000000, 257.000000)">
                  <g id="Capa_1" fill="#81C784">
                      <g id="Group">
                          <path d="M26,0 C11.664,0 0,11.663 0,26 C0,40.337 11.664,52 26,52 C40.336,52 52,40.337 52,26 C52,11.663 40.336,0 26,0 Z M26,50 C12.767,50 2,39.233 2,26 C2,12.767 12.767,2 26,2 C39.233,2 50,12.767 50,26 C50,39.233 39.233,50 26,50 Z" id="Shape"></path>
                          <path d="M38.252,15.336 L22.883,32.626 L13.624,25.219 C13.194,24.874 12.563,24.945 12.219,25.375 C11.874,25.807 11.944,26.436 12.375,26.781 L22.375,34.781 C22.559,34.928 22.78,35 23,35 C23.276,35 23.551,34.886 23.748,34.664 L39.748,16.664 C40.115,16.252 40.078,15.619 39.665,15.253 C39.251,14.885 38.62,14.922 38.252,15.336 Z" id="Shape"></path>
                      </g>
                  </g>
              </g>
          </svg>
          <p>El plan de pago se ha guardado con éxito</p>
          <span>Unidad: { this.props.params.unity.split('-').join(' ').toUpperCase() }</span>
        </div>

        <div className="NewPaymentSucess__body">
          <p>¿Qué documento deseas generar?</p>
          <div>
            <button><IconDownload/>CONTRATO DE COMPRAVENTA</button>
            <button><IconDownload/>CARTA DE INSTRUCCIONES</button>
          </div>
        </div>
      </div>
    </section>
  }
}
