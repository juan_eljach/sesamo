import React, {Component} from 'react'

export default class Test extends Component{

  constructor(props){
    super(props)
  }

  componentDidMount(){

    this.$NewpaymentDocument = $('#NewpaymentDocument')

    $.ajax({
      url: 'http://127.0.0.1:8000/api/templates/1/'
    })
    .done((data) => {
      $('.NewpaymentDocument__document').html(data.html_snippet)
    })
    .fail((error)=> console.log(error))
  }

  activeEdit(){
    this.$NewpaymentDocument.attr('contenteditable', '').focus()
  }

  sendHtml(){

    console.log(this.$NewpaymentDocument[0].outerHTML)

    // $.ajax({
    //   url: 'http://192.168.1.27:8000/api/templates/11/',
    //   method: 'POST'
    // })
    // .done((data) => {
    //   console.log(data)
    // })
    // .fail((error)=> console.log(error))
  }

  render(){
    return (
      <section className="NewpaymentDocument">

        <div className="NewpaymentDocument__header">
          <div>
            <span onClick={this.activeEdit.bind(this)}>Editar</span>
            <span onClick={this.sendHtml.bind(this)}>Guardar</span>
          </div>
        </div>

        <div className="NewpaymentDocument__document" id="NewpaymentDocument">

        </div>

      </section>
    )
  }
}
