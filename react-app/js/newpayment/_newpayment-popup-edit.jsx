'use strict'

import React, {Component} from 'react'
import moment from 'moment'

export default class NewPaymentPopupEdit extends Component {

  constructor(props){
    super(props)

    this.URL_GET = `/api/projects/${this.props.pslug}/persons?search=`
    this.URL_POST = `/api/projects/${this.props.pslug}/formalizations/person/`
    this.dataForm = {}

    this.state = {
      first_name: true,
      last_name: true,
      identity: true,
      expedition_place: true,
      email: true,
      birthday: true,
      cellphone: true,
      phone: true,
      notification_phone: true,
      notification_address: true,
      persons: [],
      slugPerson: '',
      assignedTo: 0,
      project: 0,
      tabSelected : 1
    }
  }

  componentDidMount(){

  }

  inputUpdate(e, inp){
    if(e.target.value == '') this.setState({[inp]: false})
    else{
      this.setState({[inp]: true})
      this.dataForm[inp] = e.target.value
    }
  }

  closePopup(){
    $('#NewPaymentPopupEdit').fadeOut(200)
  }

  getCookie(name){
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
  }

  updateClient(){

    let $allInputsEdit = $('.NewPaymentPopupEdit__formBody div input')

    let isFillAll = _.every($allInputsEdit, (el) => {
      return el.value !== ''
    })

    if(isFillAll){

      let inputValues = {
        first_name: $('#first_name_edit').val(),
        last_name: $('#last_name_edit').val(),
        identity: $('#identity_edit').val(),
        expedition_place: $('#expedition_place_edit').val(),
        email: $('#email_edit').val(),
        birthday: $('#birthday_edit').val(),
        cellphone: $('#cellphone_edit').val(),
        phone: $('#phone_edit').val(),
        notification_phone: $('#notification_phone_edit').val(),
        notification_address: $('#notification_address_edit').val(),
        slug: this.props.clientSelectedEditInfo.slug,
        assigned_to: this.props.clientSelectedEditInfo.assigned_to,
        is_client: this.props.clientSelectedEditInfo.is_client,
        project: this.props.clientSelectedEditInfo.project
      }

      console.log(inputValues)

      $.ajax({
        method: 'PUT',
        url: this.URL_POST + this.props.clientSelectedEditInfo.slug + '/small-update/',
        headers: {
          'X-CSRFToken': this.getCookie('csrftoken')
        },
        data: inputValues
      })
      .done((data) => {
        this.props.updateClients(data)
        $('#NewPaymentPopupEdit').fadeOut(200)
      })

      .fail((error)=> console.log(error))

    }

  }

  render () {
    return (
      <div className="NewPaymentPopup" id="NewPaymentPopupEdit">
        <div className="NewPaymentPopup__form">
          <div className="NewPaymentPopup__formHead">
            <h2>EDITAR CLIENTE</h2>
            <span id="closePopup" onClick={this.closePopup}>x</span>
          </div>

          <div className="NewPaymentPopupEdit__formBody">
              <div>
                <label htmlFor="">Nombres*</label>
                <input id="first_name_edit" type="text" name="first_name" onChange={(e)=> this.inputUpdate(e, 'first_name') } className={ this.state.first_name ? "isValid" : "isNotValid" }/>
              </div>
              <div>
                <label htmlFor="">Apellidos*</label>
                <input id="last_name_edit" type="text" name="last_name" onChange={(e)=> this.inputUpdate(e, 'last_name') } className={ this.state.last_name ? "isValid" : "isNotValid" }/>
              </div>
              <div>
                <label htmlFor="">Cédula*</label>
                <input id="identity_edit" type="text" name="identity" onChange={(e)=> this.inputUpdate(e, 'identity') } className={ this.state.identity ? "isValid" : "isNotValid" } />
              </div>
              <div>
                <label htmlFor="">Lugar de expedición*</label>
                <input id="expedition_place_edit" type="text" name="expedition_place" onChange={(e)=> this.inputUpdate(e, 'expedition_place') } className={ this.state.expedition_place ? "isValid" : "isNotValid" }/>
              </div>
              <div>
                <label htmlFor="">Correo electrónico*</label>
                <input id="email_edit" type="text" name="email" onChange={(e)=> this.inputUpdate(e, 'email') } className={ this.state.email ? "isValid" : "isNotValid" }/>
              </div>
              <div>
                <label htmlFor="">Fecha de Nacimiento*</label>
                {/* <DatePicker onChange={ this.inputDate } /> */}
                <input id="birthday_edit" type="text" name="birthday" data-toggle="datepicker" onChange={(e)=> this.inputUpdate(e, 'birthday') } className={ this.state.birthday ? "isValid" : "isNotValid" }/>
              </div>
              <div>
                <label htmlFor="">Celular*</label>
                <input id="cellphone_edit" type="text" name="cellphone" onChange={(e)=> this.inputUpdate(e, 'cellphone') } className={ this.state.cellphone ? "isValid" : "isNotValid" }/>
              </div>
              <div>
                <label htmlFor="">Teléfono*</label>
                <input id="phone_edit" type="text" name="phone" onChange={(e)=> this.inputUpdate(e, 'phone') } className={ this.state.phone ? "isValid" : "isNotValid" }/>
              </div>
              <div>
                <label htmlFor="">Teléfono de notificación*</label>
                <input id="notification_phone_edit" type="text" name="notification_phone" onChange={(e)=> this.inputUpdate(e, 'notification_phone') } className={ this.state.notification_phone ? "isValid" : "isNotValid" }/>
              </div>
              <div>
                <label htmlFor="">Dirección de notificación*</label>
                <input id="notification_address_edit" type="text" name="notification_address" onChange={(e)=> this.inputUpdate(e, 'notification_address') } className={ this.state.notification_address ? "isValid" : "isNotValid" }/>
              </div>


            <div className="NewPaymentPopup__button">
              <button onClick={this.updateClient.bind(this)}>GUARDAR</button>
            </div>
          </div>

        </div>
      </div>
    )
  }
}
