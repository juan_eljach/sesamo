import React, {Component} from 'react'
import NewPaymentPopup from './_newpayment-popup'
import NewPaymentPopupEdit from './_newpayment-popup-edit'
import NewPaymentBasicInfo from './_newpayment-basicInfo'
import NewPaymentSummary from './_newpayment-summary'
import NewPaymentPlan from './_newpayment-plan'
import moment from 'moment'
import { IconNewPayment } from './svg/_svg-icons'
import getCookie from '../lib/csrftoken'

export default class NewPayment extends Component {
  constructor(props){
    super(props)
    this.pslug = window.location.href.split('/')[4]
    this.addedClients = []
    this.finance = new Finance()
    this.NPV = 0
    this.state = {
      clients: [],
      aptos:[],
      campaigns: [],
      aptoSelected: {},
      initialFee: 0,
      initialFeePercentage: 0,
      unityCurrentPrice: 0,
      booking_price: 0,
      scheduledSavings: 0,
      severance: 0,
      housingAllowance: 0,
      othersPayments: 0,
      creditToApply: 0,
      remainingMonths: 0,
      scheduledSavingsApply: false,
      severanceApply: false,
      housingAllowanceApply: false,
      othersPaymentsApply: false,
      feePrice : 0,
      clientSelectedEditInfo: {},
      calculateMonthlyFees: 0,
      calculateMonthlyExtraFees: 0,
      isCalculateMonthlyExtraFees: false,
      firstDateFee: '',
      remainingDates: [],
      extraordinaryFeeSum: 0,
      NPV2: 0,
      paymentPlanValid: false
    }

    this.closeApto = this.closeApto.bind(this)
    this.removeClient = this.removeClient.bind(this)
    this.searchApto = this.searchApto.bind(this)
    this.clientSelectedEditInfo = this.clientSelectedEditInfo.bind(this)
    this.selectCampaign = this.selectCampaign.bind(this)
    this.updateOtherPaymentsValue = this.updateOtherPaymentsValue.bind(this)
    this.CalculateMonthlyFees = this.CalculateMonthlyFees.bind(this)
    this.othersPaymentHandler = this.othersPaymentHandler.bind(this)
    this.selectApto = this.selectApto.bind(this)
    this.firstDateErrorClose = this.firstDateErrorClose.bind(this)
    this.extraordinaryFeeUpdate = this.extraordinaryFeeUpdate.bind(this)
    this.validationPopupClose = this.validationPopupClose.bind(this)
    this.saveNewPaymentPlan = this.saveNewPaymentPlan.bind(this)
    this.validateNewpaymentPlan = this.validateNewpaymentPlan.bind(this)
  }

  componentDidMount(){
    $('#birthday').datepicker({
      format: 'yyyy-mm-dd',
      autoHide: true
    })

    $('#planTableFirstDate').datepicker({
      format: 'yyyy-mm-dd',
      autoHide: true,
      startDate: new Date()
    })

    $('#planTableFirstDate').on('pick.datepicker', ()=>{
      $('#planTableFirstDate').css({border:'1px solid #e5e5e5'})
      $('#firstDateError').css({display:'none'})
    })

    $.ajax({
      method: 'GET',
      url: `/api/projects/${this.pslug}/formalizations/campaigns/`
    })
    .done((data)=> this.setState({campaigns: data}))
    .fail((error)=> console.log(error))

    $.ajax({
      method: 'GET',
      url: `/api/projects/${this.pslug}/info/`
    })
    .done((data)=> this.setState({initialFeePercentage: data.initial_fee_percentage, booking_price: data.booking_price }))
    .fail((error)=> console.log(error))


    $('#planTableFirstDate').change((e, a) => {

      let remainingDatesTemp = []

      this.setState({firstDateFee: e.target.value})

      _.forEach(_.range(1, this.state.remainingMonths), (n, k)=>{

        remainingDatesTemp.push(moment(e.target.value).add(n, 'M').format('YYYY-MM-DD'))
      })

      this.setState({remainingDates: remainingDatesTemp})

      // NPV = (%interest, fee1, fee2, fee3...)
      const interest = 1.5
      let fees = _.times(this.state.remainingMonths, _.constant(this.state.calculateMonthlyFees / this.state.remainingMonths))
      console.log(fees);
      let NPV = this.finance.NPV(interest, 0, ...fees)
      this.NPV = NPV
      console.log(`NPV1: ${this.NPV}`)
    })

  }

  clientSelectedEditInfo(client){
    this.setState({clientSelectedEditInfo: client })
  }

  addClient(dataForm) {

    var clientsTemp = this.state.clients.concat(dataForm)

    this.setState({clients: clientsTemp})

  }

  updateClients(client){

    let temp = this.state.clients

    let remove = _.remove(temp, (el)=>{
      return el.id === client.id
    })

    temp.push(client)

    this.setState({clients: temp})

  }

  removeClient(clientId){
    this.addedClients = _.remove(this.state.clients, (c) => c.id !== clientId )
    this.setState({clients: this.addedClients})
  }

  searchApto(e){

    if(e.target.value === '') $('.NewPayment__results').fadeOut(200)
    else $('.NewPayment__results').fadeIn(200)

    $.ajax({
      method: 'GET',
      url: `/api/projects/${this.pslug}/search-unities/?search=${e.target.value}`
    })
    .done((data) => {

      this.setState({aptos: data})
    })
    .fail((error)=> console.log(error))

  }

  CalculateMonthlyFees(initialFeeOp){
    let calcOne = (initialFeeOp ? initialFeeOp : this.state.initialFee ) - this.state.booking_price

    let calcTwo = ( (this.state.scheduledSavingsApply ? parseInt(this.state.scheduledSavings) : 0) + (this.state.severanceApply ? parseInt(this.state.severance) : 0) + (this.state.housingAllowanceApply ? parseInt(this.state.housingAllowance) : 0) + (this.state.othersPaymentsApply ? parseInt(this.state.othersPayments) : 0) )

    let calc = calcOne - calcTwo

    this.setState({calculateMonthlyFees: calc})

  }

  selectApto(apto){

    this.setState({aptoSelected: apto})
    $('.NewPayment__aptoSelected').fadeIn(200)
    $('.NewPayment__results').fadeOut(200)

    let initialFeeOp = (apto.price * 30) / 100

    this.setState({initialFee:  initialFeeOp})

    this.setState({unityCurrentPrice: apto.price})
    console.log(apto)
    let months = moment(apto.delivery_date).diff(moment(moment(moment.now()).format('YYYY-MM-DD')), 'months')
    console.log(months);
    this.setState({remainingMonths: months})

    this.CalculateMonthlyFees(initialFeeOp)
  }

  closeApto(){
    this.setState({aptoSelected: {}})
    $('#newPaymentAptoSearch').val('')
    $('.NewPayment__aptoSelected').fadeOut(200)
  }

  selectCampaign(e){

    let selectCampaignInfo = _.find(this.state.campaigns, (c) => c.name === e.target.value)

    this.setState({remainingMonths: selectCampaignInfo.number_of_paymentfees})

    this.setState({initialFeePercentage: selectCampaignInfo.initial_fee_percentage, booking_price : selectCampaignInfo.booking_price})

    let initialFeeCalc = (this.state.unityCurrentPrice * selectCampaignInfo.initial_fee_percentage) / 100

    this.setState({initialFee: initialFeeCalc })

    if(this.state.scheduledSavings && this.state.severance && this.state.housingAllowance && this.state.othersPayments){
      let sum = (parseInt(this.state.scheduledSavings)) + (parseInt(this.state.severance)) + (parseInt(this.state.housingAllowance)) + (parseInt(this.state.othersPayments))

      let sumInitialFeePercentage = sum + (this.state.aptoSelected.price* this.state.initialFeePercentage)/100

      if(sumInitialFeePercentage){
        let creditToApply = this.state.aptoSelected.price - sumInitialFeePercentage

        this.setState({ creditToApply: creditToApply })

        let valor_cuotas = ((this.state.aptoSelected.price * this.state.initialFeePercentage)/100) - this.state.booking_price

        let othersPaymentsSum = ( this.state.scheduledSavingsApply ? parseInt(this.state.scheduledSavings) : 0 ) + ( this.state.severanceApply ? parseInt(this.state.severance) : 0 ) + ( this.state.housingAllowanceApply ? parseInt(this.state.housingAllowance) : 0 ) + ( this.state.othersPaymentsApply ? parseInt(this.state.othersPayments) : 0 )

        let resultado_parcial = valor_cuotas - othersPaymentsSum

        let resultado_final = resultado_parcial / this.state.remainingMonths

        this.state.feePrice = resultado_final
      }

    }
    else{
      console.log('no seleccionado')
    }

  }

  othersPaymentHandler(key, value){
    this.setState({ [key]: value })

    setTimeout(()=>{
      this.CalculateMonthlyFees()
    }, 500)
  }

  updateOtherPaymentsValue(e, stateName){
    if (!e.target.value) return;

    this.setState({[stateName]: e.target.value.split('.').join('')})

    $(`#${e.target.id}`).val(e.target.value.split('.').join('').toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."))

    if (this.state[`${stateName}Apply`]) {
      setTimeout(()=>{
        this.CalculateMonthlyFees()
      }, 500)
    }
  }

  extraordinaryFeeUpdate(e){

    $(`#${e.target.id}`).val(e.target.value.split('.').join('').toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."))

    let extraordinaryFeeInputs = 0

    _.forEach(_.range(1, this.state.remainingMonths + 1), (m) => {
      extraordinaryFeeInputs += $(`#extraordinaryFee${m}`).val() ? parseInt($(`#extraordinaryFee${m}`).val().split('.').join('')) : 0
    })


    let calculateMonthlyExtraFees = this.state.calculateMonthlyFees - extraordinaryFeeInputs

    this.setState({calculateMonthlyExtraFees: calculateMonthlyExtraFees})

    if(!this.state.isCalculateMonthlyExtraFees) this.state.isCalculateMonthlyExtraFees = true

    setTimeout(()=>{

      let fees = []

      _.forEach(_.range(1, this.state.remainingMonths + 1), (v, i)=>{

        let fee = $(`#extraordinaryFee${v}`).val() ? parseInt(( $(`#feeValue${v}`).text().split('.').join('').split('$').join('') )) + parseInt(( $(`#extraordinaryFee${v}`).val().split('.').join('') )) : $(`#feeValue${v}`).text().split('.').join('').split('$').join('')

        fees.push(fee)

      })

      // NPV = (%interest, fee1, fee2, fee3...)
      const interest = 1.5
      let NPV = this.finance.NPV(interest, 0, ...fees)
      this.setState({ NPV2: NPV })

    }, 500)

  }

  firstDateIsValidHandler(){
    $('#firstDateError').css({display:'block'})
    $('#planTableFirstDate').css({border:'1px solid #FF8A80'})
  }

  firstDateErrorClose(){
    $('#firstDateError').css({display:'none'})
  }

  validationPopupClose(){
    $('#NewPaymentValidationPopup').fadeOut(200)
  }

  validateNewpaymentPlan(){

    let firstDateIsValid = $('#planTableFirstDate').val()

    if(!firstDateIsValid) return this.firstDateIsValidHandler()

    if(this.state.NPV2 >= this.NPV || this.state.calculateMonthlyExtraFees === 0) this.setState({paymentPlanValid: true})

    $('#NewPaymentValidationPopup').css({display:'flex'}).fadeIn(200)
  }

  saveNewPaymentPlan(){
    let paymentFeesList = []
    let remainingMonths = _.range(1, this.state.remainingMonths + 1)


    remainingMonths.forEach((n, k)=>{
      if(n === 1) paymentFeesList.push({ date_to_pay: $(`#planTableFirstDate`).val(), fee: parseInt($(`#feeValue${n}`).text().replace('$', '').split('.').join('')), extraordinary_fee: parseInt($(`#extraordinaryFee${n}`).val() ? $(`#extraordinaryFee${n}`).val().split('.').join('') : 0) })
      else paymentFeesList.push({ date_to_pay: $(`#planTableDate${k}`).text().replace(/\s+/g, ''), fee: parseInt($(`#feeValue${n}`).text().replace('$', '').split('.').join('')), extraordinary_fee: parseInt($(`#extraordinaryFee${n}`).val() ? $(`#extraordinaryFee${n}`).val().split('.').join('') : 0) })
    })

    let othersPaymentsList = []

    let textInputs = $('.NewPayment__othersPaymentCont > div input[type=text]')
    let checkInputs = $('.NewPayment__othersPaymentCont > div input[type=checkbox]')

    for(var i=0; i<textInputs.length; i++){
      othersPaymentsList.push({type_of_payment: textInputs[i].name, fee: textInputs[i].value ? parseInt(textInputs[i].value.split('.').join('')) : 0, applied_to_initialfee: checkInputs[i].checked})
    }

    let clientsList = []

    _.forEach(this.state.clients, (client, i)=> {
      clientsList.push(client.id)
    })

    console.log(this.state.aptoSelected)

    let sendData = {
      persons : clientsList,
      booking_price: this.state.booking_price,
      agreed_payments: othersPaymentsList,
      payment_fees: paymentFeesList,
      unity: this.state.aptoSelected.id
    }

    console.log(sendData);

    $.ajax({
      method: 'POST',
      url: `/api/projects/${this.pslug}/formalizations/payment-agreement/create/`,
      headers: {
        'X-CSRFToken': getCookie('csrftoken'),
        'Content-Type' : 'application/json'
      },
      data: JSON.stringify(sendData),
      dataType: "json"
    })
    .done((data) => {
      $('#NewPaymentValidationPopup').fadeOut(200)
      window.location.hash = `#/newpayment/sucess/${this.state.aptoSelected.slug}`
    })
    .fail((error)=> console.log(error))
  }

  render () {
    return (
      <section className="NewPayment">
        <div className="NewPayment__title">
          <h2>NUEVO PLAN DE PAGOS</h2>
          <div><IconNewPayment/></div>
        </div>


        <NewPaymentBasicInfo
          searchApto={this.searchApto}
          selectApto={this.selectApto}
          aptoSelected={this.state.aptoSelected}
          closeApto={this.closeApto}
          aptos={this.state.aptos}
          clients={this.state.clients}
          removeClient={this.removeClient}
          clientSelectedEditInfo={this.clientSelectedEditInfo}
        />


        <NewPaymentSummary
          campaigns={this.state.campaigns}
          selectCampaign={this.selectCampaign}
          aptoSelected={this.state.aptoSelected}
          initialFee={this.state.initialFee}
          initialFeePercentage={this.state.initialFeePercentage}
          booking_price={this.state.booking_price}
          creditToApply={this.state.creditToApply}
          updateOtherPaymentsValue ={this.updateOtherPaymentsValue}
          CalculateMonthlyFees = {this.CalculateMonthlyFees}
          othersPaymentHandler={this.othersPaymentHandler}
          scheduledSavingsApply={this.state.scheduledSavingsApply}
          severanceApply={this.state.severanceApply}
          housingAllowanceApply={this.state.housingAllowanceApply}
          othersPaymentsApply={this.state.othersPaymentsApply}
        />


        <NewPaymentPlan
          calculateMonthlyFees={this.state.calculateMonthlyFees}
          calculateMonthlyExtraFees={this.state.calculateMonthlyExtraFees}
          NPV={this.NPV}
          NPV2={this.state.NPV2}
          remainingMonths={this.state.remainingMonths}
          firstDateErrorClose={this.firstDateErrorClose}
          remainingDates={this.state.remainingDates}
          remainingMonths={this.state.remainingMonths}
          isCalculateMonthlyExtraFees={this.state.isCalculateMonthlyExtraFees}
          extraordinaryFeeUpdate={this.extraordinaryFeeUpdate}
          validationPopupClose={this.validationPopupClose}
          paymentPlanValid={this.state.paymentPlanValid}
          saveNewPaymentPlan={this.saveNewPaymentPlan}
          validateNewpaymentPlan={this.validateNewpaymentPlan}
        />


        <NewPaymentPopup addClient={this.addClient.bind(this)} pslug={this.pslug}/>
        <NewPaymentPopupEdit pslug={this.pslug} clientSelectedEditInfo={this.state.clientSelectedEditInfo} updateClients={this.updateClients.bind(this)}/>

      </section>
    )
  }
}
