import React, { Component } from 'react'
import AptoClients from './_apto-clients'
import { IconBasicInfo } from './svg/_svg-icons'
import { IconSearch } from '../payments/svg/_svg-icons'

export default class NewPaymentBasicInfo extends Component {

  constructor (props) {
    super(props)

    this.state = {

    }
  }

  render () {
    return (
      <div className="NewPayment__basicInfo">
        <h3>INFORMACIÓN BÁSICA</h3>
        <IconBasicInfo/>
        <div className="NewPayment__cont">
          <div className="NewPayment__searchApto">
            <label htmlFor="">Unidad</label>
            <IconSearch />
            <input id="newPaymentAptoSearch" type="text" placeholder="Ej: T2-101" onChange={this.props.searchApto}/>
            <span className="NewPayment__aptoSelected"> { this.props.aptoSelected.name } <span className="NewPayment__chipClose" onClick={this.props.closeApto}>x</span></span>
            <div className="NewPayment__results">
              {
                this.props.aptos.map((apto, k)=>{
                  return <div key={k} onClick={ ()=> this.props.selectApto(apto) }> {apto.name} </div>
                })
              }
            </div>
          </div>

          <AptoClients clients={this.props.clients} removeClient={this.props.removeClient} clientSelectedEditInfo={this.props.clientSelectedEditInfo}/>

        </div>
      </div>
    )
  }
}