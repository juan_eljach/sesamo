import React, { Component } from 'react'
import { IconNewPayment } from './svg/_svg-icons'
import { IconDate, IconSearch } from '../payments/svg/_svg-icons'

export default class NewPaymentPlan extends Component {

  constructor (props) {
    super(props)
  }

  render () {
    return (
      <div className="NewPayment__plan">
        <div><IconNewPayment/></div>
        <div>
          <h3>PLAN DE PAGO</h3>
          <h4> CUOTA INICIAL RESTANTE: {this.props.calculateMonthlyFees.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")} </h4>
          <h4> CUOTA INICIAL RESTANTE EXTRAORDINARIA: {this.props.calculateMonthlyExtraFees.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")} </h4>
          <h4> NPV: {this.props.NPV.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")} </h4>
          <h4> NPV2: {this.props.NPV2.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")} </h4>
        </div>
        <div className="NewPayment__planDues">
          <div>{this.props.remainingMonths} cuotas</div>
        </div>

        <div className="NewPayment__planTable">

          <div className="NewPayment__planTableHead">
            <div>No. Cuota</div>
            <div>Fecha</div>
            <div>Valor de la cuota</div>
            <div>Cuota extraordinaria</div>
            <div>Comentarios</div>
          </div>
          <div className="NewPayment__planTableBody">

            <div className="NewPayment__planTableFeeDates">
              <div className="NewPayment__planTableFirstDate">
               <IconDate />
               <div className="NewPayment__firstDateError" id="firstDateError">
                 <span onClick={this.props.firstDateErrorClose}>x</span>
                 Debes seleccionar la primer fecha
               </div>
                <input id="planTableFirstDate" type="text" data-toggle="datepicker" />
              </div>
              <div className="NewPayment__planTableRemainingDates">
                {
                  this.props.remainingDates.map((n, k)=>{
                    return <div id={`planTableDate${k+1}`} key={k}> <IconDate />{ n } </div>
                  })
                }
              </div>
            </div>

            {
              _.range(1, this.props.remainingMonths + 1).map((n, k)=>{
                return <div className="NewPayment__planTableRow" key={k}>
                  <div>{n}</div>
                  <div></div>
                  <div id={`feeValue${n}`}><span>$</span>{ _.round(( ( this.props.isCalculateMonthlyExtraFees ? this.props.calculateMonthlyExtraFees : this.props.calculateMonthlyFees  ) / this.props.remainingMonths)).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.") }</div>
                  <div><span>$</span><input id={`extraordinaryFee${n}`} type="text" onChange={this.props.extraordinaryFeeUpdate}/></div>
                  <div>Comentar</div>
                </div>
              })
            }

          </div>
        </div>

        <div className="NewPayment__validationPopup" id="NewPaymentValidationPopup">
          <div>
            <span onClick={this.props.validationPopupClose}>x</span>
            <img src={ this.props.paymentPlanValid ? '/static/img/emoji-lengua.png' : '/static/img/emoji-triste.png' } width="70"/>
            <p> { this.props.paymentPlanValid ? 'El plan de pago es válido' : 'El plan de pago no es válido' }  <span>{ this.props.paymentPlanValid ? 'Esa platica ya es segura' : 'Esa platica todavía no es segura' }</span></p>
            <button onClick={ this.props.paymentPlanValid ? this.props.saveNewPaymentPlan : ()=> $('#NewPaymentValidationPopup').fadeOut(200) }> { this.props.paymentPlanValid ? 'GUARDA Y ASEGURA!' : 'MODIFICA EL PLAN!' } </button>
          </div>
        </div>

        <div className="NewPayment__validateButton">
          <button onClick={this.props.validateNewpaymentPlan}>VALIDAR</button>
        </div>

      </div>
    )
  }
}





