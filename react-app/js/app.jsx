// Only for development
module.hot.accept()
require('scss')

//Dependencies
import React, { Component } from 'react'
import { render } from 'react-dom'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import injectTapEventPlugin from 'react-tap-event-plugin';
import { Router, Route, hashHistory, IndexRoute } from 'react-router'

injectTapEventPlugin();

// $.fn.datepicker
// $('[data-toggle="datepicker"]').datepicker();

// Components
import Menu from './menu/index'
import Header from './header/index'
import Formalization from './formalization/index'
import NewPayment from './newpayment/index'
import Landing from './landing/index'
import Payments from './payments/index'
import ViewPaymentPlan from './payments/_viewpayment-plan'
import Documents from './documents/documents'
import PaymentsUnity from './payments/_payments-unity'
import NewPaymentSucess from './newpayment/_newpaymentsucess'
import NewPaymentDocument from './newpayment/_newpaymentdocument'
import Support from './support/index'
import TicketDetail from './support/pages/ticket-detail'
import LandingSupport from './support/pages/landing-support'
import FaqSupport from './support/pages/faq-support'
import NewTicket from './support/pages/new-ticket'
import Writing from './writing/index'
import Deeds from './deeds/index'
import DeedsFirstStepOnBoard from './deeds/pages/first-step-onboard'
import DeedsFirstStepSendNotifications from './deeds/pages/first-step-sendnotification'
import DeedsSecondStep from './deeds/pages/second-step'

let base_url = window.location.href,
    split_url = base_url.split('/'),
    pslug = split_url[4];


class App extends Component {

  componentDidMount(){

  }

  render () {
    return (
      <MuiThemeProvider>
        <div className="Main">
          <Header />
          <Menu />
          <div className="Wraper" id="Wraper">
            { this.props.children }
          </div>
        </div>
      </MuiThemeProvider>
    )
  }
}

render((
  <Router history={hashHistory}>
    <Route path="/" component={App}>
      <IndexRoute component={Formalization} />
      <Route path="/formalization" component={Formalization} />
      <Route path="/newpayment" component={NewPayment} />
      <Route path="/newpayment/sucess/:unity" component={NewPaymentSucess} />
      <Route path="/newpayment/document" component={NewPaymentDocument} />
      <Route path="/payments" component={PaymentsUnity} />
      <Route path="/viewpayment/:id" component={ViewPaymentPlan} />
      <Route path="/viewbusiness/:id" component={Payments} />
      <Route path="/documents" component={Documents} />
      <Route path="/support" component={Support} />
      <Route path="/support/ticket/:ticketSlug" component={TicketDetail} />
      <Route path="/support/new-ticket" component={NewTicket} />
      <Route path="/writing" component={Writing} />
      <Route path="/deeds" component={Deeds} />
      <Route path="/deeds/onboard" component={DeedsFirstStepOnBoard} />
      <Route path="/deeds/send-notification/:tower" component={DeedsFirstStepSendNotifications} />
      <Route path="/deeds/second-step/:unity" component={DeedsSecondStep} />
    </Route>
  </Router>

), document.getElementById('App'))
