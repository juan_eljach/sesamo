import React, {Component} from 'react'
import { Link } from 'react-router'

export default class Menu extends Component{

  constructor(props){
    super(props)

    this.state = {
      menuSelected: 1
    }
  }

  componentDidMount(){
    if(window.sessionStorage.getItem('tab')) this.setState({menuSelected: parseInt(window.sessionStorage.getItem('tab'))})
  }

  selectMenu(menu){
    this.setState({menuSelected: menu})
    window.sessionStorage.setItem('tab', menu)
  }

  render(){
    return (
      <nav className="Menu">
        <ul>
          <li className={ this.state.menuSelected === 1 ? 'isMenuSelected' : '' } onClick={ ()=> this.selectMenu(1) }>
            <img src="/static/img/check.png" alt=""/>
            <Link to="/formalization">Formalización</Link>
          </li>
          <li className={ this.state.menuSelected === 2 ? 'isMenuSelected' : '' } onClick={ ()=> this.selectMenu(2) }>
            <img src="/static/img/payments.png" alt=""/>
            <Link to="/payments">Pagos</Link>
          </li>
          {/* <li className={ this.state.menuSelected === 3 ? 'isMenuSelected' : '' } onClick={ ()=> this.selectMenu(3) }>
            <img src="/static/img/diploma.png" alt=""/>
            <Link to="/documents">Documentos</Link>
          </li> */}
          <li className={ this.state.menuSelected === 4 ? 'isMenuSelected' : '' } onClick={ ()=> this.selectMenu(4) }>
            <img src="/static/img/support.png" alt=""/>
            <Link to="/support">Soporte</Link>
          </li>
          <li className={ this.state.menuSelected === 5 ? 'isMenuSelected' : '' } onClick={ ()=> this.selectMenu(5) }>
            <img src="/static/img/diploma.png" alt=""/>
            <Link to="/deeds">Escrituración</Link>
          </li>
          {/* <li className={ this.state.menuSelected === 6 ? 'isMenuSelected' : '' } onClick={ ()=> this.selectMenu(6) }>
            <img src="/static/img/keys.png" alt=""/>
            <Link to="/aftersales">PostVenta</Link>
          </li> */}
          {/* <li className={ this.state.menuSelected === 7 ? 'isMenuSelected' : '' } onClick={ ()=> this.selectMenu(7) }>
            <img src="/static/img/sell.png" alt=""/>
            <Link to="/configurations">Configuraciones</Link>
          </li> */}
        </ul>

      </nav>
    )
  }
}
