import React, {Component} from 'react'
import { Link } from 'react-router'
import { IconSearch } from './svg/_svg-icons.jsx'
import ReactPaginate from 'react-paginate'

export default class Formalization extends Component {

  constructor (props) {
    super(props)

    // this.initialData = _.times(55, _.constant({client: 'Victor Aguirre', apto: 'T4301', debt: 150000000, moratorium: 150000000, moratoriumTime: '4 horas', seller: 'Karen Castro'}))

    this.pslug = window.location.href.split('/')[4]

    this.initialData = [
      {client: 'Victor Aguirre', apto: 'T4301', debt: 150000000, moratorium: 150000000, moratoriumTime: '4 horas', seller: 'Karen Castro'},
      {client: 'Victor Aguirre', apto: 'T4302', debt: 150000000, moratorium: 150000000, moratoriumTime: '4 horas', seller: 'Chuck'},
      {client: 'Victor Aguirre', apto: 'T2301', debt: 150000000, moratorium: 150000000, moratoriumTime: '4 horas', seller: 'Pepe'},
      {client: 'Victor Aguirre', apto: 'T1301', debt: 150000000, moratorium: 150000000, moratoriumTime: '4 horas', seller: 'Black'}
    ]

    this.state = {
      showCount: 10,
      page: 1,
      data: [],
      list: [],
      noResults: false
    }
  }

  updateShowData () {
    this.setState({data: this.initialData.slice((this.state.page - 1) * this.state.showCount, this.state.showCount * this.state.page)})
  }

  componentDidMount () {
    this.updateShowData()

    $.ajax({
      method: 'GET',
      url: `/api/projects/${this.pslug}/formalizations/payment-agreements`
    })
    .done((data)=> {      
      this.setState({list: data.results});
    })
    .fail((error) => console.log(error))
  }

  handlePageClick (page) {
    this.setState({page: page.selected + 1}, () => this.updateShowData())
  }

  handleShowCount (e) {
    this.setState({showCount: parseInt(e.target.value)}, () => this.updateShowData())
    console.log(e.target.value)
  }

  filterPlan(e){

    let filtered =  []

    this.state.list.map((v) => {
      if(v.first_name.toLowerCase().search(e.target.value) > -1){
        filtered.push(v)
      }
    })
    if(!filtered.length) this.setState({noResults: true})

    if(e.target.value) return this.setState({list: filtered})

    this.setState({noResults: false})
  }

  render () {    
    return (
      <section className='Formalization'>
        <div className='Formalization__header'>
          <div className='Formalization__search'>
            <IconSearch />
            <input type='text' placeholder='Buscar por persona o apto' onChange={ (e)=> this.filterPlan(e) }/>
          </div>
          <div className='Formalization__plans'>
            <div className='Formalization__showPlans'>
              {/* <div>Mostrar</div>
              <select name='' id='' onChange={(e) => this.handleShowCount(e)}>
                <option value='10'>10</option>
                <option value='20'>20</option>
                <option value='30'>30</option>
              </select>
              <div>planes</div> */}
            </div>
            <div className='Formalization__newPayment'>
              <Link to='/newpayment'>NUEVO PLAN DE PAGO</Link>
            </div>
          </div>
        </div>

        <div className='Formalization__table'>

          <div className='Formalization__cont'>
            <div className='Formalization__head'>
              <div>Cliente</div>
              <div>Apartamento</div>
              <div>Celular</div>
              <div>Telefono de notificación</div>
              <div>Fecha de creación</div>
            </div>
            <div className='Formalization__body'>
              <p className={`Formalization__noResults ${ this.state.noResults ? '' : 'displayNone' }` }>No se encontraron resultados</p>
              {
                this.state.list.map((plan, i) => {
                  if(plan){
                    return (
                      <div className='Formalization__row' key={i}>
                        <div>{ `${plan.persons[0].first_name} ${plan.persons[0].last_name}` }</div>
                        <div>{ plan.unity }</div>
                        <div>{ plan.persons[0].cellphone }</div>
                        <div>{ plan.persons[0].notification_phone }</div>
                        <div>{ plan.timestamp }</div>
                        <div>
                          <Link to='/viewpayment/1'>Ver plan</Link>
                        </div>
                      </div>
                    )
                  }
                })
              }

            </div>
          </div>
          <div className='Formalization__pagination'>

            <ReactPaginate previousLabel={'ANTERIOR'}
              nextLabel={'SIGUIENTE'}
              breakLabel={<a>...</a>}
              breakClassName={'bQqreak-me'}
              pageCount={Math.ceil(this.initialData.length / this.state.showCount)}
              activeClassName={'isActivePage'}
              onPageChange={this.handlePageClick.bind(this)} />

            {/* <div className='Formalization__back'><a href='#'>ANTERIOR</a></div>
            <div className='Formalization__pages'>
              <span>1</span>
              <span>2</span>
              <span>3</span>
              <span>4</span>
            </div>
            <div className='Formalization__next'><a href='#'>SIGUIENTE</a></div> */}
          </div>
        </div>
      </section>
    )
  }
}
