import React, {Component} from 'react'
// import { Link } from 'react-router'
import {IconSearch} from '../payments/svg/_svg-icons'

export default class Documents extends Component{

  constructor(props){
    super(props)
  }

  render(){
    return (
      <section className="DocumentsList">

        <div className="DocumentsList__header">
          <div>DOCUMENTOS PENDIENTES</div>
          <div>DOCUMENTOS DE CADA UNIDAD</div>
        </div>

        <div className="DocumentsList__search">
        <IconSearch />
          <input type="text" placeholder="Busca documento"/>
        </div>

        <div className="DocumentsList__body">

          <div className="DocumentsList__card">
            <span>Abierto hace 5 días</span>
            <div className="DocumentsList__infoCont">
              <div className="DocumentsList__round" style={{background:'#9575cd'}}>JS</div>
              <div className="DocumentsList__info">
                <div>Juan Sebastián Sandoval</div>
                <div>T3-201</div>
                <div>Documentos pendientes: </div>
                <div>Documentos totales: 10</div>
              </div>
            </div>
            <div className="DocumentsList__see">
              <div>VER</div>
            </div>
          </div>

          <div className="DocumentsList__card">
            <span>Abierto hace 5 días</span>
            <div className="DocumentsList__infoCont">
              <div className="DocumentsList__round" style={{background:'#ffd54f'}}>JS</div>
              <div className="DocumentsList__info">
                <div>Juan Sebastián Sandoval</div>
                <div>T3-201</div>
                <div>Documentos pendientes: </div>
                <div>Documentos totales: 10</div>
              </div>
            </div>
            <div className="DocumentsList__see">
              <div>VER</div>
            </div>
          </div>

          <div className="DocumentsList__card">
            <span>Abierto hace 5 días</span>
            <div className="DocumentsList__infoCont">
              <div className="DocumentsList__round" style={{background:'#29b6f6'}}>JS</div>
              <div className="DocumentsList__info">
                <div>Juan Sebastián Sandoval</div>
                <div>T3-201</div>
                <div>Documentos pendientes: </div>
                <div>Documentos totales: 10</div>
              </div>
            </div>
            <div className="DocumentsList__see">
              <div>VER</div>
            </div>
          </div>

          <div className="DocumentsList__card">
            <span>Abierto hace 5 días</span>
            <div className="DocumentsList__infoCont">
              <div className="DocumentsList__round" style={{background:'#ff8a80'}}>JS</div>
              <div className="DocumentsList__info">
                <div>Juan Sebastián Sandoval</div>
                <div>T3-201</div>
                <div>Documentos pendientes: </div>
                <div>Documentos totales: 10</div>
              </div>
            </div>
            <div className="DocumentsList__see">
              <div>VER</div>
            </div>
          </div>

          <div className="DocumentsList__card">
            <span>Abierto hace 5 días</span>
            <div className="DocumentsList__infoCont">
              <div className="DocumentsList__round" style={{background:'#80c683'}}>JS</div>
              <div className="DocumentsList__info">
                <div>Juan Sebastián Sandoval</div>
                <div>T3-201</div>
                <div>Documentos pendientes: </div>
                <div>Documentos totales: 10</div>
              </div>
            </div>
            <div className="DocumentsList__see">
              <div>VER</div>
            </div>
          </div>

          <div className="DocumentsList__card">
            <span>Abierto hace 5 días</span>
            <div className="DocumentsList__infoCont">
              <div className="DocumentsList__round" style={{background:'#9575cd'}}>JS</div>
              <div className="DocumentsList__info">
                <div>Juan Sebastián Sandoval</div>
                <div>T3-201</div>
                <div>Documentos pendientes: </div>
                <div>Documentos totales: 10</div>
              </div>
            </div>
            <div className="DocumentsList__see">
              <div>VER</div>
            </div>
          </div>

        </div>

      </section>
    )
  }
}
