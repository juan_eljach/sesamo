import React, {Component} from 'react'

export default class Landing extends Component{
  constructor(props){
    super(props)
  }

  openPopup(){

  }

  render(){
    return <div className="Landing">
      <header className="LandingHeader">
        <div className="LandingHeader__logo">
          Sesamo
        </div>
        <div className="LandingHeader__sigin">
          <a href="#">INICIA SESIÓN</a>
        </div>
      </header>

      <section className="LandingAbove">
        <h1>AUTOMATIZA LOS PROCESOS DE TU CONSTRUCTORA Y REDUCE COSTOS OPERATIVOS</h1>
        <div>
          <a>AGENDA UNA DEMOSTRACIÓN</a>
        </div>
      </section>

      <section className="LandingFunctionalities">
        <h2>FUNCIONALIDADES DE SESAMO</h2>
        <div>
          <img src="" alt="" />
          <h3>Soporte a clientes</h3>
        </div>
        <div>
          <img src="" alt="" />
          <h3>Entrega</h3>
        </div>
        <div>
          <img src="" alt="" />
          <h3>Formalización</h3>
        </div>
        <div>
          <img src="" alt="" />
          <h3>Documentos y trámites</h3>
        </div>
        <div>
          <img src="" alt="" />
          <h3>Postventa</h3>
        </div>
        <div>
          <img src="" alt="" />
          <h3>Pagos</h3>
        </div>
        <div>
          <img src="" alt="" />
          <h3>Escrituración</h3>
        </div>
      </section>

      <footer className="LandingFooter">
        <p>AGENDA UNA DEMOSTRACIÓN GRATUITA, LLÁMANOS A LA LÍNEA (057) 3204409652</p>
      </footer>

    </div>
  }
}
