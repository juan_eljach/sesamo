import React, { Component } from 'react'

export default class Header extends Component{
  constructor(props){
    super(props)
     this.state = { userLogged: {} }

     this.pslug = window.location.href.split('/')[4]
  }

  componentWillMount(){
    $.ajax({
      method: 'GET',
      url: `/api/user/`
    })
    .done((data)=> {
      this.setState({userLogged: data})
    })
    .fail((error)=> console.log(error))
  }

  componentDidMount(){
    $('#Wraper').click(()=> $('#userTooltip').fadeOut(100) )
  }

  userTooltip(){
    $('#userTooltip').fadeIn(100)
  }

  render(){
    return (
      <header className="Header">
        <div className="Header__cont">
          <div className="Header__logo">
            <h1>Sesamo</h1>
            <a href={`/projects/${this.pslug}/presale`}>Ir a Preventa</a>
          </div>
          <div className="Header__userinfo">{ `${this.state.userLogged.first_name} ${this.state.userLogged.last_name}` }
            <span className="Header__arrow" onClick={this.userTooltip}></span>
            <div className="Header__userTooltip" id="userTooltip">
              <a href="/logout">Cerrar sesión</a>
            </div>
          </div>
        </div>
      </header>
    )
  }
}
