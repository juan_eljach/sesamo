import React, { Component } from 'react'

export default class WritingSendNotificationPopup extends Component {
  constructor(props){
    super(props)

    this.state = {
      selectedOption: 1,
      isOpenPopup: true
    }

    this.actionPopup = this.actionPopup.bind(this)
  }

  componentDidMount(){

  }

  selectOption(option){

    if(option === 1) {
      let unitiesIds = _.map(this.props.unitiesByTower, 'id')
      console.log(unitiesIds);
      return this.setState({selectedOption: option})
    }

    this.setState({selectedOption: option})

  }

  actionPopup(){
    if (this.state.selectedOption === 2) this.setState({isOpenPopup: false})
  }

  render(){
    return (
      <div className={`Writing__sendNotificationPopup ${this.state.isOpenPopup ? '' : 'displayNone'}`}>

        <div>
          <h4>Deseas enviar notificaciones de escrituración a:</h4>

          <ul>

            <li>
              <div
                className={`${this.state.selectedOption === 1 ? 'backgroundGreen' : ''} `}
                onClick={()=>this.selectOption(1)}>
              </div>
              Todos los clientes de la torre 1
              <span> {this.props.quantityUnities} unidades</span>
            </li>

            <li>
              <div
                className={`${this.state.selectedOption === 2 ? 'backgroundGreen' : ''} `}
                onClick={()=>this.selectOption(2)} >

              </div>
              Seleccionar cuales clientes
            </li>

          </ul>

          <button onClick={this.actionPopup}> { this.state.selectedOption === 1 ? 'ENVIAR' : 'SELECCIONAR' } </button>
        </div>

      </div>

    )
  }
}
