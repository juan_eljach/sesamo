import React, { Component } from 'react'

export default class WritingTasks extends Component {
  constructor(props){
    super(props)

  }

  componentDidMount(){

  }

  render(){
    return (
      <div className="Writing__bodyTasks">
        <h3>Tareas</h3>

        <div className="Writing__tasksList">

          <div className="Writing__task">
            <div className="Writing__taskHead"><span>1</span> Enviar notificaciones</div>
            <ul className="Writing__subTask">
              <li>
                <span>A</span>Selecciona los apartamentos
              </li>
              <li>
                <span>B</span> Confirma el mensaje de la notificación
              </li>
            </ul>
          </div>

          <div className="Writing__task">
            <div className="Writing__taskHead"><span>2</span> Generar documentos</div>
            <ul className="Writing__subTask">
              <li>
                <span>A</span>Selecciona los apartamentos
              </li>
              <li>
                <span>B</span> Confirma el mensaje de la notificación
              </li>
            </ul>
          </div>

        </div>
      </div>
    )
  }
}
