import React, { Component } from 'react'

export default class WritingNotificationTableNav extends Component {
  constructor(props){
    super(props)

  }

  componentDidMount(){

  }

  render(){
    return (
      <nav className={`Writing__navSelectionMode ${this.props.isSelectionMode || this.props.isMessageConfirmation ? '' : 'displayNone' }`}>
        <div onClick={this.props.navigationCancel}> {this.props.isSelectionMode ? 'CANCELAR' : 'ATRAS' } </div>
        <div> {this.props.isSelectionMode ? 'Selecciona los apartamentos' : 'Confirma el mensaje de la notificación'} </div>
        <div onClick={this.props.isSelectionMode ? this.props.navigationNext : this.props.sendNotification}> { `${this.props.isSelectionMode ? 'SIGUIENTE' : 'GUARDAR Y ENVIAR'}` } </div>
      </nav>
    )
  }
}
