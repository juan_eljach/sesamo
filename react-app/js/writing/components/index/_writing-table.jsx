import React, { Component } from 'react'

import { IconEmail, IconCheck } from '../../svg/writing-icons'

import getCsrfToken from '../../../lib/csrftoken'

export default class WritingTable extends Component {
  constructor(props){
    super(props)

    this.selectedUnitiesTemp = []

    this.state = {
      selectedUnities: [],
      isSelectionMode: false,
      isMessageConfirmation: false,
      notificationOptionTab: '',
      editingTemplateNotification: false
    }

    this.navigationNext = this.navigationNext.bind(this)
    this.navigationCancel = this.navigationCancel.bind(this)
    this.sendNotification = this.sendNotification.bind(this)
    this.editTemplateNotification = this.editTemplateNotification.bind(this)
  }

  componentDidMount(){
    this.$writingEditTemplate = $('#writingEditTemplate')
  }

  checkHandler(e, unity){
    console.log(unity);
    let unityFound = _.find(this.state.selectedUnities, (unityId) => unityId === unity.id)

    if(unityFound){
      _.remove(this.selectedUnitiesTemp, (unityId) => unityId === unity.id )

      return this.setState({selectedUnities: this.selectedUnitiesTemp})
    }

    this.selectedUnitiesTemp.push(unity.id)

    this.setState({selectedUnities: _.uniq(this.state.selectedUnities.concat(this.selectedUnitiesTemp))})

  }

  selectAllByFloor(floor) {

    let unitiesByFloor = _.filter(this.props.unitiesByTower, (unity) => unity.floor == floor)

    let selectedByFloor = _.map(
      ( _.filter(unitiesByFloor, (unity, key) => {

        return _.find(this.state.selectedUnities, (selectUnity) => selectUnity == unity.id)

      }) ), 'id'
    )


    if(selectedByFloor.length === unitiesByFloor.length){

      _.remove(this.selectedUnitiesTemp, (selectedTempId) => {
        return _.find(selectedByFloor, (selectFloorId) => selectFloorId === selectedTempId )
      })

      return this.setState({ selectedUnities:  this.selectedUnitiesTemp })

    }

    this.selectedUnitiesTemp = this.state.selectedUnities.concat(_.map(unitiesByFloor, 'id'))

    this.setState({ selectedUnities: this.state.selectedUnities.concat(_.map(unitiesByFloor, 'id')) })

  }

  selectNotificationTab(optionTab){
    this.setState({ notificationOptionTab: optionTab})
    this.setState({ isSelectionMode: true})
  }

  navigationNext(){
    if(this.state.isSelectionMode) {
      // if(this.state.selectedUnities){
      // }
      this.setState({isSelectionMode: false})
      this.setState({isMessageConfirmation: true})
    }

    $.ajax({
      url: `/api/projects/${this.props.pslug}/deeds/templates/?name=notification`
    })
    .done((data) => {
      let template = `<div id="focusNotificationEditing" style="font-family:Arial; font-style:normal; font-weight:200; font-size:14px; text-align:justify">
        <p>Bucaramanga, <span>{{date}}</span></p>
        <p>
          Señor (es): <br>
          <span>{{names}}</span> <br>
          Propietario(s) Apartamento <span>{{unity}}</span> Torre <span>{{tower}}</span> <br>
          Proyecto <span>{{project}}</span>
        </p>

        <p>Ref: Proyecto <span>{{project}}</span></p>

        <p>
          Por medio de la presente le informamos que a partir del día 15 de Octubre de 2016, el apartamento adquirido por usted(es) en el proyecto <span>{{project}}</span>, se encontrará listo para iniciar trámite de escrituración. <br>
          <br>
          Les recordamos que para esta fecha también deben tener una suma aproximada de DOS MILLONES QUINIENTOS MIL PESOS ($2.500.000) M/CTE, para gastos de escrituración y registro, según estipulado en la promesa de compraventa. <br>
          <br>
          Le solicitamos ponerse en contacto con nuestra oficina ubicada en la calle 30A No. 30 -18 ubicada en el Barrio la Aurora en Bucaramanga; con el fin de resolver cualquier inquietud que se pueda presentar.
          <br>
          <br>
          <br>
          Agradezco su atención,
        </p>
        <br>
        <br>
        <br>
        <br>
        <p>
          <div style="font-weight:bold">PEDRO CLAVER GÓMEZ VEGA</div>
          Representante Legal
        </p>
      </div>`

      $('#contentNotificationTemplate').html(template)

    })
    .fail((error)=> console.log(error))

  }

  navigationCancel(){
    if(this.state.isMessageConfirmation){
      this.setState({isMessageConfirmation: false})
      return this.setState({isSelectionMode: true})
    }
    if(this.state.isSelectionMode){
      return this.setState({isSelectionMode: false})
    }
    this.setState({isMessageConfirmation: false})
    return this.setState({ isSelectionMode: false })
  }

  editTemplateNotification(){
    this.setState({editingTemplateNotification: true})

    _.each($('#contentNotificationTemplate span'), (spanTag) => spanTag.setAttribute('contenteditable', 'false'))

    $('#focusNotificationEditing')[0].setAttribute('contenteditable', 'true')

    $('#focusNotificationEditing').focus()
  }

  sendNotification(){

    let snippet = _.map($('#contentNotificationTemplate').html().split(''), (c)=>{
      switch (c) {
        case 'á':
          c = '&aacute;'
          break;
        case 'é':
          c = '&eacute;'
          break;
        case 'í':
          c = '&iacute;'
          break;
        case 'ó':
          c = '&oacute;'
          break;
        case 'ú':
          c = '&uacute;'
          break;
        case 'Á':
          c = '&Aacute;'
          break;
        case 'É':
          c = '&Eacute;'
          break;
        case 'Í':
          c = '&Iacute;'
          break;
        case 'Ó':
          c = '&Oacute;'
          break;
        case 'Ú':
          c = '&Uacute;'
          break;
        case 'ñ':
          c = '&ntilde;'
          break;
        default:

      }
     return c

    }).join('')

    let payload = {
      unities: this.state.selectedUnities,
      html_snippet: snippet
    }

    console.log(payload)

    $.ajax({
      method: 'POST',
      url: `/api/projects/${this.props.pslug}/deeds/send-notifications/`,
      data: JSON.stringify(payload),
      headers: {
        'X-CSRFToken': getCsrfToken('csrftoken'),
        'Content-Type' : 'application/json'
      }

    })
    .done((data) => {
      console.log(data);
    })
    .fail((error)=> console.log(error))

  }

  render(){
    return (
      <div className="Writing__table">

        <div className={`Writing__editTemplatePopup ${this.state.isMessageConfirmation ? '' : 'displayNone' }`}>
          <div>
            <nav className="Writing__editTemplateNav">
              <div onClick={this.navigationCancel}>CANCELAR</div>
              <div>Confirma el mensaje de la notificación</div>
              <div onClick={this.sendNotification}>GUARDAR Y ENVIAR</div>
            </nav>
            <div className="Writing__editTemplateContainer">
              <div className="Writing__editTemplateHead">
                <div>Edita el mensaje del documento</div>
                <div onClick={this.editTemplateNotification}>{ `${this.state.editingTemplateNotification ? 'Editando' : 'Editar'}` }</div>
              </div>
              <div id="contentNotificationTemplate" className="Writing__editTemplateBody">

              </div>
            </div>
          </div>
        </div>

        <div className={`Writing__notificationsTab ${this.state.isSelectionMode || this.state.isMessageConfirmation ? 'displayNone' : '' }`}>
          Deseas:
          <div onClick={()=> this.selectNotificationTab('sendNotification') }>Enviar notificaciones</div>
          <div onClick={()=> this.selectNotificationTab('generateDocuments') }>Generar documentos</div>
        </div>

        <nav className={`Writing__navSelectionMode ${this.state.isSelectionMode || this.state.isMessageConfirmation ? '' : 'displayNone' }`}>
          <div onClick={this.navigationCancel}> ATRAS </div>
          <div> Selecciona los apartamentos </div>
          <div onClick={this.state.isSelectionMode ? this.navigationNext : this.sendNotification}> { `${this.state.isSelectionMode ? 'SIGUIENTE' : 'GUARDAR Y ENVIAR'}` } </div>
        </nav>

        <div className="Writing__tableHead">
          <div className="Writing__tableHeadLabels">
            <div>Apto</div>
            <div>Piso</div>
          </div>
          <div>1</div>
          <div>2</div>
          <div>3</div>
          <div>4</div>
          <div>5</div>
          <div>6</div>
          <div>7</div>
          <div>8</div>
          <div>9</div>
          <div>10</div>
          <div>11</div>
        </div>

        <div className="Writing__tableBody">

          {
            this.props.floors ? this.props.floors.map((floor, key)=>{

              return <div className="Writing__tableRow" key={key}>
                <div className="Writing__floor" onClick={ () => this.selectAllByFloor( key + 1 ) }> {key + 1} </div>

                {
                  _.sortBy(_.filter(this.props.unitiesByTower, {floor: floor}), (u)=> u.apto ).map((unity, key) => {

                    return <div key={key}>
                      <div className={`Writing__apto ${this.state.isSelectionMode ? 'Writing__aptoHover' : ''}`} onClick={ (e)=> this.checkHandler(e, unity) }>
                        <IconEmail />
                        <IconCheck isCheck={ (_.find(this.state.selectedUnities, (unityId) => unityId === unity.id )) ? true : false } />
                        { unity.slug.toUpperCase() }
                      </div>
                    </div>

                  })
                }

              </div>
            }) : null
          }

        </div>

      </div>
    )
  }
}
