import React, { Component } from 'react'

export default class WritingHeader extends Component {

  constructor(props){
    super(props)
  }

  componentDidMount(){

  }

  render(){

    return (
        <div className="Writing__header">
          <div className="Writing__selectTower">
            <select name="" id="" onChange={ (e)=> this.props.updateSelectTower(e) }>
              {
                this.props.towers.map((tower, key) => {
                  return <option value={tower} key={key}>Torre {tower.slice(1)}</option>
                })
              }

            </select>
          </div>
          <div className="Writing__steps">
            <div className="Writing__step">
              <div className="Writing__stepsTitle">1 Paso</div>
              <div className="Writing__stepsDescription">Notificaciones</div>
              <div className="Writing__stepsCounter">20</div>
            </div>
            <div className="Writing__step">
              <div className="Writing__stepsTitle">2 Paso</div>
              <div className="Writing__stepsDescription">Validación de pagos</div>
              <div className="Writing__stepsCounter">20</div>
            </div>
            <div className="Writing__step">
              <div className="Writing__stepsTitle">3 Paso</div>
              <div className="Writing__stepsDescription">Orden de escrituración</div>
              <div className="Writing__stepsCounter">20</div>
            </div>
            <div className="Writing__step">
              <div className="Writing__stepsTitle">4 Paso</div>
              <div className="Writing__stepsDescription">Orden de escrituración</div>
              <div className="Writing__stepsCounter">20</div>
            </div>
            <div className="Writing__step">
              <div className="Writing__stepsTitle">5 Paso</div>
              <div className="Writing__stepsDescription">Orden de escrituración</div>
              <div className="Writing__stepsCounter">20</div>
            </div>
          </div>
        </div>

      )
  }
}
