import React, { Component } from 'react'

import WritingHeader from './components/index/_writing-header'
import WritingTable from './components/index/_writing-table'
import WritingTasks from './components/index/_writing-tasks'
import WritingSendNotificationPopup from './components/index/_writing-sendNotificationPopup'

export default class Writing extends Component {

  constructor(props){
    super(props)

    this.pslug = window.location.href.split('/')[4]

    this.state = {
      unities: [],
      unitiesByTower: [],
      towers: [],
      floors: [],
      towerSelected: ''
    }

    this.prepareUnities = this.prepareUnities.bind(this)
    this.updateSelectTower = this.updateSelectTower.bind(this)

  }

  componentDidMount(){
    $.ajax({
      url: `/api/projects/${this.pslug}/unities/`
    })
    .done((data) => {
      this.prepareUnities(data)

    })
    .fail((error)=> console.log(error))
  }

  prepareUnities(unities){

    let towers = []
    let floors = []

    _.forEach(unities, (unity) => {

      let splitedUnity = unity.slug.split('-')

      let floorApto = splitedUnity[1]
      unity.tower = splitedUnity[0]

      if(floorApto !== undefined && floorApto.length === 3){
        unity.floor = floorApto.substring(0,1)
        unity.apto = floorApto.substring(1,3)
      }
      if(floorApto !== undefined && floorApto.length == 4){
        unity.floor = floorApto.substring(0,2)
        unity.apto = floorApto.substring(2,4)
      }

      towers.push(unity.tower)
      floors.push(unity.floor)
    })

    towers = _.uniq(towers.sort())
    floors = _.uniq(floors.sort())

    this.setState({unities})
    this.setState({towers})
    this.setState({floors: _.sortBy(floors.map((floor) => parseInt(floor))) }, () => {
      this.updateSelectTower()
    })

  }

  updateSelectTower(e){

    let unitiesByTowerTemp = _.filter(this.state.unities, { tower: e ? e.target.value : this.state.towers[0] })

    this.setState({unitiesByTower: unitiesByTowerTemp.reverse()}, ()=>{

      this.setState({ floors: _.uniq(_.map(this.state.unitiesByTower, 'floor')) })

    })

  }

  render(){
    return (
      <section className="Writing">

        {/* <WritingHeader
          towers={this.state.towers}
          updateSelectTower={this.updateSelectTower}
        /> */}

        <div className="Writing__body">

          {/* <WritingTasks /> */}

          <div className="Writing__bodyContent">

            <WritingSendNotificationPopup unitiesByTower={this.state.unitiesByTower} quantityUnities={this.state.unitiesByTower.length}/>

            <WritingTable
              unitiesByTower={this.state.unitiesByTower}
              floors={this.state.floors}
              pslug={this.pslug}
              sendNotification={this.sendNotification}
              isSelectionMode={this.state.isSelectionMode}
              isMessageConfirmation={this.state.isMessageConfirmation}
            />

          </div>

        </div>


      </section>
    )
  }
}
