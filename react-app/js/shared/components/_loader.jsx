import React, {Component} from 'react'

export default class Loader extends Component{

  constructor(props){
    super(props)
  }

  render(){
    return (
      <section className={`Loader ${this.props.loading ? '' : 'displayNone'}`}>
        <div className="Loader__showbox">
          <div className="Loader__loader">
            <svg className="Loader__circular" viewBox="25 25 50 50">
              <circle className="Loader__path" cx="50" cy="50" r="20" fill="none" strokeWidth="2" strokeMiterlimit="10"/>
            </svg>
          </div>
        </div>
      </section>
    )
  }
}
