import React, {Component} from 'react'
import { Link } from 'react-router'
import PaymentsHeader from './_payments-header'
import PaymentPlanTable from './_payments-plan-table'
import { IconDate, IconProfits, IconPayments } from './svg/_svg-icons'
import { IconNewPayment } from '../newpayment/svg/_svg-icons'
import AddPayment from './_addpayment'

export default class Payments extends Component{

  constructor(props){
    super(props)
    this.pslug = window.location.href.split('/')[4]
    this.payments_types = {'cuota-mensual' : 'Cuota Mensual',
                           'ahorro-programado' : 'Ahorro Programado',
                           'cesantias': 'Cesantías',
                           'subsidio-vivienda' : 'Subsidio de Vivienda',
                           'otro-pago-acordado' : 'Otro Pago Acordado',
                           'gastos-notariales' : 'Gastos Notariales',
                           'otros-conceptos' : 'Otros Conceptos',
                           'credito-hipotecario' : 'Crédito Hipotecario',
                           'kit-acabados' : 'Kit de Acabados',
                           'cambios-modificaciones' : 'Cambios/Modificaciones',
                           'descuento-financiero' : 'Descuento Financiero'
                          }
    this.medium_choices = {'bancolombia' :'Bancolombia',
                           'bbva' : 'BBVA',
                           'banco-bogota' : 'Banco de Bogotá',
                           'davivienda' : 'Davivienda',
                           'cheque-gerencia' : 'Cheque de gerencia',
                           'efectivo' : 'Efectivo',
                           'fiduciaria-bogota' : 'Fiduciaria Bogotá',
                           'otro' : 'Otro'
                           }

    this.state = {
      tabSelected: 1,
      dataInfo: {}
    }

  }

  componentWillMount(){
    $.ajax({
      method: 'GET',
      url: `/api/projects/${this.pslug}/formalizations/payment-agreements/detail/${this.props.params.id}`
    })
    .done((data)=> {
      this.setState({dataInfo: data})
    })
    .fail((error)=> console.log(error))
  }

  componentDidMount(){
    this.$paymentPlanTab = $('#paymentPlanTab')
    this.$paymentTab = $('#paymentTab')
  }

  paymentSelectTab(tab){
    this.setState({tabSelected: tab})
  }

  openAddpayment(){
    let $AddPayment = $('#AddPayment')
    $AddPayment.fadeIn(200)
    $AddPayment.css({display:'flex'})
  }

  render(){
    return (
      <section className="Payments">

      <PaymentsHeader
        debtStatus={false}
        clientInfo={this.state.dataInfo.persons ? this.state.dataInfo.persons[0] : {} }
        unity={this.state.dataInfo.unity}
        agreedPayments={this.state.dataInfo.agreed_payments}
      />

        <div className="Payments__body">
          <div className="Payments__tabs">
            <div id="paymentPlanTab" onClick={()=> this.paymentSelectTab(1)} className={ this.state.tabSelected === 1 ? 'paymentTabSelected' : '' }><IconNewPayment />Plan de pagos</div>
            <div id="paymentTab" onClick={()=> this.paymentSelectTab(2)} className={ this.state.tabSelected === 2 ? 'paymentTabSelected' : '' }><IconPayments />Pagos</div>
          </div>

          <div className="Payments__content">

            <PaymentPlanTable tabSelected={this.state.tabSelected} dataInfo={this.state.dataInfo}/>

            <div className={`Payments__paymentsTable ${ this.state.tabSelected === 2 ? 'showPaymentTable' : '' }`}>
              <a onClick={this.openAddpayment}>AGREGAR PAGO</a>
              <div className="Payments__paymentsTableHead">
                <div>No.</div>
                <div><IconDate/> Fecha</div>
                <div>Valor del pago</div>
                <div><IconProfits />Tipo</div>
                <div>Medio</div>
                <div>Obervaciones</div>
              </div>

              {
                this.state.dataInfo.payments ? this.state.dataInfo.payments.map((payment, index) => {

                  return <div className="Payments__paymentsTableRow" key={index}>
                    <div>{index + 1}</div>
                    <div>{payment.date}</div>
                    <div>$ {payment.amount.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")}</div>
                    <div>{ this.payments_types[payment.type_of_payment] }</div>
                    <div>{ this.medium_choices[payment.medium] }</div>
                    <div>Observación
                      <span className="Payments__paymentsTableView">ver</span>
                      <span className="Payments__paymentsTableCount">1</span>
                    </div>
                  </div>
                }) : []
              }


            </div>

          </div>
        </div>

      <AddPayment />

      </section>
    )
  }
}
