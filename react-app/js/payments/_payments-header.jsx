import React, {Component} from 'react'
import {IconUserInfo} from './svg/_svg-icons'

export default class PaymentsHeader extends Component {
  constructor(props){
    super(props)
  }

  showViewMore(){
    $('#viewMoreContent').css({display: 'flex'})
  }

  viewMoreClose(){
    $('#viewMoreContent').css({display: 'none'})
  }

  render(){

    if(this.props.clientInfo.id){
      return (
        <div className="Payments__header">
          <div className="Payments__userInfo">
            <div className="Payments__userAvatar">
              <div>JE</div>
                <h4>{this.props.clientInfo.first_name}</h4>
              <span>{this.props.unity}</span>
            </div>

            {
              this.props.debtStatus ? (
                <div className="Payments__debtStatus">
                  <div>
                    <h5>Estado</h5>
                    <span>EN DEUDA</span>
                  </div>
                  <div>
                    <h5>Deuda total</h5>
                    <span>$90.000.000</span>
                  </div>
                  <div>
                    <h5>Deuda total en mora</h5>
                    <span>$90.000.000</span>
                  </div>
                </div>
              ) : ''
            }

            <div className="Payments__contactInfo">
              <h4><IconUserInfo />INFORMACION DE CONTACTO</h4>
              <div className="Payments__viewMoreContent" id="viewMoreContent">
                <div>
                  <span className="Payments__viewMoreClose" onClick={this.viewMoreClose}>x</span>
                  <div className="Payments__viewMoreTriangle"></div>
                  <div>Teléfono de Notificación</div>
                  <div>(037) {this.props.clientInfo.notification_phone}</div>
                </div>
                <div>
                  <div>Dirección de Notificación</div>
                  <div>{this.props.clientInfo.notification_address}</div>
                </div>
              </div>
              <span className="Payments__contactInfoViewMore" onClick={this.showViewMore}>Ver más</span>
              <div className="Payments__contactInfoCont">
                <div className="Payments__contactInfoCellphone">
                  <div>Celular</div>
                  <span>{this.props.clientInfo.cellphone}</span>
                </div>
                <div className="Payments__contactInfoPhone">
                  <div>Teléfono</div>
                  <span>(037) {this.props.clientInfo.phone}</span>
                </div>
                <div className="Payments__contactInfoEmail">
                  <div>Correo</div>
                  <span>{this.props.clientInfo.email}</span>
                </div>
              </div>
            </div>

            <div className="Payments__unityInfoLarge">
              {this.props.unity}
            </div>
          </div>

          <div className="Payments__othersPayments">
            <h4><span>$</span>OTROS PAGOS ACORDADOS</h4>
            <div className="Payments__othersPaymentsCont">
              <div className="Payments__othersPaymentsScheduledSavings">
                <div>Ahorro programado</div>
                <div>$ {this.props.agreedPayments[0].fee.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.") }</div>
                <div>CANCELADO</div>
              </div>
              <div className="Payments__othersPaymentsSeverance">
                <div>Cesantías</div>
                <div>$ {this.props.agreedPayments[1].fee.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")}</div>
                <div>SIN CANCELAR</div>
              </div>
              <div className="Payments__othersPaymentsHousingAllowance">
                <div>Subsidio de vivieda</div>
                <div>$ {this.props.agreedPayments[2].fee.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")}</div>
                <div>CANCELADO</div>
              </div>
              <div className="Payments__othersPaymentsOthers">
                <div>Otros</div>
                <div>$ {this.props.agreedPayments[3].fee.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")}</div>
                <div>CANCELADO</div>
              </div>
            </div>
          </div>
        </div>
      )
    }

    return <div></div>

  }
}
