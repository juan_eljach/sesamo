import React, {Component} from 'react'
import { Link } from 'react-router'
import PaymentsHeader from './_payments-header'
import PaymentPlanTable from './_payments-plan-table'
import { IconDate, IconProfits, IconPayments } from './svg/_svg-icons'
import { IconNewPayment } from '../newpayment/svg/_svg-icons'
import AddPayment from './_addpayment'
import { IconSearch } from './svg/_svg-icons.jsx'
import ReactPaginate from 'react-paginate'

export default class PaymentsUnity extends Component{

  constructor(props){
    super(props)

    this.state = {
      tabSelected: 1,
      addPaymentUnity: ''
    }

  }

  componentDidMount(){
    this.$paymentPlanTab = $('#paymentPlanTab')
    this.$paymentTab = $('#paymentTab')
  }

  handleShowCount(){

  }

  openViewBusinessPayment(e, unity){
    this.setState({addPaymentUnity: unity})
    $('#AddPayment').css({display:'flex'})
  }

  render(){
    return (
      <section className="PaymentsUnity">
        <div className="PaymentsUnity__header">
          <div className="PaymentsUnity__search">
            <IconSearch />
            <input type="text" placeholder="Busca por persona o apto"/>
          </div>
          <div className="PaymentsUnity__showPlans">
            <div>Mostrar</div>
            <select name='' id='' onChange={(e) => this.handleShowCount(e)}>
              <option value='10'>10</option>
              <option value='20'>20</option>
              <option value='30'>30</option>
            </select>
            <div>planes</div>
            <div>AGREGAR PAGO</div>
          </div>
        </div>

        <div className="PaymentsUnity__table">
          <div className="PaymentsUnity__tableHead">
            <div>Cliente</div>
            <div>Apartamento</div>
            <div>Estado</div>
            <div>Cuotas en mora</div>
            <div>Valor en mora</div>
            <div>Tiempo en mora</div>
            <div></div>
            <div></div>
          </div>
          <div className="PaymentsUnity__tableBody">
            <div className="PaymentsUnity__tableRow">
              <div>Victor Aguirre</div>
              <div>T4301</div>
              <div>EN DEUDA</div>
              <div>3</div>
              <div>150.000.000</div>
              <div>4 horas</div>
              <div><Link to="viewbusiness/1">Ver negocio</Link></div>
              <div onClick={(e)=>this.openViewBusinessPayment(e, 'T-101')}>Agregar pago</div>
            </div>
          </div>
        </div>

        <div className="PaymentsUnity__pagination">
          <ReactPaginate previousLabel={'ANTERIOR'}
            nextLabel={'SIGUIENTE'}
            breakLabel={<a>...</a>}
            breakClassName={'bQqreak-me'}
            activeClassName={'isActivePage'}
            // pageCount={Math.ceil(this.initialData.length / this.state.showCount)}
            // onPageChange={this.handlePageClick.bind(this)}
          />
        </div>

      <AddPayment addPaymentUnity={this.state.addPaymentUnity}/>
      </section>
    )
  }
}
