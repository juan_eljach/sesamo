import React, {Component} from 'react'
import { Link } from 'react-router'
import moment from 'moment'
import getCookie from '../lib/csrftoken'
import { IconSearch, IconDate, IconProfits, IconMuseum } from './svg/_svg-icons.jsx'

export default class AddPayment extends Component{

  constructor(props){
    super(props)
    this.pslug = window.location.href.split('/')[4]


    this.medium_choices = {'Bancolombia' : 'bancolombia',
                           'BBVA' : 'bbva',
                           'Banco de Bogotá' : 'banco-bogota',
                           'Davivienda' : 'davivienda',
                           'Cheque de gerencia' : 'cheque-gerencia',
                           'Efectivo' : 'efectivo',
                           'Fiduciaria Bogotá' : 'fiduciaria-bogota',
                           'Otro' : 'otro'
                           }
    this.payments_types = {'Cuota Mensual' : 'cuota-mensual',
                           'Ahorro Programado' : 'ahorro-programado',
                           'Cesantías' : 'cesantias',
                           'Subsidio de Vivienda' : 'subsidio-vivienda',
                           'Otro Pago Acordado' : 'otro-pago-acordado',
                           'Gastos Notariales' : 'gastos-notariales',
                           'Otros Conceptos' : 'otros-conceptos',
                           'Crédito Hipotecario' : 'credito-hipotecario',
                           'Kit de Acabados' : 'kit-acabados',
                           'Cambios/Modificaciones' : 'cambios-modificaciones',
                           'Descuento Financiero' : 'descuento-financiero'
                          }


    this.state = {
      unities: [],
      unitySelected: {},
      paymentAgreements: 0,
      AddPaymentDate: true,
      AddPaymentValue: true,
      AddPaymentType: true,
      AddPaymentMedium: true,
    }

  }

  componentDidMount(){

    $('.AddPayment__results').css({display: 'none'})

    $('[data-toggle="datepicker"]').datepicker({format: 'yyyy-mm-dd', autoHide: true});

    $('#AddPaymentDate').on('pick.datepicker', ()=> {
      this.setState({AddPaymentDate: true})
      $('#AddPaymentValue').focus()
    })


  }

  searchUnity(e){

    if(e.target.value === '') $('.AddPayment__results').fadeOut(200)
    else $('.AddPayment__results').fadeIn(200)

    $.ajax({
      method: 'GET',
      url: `/api/projects/${this.pslug}/search-unities/?search=${e.target.value}`
    })
    .done((data) => {
      console.log(data)
      this.setState({unities: data})
    })
    .fail((error)=> console.log(error))
  }

  inputUpdate(e, inp){

    if(e.target.value == '') this.setState({[inp]: false})

    else{
      if(e.target.id === 'AddPaymentValue') $('#AddPaymentValue').val(e.target.value.split('.').join('').toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."))

      this.setState({[inp]: true})

    }
  }

  selectUnity(unity){
    $('#addPaymentSearchUnity').removeClass('isNotValid')
    $('.AddPayment__unitySelected').fadeIn(200)
    $('.AddPayment__results').fadeOut(200)
    $('#searchUnity').val('')
    this.setState({unitySelected: unity})

    $.ajax({
      method: 'GET',
      url: `/api/projects/${this.pslug}/formalizations/payment-agreements/?unity=${unity.id}`
    })
    .done((data)=> this.setState({paymentAgreements: data[0].id}))
    .fail((error)=> console.log(error))

  }

  closeUnity() {
    $('.AddPayment__unitySelected').fadeOut(200)
    this.setState({unitySelected: {}})
  }

  updatePaymentValue(e){
    console.log(e.target.id)
    $('#AddPaymentValue').val(e.target.value.split('.').join('').toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."))
  }

  closeAddpayment(){
    $('#AddPayment').fadeOut(200)
    $('#AddPaymentSucessMsg').css({display:'none'})
  }

  savePayment(){

    if(!this.state.unitySelected.id) $('#addPaymentSearchUnity').addClass('isNotValid')

    if(!$('#AddPaymentDate').val()) this.setState({AddPaymentDate: false})

    if(!$('#AddPaymentValue').val()) this.setState({AddPaymentValue: false})

    if(!$('#AddPaymentType').val()) this.setState({AddPaymentType: false})

    if(!$('#AddPaymentMedium').val()) this.setState({AddPaymentMedium: false})

    let isFillAll = this.state.unitySelected.id && $('#AddPaymentDate').val() && $('#AddPaymentValue').val() && $('#AddPaymentType').val() && $('#AddPaymentMedium').val() ? true : false

    if(isFillAll){
      let data = { payment_agreement: this.state.paymentAgreements, date: $('#AddPaymentDate').val(), amount: $('#AddPaymentValue').val().split('.').join(''), type_of_payment: this.payments_types[$('#AddPaymentType').val()], medium: this.medium_choices[$('#AddPaymentMedium').val()], comment: $('#AddPaymentComment').val()}
      $.ajax({
        method: 'POST',
        url: `/api/projects/${this.pslug}/payment/create/`,
        headers: {
          'X-CSRFToken': getCookie('csrftoken')
        },
        data: data
      })
      .done((data) => {
        console.log(data)
        _.forEach($('.AddPayment__form input'), (el)=> el.value = '' )
        _.forEach($('.AddPayment__form select'), (el)=> el.value = '' )
        this.setState({unitySelected: {}})
        $('#unitySelected').css({display: 'none'})
        $('#AddPaymentSucessMsg').css({display:'flex'})

        if(this.props.addPaymentUnity) window.location.href = "/projects";
      })
      .fail((error)=> console.log(error))
    } else{
      console.log('Inputs not fill')
    }
  }

  render(){
    return (
      <section className="AddPayment" id="AddPayment">
        <div className="AddPayment__form">
          <div className="AddPayment__formHead">
            <h2>Nuevo pago</h2>
            <span onClick={this.closeAddpayment}>x</span>
          </div>
          <div className="AddPayment__searchUnity">
            <IconSearch />
            <input type="text" id="addPaymentSearchUnity" placeholder="Buscar unidad" onChange={this.searchUnity.bind(this)}/>
            <span className="AddPayment__unitySelected" id="unitySelected"> { this.state.unitySelected.name } <span className="AddPayment__chipClose" onClick={this.closeUnity.bind(this)}>x</span></span>
            <span className={`AddPayment__unitySelectedUnity ${this.props.addPaymentUnity ? '' : 'displayNone'}`}> { this.props.addPaymentUnity } <span className="AddPayment__chipClose">x</span></span>
            <div className="AddPayment__results">
              {
                this.state.unities.map((unity, k)=>{
                  return <div key={k} onClick={ ()=> this.selectUnity(unity) }> {unity.name} </div>
                })
              }
            </div>
          </div>
          <div className="AddPayment__date">
            <label htmlFor="">Fecha</label>
            <IconDate />
            <input id="AddPaymentDate" type="text" data-toggle="datepicker" onChange={ (e)=> this.inputUpdate(e, 'AddPaymentDate') } className={ this.state.AddPaymentDate ? "isValid" : "isNotValid" }/>
          </div>

          <div className="AddPayment__paymentValue">
            <label htmlFor="">Valor del pago</label>
            <span>$</span>
            <input id="AddPaymentValue" type="text" onChange={ (e)=> this.inputUpdate(e, 'AddPaymentValue') } className={ this.state.AddPaymentValue ? "isValid" : "isNotValid" }/>
          </div>

          <div className="AddPayment__type">
            <label htmlFor="">Tipo</label>
            <IconProfits />
            <select name="" id="AddPaymentType" onChange={ (e)=> this.inputUpdate(e, 'AddPaymentType') } className={ this.state.AddPaymentType ? "isValid" : "isNotValid" }>
              <option value="">Selecciona el tipo de pago</option>
              {
                Object.keys(this.payments_types).map( (el, k)=>{
                  return <option value={el} key={k}>{el}</option>
                })
              }
            </select>
          </div>
          <div className="AddPayment__method">
            <label htmlFor="">Medio</label>
            <IconMuseum />
            <select name="" id="AddPaymentMedium" onChange={ (e)=> this.inputUpdate(e, 'AddPaymentMedium') } className={ this.state.AddPaymentMedium ? "isValid" : "isNotValid" }>
              <option value="">Selecciona el medio de pago</option>
              {
                Object.keys(this.medium_choices).map( (el, k)=>{
                  return <option value={el} key={k}>{el}</option>
                })
              }
            </select>
          </div>
          <div className="AddPayment__Obervations">
            <label htmlFor="">Observaciones</label>
            <input id="AddPaymentComment" type="text"/>
          </div>
          <div className="AddPayment__button">
            <button onClick={this.savePayment.bind(this)}>GUARDAR</button>

          </div>
          <span className="AddPayment__successMsg" id="AddPaymentSucessMsg">El pago se guardó correctamente</span>
        </div>

      </section>
    )
  }
}
