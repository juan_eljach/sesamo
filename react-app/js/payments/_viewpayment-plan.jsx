import React, {Component} from 'react'
import PaymentsHeader from './_payments-header'
import PaymentPlanTable from './_payments-plan-table'

export default class ViewPaymentPlan extends Component {
  constructor(props){
    super(props)
  }

  render(){
    return (
      <section className="ViewPaymentPlan">
        <PaymentsHeader debtStatus={true}/>
        <PaymentPlanTable marginTop110={true}/>
      </section>
    )
  }
}
