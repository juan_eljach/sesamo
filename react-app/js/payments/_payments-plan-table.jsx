import React, {Component} from 'react'
import { IconDate, IconProfits, IconComments, IconPayments, IconDown } from './svg/_svg-icons'
import moment from 'moment'

export default class PaymentPlanTable extends Component{
  constructor(props){
    super(props)

  }

  componentDidMount(){
    console.log(this.props.dataInfo)
  }

  render(){
    return (
      <div className={`Payments__paymentsPlanTable ${ this.props.tabSelected === 1 ? 'showPaymentTable' : '' } ${this.props.marginTop110 ? 'u-marginTop110' : ''}`}>

        <div className="Payments__paymentsPlanTableHead">
          <div>Cuota</div>
          <div><IconDate />Fecha</div>
          <div>Valor de la cuota</div>
          <div>Cuota extraordinaria</div>
          <div><span>$</span>Saldo</div>
          <div></div>
          <div>Comentarios</div>
        </div>

        {
          this.props.dataInfo.payment_fees ? this.props.dataInfo.payment_fees.map((payment, index) => {

            let currentDate = moment(Date.now()).format('YYYY-MM-DD')
            let payDate = payment.date_to_pay

            let currentDateCount = moment(currentDate).diff(currentDate)
            let payDateCount = moment(payDate).diff(currentDate, 'days')

            let paymentState = ''
            let paymentStateColor = ''

            if(payment.paid) paymentState = 'CANCELADO'
            else if(payment.partially_paid && payDateCount > currentDateCount) paymentState = 'PAGO PARCIAL'
            else if(payment.paid === false && payDateCount < currentDateCount) paymentState = 'EN MORA'
            else if(payment.paid === false && payment.partially_paid === false && payDateCount > currentDateCount) paymentState = 'SIN CANCELAR'

            switch (paymentState) {
              case 'CANCELADO':
                paymentStateColor = 'greenColor'
                break;
              case 'PAGO PARCIAL':
                paymentStateColor = 'greenColor'
                break;
              case 'EN MORA':
                paymentStateColor = 'redColor'
                break;
              case 'SIN CANCELAR':
                paymentStateColor = 'grayColor'
                break;
              default: ''
            }

            return (
              <div className="Payments__paymentsPlanTableRow" key={index}>
                <div>{index + 1}</div>
                <div>{payment.date_to_pay}</div>
                <div>$ {payment.fee.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")}</div>
                <div>$ {payment.extraordinary_fee.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")}</div>
                <div>$ {payment.debt.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")}</div>
                <div className={paymentStateColor}>{paymentState}</div>
                <div>Comentario
                  <IconComments />
                  <span className="Payments__paymentsPlanTableView">ver</span>
                  <span className="Payments__paymentsPlanTableCount">1</span>
                </div>
              </div>
            )
          }) : []
        }

      </div>
    )
  }
}
