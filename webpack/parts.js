const webpack = require('webpack')
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const path = require('path')
const PATHS = require('./paths')
const root = path.resolve('./')

exports.commonDev = {
  resolve: {
    root: path.resolve('server-react'),
    modulesDirectories: ['node_modules'],
    alias: {
      scss: '../scss/app'
    },
    extensions: ['', '.js', '.jsx', '.scss']
  },
  entry: [
    'webpack-hot-middleware/client?path=http://localhost:8080/__webpack_hmr',
    path.resolve(root, PATHS.entry)
  ],
  output: {
    path: path.resolve(root, PATHS.dist),
    filename: 'app.js',
    publicPath: 'http://127.0.0.1:8080/dist/'
  }
}

exports.commonProd = {
  resolve: {
    root: path.resolve('react-app'),
    modulesDirectories: ['node_modules'],
    alias: {
      scss: 'scss/app'
    },
    extensions: ['', '.js', '.jsx', '.scss']
  },
  entry: {
    reactapp: path.resolve(root, PATHS.entry),
    reactstyles: path.resolve(root, PATHS.style),
    vendor: ['react', 'react-dom', 'react-router', 'moment', 'react-tap-event-plugin']
  },
  output: {
    path: path.resolve(root, PATHS.dist),
    filename: '[name].js',
    publicPath: '/dist/'
  }
}

exports.devServer = {
  devServer: {
    contentBase: path.resolve('./dist'),
    historyApiFallback: true,
    hot: true,
    inline: true,
    noInfo: true
  }
}
exports.loaderJS = {
  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        loader: 'babel',
        include: path.resolve(root, PATHS.js),
        query: {
          cacheDirectory: true,
          presets: ['react-hmre']
        }
      }
    ]
  }
}

exports.loaderCSS = {
  module: {
    loaders: [
      {
        test: /\.scss$/,
        loaders: ['style', 'css', 'sass'],
        include: path.resolve(root, PATHS.scss)
      }
    ]
  }
}
exports.hotPlugin = {
  plugins: [
    new webpack.HotModuleReplacementPlugin({
      multiStep: true
    })
  ]
}

exports.minify = {
  plugins: [
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false
      }
    })
  ]
}

exports.setFreeVariable = function(key, value) {
  const env = {}
  env[key] = JSON.stringify(value);

  return {
    plugins: [
      new webpack.DefinePlugin(env)
    ]
  };
}

exports.commonsChunk = {
  plugins: [
    // Extract bundle and manifest files. Manifest is
    // needed for reliable caching.
    new webpack.optimize.CommonsChunkPlugin({
      names: ['vendor', 'manifest']
    })
  ]
}

exports.extractCSS = function(paths) {
  return {
    module: {
      loaders: [
        // Extract CSS during build
        {
          test: /\.scss$/,
          loader: ExtractTextPlugin.extract('style', 'css!sass'),
          include: path.resolve(root, PATHS.scss)
        }
      ]
    },
    plugins: [
      // Output extracted CSS to a file
      new ExtractTextPlugin('[name].css')
    ]
  };
}
