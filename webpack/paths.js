module.exports = {
  entry: 'react-app/js/app.jsx',
  js: 'react-app/js',
  style: 'react-app/scss/app.scss',
  scss: 'react-app/scss',
  dist: 'static/js/'
};
