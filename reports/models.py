from django.db import models

class Report(models.Model):
    report_file = models.FileField()
    timestamp = models.DateTimeField(auto_now_add=True)
        