import csv
import random
from hashlib import sha1
from itertools import groupby
from django.db.models import Sum, Q
from django.conf import settings
from datetime import date, datetime, timedelta
from dateutil.relativedelta import relativedelta
from accounts.models import UserProfile
from formalizations.choices import types_of_agreed_payments
from formalizations.models import PaymentAgreement
from payments.models import Payment
from projects.models import Project
from persons.models import Person, segment_choices, PersonSegmentLogRecords
from units.models import Unity
from tablib import Dataset, Databook

class OperationalReport():
	static_headers = [
		'id',
		'Nombre',
		'Leads Totales',
		'Leads Vendidos',
		'Leads al Pool',
		'Leads al Pool que NO eran poco probables'
	]

	def __init__(self, project_id, user_ids):
		assert project_id
		assert user_ids
		self.file_name = 'reporte_operacional_vendedores.xls'
		self.final_date = datetime.today()
		self.project = Project.objects.get(id=project_id)
		self.users = UserProfile.objects.filter(pk__in=user_ids)
		assert self.users

	def add_headers(self, dataset):
		self.headers = self.static_headers
		self.segments = []
		for x,y in segment_choices:
			self.segments.append(x)
			self.headers.append(y)
		dataset.headers = self.headers
		return dataset

	def get_leads_to_pool(self, project, start_date, end_date):
		"""
		Returns Leads that went to the Pool during certaing time range
		"""
		q = PersonSegmentLogRecords.objects.filter(
			person__project__id=project.id,
			assigned_to=None,
			timestamp__lt=end_date, 
			timestamp__gte=start_date
		)
		return q

	def get_leads_not_low(self, project, start_date, end_date):
		"""
		Returns the amount of Leads that went into the Pool and their previous segment was not low
		"""
		counter = 0
		checked_users = list()
		q = self.get_leads_to_pool(project, start_date, end_date)
		for record in q:
			if record.person.id not in checked_users:
				checked_users.append(record.person.id)
				previous_record = PersonSegmentLogRecords.objects.filter(
					person=record.person,
					timestamp__lt=record.timestamp
				).order_by('-timestamp').first()
				if previous_record.segment != 'low':
					counter += 1
			else:
				continue
		return counter


	def to_csv(self,delimiter=','):
		assert self.file_name, "file_name must not be empty"
		print (self.dataset.csv)
		# with open(self.file_name,'wb') as f:
		# 	writer = csv.writer(f,delimiter=delimiter)
		# 	for row in self.dataset:
		# 		writer.writerow(row)

	def to_xls(self):
		assert self.file_name, "file_name must not be empty"
		with open(self.file_name,'wb') as f:
			f.write(self.dataset.xls)

	#TODO: change back to only one sheet and add User's name
	#TODO: Agregar ID a cada fila: Data1 Data2, Semana1 Semana2
	#TODO: Leads vendidos
	def generate(self):
		#Going backwards in time
		dataset = Dataset()
		dataset.title = 'Reporte Operacional'
		dataset = self.add_headers(dataset)
		segments = {}
		for x,y in segment_choices:
			segments[x] = y

		for user in self.users:
			base_query = Person.objects.filter(
					assigned_to = user,
					project = self.project
				)
			end_date = self.final_date
			for i in range(1,3):
				start_date = end_date - relativedelta(days=7)
				persons = base_query.filter(
					timestamp__lt=end_date, 
					timestamp__gte=start_date
				)
				sold = base_query.filter(
					sold = True,
					timestamp_for_sold__lt=end_date, 
					timestamp_for_sold__gte=start_date
				) 
				row = []
				row.append('Semana{}'.format(i))
				row.append(user.get_full_name())
				row.append(persons.count())
				row.append(sold.count())
				row.append(self.get_leads_to_pool(self.project, start_date, end_date).count())
				row.append(self.get_leads_not_low(self.project, start_date, end_date))
				for segment in segments.keys():
					amount = persons.filter(segment=segment).count()
					#TODO: 
					#Determine how this calculation should be done. If a person was created at a segment during
					#the time range and only count creations or also count if a person went from Low to Medium, in which case
					#the person should only be counted in Medium and not in Low
					row.append(amount)
				dataset.append(row)
				end_date = start_date
			dataset.append_separator('\n')

		return dataset.csv

class ManagementReport():

	def __init__(self, start_date, end_date, project, response):
		self.start_date = start_date
		self.end_date = end_date
		self.project = project
		self.file_name = self.get_file_name()
		self.response_obj = response

	def get_file_name(self):
		r = sha1(str(random.random()).encode('utf-8')).hexdigest()[:5]
		file_name = 'reporte-gerencial{}-{}-{}.csv'.format(
			self.start_date,
			self.end_date,
			r
		)
		return file_name

	def available_by_types(self):
		matrix = []

		for unity_type in self.project.unitytype_set.all():
			current_row = []
			current_row.append(str(unity_type))
			q = Unity.objects.filter(unity_type = unity_type)
			available_count = q.filter(status='available').count()
			current_row.append(available_count)
			virtually_booked_count = q.filter(status='virtually_booked').count()
			current_row.append(virtually_booked_count)

			matrix.append(current_row)

		headers = ['Tipo', 'Disponible', 'Separado Tentativamente']
		self.writer.writerow(headers)

		for row in matrix:
			self.writer.writerow(row)
		self.writer.writerow(['\n'])

	def filter_sold_units(self):
		sold_units = Unity.objects.filter(
			status = 'sold',
			project=self.project,
			timestamp_for_status__gte = self.start_date,
			timestamp_for_status__lte = self.end_date
		)
		return sold_units

	def sold_units(self):
		sold_units = self.filter_sold_units()
		aggregation = sold_units.aggregate(result=Sum('price'))
		self.writer.writerow(['Numero de Ventas', sold_units.count()])
		self.writer.writerow(['Total Ventas', aggregation.get('result')])
		self.writer.writerow(['\n'])

	def sold_by_salesperson(self):
		salespersons = self.project.projectteam.users.filter(user_type='vendedor')
		self.writer.writerow(['Vendedor', 'Ventas'])
		for person in salespersons:
			pa_count = person.as_seller.filter(
				timestamp__gte=self.start_date,
				timestamp__lt=self.end_date
			).count()
			self.writer.writerow([person.get_full_name(), pa_count])
		self.writer.writerow(['\n'])

	def sales_report(self):
		pa_query = PaymentAgreement.objects.filter(
			timestamp__gte=self.start_date,
			timestamp__lte=self.end_date,
			project=self.project,
			seller__isnull=False
		)
		self.writer.writerow(['Apartamento', 'Valor', '(Valor-Separacion)' , 'Vendedor'])
		for pa in pa_query:
			self.writer.writerow([pa.unity.name, pa.unity.price, (pa.unity.price - pa.booking_price), pa.seller])
		self.writer.writerow(['\n'])


	def generate(self):
		self.writer = csv.writer(self.response_obj)
		self.sold_units()
		self.available_by_types()
		self.sold_by_salesperson()
		self.sales_report()


#TODO: REFACTOR ALL OF THIS INTO A SINGLE CLASS

class PaymentsReport():

	def __init__(self, start_date, end_date, project, response):
		self.start_date = start_date
		self.end_date = end_date
		self.project = project
		self.file_name = self.get_file_name()
		self.response_obj = response

	def get_file_name(self):
		r = sha1(str(random.random()).encode('utf-8')).hexdigest()[:5]
		file_name = 'payments-report{}-{}-{}.csv'.format(
			self.start_date,
			self.end_date,
			r
		)
		return file_name

	def get_payments(self):
		payments_q = Payment.objects.filter(
			date__gte=self.start_date,
			date__lte=self.end_date,
			payment_agreement__project=self.project
		)
		self.writer.writerow(['Fecha', 'Unidad', 'Tipo', 'Valor'])
		for payment in payments_q:
			self.writer.writerow([payment.date, payment.payment_agreement.unity.name, payment.type_of_payment, payment.amount])

	def generate(self):
		self.writer = csv.writer(self.response_obj)
		self.get_payments()

class PaymentAgreementReport():

	def __init__(self, start_date, end_date, project, response):
		self.start_date = start_date
		self.end_date = end_date
		self.project = project
		self.file_name = self.get_file_name()
		self.response_obj = response

	def get_file_name(self):
		r = sha1(str(random.random()).encode('utf-8')).hexdigest()[:5]
		file_name = 'payments-report{}-{}-{}.csv'.format(
			self.start_date,
			self.end_date,
			r
		)
		return file_name

	def get_payments(self):
		pa_query = PaymentAgreement.objects.filter(
			timestamp__gte=self.start_date,
			timestamp__lte=self.end_date,
			project=self.project,
			seller__isnull=False
		)
		self.writer.writerow([
			'Fecha', 
			'Unidad', 
			'Valor Unidad',
			'Separacion', 
			'Cuota Inicial', 
			'Ahorro',
			'Cesantias',
			'Subsidio',
			'Otros',
			'Separacion',
			'Credito Hipotecario'
		])
		for pa in pa_query:
			current_row = [
				pa.timestamp,
				pa.unity.name,
				pa.unity.price,
				pa.booking_price,
				pa.initial_fee,
			]
			for type_of_payment, type_str in types_of_agreed_payments:
				exists = pa.agreed_payments.filter(type_of_payment=type_of_payment).first()
				if exists:
					current_row.append(exists.fee)
				else:
					current_row.append(None)
			
			self.writer.writerow(current_row)

	def generate(self):
		self.writer = csv.writer(self.response_obj)
		self.get_payments()

