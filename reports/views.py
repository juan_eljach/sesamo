from datetime import datetime
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views.generic import View
from accounts.models import UserProfile
from sesamo.mixins import SlugManagerMixin
from .forms import OperationalReportForm, ManagementReportForm, PaymentsReportForm
from .reports import OperationalReport, ManagementReport, PaymentsReport, PaymentAgreementReport
# Create your views here.



class Report:

    def get(self, request, *args, **kwargs):
        project = self.get_project()
        user_queryset = project.projectteam.users.filter()
        form = self.form_class(initial={'users':user_queryset})
        return render(request, self.template_name, {'form': form})
    

@method_decorator(login_required, name='dispatch')
class OperationalReportView(View, Report, SlugManagerMixin):
    form_class = OperationalReportForm
    template_name = 'reports/operational_report_template.html'

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            project = self.get_project()
            users = form.cleaned_data.get('users')
            op_report = OperationalReport(project.id, users)
            op_report_response = op_report.generate()
            now = datetime.today()
            response = HttpResponse(op_report_response, content_type="csv")
            response['Content-Disposition'] = 'attachment; filename=ReporteOperacional-{}.csv'.format(now)
            return response

        return render(request, self.template_name, {'form': form})


@method_decorator(login_required, name='dispatch')
class ManagementReportView(View, Report, SlugManagerMixin):
    form_class = ManagementReportForm
    template_name = 'reports/management_report_template.html'

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            project = self.get_project()
            response = HttpResponse(content_type='text/csv')
            start_date = form.cleaned_data.get('start_date')
            end_date = form.cleaned_data.get('end_date')
            management_report = ManagementReport(start_date, end_date, project, response)
            management_report_response = management_report.generate()
            response = management_report.response_obj
            response['Content-Disposition'] = 'attachment; filename={}'.format(management_report.file_name)
            return response

        return render(request, self.template_name, {'form': form})


@method_decorator(login_required, name='dispatch')
class PaymentsReportView(View, Report, SlugManagerMixin):
    form_class = PaymentsReportForm
    template_name = 'reports/payments_report_template.html'

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            project = self.get_project()
            response = HttpResponse(content_type='text/csv')
            start_date = form.cleaned_data.get('start_date')
            end_date = form.cleaned_data.get('end_date')
            payment_report = PaymentsReport(start_date, end_date, project, response)
            payment_report_response = payment_report.generate()
            response = payment_report.response_obj
            response['Content-Disposition'] = 'attachment; filename={}'.format(payment_report.file_name)
            return response

        return render(request, self.template_name, {'form': form})

@method_decorator(login_required, name='dispatch')
class PaymentAgreementReportView(View, Report, SlugManagerMixin):
    form_class = PaymentsReportForm
    template_name = 'reports/payment_agreements_report_template.html'

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            project = self.get_project()
            response = HttpResponse(content_type='text/csv')
            start_date = form.cleaned_data.get('start_date')
            end_date = form.cleaned_data.get('end_date')
            payment_agreement_report = PaymentAgreementReport(start_date, end_date, project, response)
            payment_agreement_report_response = payment_agreement_report.generate()
            response = payment_agreement_report.response_obj
            response['Content-Disposition'] = 'attachment; filename={}'.format(payment_agreement_report.file_name)
            return response

        return render(request, self.template_name, {'form': form})