from django import forms
from accounts.models import UserProfile

class OperationalReportForm(forms.Form):
    users = forms.ModelMultipleChoiceField(
        queryset=UserProfile.objects.all(), 
        required=True
    )

class CustomDateInput(forms.widgets.TextInput):
    input_type = 'date'

class ManagementReportForm(forms.Form):
    start_date = forms.DateField(widget=CustomDateInput)
    end_date = forms.DateField(widget=CustomDateInput)

class PaymentsReportForm(ManagementReportForm):
    start_date = forms.DateField(widget=CustomDateInput)
    end_date = forms.DateField(widget=CustomDateInput)